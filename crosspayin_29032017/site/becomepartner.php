<?php 
session_start();
//error_reporting(E_ALL);
//ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();

include_once("../Classes/customer.class.php");
$customer = new customer();

require("../phpMailer/PHPMailerAutoload.php");

$phone = isset($_REQUEST['txtphone'])?$_REQUEST['txtphone']:'';
$emailaddress = isset($_REQUEST['txtemail'])?$_REQUEST['txtemail']:'';
$name = isset($_REQUEST['txtname'])?$_REQUEST['txtname']:'';

$company = isset($_REQUEST['txtcompany'])?$_REQUEST['txtcompany']:'';
$address = isset($_REQUEST['txtaddress'])?$_REQUEST['txtaddress']:'';
$comment = isset($_REQUEST['txtcomment'])?$_REQUEST['txtcomment']:'';
$userip = $_SERVER['REMOTE_ADDR'];

$status = 0;
$isexist = 0;
$postvalue = array();


if ($phone){
	   
	   $numbercount = $customer->checkpartner($phone,$emailaddress);
	   if ($numbercount==0)	{
			$randomno = GenerateRandomID(6,'NUMERIC');
			$mess="Your phone verification code is ".$randomno;
			$main->sendSms($phone,$mess);
			$postvalus = array('name'=>$name,'email' => $emailaddress,'phoneno'=>$phone,'company'=>$company,'address'=>$address,'comment'=>$comment,'userip'=>$userip,'isvalid'=>0,'code'=>$randomno,'creationdate'=>date("Y-m-d H:i:s"));
			$customer->addpartner($postvalus);
			$displayphone = substr($phone, 0, 2)."******".substr($phone, -2,2);
			$status = 1;
		} else {
			$isexist = 1;									
		}	
	} else {
		$status = 0;								
	}		

	
if (isset($_REQUEST['action']) && ($_REQUEST['action']=="checkverificationcode")){
	
	$phoneno = $_REQUEST['phoneno'];
	$email = $_REQUEST['email'];
	$verificationcode = $_REQUEST['varificationcode'];
	$result= $customer->checkpartnercode($phoneno,$verificationcode);
	if ($result==1){
		$customer->updatepartnerstatus($phoneno,$verificationcode);
		
		$to      = $email; // Send email to our user
		$subject = 'Become a Partner'; // Give the email a subject 
		$message=$config['EMAIL_HEADER'];
		$message.= "Dear Partner,<br><br>";
		$message.= "We have received your request and our support team will contact you soon.";
		$message.=$config['EMAIL_FOOTER'];
			
		//Answer key attachment
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $config['SMTP']['HOST'];
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $config['SMTP']['PORT'];
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $config['SMTP']['USER'];
		//Password to use for SMTP authentication
		$mail->Password = $config['SMTP']['PASSWORD'];
		
		$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
		$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
		$mail->addAddress(''.$to.'');
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->IsHTML(true); 
		$mail->send();
			
		$mess="A new become a partner request with phone no - $phoneno has been received. Kindly check details in payu admin section";
		$main->sendSms($config['SUPPORT']['PHONENO'],$mess);
		
		echo "1";
	} else {
		echo "0";
	}	
	
	exit();
}	
?>

<div class="slider" id="slider">

	<div class="container">

		<div class="col-lg-8 col-sm-8 col-lg-offset-2 ">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab3" data-toggle="tab">
							<span class="fa fa-user" aria-hidden="true"></span>
							<div class="hidden-xs">Become a Partner</div>
						</button>
					</div>
					
				</div>

				<div class="well">
				<div id="searchresult"></div>
					 <div class="tab-content" id="tabcontent">
					 
					<div class="tab-pane fade in active" id="tab3">
					<?php if ($isexist==1) { ?>

						<div style="margin-bottom: 12px" class="input-group">You have already submitted request. Support team will contact you soon.</div>
					<? } ?>
					<?php if ($status==1) { ?>
						Please enter OPT received on your mobile no <?php echo $displayphone ;?>
							
									<div style="margin-bottom: 12px" class="input-group">
											<span class="input-group-addon"><i class="fa fa-user-secret" aria-hidden="true"></i></span>
											<input id="txtverificationcode" maxlength="6" type="tel" class="form-control inp-field" name="txtverificationcode" value="" placeholder="Enter Veification Code">
											<input id="txtphonno" type="hidden" class="form-control inp-field" name="txtphonno" value="<?php echo $phone ?>" >
											<input id="emailaddress" type="hidden" class="form-control inp-field" name="emailaddress" value="<?php echo $emailaddress ?>" >
									</div>
									<button type="submit" class="btn btn-vk btn-block rch-btn" onclick="Checkverificationcode();" >Submit</button>
									<hr style="margin-top:10px;margin-bottom:10px;" >
						
						<?php } else { ?>
						
					
						<form name="becomepartnerform"  Method="POST" id="becomepartnerform"  onsubmit="return checkbecomepartner();" action="">

								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
										<input id="txtname" maxlength="50" type="text" class="form-control inp-field" name="txtname" value="" placeholder="Name">
								</div>
								
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true" ></i></span>
										<input id="txtemail" maxlength="30" type="tel" class="form-control inp-field" name="txtemail" value="" placeholder="Email" required>
								</div>
								
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
										<input id="txtphone" maxlength="15" type="tel" class="form-control inp-field" name="txtphone" value="" placeholder="Phone No" required>
								</div>
								
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
										<input id="txtcompany" maxlength="100" type="tel" class="form-control inp-field" name="txtcompany" value="" placeholder="Company">
								</div>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i></span>
										<textarea placeholder="Address" id="txtaddress" rows="1"  class="form-control inp-field" name="txtaddress" ></textarea>
									
								</div>	
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-comments" aria-hidden="true"></i></span>
										<textarea placeholder="Comment" id="txtcomment" rows="1"  class="form-control inp-field" name="txtcomment" ></textarea>
								</div>	
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Submit</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
						</form>
						<?php } ?>
						</div>
						
						
					
					  </div>
				</div>
			</div>

			
	</div>


</div>
  
<!-- //slider -->

<script type="text/javascript">
<!--

function Checkverificationcode(){

	var varificationcode =$('#txtverificationcode').val();
	var phonno =$('#txtphonno').val();
	var emailaddress =$('#emailaddress').val();
	
		if (varificationcode == "")
		{
			alert("Please enter 6 digit verification code!");
			$('#txtverificationcode').focus();
			return false;
		}
		var len = varificationcode.length;
		if(len !=6)
		{
			alert("Please enter 6 digit verification code!");
			$('#txtverificationcode').focus();
			return false;
		}
		
var dataString = 'action=checkverificationcode&phoneno=' + phonno +'&varificationcode=' + varificationcode +'&email=' + emailaddress ;

	$.ajax({
		type: "POST",
		dataType: "text",
		url: "../site/becomepartner.php",
		data: dataString,
		beforeSend: function() {
		$('#searchresult').html('<div class="success"><img src="../Images/loading.gif" height="40"></div>').show('fast');

		},
		success: function(result){
			if(result=='1'){
				$("#tabcontent").css("display","none");
				$('#searchresult').html('<div class="success">We have received your request and our support team will contact you soon.</div>');
			}else{
				$('#searchresult').html('<div class="Alert" >you have entered wrong verification code</div>');
			}
		}
	}); 
	
}
</script>

<?  End_Response(); ?>