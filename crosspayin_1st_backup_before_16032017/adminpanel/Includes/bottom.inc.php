<div class="clear"></div>
<div class="float-left padding_middle_content"></div>
<div class="clear"></div>
<footer>
  <div class="center">
  <div class="footer-container">
     <!-- Buy Credits menus start here 	-->
      <div class="footer_panel float-left first">
  		<div class="footer_heading"><a href="../Purchase/purchase.php" title="Buy Credits">BUY CREDITS</a></div>
  		<div class="footer_heading"><a href="../Home/resellers.php" title="Resellers">RESELLERS</a></div>
		<div class="footer_heading"><a href="../Home/premium_partner.php" title="Premium Partner">Premium Partner</a></div>
  		<div class="footer_heading"><a href="../Home/how_softcall_works.php" title="How SoftCall Works">How SoftCall<br>Works</a></div>
	</div>
    <!-- RATES &amp; PLANS menus start here 	-->
    <div class="footer_panel float-left">
        <div class="footer_heading"><a href="../Home/rates.php" title="RATES &amp; PLANS">RATES &amp; Offers</a></div>
        <ul class="footer_text">
        <li><a href="../Home/referral_program.php" title="Referral Program">Referral Program</a></li>
          <li><a href="../Purchase/monthly_calling_plans.php" title="Special Offers">Special Offers</a></li>
          <li><a href="../Home/rates_search.php" title="SoftCall Rates">SoftCall Rates</a></li>
          <li><a href="../Cheap-Calls" title="International VoIP Rates">International VoIP Rates</a></li> 
        </ul>
      </div>
     <!-- SoftCall Features menus start here 	-->
      <div class="footer_panel float-left">
        <div class="footer_heading"><a href="../Home/softcall_features.php" title="SoftCall Features">SoftCall Features</a></div>
          <ul class="footer_text">
          	<li><a href="../Home/softcall_features.php" title="Call Details Records">Call Details Records</a></li>
            <li><a href="../Home/softcall_features.php" title="Call Hold / Call Waiting">Call Hold / Call Waiting</a></li>
          	<li><a href="../Home/softcall_features.php" title="Call Recording">Call Recording</a></li>
          	<li><a href="../Home/softcall_features.php" title="Display Caller-ID">Display Caller-ID</a></li>
          	<li><a href="../Home/softcall_features.php" title="Follow-Me">Follow-Me</a></li>
          	<li><a href="../Home/softcall_features.php" title="Voicemail-to-Email">Voicemail-to-Email</a></li>
         </ul>
      </div>
     <!-- Downloads menus start here 	-->
      <div class="footer_panel float-left">
        <div class="footer_heading"><a href="../Home/download.php" title="Download SoftCall Dialers">Download SoftCall Dialers</a></div>
          <ul class="footer_text">
          	<li><a href="../Home/windows_pc.php" title="SoftCall for Windows">SoftCall for Windows</a></li>
            <li><a href="../MobileVoIP/mobilevoip_iphone.php" title="SoftCall for iPhone">SoftCall for iPhone</a></li>
          	<li><a href="../MobileVoIP/mobilevoip_ipad.php" title="SoftCall for iPad">SoftCall for iPad</a></li>
          	<li><a href="../MobileVoIP/mobilevoip_android.php" title="SoftCall for Android">SoftCall for Android</a></li>
          	<li><a href="../MobileVoIP/mobilevoip_blackberry.php" title="SoftCall for BlackBerry">SoftCall for BlackBerry</a></li>
          	<li><a href="../MobileVoIP/mobilevoip_symbian.php" title="SoftCall for Symbain">SoftCall for Symbain</a></li>
          	<li><a href="../MobileVoIP/mobilevoip_windows_mobile.php" title="SoftCall for Windows Mobiles">SoftCall for Windows Mobiles</a></li>
          </ul>
      </div>
     <!-- Support menus start here 	-->
     <div class="footer_panel float-left last">
       <div  class="footer_heading"><a href="../Home/support.php" title="SUPPORT">SUPPORT</a></div>
        <ul class="footer_text">
          <li><a href="../Home/support_form.php" title="Email Support">Email Support</a></li>
          <li><a href="../Home/contents.php?id=27" title="Knowledge Base &amp; FAQ">Knowledge Base &amp; FAQ</a></li>
          <li><a href="../Home/support.php#Live_Support" title="Live Help">Live Help</a></li>
          <li><a href="../SIP/sip_device.php" title="SIP Settings">SIP Settings</a></li>
        </ul>
     </div>
   <!-- Social Media Starts Here	-->
		<?
    	global $KoolControlsFolder;
			require_once($KoolControlsFolder."/KoolSocialShare/koolsocialshare.php");

			$socialshare1 = new KoolSocialShare("socialshare1");
			// $socialshare1->styleFolder  = $KoolControlsFolder."/KoolSocialShare/styles/web20";
			$socialshare1->styleFolder  = "none";
			$socialshare1->scriptFolder = $KoolControlsFolder."/KoolSocialShare";
			$socialshare1->UrlToShare 	= "http://www.facebook.com/SoftCall";
			//$socialshare1->Width = "300px";

			$fb_button = new FacebookButton();
			$fb_button->Layout = "button_count";
			$fb_button->SendButton = true;
			$fb_button->ShowFaces = false;
			//$fb_button->Width = "300px";

			$socialshare1->Add($fb_button);

			/*$socialshare2 = new KoolSocialShare("socialshare2");
			$socialshare2->styleFolder  = "none";
			$socialshare2->scriptFolder = $KoolControlsFolder."/KoolSocialShare";
			$socialshare2->UrlToShare 	= "http://www.softcall.me/";
			$socialshare2->TitleToShare = "SoftCall offers the cheapest international calling rates! Try it Now!";
			$socialshare2->Add(new TwitterWithCount());*/
    ?>
     <div class="followus_heading">
      <div class="footer_heading clear">FOLLOW US</div>
      	<ul>
      		<li><div class="social-icon-f"><a href="https://www.facebook.com/SoftCall/" title="Facebook" target="_blank"></a></div></li>
        	<li><div class="social-icon-t"><a href="https://www.twitter.com/SoftCallME/" title="Twitter" target="_blank"></a></div></li>
              <!-- changed on 13 feb 2015 for google plus start --> <!-- <li class="gplus-icon"><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
<div class="g-plusone" data-size="standard" data-count="true"></div></li> -->
<li class="gplus-icon"><a href="https://plus.google.com/103801886619639243618?prsrc=3" rel="publisher" target="_blank" style="text-decoration:none;">
<img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:30px;height:28px;"/></a></li>
<!-- changed on 13 feb 2015 for google plus ends -->
            
				</ul><br><br>
      	<div><?=$socialshare1->Render();?><br></div> <!-- Social Media Ends Here	-->
        </div>
     <div class="clearfix"></div>
     </div>
		<!--end footer container -->
   
    <!-- Copy rights and Terms & Conditions start here	-->
   
      <span class="footer_text_left float-left clear hide-elements-for-mobile">Copyright &copy; <?=date('Y')?> - Cloud Communications and Computing Corp. All Rights Reserved.</span>
      <span class="footer_text_right float-right hide-elements-for-mobile"><a href="../Home/contents1.php?id=2" title="Company">Company</a> | <a href="../Home/support_form.php" title="Contact Us">Contact Us</a> | <a href="../Home/material.php?id=112" title="Privacy Policy">Privacy Policy</a> | <a href="../Home/material.php?id=111" title="Terms &amp; Conditions">Terms &amp; Conditions</a></span>
     <!-- for mobile version -->  
	<span class="footer_text_right float-right hidemefromweb">Use of this website constitutes acceptance of the<br><a href="../Home/material.php?id=111" title="Terms &amp; Conditions">Terms &amp; Conditions</a> | <a href="../Home/material.php?id=112" title="Privacy Policy">Privacy Policy</a></span>   
     <span class="footer_text_left float-left clear hidemefromweb">Copyright &copy; <?=date('Y')?>  <br>  Cloud Communications and Computing Corp.<br> All Rights Reserved.</span>
      
<!-- for mobile version ends -->    
  </div>
</footer>