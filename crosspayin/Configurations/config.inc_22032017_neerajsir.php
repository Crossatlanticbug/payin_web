<?php
global $config;

//============================== Development Purposes Configurations ================================================
$config['TEST_ENV'] = TRUE;  // Identify if this configuration file is to be used for test or live environment
$config['HOST'] = ($config['TEST_ENV']) ? "http://www.payin.in/development" : "http://www.payin.in";
$config['WEB_URL'] = ($config['TEST_ENV']) ? "http://www.payin.in/development" : "http://www.payin.in";

//============================== END Development Purposes Configurations ============================================

//============================== Website DB Configurations ==========================================================

// Production DB

$config['WEB_DB']['LIVE']['HOST']	= 'localhost';
$config['WEB_DB']['LIVE']['USERNAME'] = 'payin_cross';
$config['WEB_DB']['LIVE']['PASSWORD'] = 'B0ylAH8n9stW';

// dev database configurtion

$config['WEB_DB']['TEST']['HOST']	= 'localhost';
$config['WEB_DB']['TEST']['USERNAME'] = 'payin_devuser';
$config['WEB_DB']['TEST']['PASSWORD'] = 'WqhWy0DmunoT';

date_default_timezone_set('asia/kolkata');

$config['ADMIN_MAIL'] = 'info@payin.in';
$config['SUPPORT_MAIL'] = 'support@payin.in';
$config['NOREPLY'] = 'noreply@payin.in';


/* API Credential */

$config['API']['TEST']['URL']	= 'https://api.goprocessing.in/';
$config['API']['TEST']['GOID'] = 5161058236;
$config['API']['TEST']['APIKEY'] = 'DM64216a5wpA4y2';
$config['API']['TEST']['MOBILE']['PREPAID']['APIKEY'] = 'DM64216a5wpA4y2';
$config['API']['TEST']['MOBILE']['POSTPAID']['APIKEY'] = 'lG75l4fr9xEGm6E';
$config['API']['TEST']['DTH']['APIKEY'] = 'SNN9eqBXSPHd7WY';
$config['API']['TEST']['DATACARD']['APIKEY'] = 'H4pqww46LcFGHt1';
$config['API']['TEST']['RTYPE'] = 'json';	// To Change Response Format XML=>"xml" ,JSON =>"json" , HTML=>"html"  
$config['API']['TEST']['APIMODE'] = 'test';


$config['API']['LIVE']['URL']	= 'https://api.goprocessing.in/';
$config['API']['LIVE']['GOID'] = 5161058236;
$config['API']['LIVE']['MOBILE']['PREPAID']['APIKEY'] = 'DM64216a5wpA4y2';
$config['API']['LIVE']['MOBILE']['POSTPAID']['APIKEY'] = 'lG75l4fr9xEGm6E';
$config['API']['LIVE']['DTH']['APIKEY'] = 'SNN9eqBXSPHd7WY';
$config['API']['LIVE']['DATACARD']['APIKEY'] = 'H4pqww46LcFGHt1';
$config['API']['LIVE']['RTYPE'] = 'json';	// To Change Response Format XML=>"xml" ,JSON =>"json" , HTML=>"html" 
$config['API']['LIVE']['APIMODE'] = 'live';

$config['API']['SF']['mobile']['prepaid'] = '1';
$config['API']['SF']['mobile']['postpaid'] = '7';
$config['API']['SF']['dth'] = '2';
$config['API']['SF']['datacard'] = '3';

/*$config['API']['LIVE']['URL']	= 'http://api.egpay.in/';
$config['API']['LIVE']['UID'] = 'CROSSATLANTIC';
$config['API']['LIVE']['UPASS'] = 'CRO@ATLANTIC';
$config['API']['LIVE']['SUBUID'] = 'SUBCROSSATLANTIC';
$config['API']['LIVE']['SUBUPASS'] = 'SUBCRO@ATLANTIC';
*/


/* API Credential */

/* PayU Credential */

$config['PAYU']['TEST']['URL']	= 'https://test.payu.in/_payment';
$config['PAYU']['TEST']['KEY'] = 'gtKFFx';
$config['PAYU']['TEST']['SALT'] = 'eCwWELxi';
/*
$config['PAYU']['LIVE']['URL']	= 'https://test.payu.in/_payment';
$config['PAYU']['LIVE']['KEY'] = 'gtKFFx';
$config['PAYU']['LIVE']['SALT'] = 'eCwWELxi';
*/
$config['PAYU']['LIVE']['URL']	= 'https://secure.payu.in/_payment';
$config['PAYU']['LIVE']['KEY'] = 'ZKvxoA';
$config['PAYU']['LIVE']['SALT'] = 'ZrjqF30L';

/* PayU Credential */

/* SMS Credential */

$config['SMS']['USER']="cra-payin";
$config['SMS']['PASSWORD']="98765";
$config['SMS']['SENDERID']="MPAYIN";
$config['SMS']['URL']="http://sms.crossatlanticsoftware.in/bulksms/bulksms?";

/* SMS Credential */


/* SMTP CREDENTIAL */

$config['SMTP']['USER']="phpmail@onlineexpert.in";
$config['SMTP']['PASSWORD']="g1IazMuC2wIX";
$config['SMTP']['HOST']="smtpout.asia.secureserver.net";
$config['SMTP']['PORT']="80";

/* SMTP CREDENTIAL */


/* EMAIL TEMPLATE HEADER AND FOOTER */
$config['EMAIL_HEADER']='<html xmlns="http://www.w3.org/1999/xhtml"><head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payin </title>
</head>
<body style="margin:0; padding:0; background:#fff;">
<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <table width="546" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#fff">
		<tr>
           <td align="center" valign="middle" style="border:1px solid #c0202f;">
          	<table border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" width="100%">
            <tr>
               	<td bgcolor="#fff" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #fff;"><a class="" href="'.$config['HOST'].'" style="text-decoration:none;" title="Payin" target="_blank"><img style="width: 90px;" src="'.$config['HOST'].'/Images/payin-without-bg.png" alt="Payin" /></a></td> 
          
            </tr>
			<tr><td style="padding:30px;">';
			
$config['EMAIL_FOOTER']='</td></tr><tr>
					<td  valign="middle" height="82" style="background-color: #59595B;">
					<table border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#59595B" width="100%">
					<tr>
					<td> <a href="'.$config['HOST'].'/#" style="color: #fff; margin-bottom: 16px;text-decoration:none;font-family: Source Sans Pro,Helvetica,Arial,sans-serif;">Become a Reseller</a></td>
					<td> <a href="'.$config['HOST'].'/site/mobile.php" style="color: #fff; margin-bottom: 16px;text-decoration:none;font-family: Source Sans Pro,Helvetica,Arial,sans-serif;">Mobile</a></td>
					<td> <a href="'.$config['HOST'].'/site/dth.php" style="color: #fff; margin-bottom: 16px;text-decoration:none;font-family: Source Sans Pro,Helvetica,Arial,sans-serif;">DTH</a></td>
					<td> <a href="'.$config['HOST'].'/site/datacard.php" style="color: #fff; margin-bottom: 16px;text-decoration:none;font-family: Source Sans Pro,Helvetica,Arial,sans-serif;">Datacard</a></td>
					
				</tr>
				</table></td>
			</tr>
			</table></td>
		</tr>
		</table></td>
  </tr>
</table>
</body>
</html>';
/* EMAIL TEMPLATE HEADER AND FOOTER */
?>