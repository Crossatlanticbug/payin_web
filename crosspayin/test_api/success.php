<?php

session_start();
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();

echo '<pre>';
print_r($_POST);
echo '</pre>';

$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt=$config['PAYU'][$ENV]['SALT'];
$phone =$_POST["phone"];
$ORDERID =$_POST["udf1"];
$type =$_POST["udf2"];

//$_SESSION['ORDERID'] =$_POST["udf1"];
include_once("../Classes/order.class.php");
$orderprocess = new Order();
require ("../phpMailer/PHPMailerAutoload.php");

	/*----Orderid Blank-------*/
	if (!isset($ORDERID)){
		exit("<script>window.location.href='../site/error.php?err=invalid';</script>");
	}
	
	/*----Orderid Already Succeed-------*/
	if ($type=="Wallet" && isset($ORDERID)){
		$datas=$orderprocess->ExistAddOrder($ORDERID);
		$sts= $datas->paymentstatus;
		if($sts=="success"){
			exit("<script>window.location.href='../site/error.php?err=walletSucccess';</script>");
		}
		if($sts=="failure"){
			exit("<script>window.location.href='../site/error.php?err=walletFailure';</script>");
		}
	}

	/*----Orderid Already Failed-------*/
	if ($type=="Recharge" && isset($ORDERID)){
		$data=$orderprocess->ExistOrder($ORDERID);
		$sts= $data->paymentstatus;
		if($sts=="success"){
			exit("<script>window.location.href='../site/error.php?err=rechargeSuccess';</script>");
		}
		if($sts=="failure"){
			exit("<script>window.location.href='../site/error.php?err=rechargeFailure';</script>");
		}
	}

	$logvalue = new stdClass();
	$logvalue->paymentlog=json_encode($_POST) ;
	$logvalue->rechargelog='';
	$logvalue->mobileno=$phone;
	$logvalue->emailid=$email; 
	$logvalue->userip=$_SERVER['REMOTE_ADDR'];
	$logvalue->pageurl= '';
	// $logvalue->pageurl= $_SERVER['SCRIPT_URI'];
	$logid=$orderprocess->AddDevelopmentLog($logvalue);

if ($_POST["status"]=="success"){
	
	$retHashSeq =$salt.'|'.$status.'|||||||||'.$type.'|'.$ORDERID.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;	
	$hash = hash("sha512", $retHashSeq);
	
	if ($hash != $posted_hash) {
	       echo "Invalid Transaction. Please try again";
	} else {
		
		$ordervalue="transactionid='".$txnid."',paymentstatus='".$_POST["status"]."'";
		
		if ($type=="Wallet") {
			$orderprocess->UpdateAddMoneyOrder($ordervalue,$ORDERID) ;  
			$CurrentUser = &$_SESSION['CurrentUser'];
			$orderprocess->UpdateBalance($amount,$CurrentUser->id);
			$CurrentUser->balance+=$amount;
			$balancetype = 'DR';
			$comment = 'Add Money In Wallet' ;
			$objbal=  new stdclass();
			$objbal->amount=$amount;
			$objbal->user=$CurrentUser->id;
			$objbal->amounttype=$balancetype;
			$objbal->comment=$comment;
			$objbal->addedby='';
			$orderprocess->UpdateVirtualBalance($objbal);
		} else {
			$orderprocess->UpdateRechargeOrder($ordervalue,$ORDERID) ; 	 
		}	

		$msg = "<h3>Thank You. Your order status is ". $status .".</h3>";
        $msg .= "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";
        $msg .= "<h4>We have received a payment of Rs. " . $amount . ". </h4>";
			
		if ($type=="Wallet") {
			$result = $orderprocess->getAddMoneyOrderInfo($ORDERID);
		} else {
			$result = $orderprocess->getRechargeOrderInfo($ORDERID);
		}
		
		include_once("../Classes/api.class.php");
		$apiprocess= new RechageApi();
		$randomno = GenerateRandomID(20);
		$phone=$result->mobileno;
		$amount=floor($result->amount);
		
		$mess="Your Order has been  successfully placed with transaction id ".$txnid.". ";
		if ($type=="Wallet") {
			$mess .="Your current balance is INR ". $result->balance;
		}		
		
		$main->sendSms($phone,$mess);
		
		$to      = $result->email; // Send email to our user
		$subject = 'Order Confirmation'; // Give the email a subject 
		$message=$config['EMAIL_HEADER'];
		$message.= "Dear ".$result->name.",<br><br>";
		$message.= "A order has been successfully submitted. Please Find below details regarding the transaction:<br><br>";
		$message.= "<strong>Order No :</strong>".$ORDERID."<br>";
		$message.= "<strong>Transaction ID :</strong>".$txnid."<br>";
		$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
		if ($type=="Wallet") {
			$message.= "<strong>Balance :</strong>INR ".$result->balance."<br>";
		}
		$message.= "<strong>Payment Status :</strong>".$status."<br>";
				 
		$message.=$config['EMAIL_FOOTER'];
			
		//Answer key attachment
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $config['SMTP']['HOST'];
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $config['SMTP']['PORT'];
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $config['SMTP']['USER'];
		//Password to use for SMTP authentication
		$mail->Password = $config['SMTP']['PASSWORD'];
		
		$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
		$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
		$mail->addAddress(''.$to.'');
		$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->IsHTML(true); 
		$mail->send();
		unset($_SESSION['ADDMONEYORDERID']);
		
		if ($type=="Recharge") {
			$rechargetype=$result->rechargetype;
			$mobiletype= $result->mobiletype;
			if($mobiletype=="prepaid")		$operatorcode=$result->operatorid;
			if($mobiletype=="postpaid")		$vendor_code=$result->operatorid;
			
			
			If (!empty($phone)){
				if($mobiletype=="prepaid")	{
					$apiresponse= $apiprocess->MobileRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][$rechargetype]);
				}
				if($mobiletype=="postpaid")	{
					$apiresponse= $apiprocess->MobileRechargePostPaid($phone,$vendor_code,$amount,$randomno,$config['API']['SF'][$rechargetype]);
					if (isset($apiresponse['ErrorCode'])){
						exit("<script>window.location.href='../site/error.php?err=invalid';</script>");
					}
				}
				
		echo '<pre>';
		print_r($apiresponse);
		echo '</pre>';
		
				$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
				$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
				
				if ($apiresponse['response_code'] == 0){
					
					if($mobiletype=="prepaid"){
						$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
						$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
						unset($_SESSION['ORDERID']);
						
						$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
						$main->sendSms($phone,$mess);
						$to      = $result->email; // Send email to our user
						$subject = 'Recharge Confirmation'; // Give the email a subject 
						$message=$config['EMAIL_HEADER'];
						$message.= "Dear ".$result->name.",<br><br>";
						$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
						$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
						$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
						$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
						$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
							 
						$message.=$config['EMAIL_FOOTER'];
						//Answer key attachment
						$mail = new PHPMailer;
						$mail->isSMTP();
						$mail->Host = $config['SMTP']['HOST'];
						//Set the SMTP port number - likely to be 25, 465 or 587
						$mail->Port = $config['SMTP']['PORT'];
						//Whether to use SMTP authentication
						$mail->SMTPAuth = true;
						//Username to use for SMTP authentication
						$mail->Username = $config['SMTP']['USER'];
						//Password to use for SMTP authentication
						$mail->Password = $config['SMTP']['PASSWORD'];
					
						$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
						$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
						$mail->addAddress(''.$to.'');
						$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
						$mail->Subject = $subject;
						$mail->Body = $message;
						$mail->IsHTML(true); 
						$mail->send();
					}
		
					if($mobiletype=="postpaid")	{
						$rechargevalue="rechargekey='".$apiresponse['trans_id']."',rechargestatus='".$apiresponse['status']."'";
						$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
						unset($_SESSION['ORDERID']);
						
						$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
						$main->sendSms($phone,$mess);
						$to      = $result->email; // Send email to our user
						$subject = 'Recharge Confirmation'; // Give the email a subject 
						$message=$config['EMAIL_HEADER'];
						$message.= "Dear ".$result->name.",<br><br>";
						$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
						$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
						$message.= "<strong>Transaction ID :</strong>".$apiresponse['trans_id']."<br>";
						$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
						$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
						$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
						$message.= "<strong>Recharge Date :</strong>".$apiresponse['datetime']."<br>";
							 
						$message.=$config['EMAIL_FOOTER'];
						//Answer key attachment
						$mail = new PHPMailer;
						$mail->isSMTP();
						$mail->Host = $config['SMTP']['HOST'];
						//Set the SMTP port number - likely to be 25, 465 or 587
						$mail->Port = $config['SMTP']['PORT'];
						//Whether to use SMTP authentication
						$mail->SMTPAuth = true;
						//Username to use for SMTP authentication
						$mail->Username = $config['SMTP']['USER'];
						//Password to use for SMTP authentication
						$mail->Password = $config['SMTP']['PASSWORD'];
					
						$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
						$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
						$mail->addAddress(''.$to.'');
						$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
						$mail->Subject = $subject;
						$mail->Body = $message;
						$mail->IsHTML(true); 
						$mail->send();
					}
					
		
				} else {
					$msg=$apiresponse['Response-status'] ;
				}	
			}
		}
    }         	 
} else  {
	 echo "Invalid Transaction. Please try again";
}	
?>
<div class='container' style='padding:50px 0;'><?php echo $msg ;?></div>
<?  End_Response(); ?>