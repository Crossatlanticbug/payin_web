<?php
 
class RechageApi 
{
    function __construct()
    {
      	global $config; 
		$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
		$this->apiurl = $config['API'][$ENV]['URL'] ; 
		$this->goid = $config['API'][$ENV]['GOID']; 
		$this->apikeyprepaid = $config['API'][$ENV]['MOBILE']['PREPAID']['APIKEY']; 
		$this->apikeypostpaid = $config['API'][$ENV]['MOBILE']['POSTPAID']['APIKEY']; 
		$this->apikeydth = $config['API'][$ENV]['DTH']['APIKEY']; 
		$this->apikeydatacard = $config['API'][$ENV]['DATACARD']['APIKEY']; 
		$this->rtype = $config['API'][$ENV]	['RTYPE'];
		$this->apimode = $config['API'][$ENV]['APIMODE'];
	}
		

    function MobileRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {
		$scriptname = "serviceTrans.go";
		$parameter="msisdn=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyprepaid."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
		
    }
	
	function MobileRechargePostPaid($mobile,$vendor_code,$amount,$txnid,$rechargetype)
    {
		$scriptname = "billTrans.go";
		$parameter="msisdn=$mobile&vendor_code=$vendor_code&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeypostpaid."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function DataCardRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {	
		$scriptname = "serviceTrans.go";
		$parameter="msisdn=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeydatacard."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
		
    }
	
	function DTHRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {
		$scriptname = "serviceTrans.go";
		$parameter="custid=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeydth."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function getConnectAPIcurl($scriptname,$parameter){
			
			global $config;
			global $token;
			
			$ch = curl_init();
			$headers = array(
			'Accept: application/json',
			'Content-Type: application/json',

			);
			$authurl=$scriptname."?UID=".$this->uid."&UPASS=".$this->upass."&SUBUID=".$this->subuid."&SUBUPASS=".$this->subupass;
			echo $this->apiurl.$authurl."&".$parameter;
			curl_setopt($ch, CURLOPT_URL, $this->apiurl.$authurl."&".$parameter);
		//	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
			//curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// Timeout in seconds
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			   $result = curl_exec($ch);
			curl_close($ch);

				if(curl_errno($ch))  //catch if curl error exists and show it
				  echo 'Curl error: ' . curl_error($ch);
				else
				  echo $result;
		
			//return $result;
				echo "<pre>";
				print_r($result);
				echo "</pre>";
		}

	function GetMobileInfo($mobile)
	{
		$scriptname = "findMsisdnInfo.go";
		$parameter="msisdn=$mobile";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyprepaid."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		$operatorcode= isset($array['operator_code']) ?$array['operator_code'] :'';
		return $operatorcode;
		
    }
		
	
}
?>