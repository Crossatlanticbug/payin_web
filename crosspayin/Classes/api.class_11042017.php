<?php
 
class RechageApi 
{
    function __construct()
    {
      	global $config; 
		$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
		$this->apiurl = $config['API'][$ENV]['URL'] ; 
		$this->goid = $config['API'][$ENV]['GOID']; 
		$this->apikeyprepaid = $config['API'][$ENV]['MOBILE']['PREPAID']['APIKEY']; 
		$this->apikeypostpaid = $config['API'][$ENV]['MOBILE']['POSTPAID']['APIKEY']; 
		$this->apikeydth = $config['API'][$ENV]['DTH']['APIKEY']; 
		$this->apikeydatacard = $config['API'][$ENV]['DATACARD']['APIKEY']; 
		$this->apikeyutility = $config['API'][$ENV]['UTILITYBILL']['APIKEY'];
		$this->rtype = $config['API'][$ENV]	['RTYPE'];
		$this->apimode = $config['API'][$ENV]['APIMODE'];
		$this->apisfutility = $config['API']['SF']['utilitybill'];

	}
		

    function MobileRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {
		$scriptname = "serviceTrans.go";
		$parameter="msisdn=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyprepaid."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
		
    }
	
	function MobileRechargePostPaid($mobile,$vendor_code,$amount,$txnid,$rechargetype)
    {
		$scriptname = "billTrans.go";
		$parameter="msisdn=$mobile&vendor_code=$vendor_code&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeypostpaid."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function DataCardRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {	
		$scriptname = "serviceTrans.go";
		$parameter="msisdn=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeydatacard."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
		
    }
	
	function DTHRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {
		$scriptname = "serviceTrans.go";
		$parameter="custid=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeydth."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function LandlineRecharge($phone,$operatorcode,$amount,$txnid,$sf,$custacc,$custno)
    {
		$scriptname = "billTrans.go";
		$parameter="vendor_code=$operatorcode&amount=$amount&amount=$amount&client_trans_id=$txnid&msisdn=$phone&cust_acc=$custacc&cust_no=$custno&service_family=$sf";	
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyutility."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function getConnectAPIcurl($scriptname,$parameter){
			
			global $config;
			global $token;
			
			$ch = curl_init();
			$headers = array(
			'Accept: application/json',
			'Content-Type: application/json',

			);
			$authurl=$scriptname."?UID=".$this->uid."&UPASS=".$this->upass."&SUBUID=".$this->subuid."&SUBUPASS=".$this->subupass;
			echo $this->apiurl.$authurl."&".$parameter;
			curl_setopt($ch, CURLOPT_URL, $this->apiurl.$authurl."&".$parameter);
		//	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
			//curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// Timeout in seconds
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			   $result = curl_exec($ch);
			curl_close($ch);

				if(curl_errno($ch))  //catch if curl error exists and show it
				  echo 'Curl error: ' . curl_error($ch);
				else
				  echo $result;
		
			//return $result;
				echo "<pre>";
				print_r($result);
				echo "</pre>";
		}

	function GetMobileInfo($mobile,$apikey)
	{
		if($apikey=="prepaid")	$apikey = $this->apikeyprepaid;
		if($apikey=="postpaid")	$apikey = $this->apikeypostpaid;
		
		$scriptname = "findMsisdnInfo.go";
		$parameter="msisdn=$mobile";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$apikey."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		$operatorcode= isset($array['operator_code']) ?$array['operator_code'] :'';
		return $operatorcode;
		
    }
	
	function GetCircle($mobile,$apikey)
	{
		if($apikey=="prepaid")	$apikey = $this->apikeyprepaid;
		if($apikey=="postpaid")	$apikey = $this->apikeypostpaid;
		
		$scriptname = "findMsisdnInfo.go";
		$parameter="msisdn=$mobile";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$apikey."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		$circle_code= isset($array['circle_code']) ?$array['circle_code'] :'';
		return $circle_code;
		
    }
	
	function GetLandlineOperator()
	{
		$scriptname = "serviceData.go";
		$parameter="service_family=".$this->apisfutility;
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyutility."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		return $array;
		
    }
	
	function GetPlan($operatorCode,$type,$circle)
	{	
		$ch = curl_init();
		$apikey= 172803667719761;
		//TUP=> Top-up Recharge, FTT=> Full Talk-time Recharge, 2G=> 2G Data Recharge, 3G=> 3G/4G Data Recharge, SMS=> SMS Pack Recharge, LSC=> Local/STD/ISD Call Recharge, OTR=> Other Recharge, RMG=> National/International Roaming Recharge
		$nullvalue = "Enter 10 digit mobile number in given form on the left to view plans.";
		if($operatorCode=="" OR $circle=="")	return $nullvalue;
	
		$myurl = "https://joloapi.com/api/findplan.php?userid=payin&key=$apikey&opt=$operatorCode&cir=$circle&typ=$type&max=&amt=&type=json";

		// echo $myurl;
	
		$msg= '';
		curl_setopt ($ch, CURLOPT_URL, $myurl);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 30);
		$jsonxx = curl_exec($ch);
		$curl_error = curl_errno($ch);
		curl_close($ch);
		$someArray = json_decode($jsonxx, true);
		if (count($someArray) > 0) {
			$msg.=  "<table><thead><tr>
			<th>Detail</th>
			<th>Amount (Rs.)</th>
			<th>Validity (days)</th>
			<th>Select Plan</th>
			</tr></thead><tbody>";
			foreach ($someArray as $key => $value) {
				$radioid = (int)$key+1;
				$amount = $value["Amount"];
				$msg.= " <tr><td>" .$value["Detail"] . "</td> <td>" .$value["Amount"] . "</td> <td>" .$value["Validity"] . '</td><td><input type="radio" id="'.$radioid.'" name="PLANamt" onclick="PushAmount(id,'.$amount.')" ondblclick="Uncheck()"></td> </tr>';
			}
			$msg.=  "</tbody></table><br/>";

		}else{
			$msg.="No offer details available for this category";
		}	
		echo $msg;
	}
	
}
?>