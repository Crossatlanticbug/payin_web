<?
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* WEB Database
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
global $config;

if (!isset($config))
{
	require_once("../../Configurations/config.inc.php");
}

$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';

// IP ADDRESS OR SERVER NAME OF MYSQL SERVER
$host = $config['WEB_DB'][$ENV]['HOST'];

// DATABASE YOU WANT TO USE. MUST EXIST
$database = "payin_dev";

// USERNAME TO CONNECT TO SERVER
$user = $config['WEB_DB'][$ENV]['USERNAME'];

// PASSWORD TO CONNECT TO SERVER
$pwd = $config['WEB_DB'][$ENV]['PASSWORD'];

// 1=SAVES ALL SQL QUERIES TO LOG FILE
$keepLog = 0;

// PATH WHERE LOG SHOULD BE KEPT
$logPath = "../Log/connection.log";

// 1=SHOW FULL MYSQL ERROR (QUERY, DB CONNECT INFO) | 0=SHOW MYSQL ERROR
$fullErr = 0;

// 1=Rollback support on database (begin; commit; or begin; rollback;) This should only
// be active if your tables are transactional like InnoDB.
// MyISAM does not provide transactional support
$rollBackSup = 0;

?>