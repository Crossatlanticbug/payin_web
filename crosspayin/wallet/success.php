<?php

	session_start();
	include_once("../Includes/template.inc.php");

	include_once("../Classes/main.class.php");
	$main = new main();
	include_once("../Classes/order.class.php");
	$orderprocess = new Order();
	require("../phpMailer/PHPMailerAutoload.php");

	global $config; 
	$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
	if (!isset($_SESSION['ORDERID'])){
		exit("<script>window.location.href='../site/error.php?err=invalid';</script>");
	}
	
	$codestatus=true;
	$result = $orderprocess->getRechargeOrderInfo($_SESSION['ORDERID']);	
	$phone=$result->mobileno;
	$amount=floor($result->amount);
	$status="success";
	$firstname=$result->name;
	$amount=$amount;
	$txnid=substr(hash('sha256', mt_rand() . microtime()), 0, 20);;
	$email=$result->email;
	$phone =$phone;
	$ORDERID =$_SESSION['ORDERID'];

	$logvalue = new stdClass();
	$logvalue->paymentlog=json_encode($result) ;
	$logvalue->rechargelog='';
	$logvalue->mobileno=$phone;
	$logvalue->emailid=$email; 
	$logvalue->userip=$_SERVER['REMOTE_ADDR'];
	$logvalue->pageurl= $_SERVER['REQUEST_URI'];
	$logid=$orderprocess->AddDevelopmentLog($logvalue);
	$rechargeamount=$amount*(-1);
	$ordervalue="transactionid='".$txnid."',paymentstatus='success'";
	$CurrentUser = &$_SESSION['CurrentUser'];
	$orderprocess->UpdateRechargeOrder($ordervalue,$ORDERID) ; 
	$orderprocess->UpdateBalance($rechargeamount,$CurrentUser->id);	
	$CurrentUser->balance+=$rechargeamount;
	$msg = "<h3>Thank You. Your order status is ". $status .".</h3>";
	$msg .= "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";

		
		include_once("../Classes/api.class.php");
		$apiprocess= new RechageApi();
		$randomno = GenerateRandomID(20);
		$phone=$result->mobileno;
		$amount=floor($result->amount);
		
		$mess="Your Order has been  successfully placed with transaction id ".$txnid.". ";
			
		
		$main->sendSms($phone,$mess);
		
		$to      = $email; // Send email to our user
		$subject = 'Order Confirmation'; // Give the email a subject 
		$message=$config['EMAIL_HEADER'];
		$message.= "Dear ".$result->name.",<br><br>";
		$message.= "A order has been successfully submitted. Please Find below details regarding the transaction:<br><br>";
		$message.= "<strong>Order No :</strong>".$ORDERID."<br>";
		$message.= "<strong>Transaction ID :</strong>".$txnid."<br>";
		$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
		$message.= "<strong>Payment Status :</strong>".$status."<br>";
				 
		$message.=$config['EMAIL_FOOTER'];
			
		//Answer key attachment
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $config['SMTP']['HOST'];
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $config['SMTP']['PORT'];
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $config['SMTP']['USER'];
		//Password to use for SMTP authentication
		$mail->Password = $config['SMTP']['PASSWORD'];
		
		$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
		$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
		$mail->addAddress(''.$to.'');
		$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->IsHTML(true); 
		$mail->send();
		

			$rechargetype=$result->rechargetype;
			$operator=$result->operator;
			$mobiletype= $result->mobiletype;
			If (!empty($phone)){
					/*-------------------------		Mobile Recharge	Start	--------------------------------*/
					If ($rechargetype == "mobile"){
						
						//Pre-paid Mobile Recharge
						if($mobiletype=="prepaid")	{
							$operatorcode=$result->operatorid;
							$apiresponse= $apiprocess->MobileRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][$rechargetype][$mobiletype]);
						}
						
						//Post-Paid Mobile Recharge
						if($mobiletype=="postpaid")	{
							$vendor_code=$result->operatorid;
							$apiresponse= $apiprocess->MobileRechargePostPaid($phone,$vendor_code,$amount,$randomno,$config['API']['SF'][$rechargetype][$mobiletype]);
							if (isset($apiresponse['ErrorCode'])){
								$msg= $apiresponse['Message'];
								$codestatus=false;
							}
						}				
				
						$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
						$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
						
						if ($codestatus=true AND $apiresponse['response_code'] == 0){
							
							if($mobiletype=="prepaid"){
								$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
								$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
								unset($_SESSION['ORDERID']);
								
								$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
								$main->sendSms($phone,$mess);
								$to      = $result->email; // Send email to our user
								$subject = 'Recharge Confirmation'; // Give the email a subject 
								$message=$config['EMAIL_HEADER'];
								$message.= "Dear ".$result->name.",<br><br>";
								$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
								$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
								$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
								$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
								$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
									 
								$message.=$config['EMAIL_FOOTER'];
								//Answer key attachment
								$mail = new PHPMailer;
								$mail->isSMTP();
								$mail->Host = $config['SMTP']['HOST'];
								//Set the SMTP port number - likely to be 25, 465 or 587
								$mail->Port = $config['SMTP']['PORT'];
								//Whether to use SMTP authentication
								$mail->SMTPAuth = true;
								//Username to use for SMTP authentication
								$mail->Username = $config['SMTP']['USER'];
								//Password to use for SMTP authentication
								$mail->Password = $config['SMTP']['PASSWORD'];
							
								$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
								$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
								$mail->addAddress(''.$to.'');
								$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
								$mail->Subject = $subject;
								$mail->Body = $message;
								$mail->IsHTML(true); 
								$mail->send();
							}
				
							if($mobiletype=="postpaid")	{
								$rechargevalue="rechargekey='".$apiresponse['trans_id']."',rechargestatus='".$apiresponse['status']."'";
								$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
								unset($_SESSION['ORDERID']);
								
								$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
								$main->sendSms($phone,$mess);
								$to      = $result->email; // Send email to our user
								$subject = 'Recharge Confirmation'; // Give the email a subject 
								$message=$config['EMAIL_HEADER'];
								$message.= "Dear ".$result->name.",<br><br>";
								$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
								$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
								$message.= "<strong>Transaction ID :</strong>".$apiresponse['trans_id']."<br>";
								$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
								$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
								$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
								$message.= "<strong>Recharge Date :</strong>".$apiresponse['datetime']."<br>";
									 
								$message.=$config['EMAIL_FOOTER'];
								//Answer key attachment
								$mail = new PHPMailer;
								$mail->isSMTP();
								$mail->Host = $config['SMTP']['HOST'];
								//Set the SMTP port number - likely to be 25, 465 or 587
								$mail->Port = $config['SMTP']['PORT'];
								//Whether to use SMTP authentication
								$mail->SMTPAuth = true;
								//Username to use for SMTP authentication
								$mail->Username = $config['SMTP']['USER'];
								//Password to use for SMTP authentication
								$mail->Password = $config['SMTP']['PASSWORD'];
							
								$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
								$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
								$mail->addAddress(''.$to.'');
								$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
								$mail->Subject = $subject;
								$mail->Body = $message;
								$mail->IsHTML(true); 
								$mail->send();
							}
						}
					}
	/*-------------------------		Mobile Recharge	End		--------------------------------*/
			/*-------------------------		Data Card Recharge Start	--------------------------------*/
					If ($rechargetype == "datacard"){
						$operatorcode=$result->operatorid;
						$apiresponse= $apiprocess->DataCardRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][$rechargetype]);
					
						if (isset($apiresponse['ErrorCode'])){
							$msg= $apiresponse['Message'];
							$codestatus=false;
						}
						
						$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
						$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
					
					if ($codestatus==true AND $apiresponse['response_code'] == 0){
								$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
								$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
								unset($_SESSION['ORDERID']);
								
								$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
								$main->sendSms($phone,$mess);
								$to      = $result->email; // Send email to our user
								$subject = 'Recharge Confirmation'; // Give the email a subject 
								$message=$config['EMAIL_HEADER'];
								$message.= "Dear ".$result->name.",<br><br>";
								$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
								$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
								$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
								$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
								$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
									 
								$message.=$config['EMAIL_FOOTER'];
								//Answer key attachment
								$mail = new PHPMailer;
								$mail->isSMTP();
								$mail->Host = $config['SMTP']['HOST'];
								//Set the SMTP port number - likely to be 25, 465 or 587
								$mail->Port = $config['SMTP']['PORT'];
								//Whether to use SMTP authentication
								$mail->SMTPAuth = true;
								//Username to use for SMTP authentication
								$mail->Username = $config['SMTP']['USER'];
								//Password to use for SMTP authentication
								$mail->Password = $config['SMTP']['PASSWORD'];
							
								$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
								$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
								$mail->addAddress(''.$to.'');
								$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
								$mail->Subject = $subject;
								$mail->Body = $message;
								$mail->IsHTML(true); 
								$mail->send();
							}
						}
	/*-------------------------		Data Card Recharge	End		--------------------------------*/
	
	/*-------------------------		DTH Recharge Start	--------------------------------*/
				If ($rechargetype == "dth"){
					$operatorcode=$result->operatorid;
					$apiresponse= $apiprocess->DTHRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][$rechargetype]);

					if (isset($apiresponse['ErrorCode'])){
						$msg= $apiresponse['Message'];
						$codestatus=false;
					}
					
					$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
					$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
					
					if ($codestatus==true AND $apiresponse['response_code']== 0){
							$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
							$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
							unset($_SESSION['ORDERID']);
							
							$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
							$main->sendSms($phone,$mess);
							$to      = $result->email; // Send email to our user
							$subject = 'Recharge Confirmation'; // Give the email a subject 
							$message=$config['EMAIL_HEADER'];
							$message.= "Dear ".$result->name.",<br><br>";
							$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
							$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
							$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
							$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
							$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
								 
							$message.=$config['EMAIL_FOOTER'];
							//Answer key attachment
							$mail = new PHPMailer;
							$mail->isSMTP();
							$mail->Host = $config['SMTP']['HOST'];
							//Set the SMTP port number - likely to be 25, 465 or 587
							$mail->Port = $config['SMTP']['PORT'];
							//Whether to use SMTP authentication
							$mail->SMTPAuth = true;
							//Username to use for SMTP authentication
							$mail->Username = $config['SMTP']['USER'];
							//Password to use for SMTP authentication
							$mail->Password = $config['SMTP']['PASSWORD'];
						
							$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
							$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
							$mail->addAddress(''.$to.'');
							$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
							$mail->Subject = $subject;
							$mail->Body = $message;
							$mail->IsHTML(true); 
							$mail->send();
						}
					}
	/*-------------------------		DTH Recharge End	--------------------------------*/
	
	/*-------------------------		Landline Recharge Start	--------------------------------*/
				If ($rechargetype == "landline"){
					$operatorcode=$result->operatorid;
					$custacc=$result->custacc;
					$custno=$result->custno;
					$apiresponse= $apiprocess->LandlineRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF']['utilitybill'],$custacc,$custno);
					
					if (isset($apiresponse['ErrorCode'])){
						$msg= $apiresponse['Message'];
						$codestatus=false;
					}
			
					$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
					$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
					
					if ($codestatus==true AND $apiresponse['response_code'] == 0){
							$rechargevalue="rechargekey='".$apiresponse['trans_id']."',rechargestatus='".$apiresponse['status']."'";
							$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
							unset($_SESSION['ORDERID']);
							
							$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
							$main->sendSms($phone,$mess);
							$to      = $result->email; // Send email to our user
							$subject = 'Recharge Confirmation'; // Give the email a subject 
							$message=$config['EMAIL_HEADER'];
							$message.= "Dear ".$result->name.",<br><br>";
							$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
							$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
							$message.= "<strong>Recharge key :</strong>".$apiresponse['trans_id']."<br>";
							$message.= "<strong>Customer Account :</strong>".$apiresponse['cust_acc']."<br>";
							$message.= "<strong>Customer Number :</strong>".$apiresponse['cust_no']."<br>";
							$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
							$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
								 
							$message.=$config['EMAIL_FOOTER'];
							//Answer key attachment
							$mail = new PHPMailer;
							$mail->isSMTP();
							$mail->Host = $config['SMTP']['HOST'];
							//Set the SMTP port number - likely to be 25, 465 or 587
							$mail->Port = $config['SMTP']['PORT'];
							//Whether to use SMTP authentication
							$mail->SMTPAuth = true;
							//Username to use for SMTP authentication
							$mail->Username = $config['SMTP']['USER'];
							//Password to use for SMTP authentication
							$mail->Password = $config['SMTP']['PASSWORD'];
						
							$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
							$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
							$mail->addAddress(''.$to.'');
							$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
							$mail->Subject = $subject;
							$mail->Body = $message;
							$mail->IsHTML(true); 
							$mail->send();
						}
					}
	/*-------------------------		Landline Recharge End	--------------------------------*/
	
	} else  {
		echo "Invalid Transaction. Please try again";
	}
	
?>
<div class='container' style='padding:50px 0;'><?php echo $msg ;?></div>
<?  End_Response(); ?>