<!--footer-->
<footer class="footer1">
		<div class="container">

		<div class="row"><!-- row -->

						<div class="col-lg-3 col-md-3"><!-- widgets column left -->
								<ul class="list-unstyled clear-margins"><!-- widgets -->
									<li class="widget-container widget_nav_menu"><!-- widgets list -->
										<h1 class="title-widget">COMPANY</h1>
										<ul>
											<li><a  href="../pages/about.php"><i class="fa fa-angle-double-right"></i> About Us </a></li>
											<li><a  href="../pages/contact.php"><i class="fa fa-angle-double-right"></i> Contact Us </a></li>
											<!--<li><a  href="#"><i class="fa fa-angle-double-right"></i> Career </a></li>-->
											<li><a  href="#"><i class="fa fa-angle-double-right"></i> Sitemap </a></li>
										</ul>
									</li>
								</ul>	  
						</div><!-- widgets column left end -->

						<div class="col-lg-3 col-md-3"><!-- widgets column left -->
								<ul class="list-unstyled clear-margins"><!-- widgets -->
									<li class="widget-container widget_nav_menu"><!-- widgets list -->
										<h1 class="title-widget">HELP & SUPPORT</h1>
										<ul>
											<li><a  href="#"><i class="fa fa-angle-double-right"></i> Blog </a></li>
											<li><a  href="../pages/payment-options.php"><i class="fa fa-angle-double-right"></i> Payment Options	</a></li>
											<li><a  href="../pages/faq.php"><i class="fa fa-angle-double-right"></i> FAQ </a></li>
											<li><a  href="#"><i class="fa fa-angle-double-right"></i> Support </a></li>
										</ul>
									</li>
								</ul>
						</div><!-- widgets column left end -->

						<div class="col-lg-3 col-md-3"><!-- widgets column left -->
								<ul class="list-unstyled clear-margins"><!-- widgets -->
									<li class="widget-container widget_nav_menu"><!-- widgets list -->
										<h1 class="title-widget">POLICIES</h1>
										<ul>
											<li><a href="../pages/privacy-policy.php"><i class="fa fa-angle-double-right"></i> Privacy Policy</a></li>
											<li><a href="../pages/refund-policy.php"><i class="fa fa-angle-double-right"></i> Refund Policy</a></li>
											<li><a href="../pages/grievance-policy.php"><i class="fa fa-angle-double-right"></i> Grievance Policy</a></li>
										</ul>
									</li>
								</ul>
						</div><!-- widgets column left end -->

						<div class="col-lg-3 col-md-3"><!-- widgets column center -->
							<ul class="list-unstyled clear-margins"><!-- widgets -->
								<li class="widget-container widget_recent_news"><!-- widgets list -->
									<h1 class="title-widget">Contact Detail </h1>
									<div class="footerp"> 
										<h2 class="title-median">Payin Utilities & services Pvt. Ltd.</h2>
										<p><b>Email id:</b> <a href="mailto:info@Payin.in">info@Payin.in</a></p>
										<p><b>Helpline Numbers </b>
										<b >(24X7 Support)</b>  <span> 18001802259 </span> </p>
									</div>

									<div class="social-icons">
										<ul class="nomargin">
												<a href="#"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
												<a href="#"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
												<a href="#"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
												<a href="mailto:mail@gmail.com"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
										</ul>
									</div>
								</li>
							 </ul>
						</div>		   
			</div>

		</div>
</footer>

<!--header-->

<div class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="copyright">		© 2017, payin, All rights reserved	</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="design">
					<a target="_blank" href="http://crossatlanticsoftware.in">
					<img src="../Images/crossatlanticwhite.jpg" alt="Design & Development by Cross Atlantic Software Pvt. Ltd."></a>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
					<img class="img-circle" id="img_logo" src="../Images/logo1.jpg">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</button>
				</div>
                
                <!-- Begin # DIV Form -->

						<div class="modal-body">
							<div class="tab-content">
									<div id="LogIn" class="tab-pane fade in active">
												<h2>Log in</h2>
												 <div id="dataresult"> </div>
												<div class="form-group">
													 <label class="sr-only" for="exampleInputmobile">Mobile No.</label>
													 <input type="tel" class="form-control" id="txtloginmobileno" name="txtloginmobileno" placeholder="Mobile No." >
												</div>
												<div class="form-group"> OR </div>
													<div class="form-group">
														 <label class="sr-only" for="exampleInputEmail2">Email address</label>
														 <input type="email" class="form-control" id="txlogintemailaddress" name="txlogintemailaddress" placeholder="Email address" >
													</div>
													<div class="form-group">
														 <label class="sr-only" for="exampleInputPassword2">Password</label>
														 <input type="password" class="form-control" id="txtloginpassword" name="txtloginpassword" placeholder="Password" >
														 
													</div>
													<div class="form-group">
														 <button type="submit" class="btn btn-primary btn-block" onclick="return Checklogin();">Sign in</button>
													</div>
													<div class="help-block text-right">
														 <a data-toggle="pill" href="#lostPassword"> Forget the password ?</a>
													 </div>
													<!--<div class="checkbox">
														 <label>
														 <input type="checkbox"> keep me logged-in
														 </label>
													</div>-->
											 
									</div>
									
									<div id="SignUp" class="tab-pane fade">
										 <h2>Sign Up</h2>
										 <form class="form" role="form" method="post" action="../site/registration.php" accept-charset="UTF-8" id="signup">
												<div class="form-group">
													 <label class="sr-only" for="exampleInputmobile">Mobile No.</label>
													 <input type="tel" class="form-control" id="txtmobileno" name="txtmobileno" placeholder="Mobile No." required="">
												</div>
												
												<div class="form-group">
													 <label class="sr-only" for="exampleInputEmail2">Email address</label>
													 <input type="email" class="form-control" id="txtemailaddress" name="txtemailaddress" placeholder="Email address" required="">
												</div>
												<div class="form-group">
													 <label class="sr-only" for="exampleInputPassword2">Password</label>
													 <input type="password" class="form-control" id="txtpassword" name="txtpassword" placeholder="Password" required="">
													 
												</div>
												<div class="form-group">
													 <button type="submit" class="btn btn-primary btn-block">Sign up</button>
												</div>
												<!--<div class="checkbox">
													 <label>
													 <input type="checkbox"> keep me logged-in
													 </label>
												</div>-->
										</form>
										<div class="help-block text-right">
											 <a data-toggle="pill" href="#lostPassword"> Forget the password ?</a>
										</div>
									</div>
									
									<div id="lostPassword" class="tab-pane fade">
									
										 <h2>Lost Password</h2>
										  <div id="dataforgotresult"> </div>
										 <form class="form" role="form" method="post" action="" accept-charset="UTF-8" id="login-nav">

												<div class="form-group">
													 <label class="sr-only" for="exampleInputEmail2">Email address</label>
													 <input type="text" class="form-control" id="txtemail" name="txtemail" placeholder="Email/Phone" required="">
												</div>
												<div class="form-group">
													 <button type="button" id="forgotbtn" onclick="checkPassword()" class="btn btn-primary btn-block">submit</button>
												</div>
										</form>
									</div>
							</div>
						</div>
                <!-- End # DIV Form -->
				
							<div class="modal-footer" align="center">
									<a data-toggle="pill" href="#LogIn">Log In</a>	/	
									<a data-toggle="pill" href="#SignUp">Register</a>
							</div>
                
			</div>
		</div>
	</div>
    <!-- END # MODAL LOGIN -->

	<script type="text/javascript">
<!--

function Checklogin(){

		
		var mobileno =$('#txtloginmobileno').val();
		var emailid =$('#txlogintemailaddress').val();
		var password =$('#txtloginpassword').val();
		
			if ((mobileno == "") && (emailid == ""))
			{
				alert("Please enter mobile no or emailid for login");
				$('#txtloginmobileno').focus();
				return false;
			}
			if (password == "") 
			{
				alert("Please enter password");
				$('#txtloginpassword').focus();
				return false;
			}
			
		var dataString = "action=actchecklogin&mobileno=" + mobileno +"&emailid=" + emailid +"&pass=" + password ;
		
		$.ajax({
			type: "POST",
			dataType: "text",
			url: "../site/postaction.php",
			data: dataString,
			beforeSend: function() {
			$('#dataresult').html("<div style='text-align: center; width: 325px;padding-botttom:10px'><img src='../Images/loader.gif' height='40'></div>").show('fast');	
			},
			success: function(result){
				if(result=='1'){
					window.location.href='../account/myprofile.php';
				}else{
					$('#dataresult').html('<div class="Alert" style="margin-top:0px;">Wrong username or password</div>');
				}
			}
			
		}); 
	
}

function checkPassword(){

		var emailforgot =$('#txtemail').val();
		
			if ((emailforgot == ""))
			{
				alert("Please enter emailid");
				$('#txtemail').focus();
				return false;
			}

		$.ajax({
			type: "POST",
			cache:false,
			url: "../site/forgotpassword.php",
			data: "emailid=" + emailforgot,
			beforeSend: function() {
			$('#dataforgotresult').html("<div style='text-align: center; width: 325px;padding-botttom:10px'><img src='../Images/loader.gif' height='40'></div>").show('fast');	
			},
			success: function(result){
				if(result==1){
					$('#dataforgotresult').html('<div class="Alert" style="margin-top:0px;">Your Password has been sent on your registered Emailid/Phone. </div>');
					window.location.href='../site/index.php#LogIn';
				}else{
					$('#dataforgotresult').html('<div class="Alert" style="margin-top:0px;">Please Enter Correct Emailid.</div>');
				}
				
			}
			
		}); 
}
</script>	