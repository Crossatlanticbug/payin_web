<?
	global $lang;
	global $cart;
	global $language;
	global $config;
	global $koolajax;
	global $page_title;
	global $page_description;
	global $CurrentPage;
	global $CurrentDirectory;
	global $buffer_contents;
	global $httpUserAgent;
	global $visitorIP;



	// Check if current user is logged in or not
	$currentUser = &$_SESSION['CurrentUser'];
	$logged_in   = (isset($currentUser) && is_object($currentUser));

	if ($CurrentPage != "login.php") $_SESSION['PageTo'] = $_SERVER['SCRIPT_NAME'];

	if (isset($_SESSION['PageTo']))
	{
		if (($_SESSION['PageTo'] == "/Home/login.php") || ($_SESSION['PageTo'] == "/Home/index.php"))
		{
			$_SESSION['PageTo'] = "/Account/index.php";
		}
	}



	$currentURL= "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
//echo $currentURL;
?>
<!DOCTYPE html>
<html>
<head>
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="-1">
		<meta charset="UTF-8">
		<title>Payin</title>
		<link rel="icon" type="image/png" href="../Images/favicon.png">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='//fonts.googleapis.com/css?family=Roboto:700,400&subset=latin' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css'>
		<link rel="stylesheet" href="../css/style.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		
<!----	Admin lte Start	---->		
		
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<!-- DataTables -->
		<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		   folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
		
		<!-- jQuery 2.2.3 -->
		<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
		<!-- DataTables -->
		<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
		<!-- SlimScroll -->
		<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="../plugins/fastclick/fastclick.js"></script>
		<!-- AdminLTE App -->
		<!--<script src="../dist/js/app.min.js"></script>-->
		<!-- AdminLTE for demo purposes -->
		<!--<script src="../dist/js/demo.js"></script>-->
		<!-- page script -->		
		
<!----	Admin lte End	---->

</head>


<body>

		<? include "../Includes/top.inc.php"; ?>
		<div class="content_middle" ><?=$buffer_contents?></div>
		<? include "../Includes/bottom.inc.php"; ?>
	
		<script src="../js/index.js"></script>
		<script>	$('.carousel').carousel({      interval: 10000	    })		</script>

</body>
</html>
