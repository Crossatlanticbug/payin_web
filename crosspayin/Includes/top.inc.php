<?php $CurrentUser = &$_SESSION['CurrentUser'];	?> 
<div class="top_bar">
	<div class="container">
			<div class="col-md-6">
				<ul class="social">
					<li><a target ="_blank" href="https://www.facebook.com/Payin-1824191161128945/"><i class="fa fa-facebook text-white"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
				</ul>
			</div>

			<div class="col-md-6">
				<ul class="rightc">
				<li><i class="fa fa-envelope-o"></i> <a href="mailto:info@payin.in">info@payin.in</a></li>
				<li><i class="fa fa-user"></i> <a href="../site/becomepartner.php" >Become a Reseller</a></li>      
				</ul>
			</div>
	</div>
</div>
<!--top_bar-->


<!-- header -->

<nav class="navbar navbar-default" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
		<a class="navbar-brand hexagon" href="../site/index.php">
			<img src="../Images/payin-without-bg.png">
		</a>
    </div>

    <div class="navbar-collapse" id="bs-megadropdown-tabs" style="padding-left: 0px;">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="../site/index.php"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="../site/mobile.php"><i class="fa fa-mobile"></i> Mobile</a></li>
            <li><a href="../site/dth.php"><i class="fa fa-television"></i> DTH</a></li>
			<li><a href="../site/datacard.php"><i class="fa fa-signal"></i> Datacard</a></li>
			<li><a href="../site/landline.php"><i class="fa fa-signal"></i> Landline</a></li>
            <!--<li><a href="#"><i class="fa fa-lightbulb-o"></i> Electricity</a></li>
            <li><a href="#"><i class="fa fa-phone"></i> Landline </a></li>
            <li><a href="#"><i class="fa fa-fire"></i> Gas</a></li>-->

			<?php if (!empty($_SESSION['UsernNo'])){?>
			<?php if ($CurrentUser->isreseller==2){ ?>
				<li><a href="../account/support.php"><i class="fa fa-home"></i> Support</a></li>
			<?php } ?>
			<li><a href="../account/addmoney.php"><i class="fa fa-inr"></i> Add Money</a></li>
			<li><a href="../account/sendmoney.php"><i class="fa fa-paper-plane"></i> Send Money</a></li>
			<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i> Profile <span class="caret"></span>
                    </a>
                    <ul style="background-color:white;" class="dropdown-menu">
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <i class="glyphicon glyphicon-user icon-size"></i>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong><?php echo isset($CurrentUser->name) ?$CurrentUser->name :'';?></strong></p>
                                        <p class="text-left small"><?php echo isset($CurrentUser->email) ?$CurrentUser->email :'';?></p>
										<p class="text-left small">My Wallet : INR <?php echo isset($CurrentUser->balance) ?$CurrentUser->balance :'';?></p>
                                        <p class="text-left">
											 <a href="../account/myprofile.php" class="btn btn-primary btn-block btn-sm">View Profile</a>
											 
                                           <!-- <a href="../account/editprofile.php" class="btn btn-success btn-block btn-sm">Edit Profile</a>-->
                                           
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a href="../site/logout.php" class="btn btn-danger btn-block">Logout</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
			<?php } else { ?>
				<li>
					<a href="#" role="button" data-toggle="modal" data-target="#login-modal"><i class="fa fa-key"></i> Log in / Sign In</a>
				</li>
			<?php } ?>
			<!--	<li>
					<div class="search-icn">
						<a href="#search" >
							<i class="fa fa-search" aria-hidden="true"></i>
						</a>
					</div>
				</li>-->
        </ul>
    </div>
  </div>
</nav>
<!-- /header -->

<!-- search -->
<!--<div id="search">
    <button type="button" class="close">×</button>
    <form method="get" >
        <input type="search" name="search_inp" name="search" value="" placeholder="Search here" />
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
</div>-->
<!--// search -->
