//	search

$(function () {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
});

function ActionRecharge()
{
	
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['rechargeform'])
  {
  	var paymenttype = optpayfor.value;
  	var mobileno = txtmobileno.value;
  	var operator = cmboperator.value;
	var amount = txtamount.value;

	
		if (paymenttype == "") {
	      alert("Please Select Prepaid or Postpaid Option!!!");
	      optpayfor.focus();
	      return false;
		
		}
		if(isNaN(parseInt(mobileno)))
		{
			alert("Please Enter Valid Mobile No!");
			txtmobileno.focus();
			return false;
		}
		else if (mobileno.length!=10)
		{
			alert("Please Enter Valid Mobile No!");
			txtmobileno.focus();
			return false;
		}
		
		if (operator == "") {
	      alert("Please Select Operator!!!");
	      cmboperator.focus();
	      return false;
		}
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
		/*else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} */
	}
}

function ActionRechargeDTH()
{
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['rechargedthform'])
  {
  
  	var dthno = txtmobileno.value;
  	var operator = cmboperator.value;
	var amount = txtamount.value;

		
		
		if(isNaN(parseInt(dthno)))
		{
			alert("Please Enter Valid DTH No!");
			txtmobileno.focus();
			return false;
		}
		
		
		if (operator == "") {
	      alert("Please Select Operator!!!");
	      cmboperator.focus();
	      return false;
		}
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<100)
		{
			alert("Please insert an amount greater than 99!");
			txtamount.focus();
			return false;
		} 
		else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} 
	}
}

function ActionRechargedatacard()
{
	
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['rechargedatacardform'])
  {
  	var paymenttype = optpayfor.value;
  	var mobileno = txtmobileno.value;
  	var operator = cmboperator.value;
	var amount = txtamount.value;

	
		if (paymenttype == "") {
	      alert("Please Select Prepaid or Postpaid Option!!!");
	      optpayfor.focus();
	      return false;
		
		}
		if(isNaN(parseInt(mobileno)))
		{
			alert("Please Enter Valid Datacard No!");
			txtmobileno.focus();
			return false;
		}
		else if (mobileno.length!=10)
		{
			alert("Please Enter Valid Datacard No!");
			txtmobileno.focus();
			return false;
		}
		
		if (operator == "") {
	      alert("Please Select Operator!!!");
	      cmboperator.focus();
	      return false;
		}
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
		/*else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} */
	}
}

function ActionRechargelandline()
{
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['rechargelandlineform'])
  {
  
  	var landlineno = txtmobileno.value;
  	var operator = cmboperator.value;
	var custacc = txtcustacc.value;
	var custno = txtcustno.value;
	var amount = txtamount.value;

	
		
		if(isNaN(parseInt(landlineno)))
		{
			alert("Please Enter Registered Mobile No!");
			txtmobileno.focus();
			return false;
		}
		
		if (operator == "") {
	      alert("Please Select Operator!!!");
	      cmboperator.focus();
	      return false;
		}
		
		if(isNaN(parseInt(custacc)))
		{
			alert("Please insert a valid Account Number!");
			txtcustacc.focus();
			return false;
		}
		
		if(isNaN(parseInt(custno)))
		{
			alert("Please insert a valid Customer Number!");
			txtcustno.focus();
			return false;
		}
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
		/*else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} */
	}
}


function ActionRechargepreview(val)
{
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['rechargeform'])
	{
		var recharge = rechargetype.value;
		var mobileno = txtmobileno.value;
		var operator = cmboperator.value;
		var amount = txtamount.value;
		var name = txtname.value;
		var email = txtemail.value;
		
		if (recharge!="dth" && recharge!="landline") {
			var paymenttype = optpayfor.value;
		}
		
		if ((paymenttype == "") && (recharge!="dth") && (recharge!="landline") ){
	      alert("Please Select Prepaid or Postpaid Option!!!");
	      optpayfor.focus();
	      return false;
		
		}
		if(isNaN(parseInt(mobileno)))
		{
			alert("Please Enter Valid "+recharge+" No!");
			txtmobileno.focus();
			return false;
		}
		else if ((mobileno.length!=10)  && (recharge!="dth"))
		{
			alert("Please Enter Valid "+recharge+" No!");
			txtmobileno.focus();
			return false;
		}
		
		if (operator == "") {
	      alert("Please Select Operator!!!");
	      cmboperator.focus();
	      return false;
		}
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
	/*	else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} */
		if (name == "") {
	      alert("Name should not be blank!");
	      txtname.focus();
	      return false;
		
		}
		if (email == "") {
			alert("You must enter your e-mail address!");
			txtemail.focus();
			return false;
    }
		if (email != "")
		{
			if (!isValidMail(email))
			{
				txtemail.focus();
				return false;
			}
	  }
	  
	  if (val =="2"){
		  document.getElementById('paymenttype').value="WALLET";
	  } else {
		  document.getElementById('paymenttype').value="PAYU";
	  } 
	}

}

function ActionAddMoney()
{
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['addmoneyform'])
	{
		var amount = txtamount.value;
	
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
		/*else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} */
		if (name == "") {
	      alert("Name should not be blank!");
	      txtname.focus();
	      return false;
		
		}
	}

}


function ActionSendMoney()
{
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['sendmoneyform'])
	{
		var amount = txtamount.value;
		var mobileno = txtsendmobileno.value;
		var balance = txtbalance.value;
		
		if(isNaN(parseInt(mobileno)))
		{
			alert("Please Enter Valid Mobile No!");
			txtsendmobileno.focus();
			return false;
		}
		else if (mobileno.length!=10)
		{
			alert("Please Enter Valid Mobile No!");
			txtsendmobileno.focus();
			return false;
		}
		
		if(isNaN(parseFloat(amount)))
		{
			alert("Please insert a valid amount!");
			txtamount.focus();
			return false;
		}
		else if (parseFloat(amount)<=0)
		{
			alert("Please insert an amount greater than 0!");
			txtamount.focus();
			return false;
		}
		/*else if (parseFloat(amount)>9999)
		{
			alert("Please insert an amount less than 9999!");
			txtamount.focus();
			return false;
		} */
		else if (parseFloat(amount)>balance)
		{
			alert("Please insert an amount less than current balance!");
			txtamount.focus();
			return false;
		} 
	}

}


function isValidMail(emailStr, alertMe) {
  var checkTLD=1;
  var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
  var emailPat=/^(.+)@(.+)$/;
  var specialChars= "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
  var validChars= "\[^\\s" + specialChars + "\]";
  var quotedUser="(\"[^\"]*\")";
  var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
  var atom=validChars + '+';
  var word="(" + atom + "|" + quotedUser + ")";
  var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
  var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
  var matchArray=emailStr.match(emailPat);
  if (matchArray==null) {
  	if(alertMe == null || alertMe == true )
    alert("Your email address is missing or incorrectly spelled check!!");
    return false;

  }
  var user=matchArray[1];
  var domain=matchArray[2];
  for (i=0; i<user.length; i++) {
    if (user.charCodeAt(i)>127) {
      alert("Your email address contains invalid characters.");
      return false;
     }
  }
  for (i=0; i<domain.length; i++) {
    if (domain.charCodeAt(i)>127) {
      alert("Your email domain name contains invalid characters.");
      return false;
     }
  }
  if (user.match(userPat)==null) {
    alert("Your email address does not seem to be valid - check your spelling, including wrong use of commas, or a full point . at the end of the address!!");
    return false;
  }
  var IPArray=domain.match(ipDomainPat);
  if (IPArray!=null) {
    for (var i=1;i<=4;i++) {
      if (IPArray[i]>255) {
        alert("Destination IP address is invalid!");
        return false;
      }
    }
    return true;
  }
  var atomPat=new RegExp("^" + atom + "$");
  var domArr=domain.split(".");
  var len=domArr.length;
  for (i=0;i < len;i++) {
    if (domArr[i].search(atomPat)==-1) {
      alert("Your email address does not seem to be valid - check your spelling, including wrong use of commas, or a full point . at the end of the address.");
      return false;
     }
  }
  if (checkTLD && domArr[domArr.length-1].length!=2 &&
  domArr[domArr.length-1].search(knownDomsPat)==-1) {
    alert("Your email address must end in a well-known domain or two letter " + "country.");
    return false;
  }
  if (len < 2) {
    alert("Your email address is missing a hostname - check spelling.");
    return false;
  }
  return true;
}

//	end search

/******---------------		 Operator, Circle, Recharge Plan Select Default On Mobile Recharge Start	----------------*****/
     $(document).ready(function (){
		 
		 $('#rechargeform #txtmobileno').keyup(function(){   
			var optpayfor = $("#rechargeform #optpayfors").val();
			var txtmobileno = $("#rechargeform #txtmobileno").val().substr(0,4);
			 $.ajax({
				cache: false,
				type: 'POST',
				url: '../site/operator.php',
				data: 'txtmobileno='+txtmobileno+'&optpayfor='+optpayfor,
				success: function(result){
				    $opt = $('#cmboperators option[value="' + result + '"]');
					$opt.prop('selected', true);     			
				}
		   });
		   
			var optpayfor = $("#rechargeform #optpayfors").val();
			var txtmobileno = $("#rechargeform #txtmobileno").val().substr(0,10);
			 $.ajax({
				cache: false,
				type: 'POST',
				url: '../site/circle.php',
				data: 'txtmobileno='+txtmobileno+'&optpayfor='+optpayfor,
				success: function(result){
				    if(result!=""){
						$opt = $('#cmbcircles option[value="' + result + '"]');
						$opt.prop('selected', true);
					}else{
						$opt = $('#cmbcircles option[value=""]').html("Select Circle");;
						$opt.prop('selected', true);
					}
				}
		   });
		});
	   
		$('#rechargeform #txtmobileno').blur(function(){      
			var optpayfor = $("#rechargeform #optpayfors").val();
			var txtmobileno = $("#rechargeform #txtmobileno").val().substr(0,4);
			 $.ajax({
				cache: false,
				type: 'POST',
				url: '../site/operator.php',
				data: 'txtmobileno='+txtmobileno+'&optpayfor='+optpayfor,
				success: function(result){
				    $opt = $('#cmboperators option[value="' + result + '"]');
					$opt.prop('selected', true);     			
				}
			});
		   
			var optpayfor = $("#rechargeform #optpayfors").val();
			var txtmobileno = $("#rechargeform #txtmobileno").val().substr(0,10);
			 $.ajax({
				cache: false,
				type: 'POST',
				url: '../site/circle.php',
				data: 'txtmobileno='+txtmobileno+'&optpayfor='+optpayfor,
				success: function(result){
				    if(result!=""){
						$opt = $('#cmbcircles option[value="' + result + '"]');
						$opt.prop('selected', true);
					}else{
						$opt = $('#cmbcircles option[value=""]').html("Select Circle");
						$opt.prop('selected', true);
					}		
				}
		   });
		   
		  
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='TUP';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultTUP").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='FTT';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultFTT").replaceWith(result);
				}
		 
		   });
		   
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='2G';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#Result2G").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='3G';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#Result3G").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='SMS';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultSMS").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='LSC';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultLSC").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='RMG';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultRMG").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='OTR';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultOTR").replaceWith(result);
				}
		 
		   });
		    
		});
		
		 $('#rechargeform #cmboperators').change(function(){
			var optpayfor = $("#optpayfors").val();
			var txtmobileno = $("#rechargeform #txtmobileno").val().substr(0,10);
			 $.ajax({
				cache: false,
				type: 'POST',
				url: '../site/circle.php',
				data: 'txtmobileno='+txtmobileno+'&optpayfor='+optpayfor,
				success: function(result){
				    if(result!=""){
						$opt = $('#cmbcircles option[value="' + result + '"]');
						$opt.prop('selected', true);
					}else{
						$opt = $('#cmbcircles option[value=""]').html("Select Circle");
						$opt.prop('selected', true);
					}		
				}
		   });
		   
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='TUP';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultTUP").replaceWith(result);
				}
		 
		   });
		   
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='FTT';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultFTT").replaceWith(result);
				}
		 
		   });
		   
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='2G';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#Result2G").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='3G';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#Result3G").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='SMS';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultSMS").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='LSC';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultLSC").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='RMG';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultRMG").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='OTR';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultOTR").replaceWith(result);
				}
		 
		   });
		    
		});
		
		 $('#rechargeform #cmbcircles').change(function(){      
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='TUP';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultTUP").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='FTT';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultFTT").replaceWith(result);
				}
		 
		   });
		   
			var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='2G';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#Result2G").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='3G';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#Result3G").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='SMS';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultSMS").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='LSC';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultLSC").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='RMG';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultRMG").replaceWith(result);
				}
		 
		   });
		   
		   var operatorCode = $("#rechargeform #cmboperators").val();
			var type ='OTR';
			var circle = $("#rechargeform #cmbcircles").val();
			$.ajax({
				cache: false,
				type: 'POST',
				url: '../site/plan.php',
				data: 'operatorCode='+operatorCode+'&type='+type+'&circle='+circle,
				success: function(result){
					$("#ResultOTR").replaceWith(result);
				}
		 
		   });
		    
		});
		
	   });	  
	   
	   
	   
	   
	   
/******---------------		 Operator, Circle, Recharge Plan Select Default On Mobile Recharge End	----------------*****/

// profile

$('.nav-tabs-dropdown').each(function(i, elm) {
       $(elm).text($(elm).next('ul').find('li.active a').text());
});
  
$('.nav-tabs-dropdown').on('click', function(e) {
    e.preventDefault();
    $(e.target).toggleClass('open').next('ul').slideToggle();
});

$('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function(e) {
    e.preventDefault();
	$(e.target).closest('ul').hide().prev('a').removeClass('open').text($(this).text());
});

// end profile

/*		Push Amount Of plan	Start */
function PushAmount(id,amount)
{
	var RadioID = id;
	var RadioAmount = amount;
	var PLANamt = $("#txtamount").val();
	$("#txtamount").val(RadioAmount);
}

function Uncheck()
{
	var RadioAmount = "";
	$(this).prop('checked', false);
	$("#txtamount").val(RadioAmount);
}

function checkbecomepartner()
{
	
	var error = 'An error occured with the request, please try again';
	var loading = 'Please wait while loading ...';

	with (document.forms['becomepartnerform'])
	{
		var email = txtemail.value;
		var mobileno = txtphone.value;
			
		
		if(isNaN(parseInt(mobileno)))
		{
			alert("Please Enter Valid phone No!");
			txtphone.focus();
			return false;
		}
		else if (mobileno.length!=10) 
		{
			alert("Please Enter Valid phone No!");
			txtphone.focus();
			return false;
		}
		
		
		if (email == "") {
			alert("You must enter your e-mail address!");
			txtemail.focus();
			return false;
		}
		if (email != "")
		{
			if (!isValidMail(email))
			{
				txtemail.focus();
				return false;
			}
		}
	 
	 
	}

}


/*		Push Amount Of plan	End */
