<?php 
session_start();
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once("../Includes/template.inc.php");
include_once("../Classes/order.class.php");
$order = new order();
global $dbLink;
include_once("../Classes/api.class.php");
$apiprocess= new RechageApi();
$CurrentUser = &$_SESSION['CurrentUser'];
if (!isset($CurrentUser->id)){
	exit("<script>window.location.href='../site/index.php';</script>");
}
$phoneno = $CurrentUser->phone;
include_once("../Classes/customer.class.php");
$customer = new customer();
If (isset($_POST) &&  count($_POST)>0){
	$postupdatevalues->address=isset($_POST['txtaddress'])?$_POST['txtaddress']:'';
	$postupdatevalues->city=isset($_POST['txtcity'])?$_POST['txtcity']:'';
	$postupdatevalues->state=isset($_POST['txtstate'])?$_POST['txtstate']:'';
	$postupdatevalues->zipcode=isset($_POST['txtzipcode'])?$_POST['txtzipcode']:'';
	$postupdatevalues->name=isset($_POST['txtname'])?$_POST['txtname']:'';

	$customer->updateprofile($CurrentUser->id,$postupdatevalues);	
	$msg = "Profile updated successfully";
}	
$cond = "id='".$CurrentUser->id."'";
$result= $customer->getcustomerinfo($cond);

?>

	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<a href="#" class="nav-tabs-dropdown btn btn-block btn-primary">Edit Profile</a>
					<h3>Edit Profile</h3>
					<form class="form" role="form" method="post" action="" accept-charset="UTF-8" id="editprofile">
					<div class="tab-content">
					<?php if (isset($msg) && !empty($msg)){?>
					<div class="Alert" style="margin-top:0px;text-align: center;"><?php echo $msg;?></div>
					<? } ?>
						<div class="col-sm-6 col-sm-offset-3">

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtname" id="txtname" placeholder="Name" autocomplete="off" value="<?php echo $result->name;?>">
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtaddress" id="txtaddress" placeholder="Address" autocomplete="off" value="<?php echo $result->address;?>">
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtcity" id="txtcity" placeholder="City" autocomplete="off" value="<?php echo $result->city;?>">
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtstate" id="txtstate" placeholder="State" autocomplete="off" value="<?php echo $result->state;?>">
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtzipcode" id="txtzipcode" placeholder="Zipcode" autocomplete="off" value="<?php echo $result->zipcode;?>">
								</div>

						</div>

						<div class="col-sm-6 col-sm-offset-3">
							<button type="submit" class="btn btn-primary btn-block">Submit</button>
						</div>
						
					
					</div>
					</form>
					</div>
				</div>
			</div>

</section>

<!--	//	profile		-->

<?  End_Response(); ?>