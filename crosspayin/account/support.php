<?php 
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");
	include_once("../Classes/order.class.php");
	$order = new order();
	global $dbLink;
	include_once("../Classes/api.class.php");
	$apiprocess= new RechageApi();
	$CurrentUser = &$_SESSION['CurrentUser'];
	if (!isset($CurrentUser->id)){
		exit("<script>window.location.href='../site/index.php';</script>");
	}
	
?>
<style>
	.mainDIV{
		padding:60px 20px 20px 20px;
	}
</style>
<div class="mainDIV">
      <div class="row">
        <div class="col-xs-12">
		
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Payment/Recharge Detail</h3>
            </div>
            <!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Name</th>
								<th>Mobile No</th>
								<th>Amount</th>
								<th>Date</th>
								<th>Transaction ID</th>
								<th>Payment Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?
						$cond="LOWER(paymentstatus)='success' AND LOWER(rechargestatus)='pending' ";
						$results= $order->Supportrechargeinfo($cond);
						while ($rows=mysqli_fetch_object($results))
						{
						?>
							<tr>
								
									<td><?=$rows->name?></td>
									<td><?=$rows->mobileno?></td>
									<td><b><?=number_format($rows->amount,2)?></b></td>
									<td><?=$rows->rechargedate?></td>
									<td><?=$rows->transactionid?></td>
									<td><?=$rows->paymentstatus?></td>
								
								<td class="help-block text-right">
									<a href="../site/rechargedetail.php?ordid=<?=$rows->orderid?>"><button type="button" class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i></button></a>
									
									<a href="../site/rerecharge.php?ordid=<?=$rows->orderid?>"><button type="button" class="btn btn-success"> Recharge </button></a>
									
									<a href="../site/reffertoadmin.php?ordid=<?=$rows->orderid?>"><button type="button" class="btn btn-warning">Refund</button></a>
									
									<a href="../site/informtocustomer.php?ordid=<?=$rows->orderid?>"><button type="button" class="btn btn-info">Inform</button></a>
								</td>
							</tr>
						<?
							}
						?>		
					</tbody>
				</table>
            </div>
			<div id="Detail"></div>
		<!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div> 
 
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>	
	
<?  End_Response(); ?>