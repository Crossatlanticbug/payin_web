<?php 
session_start();
header( 'Cache-Control: no-store, no-cache, must-revalidate' ); 
header( 'Cache-Control: post-check=0, pre-check=0', false ); 
header( 'Pragma: no-cache' );
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/order.class.php");
$orderprocess= new Order();

$CurrentUser = &$_SESSION['CurrentUser'];
if (!isset($CurrentUser->id)){
	exit("<script>window.location.href='../site/index.php';</script>");
}



include_once("../Classes/customer.class.php");
$customer = new customer();

include_once("../Classes/main.class.php");
$main = new main();
require ("../phpMailer/PHPMailerAutoload.php");

$amount = isset($_REQUEST['txtamount']) ?$_REQUEST['txtamount'] :'';
$sendmobileno = isset($_REQUEST['txtsendmobileno']) ?$_REQUEST['txtsendmobileno'] :'';

$process =1;
if (count($_POST)>0){
	if (empty($sendmobileno)){
		$errormsg ="Please Enter Valid Mobile No!";
		$process =0;
	}  else {
		
		if($CurrentUser->phone==$sendmobileno){
			$errormsg ="Please Enter Another Registered Mobile No!";
			$process =0;
		}
		$destinationnodetails= $customer->checkdestinationacount($sendmobileno);
		if (!isset($destinationnodetails->id)) {
			$errormsg ="Please Enter Valid Mobile No!";
			$process =0;
		}	
		
	}	

	if (empty($amount)){
		$errormsg ="Please insert a valid amount!";
		$process =0;
	}	else  {
		if ($amount>$CurrentUser->balance)
		{
			$errormsg ="Please insert an amount less than current balance!";
			$process =0;
		}
	}	



	if ($process ==1){
		
		$sendmoneyvalue->frommobile =  $CurrentUser->phone;
		$sendmoneyvalue->tomobile =  $sendmobileno;
		$sendmoneyvalue->amount = $amount;
		$sendmoneyvalue->payment = "WALLET";
		$sendmoneyvalue->transactionid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);;
		$sendmoneyvalue->userip = $_SERVER['REMOTE_ADDR'];
		$sendmoneyvalue->paymentstatus = "success"; 
		$orderid = $orderprocess->SendMoney($sendmoneyvalue);
		$_SESSION['senderorderid']=$orderid;
		$orderprocess->updateSenderMoney($CurrentUser->phone,$amount);
		$sendercond = "phone='".$CurrentUser->phone."'";
		$senderinfo=$customer->getcustomerinfo($sendercond);
		$mess="Your Order has been  successfully placed with transaction id ".$sendmoneyvalue->transactionid .". Your current balance is INR ". $senderinfo->balance;
		$main->sendSms($CurrentUser->phone,$mess);
			
			$name= isset($senderinfo->name)?$senderinfo->name:'Customer';
			$to      = $senderinfo->email; // Send email to our user
			$subject = 'Amount Sent'; // Give the email a subject 
			$message=$config['EMAIL_HEADER'];
			$message.= "Dear ".ucfirst($name).",<br><br>";
			$message.= "Your order has been successfully submitted. Please Find below details regarding the transaction:<br><br>";
			$message.= "<strong>Order No : </strong>".$orderid."<br>";
			$message.= "<strong>Transaction ID : </strong>".$sendmoneyvalue->transactionid."<br>";
			$message.= "<strong>Receiver Mobile : </strong>".$sendmoneyvalue->tomobile."<br>";
			$message.= "<strong>Amount : </strong> INR ".$amount."<br>";
			$message.= "<strong>Balance : </strong> INR ".$senderinfo->balance."<br>";
			$message.= "<strong>Payment Status : </strong>success<br>";
					 
			$message.=$config['EMAIL_FOOTER'];
				
			//Answer key attachment
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = $config['SMTP']['HOST'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $config['SMTP']['PORT'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $config['SMTP']['USER'];
			//Password to use for SMTP authentication
			$mail->Password = $config['SMTP']['PASSWORD'];
			
			$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
			$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addAddress(''.$to.'');
			$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->IsHTML(true); 
			$mail->send();	
		
		$orderprocess->updateReceiverMoney($sendmobileno,$amount);
		$receivercond = "phone='".$sendmobileno."'";
		$receiverinfo=$customer->getcustomerinfo($receivercond);
		$mess="You have received payment INR ".$amount." from ".$sendmoneyvalue->frommobile.". Your current balance is INR ". $receiverinfo->balance;
		$main->sendSms($receiverinfo->phone,$mess);
		
			$name= isset($receiverinfo->name)?$receiverinfo->name:'Customer';
			$to      = $receiverinfo->email; // Send email to our user
			$subject = 'Amount Received'; // Give the email a subject 
			$message=$config['EMAIL_HEADER'];
			$message.= "Dear ".ucfirst($name).",<br><br>";
			$message.= "You have received payment INR ".$amount." from ".$sendmoneyvalue->frommobile.". Your current balance is INR ". $receiverinfo->balance;
						 
			$message.=$config['EMAIL_FOOTER'];
				
			//Answer key attachment
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = $config['SMTP']['HOST'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $config['SMTP']['PORT'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $config['SMTP']['USER'];
			//Password to use for SMTP authentication
			$mail->Password = $config['SMTP']['PASSWORD'];
			
			$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
			$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addAddress(''.$to.'');
			$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->IsHTML(true); 
			$mail->send();	
			
			exit("<script>window.location.href='../wallet/response.php';</script>");
			
	}	
}	



?>
<div class="slider" id="slider">

	<div class="container">

		<div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-3">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab3" data-toggle="tab">
							<span class="fa fa-paper-plane" aria-hidden="true"></span>
							<div class="hidden-xs">Send Money</div>
						</button>
					</div>
					
				</div>

				<div class="well">
					  <div class="tab-content">
					  <?php if (isset($errormsg)){ ?>
					  <div class="Alert" style="margin-top:0px;"><?php echo $errormsg ;?></div>
					  <? } ?>
					<div class="tab-pane fade in active" id="tab3">
						<form name="sendmoneyform"  Method="POST" id="sendmoneyform" onsubmit="return ActionSendMoney();" action="">

								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
										<input id="txtsendmobileno" maxlength="12" type="tel" class="form-control inp-field" name="txtsendmobileno" value="" placeholder="Mobile No">
								</div>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
										<input id="txtamount" maxlength="12" type="number" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
										<input id="txtbalance" maxlength="12" type="hidden" class="form-control inp-field" name="txtbalance" value="<?php echo $CurrentUser->balance;?>" placeholder="Amount">
								</div>
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Send Money</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
						

						</form>

						</div>
						
						
					
					  </div>
				</div>
			</div>

			
	</div>


</div>
<!-- //slider -->



<?  End_Response(); ?>
