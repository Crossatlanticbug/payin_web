	<?php	include('head-321.php');	?>


<!-- 		profile		-->



	<section class="section">
	
	
			<div class="container">
				
				<div class="row">
					<div class="col-sm-12">

						<a href="#" class="nav-tabs-dropdown btn btn-block btn-primary">Tabs</a>

						<ul id="nav-tabs-wrapper" class="nav nav-tabs nav-tabs-horizontal">
								<li class="active"><a href="#htab1" data-toggle="tab">Personal Info</a></li>
								<li><a href="#htab2" data-toggle="tab">Address</a></li>
								<li><a href="#htab3" data-toggle="tab">Change Password</a></li>
								<li><a href="#htab4" data-toggle="tab">Merchants Authorized by You</a></li>
								<li><a href="#htab5" data-toggle="tab">Request Wallet Upgrade</a></li>
								<li><a href="#htab6" data-toggle="tab">Favourites</a></li>
						</ul>

						<div class="tab-content">

								<div role="tabpanel" class="tab-pane fade in active" id="htab1">
									<h3>Tab 1</h3>
									<p> Mauris imperdiet dignissim ante, in efficitur mauris elementum sed. Cras vulputate malesuada magna. Morbi tincidunt eros a dui cursus, vitae dignissim nulla scelerisque. Duis pharetra scelerisque mi vel luctus. Cras eu purus efficitur, blandit nisi id, fringilla nulla.</p>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="htab2">
									<h3>Tab 2</h3>
									<p> Mauris imperdiet dignissim ante, in efficitur mauris elementum sed. Cras vulputate malesuada magna. Morbi tincidunt eros a dui cursus, vitae dignissim nulla scelerisque. Duis pharetra scelerisque mi vel luctus. Cras eu purus efficitur, blandit nisi id, fringilla nulla.</p>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab3">
									<h3>Tab 3</h3>
											<div class="col-sm-6 col-sm-offset-3">
												<p class="text-center">Use the form below to change your password. Your password cannot be the same as your username.</p>
												<form method="post" id="passwordForm">
													<input type="password" class="input-lg form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off">
													<div class="row">
													<div class="col-sm-6">
													<span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> 8 Characters Long<br>
													<span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Uppercase Letter
													</div>
													<div class="col-sm-6">
													<span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Lowercase Letter<br>
													<span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Number
													</div>
													</div>
													<input type="password" class="input-lg form-control" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off">
													<div class="row">
													<div class="col-sm-12">
													<span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
													</div>
													</div>
													<input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
												</form>
											</div>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab4">
									<h3>Tab 4</h3>
									<p>Etiam id pharetra quam. Morbi tristique nunc vel sapien dapibus, sit amet imperdiet quam venenatis. Vestibulum et suscipit urna. Suspendisse volutpat quis est eu volutpat. Nulla non tortor venenatis turpis congue aliquet. Vivamus at elit vel massa elementum tempor sit amet sed odio. Nullam placerat, arcu sed ullamcorper ornare, erat erat placerat quam, in feugiat nulla purus in nunc. Maecenas vitae erat auctor, aliquam tellus et, vulputate eros.</p>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab5">
									<h3>Tab 5</h3>
									<p>Etiam id pharetra quam. Morbi tristique nunc vel sapien dapibus, sit amet imperdiet quam venenatis. Vestibulum et suscipit urna. Suspendisse volutpat quis est eu volutpat. Nulla non tortor venenatis turpis congue aliquet. Vivamus at elit vel massa elementum tempor sit amet sed odio. Nullam placerat, arcu sed ullamcorper ornare, erat erat placerat quam, in feugiat nulla purus in nunc. Maecenas vitae erat auctor, aliquam tellus et, vulputate eros.</p>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab6">
									<h3>Tab 6</h3>
									<p>Etiam id pharetra quam. Morbi tristique nunc vel sapien dapibus, sit amet imperdiet quam venenatis. Vestibulum et suscipit urna. Suspendisse volutpat quis est eu volutpat. Nulla non tortor venenatis turpis congue aliquet. Vivamus at elit vel massa elementum tempor sit amet sed odio. Nullam placerat, arcu sed ullamcorper ornare, erat erat placerat quam, in feugiat nulla purus in nunc. Maecenas vitae erat auctor, aliquam tellus et, vulputate eros.</p>
								</div>

						</div>
					</div>
				</div>
			</div>

</section>

<!--	//	profile		-->



<?php	include('foot-321.php');	?>