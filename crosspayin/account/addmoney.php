<?php 
session_start();
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/order.class.php");
$orderprocess= new Order();
$CurrentUser = &$_SESSION['CurrentUser'];
if (!isset($CurrentUser->id)){
	exit("<script>window.location.href='../site/index.php';</script>");
}

$amount = isset($_REQUEST['txtamount']) ?$_REQUEST['txtamount'] :'';

if (!empty($amount)){
	$addmoneyvalue= new stdclass();
	$addmoneyvalue->customerid = $CurrentUser->id;
	$addmoneyvalue->amount = $amount;
	$addmoneyvalue->payment = "PAYU";
	$addmoneyvalue->paymentstatus = "pending"; 
	$addmoneyvalue->transactionid = "";
	$addmoneyvalue->userip = $_SERVER['REMOTE_ADDR'];
	
	$orderid = $orderprocess->AddMoney($addmoneyvalue);
	$_SESSION['ADDMONEYORDERID'] = $orderid;
	if (!empty($orderid)){
		exit("<script>window.location.href='../payumoney/payuprocess.php';</script>");
	} else {
		exit("<script>window.location.href='../site/error.php?err=errorprocess';</script>");
	}	
}	else {
	$errormsg = "Please insert a valid amount!";
	
}


?>

<div class="slider" id="slider">

	<div class="container">

		<div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-3">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab3" data-toggle="tab">
							<span class="fa fa-inr" aria-hidden="true"></span>
							<div class="hidden-xs">Add Money</div>
						</button>
					</div>
					
				</div>

				<div class="well">
					  <div class="tab-content">
					   <?php if ($errormsg){ ?>
					  <div class="Alert" style="margin-top:0px;"><?php echo $errormsg ;?></div>
					  <? } ?>
					<div class="tab-pane fade in active" id="tab3">
						<form name="addmoneyform"  Method="POST" id="addmoneyform" onsubmit="return ActionAddMoney();" action="">

								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
										<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
								</div>
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Add In wallet</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
						

						</form>

						</div>
						
						
					
					  </div>
				</div>
			</div>

			
	</div>


</div>
  
<!-- //slider -->



<?  End_Response(); ?>
