<?php 
session_start();
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once("../Includes/template.inc.php");
include_once("../Classes/order.class.php");
$order = new order();
global $dbLink;
include_once("../Classes/api.class.php");
$apiprocess= new RechageApi();
$CurrentUser = &$_SESSION['CurrentUser'];
if (!isset($CurrentUser->id)){
	exit("<script>window.location.href='../site/index.php';</script>");
}
$phoneno = $CurrentUser->phone;
$cond = "id='".$CurrentUser->id."'";
$result= $customer->getcustomerinfo($cond);

?>
<style>
.errorMSG{
	color:red;
}
</style>
	<section class="section">
	
	
			<div class="container">
				
				<div class="row">
					<div class="col-sm-12">

						<a href="#" class="nav-tabs-dropdown btn btn-block btn-primary"></a>

						<ul id="nav-tabs-wrapper" class="nav nav-tabs nav-tabs-horizontal">
								<li class="active">
								<a href="#htab1" data-toggle="tab">Personal Info</a>
								</li>
								<li><a href="#htab2" data-toggle="tab">Address</a></li>
								<li><a href="#htab3" data-toggle="tab">Change Password</a></li>
								<li><a href="#htab4" data-toggle="tab">Recharge Info</a></li>
								<li><a href="#htab5" data-toggle="tab">Send Money</a></li>
								<li><a href="#htab6" data-toggle="tab">Receive Money</a></li>
								<li><a href="#htab7" data-toggle="tab">Edit Profile</a></li>
						</ul>

						<div class="tab-content">

								<div role="tabpanel" class="tab-pane fade in active" id="htab1">
									<h3>Personal Info</h3>
									 <p class="text-left"><strong>Name : </strong><?php echo $CurrentUser->name;?></strong></p>
                                        <p class="text-left small"><strong>Email : </strong><?php echo $CurrentUser->email;?></p>
										<p class="text-left small"><strong>My Wallet : </strong>INR <?php echo $CurrentUser->balance;?></p>
										<p class="text-left small"><strong>Phone : </strong><?php echo $CurrentUser->phone;?></p>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="htab2">
									<h3>Address</h3>
									
									<p class="text-left"><strong>Address : </strong><?php echo $CurrentUser->address;?></strong></p>
										<p class="text-left small"><strong>City : </strong><?php echo $CurrentUser->city;?></p>
										<p class="text-left small"><strong>State : </strong><?php echo $CurrentUser->state;?></p>
										<p class="text-left small"><strong>Zipcode : </strong><?php echo $CurrentUser->zipcode;?></p>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab3">
									<h3>Change Password</h3>
											<div class="col-sm-6 col-sm-offset-3">
												<!--<p class="text-center">Use the form below to change your password. Your password cannot be the same as your username.</p>-->
												<form method="post" action="" id="passwordForm">

															<div class="form-group">
																<span id="dataresult"> </span>
																<input type="password" class="input-lg form-control" name="password1" id="password1" autocomplete="off" placeholder="New Password">
																<span class="errorMSG" id="msgPASS1"> </span>
															</div>

															<div class="form-group">
																<input type="password" class="input-lg form-control" name="password2" id="password2" autocomplete="off" placeholder="Re-enter Password">
																<span class="errorMSG" id="msgPASS2"> </span>
															</div>

															<div class="form-group">
																<input type="button" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." onclick="ResetPassword()" value="Change Password">
															</div>

												</form>
											</div>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab4">
									<h3>Recharge Info</h3>
									<p>



									<table class="table table-hover">
											<thead>
												<tr>
													<th>Mobile No</th>
													<th>Operator</th>
													<th>Amount</th>
													<th>Date</th>
													<th>Payment</th>
													<th>Transaction ID</th>
													<th>Payment Status</th>
													<th>Recharge Status</th>
												</tr>
											</thead>
											<tbody>

											<?
											$results= $order->getrechargeinfo($CurrentUser->id);
											if($results){
												while ($rows=mysqli_fetch_object($results))
												{
											?>
													<tr>
														<td><?=$rows->mobileno?></td>
														<td><?=$rows->operator?></td>
														<td><b><?=number_format($rows->amount,2)?></b></td>
														<td><?=$rows->rechargedate?></td>
														<td><?=$rows->payment?></td>
														<td><?=$rows->transactionid?></td>
														<td><?=$rows->paymentstatus?></td>
														<td><?=$rows->rechargestatus?></td>
													</tr>
											<?	
														
												}
											}else{
											?>
												<tr>
													<td class="gridEmpty">-----</td>
													<td class="gridEmpty">-----</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
												</tr>
										<?
											}
										?>
											
							</tbody>
						  </table>
									</p>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab5" >
									<h3>Send Money</h3>
									<p>
									
									
									
									
									<table class="table table-hover">
										<thead>
											<tr>
  												<th>From Mobile</th>
												<th>To Mobile</th>
												<th>Amount</th>
												<th>Date</th>
												<th>Transaction ID</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>

											<?
											$results= $order->getsendmoney($phoneno);
											if($results){
												while ($rows=mysqli_fetch_object($results))
												{
											?>
													<tr>
														<td><?=$rows->frommobile?></td>
														<td><?=$rows->tomobile?></td>
														<td><b><?=number_format($rows->amount,2)?></b></td>
														<td><?=$rows->processdate?></td>
														<td><?=$rows->transactionid?></td>
														<td><?=$rows->paymentstatus?></td>
													</tr>
											<?			
												}
											}else{
											?>
												<tr>
													<td class="gridEmpty">-----</td>
													<td class="gridEmpty">-----</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
												</tr>
										<?
											}
										?>

										    </tbody>
										</table>

									</p>
								</div>
								<div role="tabpanel" class="tab-pane fade in" id="htab6">
									<h3>Receive Money</h3>
									<p>


									<table class="table table-hover">
										<thead>
										  <tr>
												<th>From Mobile</th>
												<th>To Mobile</th>
												<th>Amount</th>
												<th>Date</th>
												<th>Transaction ID</th>
												<th>Status</th>
											</tr>
										</thead>
									<tbody>
											<?
											$resultr= $order->getreceivemoney($phoneno);
											if($resultr){
												while ($rowr=mysqli_fetch_object($resultr))
												{
											?>
													<tr  border="1px solid grey">
														<td><?=$rowr->frommobile?></td>
														<td><?=$rowr->tomobile?></td>
														<td><b><?=number_format($rowr->amount,2)?></b></td>
														<td><?=$rowr->processdate?></td>
														<td><?=$rowr->transactionid?></td>
														<td><?=$rowr->paymentstatus?></td>
													</tr>
											<?	
														
												}
											}else{
											?>
												<tr  border="1px solid grey">
													<td class="gridEmpty">-----</td>
													<td class="gridEmpty">-----</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
													<td class="gridEmpty">---------</td>
												</tr>
										<?
											}
										?>

											</tbody>
										  </table>

										</p>
								</div>
			
				<div role="tabpanel" class="tab-pane fade in" id="htab7">
					<h3>Edit Profile</h3>
					<form class="form" role="form" method="post" action="" accept-charset="UTF-8" id="editprofile">
					<div class="tab-content">
						<div class="col-sm-6 col-sm-offset-3">
								
								<div class="form-group">
									<span id="dataresultp"> </span>
									<input type="text" class="input-lg form-control" name="txtname" id="txtname" placeholder="Name" autocomplete="off" value="<?php echo $result->name;?>">
									<span class="errorMSG" id="msgNAME"></span>
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtaddress" id="txtaddress" placeholder="Address" autocomplete="off" value="<?php echo $result->address;?>">
									<span class="errorMSG" id="msgADDRESS"></span>
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtcity" id="txtcity" placeholder="City" autocomplete="off" value="<?php echo $result->city;?>">
									<span class="errorMSG" id="msgCITY"></span>
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtstate" id="txtstate" placeholder="State" autocomplete="off" value="<?php echo $result->state;?>">
									<span class="errorMSG" id="msgSTATE"></span>
								</div>

								<div class="form-group">
									<input type="text" class="input-lg form-control" name="txtzipcode" id="txtzipcode" placeholder="Zipcode" autocomplete="off" value="<?php echo $result->zipcode;?>">
									<span class="errorMSG" id="msgZIPCODE"></span>
								</div>

						</div>

						<div class="col-sm-6 col-sm-offset-3">
							<input type="button" class="btn btn-primary btn-block" data-loading-text="Updating Profile..." onclick="UpdateProfile()" value="Update Profile">
						</div>
						
					
					</div>
					</form>
					</div>
				
			</div>

						</div>
					</div>
				</div>
			</div>

</section>

<!--	//	profile		-->
<script>
function ResetPassword(){

		var pass1 =$('#password1').val();
		var pass2 =$('#password2').val();
			if ((pass1 == ""))
			{
				document.getElementById("msgPASS1").innerHTML = "Please enter password";
				$('#password1').focus();
				return false;
			}
			
			if ((pass2 == ""))
			{
				document.getElementById("msgPASS2").innerHTML = "Please re-enter password";
				$('#password2').focus();
				return false;
			}
			
			if(pass1!=pass2){
				document.getElementById("msgPASS2").innerHTML = "Please Enter Same Password";
				$('#password2').focus();
				return false;
			}
			document.getElementById("msgPASS1").innerHTML = "";
			document.getElementById("msgPASS2").innerHTML = "";
			document.getElementById("msgPASS2").innerHTML = "";
		$.ajax({
			type: "POST",
			cache:false,
			url: "../site/resetpassword.php",
			data: "pass1=" + pass1,
			beforeSend: function() {
			$('#dataresult').html("<div style='text-align: center; width: 325px;padding-botttom:10px'><img src='../Images/loader.gif' height='40'></div>").show('fast');	
			},
			success: function(result){
				if(result=='1'){
					$('#dataresult').html('<div class="Alert" style="margin-top:0px;border:1px solid #696;color: green;font-size: 13px;padding: 10px 0 10px 30px;text-align: left;background: #e3f5e7;">Dear Customer Password Successfully Updated, Please check Your Mail </div><br>');
					window.location.href='../account/myprofile.php#htab3';
				}else{
					$('#dataresult').html('<div class="Alert" style="margin-top:0px;">Please Try Again...</div>');
				}
				
			}
			
		}); 
}
function UpdateProfile(){

		var Name =$('#txtname').val();
		var Address =$('#txtaddress').val();
		var City =$('#txtcity').val();
		var State =$('#txtstate').val();
		var ZipCode =$('#txtzipcode').val();
		
			if ((Name == ""))
			{
				document.getElementById("msgNAME").innerHTML = "Please Enter Name";
				$('#txtname').focus();
				return false;
			}

			if ((Address == ""))
			{
				document.getElementById("msgADDRESS").innerHTML = "Please Enter Your Address";
				$('#txtaddress').focus();
				return false;
			}

			if ((ZipCode == ""))
			{
				document.getElementById("msgZIPCODE").innerHTML = "Please Enter Your Zip Code";
				$('#txtzipcode').focus();
				return false;
			}
			
			document.getElementById("msgNAME").innerHTML = "";
			document.getElementById("msgADDRESS").innerHTML = "";
			document.getElementById("msgZIPCODE").innerHTML = "";
			
		$.ajax({
			type: "POST",
			cache:false,
			url: "../site/profileupdate.php",
			data: "Name=" + Name+ "&Address=" + Address+ "&City="+ City+ "&State=" + State+ "&ZipCode=" + ZipCode,
			beforeSend: function() {
			$('#dataresultp').html("<div style='text-align: center; width: 325px;padding-botttom:10px'><img src='../Images/loader.gif' height='40'></div>").show('fast');	
			},
			success: function(result){
				if(result=='1'){
					$('#dataresultp').html('<div class="Alert" style="margin-top:0px;border:1px solid #696;color: green;font-size: 13px;padding: 10px 0 10px 30px;text-align: left;background: #e3f5e7;">Profile Updated Successfully.</div><br>');
					window.location.href='../account/myprofile.php#htab7';
				}else{
					$('#dataresultp').html('<div class="Alert" style="margin-top:0px;">Please Try Again...</div>');
				}
				
			}
			
		}); 
}

</script>
<?  End_Response(); ?>