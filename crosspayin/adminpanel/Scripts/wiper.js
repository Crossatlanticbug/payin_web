/*********************************************************************************************
* Created By: Darwish A. Khalil
* Date: 04/12/2005 - 03:56 PM
*********************************************************************************************/

<!-- Hide from old browsers


var speed = 120 // decrease value to increase speed (must be positive)
var pause = 4000 // increase value to increase pause
var timerID = null
var bannerRunning = false
var ar = new Array()
ar[0] = "<< Content Management System of Nadi Addahyi Website >>    "

var currentMessage = 0
var offset = 0
function stopBanner() {
        if (bannerRunning)
                clearTimeout(timerID)
        bannerRunning = false
}
function wiper() {
        stopBanner()
        showBanner()
}
function showBanner() {
        var text = ar[currentMessage]
        if (offset < text.length) {
                if (text.charAt(offset) == " ")
                        offset++
                var partialMessage = text.substring(0, offset + 1)
                window.status = partialMessage
                offset++
                timerID = setTimeout("showBanner()", speed)
                bannerRunning = true
        } else {
                offset = 0
                currentMessage++
                if (currentMessage == ar.length)
                        currentMessage = 0
                timerID = setTimeout("showBanner()", pause)
                bannerRunning = true
        }
}
// -->