
// This function is used to check if a certain text is numeric or not
function IsNumeric(sText)
{
  var ValidChars = "0123456789.,";
  var IsNumber=true;
  var Char;

  for (i = 0; ((i < sText.length) && (IsNumber == true)); i++) {
    Char = sText.charAt(i);
    if (ValidChars.indexOf(Char) == -1) {
      IsNumber = false;
    }
  }
  return IsNumber;
}

function IsNotZero(nNumber)
{
	return ( isNaN(nNumber) || parseFloat(nNumber)==0 );
}

// This function is used to change the CSS class of a certain object
function ChangeCSSClass(obj, newCSSClass)
{
	identity = document.getElementById(obj);
	identity.className = newCSSClass;
}


// This function returns true if a is the undefined value. You can get the undefined value
// from an uninitialized variable or from an object's missing member.
function IsUndefined(a)
{
   return (typeof(a) == 'undefined');
}

// This function is used to restrict users to enter only numbers inside a given text box
function NumbersOnly(obj)
{
	with (document.form)
	{
		// Only integer numbers allowed!
		if (!/^\d*$/.test(obj.value)) {
			obj.value = obj.value.replace(/[^\d]/g,"");
			obj.focus();
		}
	}
}

// This function is used to restrict users to enter only numbers and decimal point in a given text box
function filterNum(myfield, e, dec)
{
	var key;
  var keychar;

	if (window.event)
	   key = window.event.keyCode;
	else if (e)
	   key = e.which;
	else
	   return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) ||
	   (key==9) || (key==13) || (key==27) )
	   return true;

	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
	   return true;

	// decimal point jump
	else if (dec && (keychar == "."))
	   {
	   myfield.form.elements[dec].focus();
	   return false;
	   }
	else
	   return false;
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
// Remove both the leading and the trailing space/s of a given string
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
function Trim(str) {
	// Remove leading spaces and carriage returns
  while (str.substring(0,1) == ' ') {
  	str = str.substring(1,str.length);
	}

	// Remove trailing spaces and carriage returns
  while (str.substring(str.length-1,str.length) == ' ') {
  	str = str.substring(0,str.length-1);
  }
  return str;
}

// This function is to be used in the show/hide row contents without img
function ShowHideRow(Row, Img) {
  if (Row.style.display == '') {
    Row.style.display = 'none';
    Img.src = '../Images/icon_plus.jpg';
  }else{
    Row.style.display = '';
    Img.src = '../Images/icon_minus.jpg';
  }
}

/*-*-*-*-*-* This function is to be used in the show/hide row contents with img *-*-*-*-*-*/
function ToggleInfoRow(Row, Img) {
  if (Row.style.display == '') {
    Row.style.display = 'none';
    Img.src = '../Images/arrowOffEN.gif';
  }else{
    Row.style.display = '';
		Img.src = '../Images/arrowOn.gif';
  }
}

/*-*-*-*-*-* This function is to be used in the show a certain object and hide another one *-*-*-*-*-*/
function ShowHideObjects(objShow, objHide)
{
  objShow.style.display = '';
  objHide.style.display = 'none';
}

// This function is used to open a popup window
function openNewWindow(url,width,height) {
  var left =  Math.floor(screen.width/2)-Math.floor(width/2);
  var top =  Math.floor(screen.height/2)-Math.floor(height/2);
	popwindow = window.open(url, "popwindow", "toolbar=no,width="+width+",height="+height+",top="+top+",left="+left);
	popwindow.focus();
}

// This function is used to filter data of a certain combobox regarding those written on the textbox used for the filtration
function SelectObject( txt, cmb )
{

	var txtObj = document.getElementById(txt);
	var cmbObj = document.getElementById(cmb);
	for( var i=0;i < cmbObj.options.length;i++ )
	{
		if( cmbObj.options[i].text.indexOf( txtObj.value ) == 0 )
		{
			cmbObj.selectedIndex = i;
			break;
		}
	}
}

// This function is used in the login process
function login() {
  with (document.form) {
    if (txtUsername.value=="") {
      alert("Please enter your UserName!");
    	txtUsername.focus();
   	  return false;
		}
    if (txtPassword.value=="") {
      alert("Please you must enter a password!");
      txtPassword.focus();
      return false;
		}

  	useraction.value = "login";
    action = "login.php";
    submit();
	}
}

// This function is used to submit form when the user press enter
function submitEnter(myfield,myevent,myaction) {
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (myevent) keycode = myevent.which;
  else return true;
	//alert (keycode);
  if (keycode == 13) {
    if (myaction == "login") {
      login();
    }else if (myaction == "search") {
      SearchForMaterials();
    }else if (myaction == "subscribe") {
      SubscribeMe();
    }else if (myaction == "unsubscribe") {
      UnSubscribeMe();
    }else{
      myfield.form.submit();
    }
    return false;
  }
  else
     return true;
}


// This function is used to check if a certain email is valid
function isValidMail(emailStr) {
  var checkTLD=1;
  var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
  var emailPat=/^(.+)@(.+)$/;
  var specialChars= "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
  var validChars= "\[^\\s" + specialChars + "\]";
  var quotedUser="(\"[^\"]*\")";
  var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
  var atom=validChars + '+';
  var word="(" + atom + "|" + quotedUser + ")";
  var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
  var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
  var matchArray=emailStr.match(emailPat);
  if (matchArray==null) {
    alert("<?=$lang['Email_Missing']?>");
    return false;
  }
  var user=matchArray[1];
  var domain=matchArray[2];
  for (i=0; i<user.length; i++) {
    if (user.charCodeAt(i)>127) {
      alert("Your email address contains invalid characters.");
      return false;
     }
  }
  for (i=0; i<domain.length; i++) {
    if (domain.charCodeAt(i)>127) {
      alert("Your email domain name contains invalid characters.");
      return false;
     }
  }
  if (user.match(userPat)==null) {
    alert("Your email address does not seem to be valid - check your spelling, including wrong use of commas, or a full point . at the end of the address!!");
    return false;
  }
  var IPArray=domain.match(ipDomainPat);
  if (IPArray!=null) {
    for (var i=1;i<=4;i++) {
      if (IPArray[i]>255) {
        alert("Destination IP address is invalid!");
        return false;
      }
    }
    return true;
  }
  var atomPat=new RegExp("^" + atom + "$");
  var domArr=domain.split(".");
  var len=domArr.length;
  for (i=0;i < len;i++) {
    if (domArr[i].search(atomPat)==-1) {
      alert("Your email address does not seem to be valid - check your spelling, including wrong use of commas, or a full point . at the end of the address.");
      return false;
     }
  }
  if (checkTLD && domArr[domArr.length-1].length!=2 &&
  domArr[domArr.length-1].search(knownDomsPat)==-1) {
    alert("Your email address must end in a well-known domain or two letter " + "country.");
    return false;
  }
  if (len < 2) {
    alert("Your email address is missing a hostname - check spelling.");
    return false;
  }
  return true;
}

// This function is used to check if the email is valid or not
function checkEmail() {
  with (document.form) {
    if (txtEmail.value == "") {
      alert("Please you must enter your e-mail account!!");
      txtEmail.focus();
      return false;
    }else if (txtEmail.value != "") {
      if (!isValidMail(txtEmail.value)) {
	      txtEmail.focus();
	      return;
	    }
    }
    return true;
  }
}

// This function is used to submit the form
function submitForm() {
  document.forms[0].submit();
}

function SubmitFormAction(sURL)
{
	with (document.form)
	{
		action = sURL;
		submit();
	}
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Material Types Page Scripts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


// This function is used to check the required fields
function CheckName()
{
  with (document.form) {
    if (txtName.value == "") {
      alert("<?=$lang['Check_Name']?>");
      txtName.focus();
      return false;
    }

    useraction.value = "AddEdit";
    submit();
	}
}

// This function is used to confirm deletion
function ConfirmDeletion(ID)
{
	with (document.form)
	{
	  if (!confirm("<?=$lang['Confirm_Deletion']?>"))
	  {
	  	return false;
	  }

	  useraction.value = "Delete";
	  id.value = ID;
	  submit();
	}
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Attachment Types Page Scripts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


// This function is used to check the required fields
function CheckNames()
{
  with (document.form) {
    if (txtName.value == "") {
      alert("<?=$lang['Check_Name']?>");
      txtName.focus();
      return false;
    }

    useraction.value = "AddEdit";
    AllowedExtensions.value = generateExtensionsStr();
    submit();
	}
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Used for lists
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
function AddItem(List1,List2) {
  if (List1.options.selectedIndex>=0 && List1.options[0].value != "-1")
  {
		// Add the selected item of List1 in List2
    Element = new Option(List1.options[List1.options.selectedIndex].text,List1.options[List1.options.selectedIndex].value, false, true);
    if (List2.options[0].value != "-1") {
			List2.add(Element,List1.options[List1.options.selectedIndex].value); //List2.add(Element,List1.length);
		} else {
			List2.remove(0);
			List2.add(Element,0);
		}

		// Remove the selected item of List1
		List1.remove(List1.options.selectedIndex);
    Element = new Option("----------------------------------------------","-1");
    if (List1.options.length == 0) {
			List1.add(Element,0);
		}
	}
}

function SwapLists(List1,List2) {
  if (List1.options.length>0 && List1.options[0].value != "-1")
  {
		// Add items of List1 in List2
    if (List2.options[0].value == "-1") List2.remove(0);
    for(i=0;i < List1.options.length;i++) {
		  Element = new Option(List1.options[i].text,List1.options[i].value);
			List2.add(Element,List1.options[i].value); // List2.add(Element,i);
		}
		// Remove items of List1
		L = List1.options.length
    for(i=0;i < L;i++) {
			List1.remove(0);
		}
    Element = new Option("----------------------------------------------","-1");
		List1.add(Element,0);
	}
}

function MultiDimensionalArray(iRows,iCols)
{
	var i;
	var j;
	var a = new Array(iRows);
	for (i=0; i < iRows; i++)
	{
	   a[i] = new Array(iCols);
	   for (j=0; j < iCols; j++)
	   {
	       a[i][j] = "";
	   }
	}
	   return(a);
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
function mySplit(whole, link)
{
	if (whole.split) {
		return(whole.split(link));
	}

 	  var parts = new makeArray(0);
 	  var d = 0;
 	  var p = 0;
 	  var q = whole.indexOf(link);

    while (q > -1) {
      parts[d++] = whole.substring(p, q);
    	parts.length = d;
    	p = q + 1;
    	q = whole.indexOf(link, p);
		}

    parts[d++] = whole.substring(p);
 	  parts.length = d;
	  return(parts);
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
	function myJoin(parts, link) {
    if (parts.join) {
      return(parts.join(link));
    }

    var whole = "";
    var thisLink = "";

    for(var d = 0; d < parts.length;d++) {
      if (parts[d] != "") {
        whole = whole + thisLink + parts[d];
     		thisLink = link;
			}
		}
	  return(whole);
	}

	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
  function generateExtensionsStr()
  {
	  var extensions = new Array();
	  var extStr = "";

	  var extList = document.form.List2;

	  for(var i=0; i < extList.options.length; i++) {
		  extensions[i] = extList.options[i].text;
		}


		extStr = myJoin(extensions, ";");
		// alert(extStr);
		return (extStr);
	}


// This function is used to check the priority field if numeric or not
// if not numeric then assign 50 as a default value
function CheckPriority(objName)
{
	with (document.form)
	{
		if (!IsNumeric(objName.value) || (objName.value==""))
			objName.value = "50";
	}
	return true;
}

// This function is used to check a certain field if is numeric or not
// if not numeric then assign it a default value
function CheckNumericField(sFieldName, nDefaultValue)
{
	with (document.form)
	{
		if (!IsNumeric(sFieldName.value) || (sFieldName.value==""))
			sFieldName.value = nDefaultValue;
	}
	return true;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Manage Users Page Scripts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


// This function is used to check the required fields
function CheckUserInfo()
{
  with (document.form)
  {
    if (txtName.value == "") {
      alert("You must enter the Name.");
      txtName.focus();
      return false;
    }

    if (txtUsername.value == "") {
      alert("You must enter the username.");
      txtUsername.focus();
      return false;
    }

    if (txtPassword.value == "") {
      alert("You must enter the password.");
      txtPassword.focus();
      return false;
    }

 

    useraction.value = "AddEdit";
    submit();
	}
}

function CheckBalanceInfo()
{
  with (document.form)
  {
    if (txtamount.value == "") {
      alert("You must enter the amount.");
      txtamount.focus();
      return false;
    }

    

    useraction.value = "AddEdit";
    submit();
	}
}


// This function is used to check the required fields
function CheckReselerInfo()
{
  with (document.form)
  {
    if (txtphone.value == "") {
      alert("You must enter the Reseller Phone No.");
      txtphone.focus();
      return false;
    }
	
	if (id.value==""){
		if (txtPassword.value == "") {
		  alert("You must enter the password.");
		  txtPassword.focus();
		  return false;
		}

	}
    useraction.value = "AddEdit";
    submit();
	}
}

// This function is used to check the required fields
function CheckReselerTypeInfo()
{
  with (document.form)
  {
    if (txtresellertype.value == "") {
      alert("You must enter the Reseller Type.");
      txtresellertype.focus();
      return false;
    }

    useraction.value = "AddEdit";
    submit();
	}
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Menu Setup Page Scripts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
function CheckAll(chkName, chkAll, tdName){
  var frm = document.forms[0];
  n = chkAll.checked;
  j = 1;
  for (var i=0;i < frm.elements.length;i++)
  {
    var e=frm.elements[i];
    if (e.type=='checkbox')
    {
    	var str = e.name;
    	if (str.indexOf(chkName) != -1)
    	{
	    	var chk = chkName + j;
	    	var td = tdName + j;
	    	if (e.name==chk)
	    	{
	    		var color = (!n) ? ((j%2==0) ? "#EBEBEB" : "#F2F2F2") : '#E6EBF0';
	      	e.checked = n;
	      	HighlightCell(eval(td), chk, color);
	      	j++;
		  	}
	  	}
		}
	}
}

function HighlightCell(td, chk, color)
{
	with (document.form)
	{
		if (chk.checked) {
			td.style.backgroundColor = "#E6EBF0";
		}else{
			td.style.backgroundColor = color;
		}
	}
}


// This function is used to check the required fields
function CheckReselertypeInfo()
{
  with (document.form)
  {
    if (txtphone.value == "") {
      alert("You must enter the Reseller Phone No.");
      txtphone.focus();
      return false;
    }

    if (txtPassword.value == "") {
      alert("You must enter the password.");
      txtPassword.focus();
      return false;
    }
	 if (resellertype.value == "") {
      alert("You select reseller type");
      txtPassword.focus();
      return false;
    }
    useraction.value = "AddEdit";
    submit();
	}
}






