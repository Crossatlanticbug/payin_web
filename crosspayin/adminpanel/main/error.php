<?php

 	include_once("../Includes/template.inc.php");
 	require_once("../Classes/error.class.php");

	$page_title = $lang['Error_Page_Title'];

	global $logged_in;
  if ($logged_in == 0) {
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

?>

<table border="0" cellpadding="0" cellspacing="0" width="400" align="center" style="height:50%;">
  <tr>
    <td>
      <div class="Alert">
        <?
          if (!defined($_GET['err'])) {
            echo "Don't be smart, this constant is not defined!";
          }else{
            echo constant($_GET['err']);
          }
        ?>
      </div>
    </td>
  </tr>
</table>

<? End_Response(); ?>