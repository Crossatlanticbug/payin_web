<?php
//error_reporting(E_ALL);
//ini_set("display_errors",1);
 	include_once("../Includes/template.inc.php");
 	global $page_title;
	$page_title = "Login Page";
 	//------------------------------------------------------------------------------------
 	global $logged_in;
	if($logged_in) {
	  exit("<script>window.location.href='../admin/account.php';</script>");
	}
?>

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" align="center">
			<table border="0" cellpadding="0" cellspacing="0" width="220">
			  <? if (!empty($_GET['err'])) {?>
				<tr>
          <td width="100%" valign="top">
            <table border="0" cellpadding="3" cellspacing="1" width="100%" style="border:2px outset white;">
              <tr>
              	<td><img border="0" src="../Images/warning.jpg"></td>
                <td width="100%" height="20" class="warning">
            			<?
       						  if (empty($_GET['err'])) {
								echo "Hacking Attempt!";
							}else{
							echo "Please Login to enter Restricted Admin Panel!";
						}
                  ?>
								</td>
							</tr>
            </table>
					</td>
				</tr>
				<tr>
          <td width="100%" height="5"></td>
        </tr>
        <? } ?>
				<tr>
				  <td>
				  	<table border="0" cellpadding="3" cellspacing="1" width="100%" style="border:2px outset white;">
				    	<tr height="30">
            	  <td bgcolor="#C0202F">Username</td>
                <td bgcolor="#C0202F" style="text-align:RIGHT;"><input type="text" name="txtUsername" value="" style="width:145;"></td>
              </tr>
              <tr height="30">
            	  <td bgcolor="#C0202F">Password</td>
                <td bgcolor="#C0202F" style="text-align:RIGHT;"><input type="password" name="txtPassword" value="" onkeydown="return submitEnter(this,event,'login')" style="width:145;"></td>
              </tr>
              <tr height="30" bgcolor="#C0202F">
              	<td colspan="2">
              		<table border="0" cellpadding="0" cellspacing="0" width="100%">
              			<tr>
              				<td height="20" style="text-align:center;"></td>
              				<td height="20" style="text-align:RIGHT;"><input type="button" name="user_login" value="Login" class="button" onclick="javascript:login();"></td>
                		</tr>
                	</table>
              	</td>
              </tr>
          	</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>