<?php

global $language;

// Start using session
require_once("../Classes/session.class.php");
$session = new session(true);
//$session->session(true);

if ((isset($_POST['useraction'])) && ($_POST['useraction']=="login"))
{
	// Open connection to the database
  global $db;
	global $dbLink;

	require_once("../Classes/connection.class.php");
	$db = new connection("connection.cfg.php");
	$dbLink = $db->dbLink;

	// Include the member class
	require_once("../Classes/member.class.php");
	$member = new member();

  // Add slashes to session username before using in a query.
  if (!get_magic_quotes_gpc()) {
		$_POST['txtUsername'] = mysqli_real_escape_string($dbLink, trim($_POST['txtUsername']));
    $_POST['txtPassword'] = mysqli_real_escape_string($dbLink, trim($_POST['txtPassword']));
	}

  $username = $_POST['txtUsername'];
  $password = $_POST['txtPassword'];

  // Remember that the password saved in the database is encrypted
	$userid = $member->loginUser($username, md5($password));
	
	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	* If the user logged in successfully proceed,
	* otherwise Inform him that he entered an invalid username or password.
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
  if ($userid>0) {

    // Retrieve information about this member
    $result = $member->getUserInfo($userid);

    $row = mysqli_fetch_object($result);

    // If the user is enabled allow him to proceed, otherwise no.
    if ($row->Enabled == 1) {
      // Save user information into session variables to be used later
      $_SESSION['UserID'] = $row->UserID;
	    $_SESSION['Username'] = stripslashes($username);
			$_SESSION['Password'] = stripslashes($password);
			$_SESSION['Name'] = $row->Name;
			// $_SESSION['IsAdmin'] = $row->IsAdmin;
			$_SESSION['Timezone'] = $row->Timezone;
			$_SESSION['LastLoginDate'] = $row->LastLoginDate;

			$member->setLastLoginDate($userid);

    	if (isset($_SESSION['BO_PageTo'])) {
    		exit("<script>window.location.href='".$_SESSION['BO_PageTo']."';</script>");
			}else{
				exit("<script>window.location.href='../admin/account.php';</script>");
			}

			

		}else{
			// Don't allow the user to login because his account is disabled
      exit("<script>window.location.href='../main/index.php?err=ERR_USER_DISABLED';</script>");
		} // if ($row->Enabled == 1)..

	}else{
		// When the user entered an invalid username or password
		exit("<script>window.location.href='../main/index.php?err=ERR_INVALID_USER';</script>");
 	} // if ($userid>0)..

} // if ($_POST['useraction']=="login")..

?>