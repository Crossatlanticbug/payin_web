<?php

global $logged_in;
global $dbLink;

// Start using session
require_once("../Classes/session.class.php");
$session = new session(true);
//$session->session(true);

if (!isset($_SESSION['Username']) || !isset($_SESSION['Password'])) {
	$logged_in = 0;
}else{

  require_once("../Classes/member.class.php");
	$member = new member();

  // Add slashes to session username before using in a query.
  if (!get_magic_quotes_gpc()) {
		$username = mysqli_real_escape_string($dbLink, $_SESSION['Username']);
    $password = mysqli_real_escape_string($dbLink, $_SESSION['Password']);
	}else{
    $username = $_SESSION['Username'];
    $password = $_SESSION['Password'];
	}

  // Remember that the password saved in the database is encrypted
	$userid = $member->loginUser($username, md5($password));

  if ($userid>0) {
    $logged_in = 1;
	}else{
    $logged_in = 0;
    unset($_SESSION['Username']);
    unset($_SESSION['Password']);
	}
}

?>