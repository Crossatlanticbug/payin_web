<?php

// Start using session
include_once("../Includes/template.inc.php");
//require_once("../Classes/session.class.php");
$session = new session();
//$session->session(true);

// Reset session array
$_SESSION = array();

// Kill all session variables and destroy the session itself.
session_destroy();

// Redirect the user to the main page.
echo "<script>window.location.href='../main/index.php';</script>";

?>