<?


// Get value of a given object variable
function get_value($obj, $name)
{
	$expr = "\$value=&\$obj->$name;";
	eval($expr);
  return $value;
}

// Add slashes to object variables
function addslashes_object_vars($obj)
{
	global $dbLink;

	$result = array();
  $object_vars = get_object_vars($obj);
  foreach($object_vars as $name => $value) {
	  $result[$name]=get_value($obj, $name);
    if (!get_magic_quotes_gpc()) {
    	$result[$name]=mysqli_real_escape_string($dbLink, $result[$name]);
    }
  }
}

// Change date format from dd/mm/yyyy to yyyy-mm-dd to match the date in the MySQL database
function formatDate($mydate)
{
  if ($mydate!='') {
	$date_array = explode('/', $mydate);
	$day = $date_array[0];
	$month = $date_array[1];
	$year = $date_array[2];
	if ($year<1970) $year=1970;
	// format of mktime(hour,minute,second,month,day,year)
	// $mydate = date("Y-m-d",mktime(0,0,0,$month,$day,$year));
	}
  return $mydate;
}

/**
* Writes localized date
* @param   string   the current timestamp
* @return  string   the formatted date
* @access  public
**/
function localizeDate($timestamp = -1, $format = '')
{
  global $lang;

  if ($format == '') {
      $format = $lang['DATE_FORMAT'];
  }

  if ($timestamp == -1) {
      $timestamp = time();
  }

  $date = preg_replace('@%[lL]@', $lang['DAY_OF_WEEK'][(int)strftime('%w', $timestamp)], $format);
  $date = preg_replace('@%[fF]@', $lang['MONTH'][(int)strftime('%m', $timestamp)-1], $date);

  return strftime($date, $timestamp);
}

// This function is used for the pagination
function Pagination($screen, $perpage, $pages, $link)
{
	global $lang;

	$prev = $lang['Previous'];
	$next = $lang['Next'];

	$arrPath = parse_url($link, PHP_URL_PATH);

	// print_r($arrPath);

	$sOp = "?"; // (count($arrPath['query']) > 0) ? "&" : "?";

	// Previous Link
	if ($screen > 0)
	{
		// First Link
		$url = $link.$sOp."screen=0#result";
	  echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink' title='Go to the first page'><span dir='rtl'>&#187;</span></a>  ";
		$url = $link.$sOp."screen=".($screen - 1)."#result";
   	echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink'> ".$prev."</a>  ";
	}
	// Pages links
	$first = $screen - ($screen % $perpage);
	$last = min($pages, $first + $perpage);
  for ($i=$first; $i<$last; $i++)
  {
    if ($i<9) $strZero = "0"; else $strZero = "";
    if ($i != $screen) {
    	$url = $link.$sOp."screen=".$i."#result";
    	echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink'> ".$strZero.($i+1)."</a> ";
		}else{
      echo "<span class='SelectedPage'>".$strZero.($i+1)."</span>";
		}
	}
	// Next Link
  if ($screen < $pages-1)
  {
		$url = $link.$sOp."screen=".($screen + 1)."";
    echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink'>".$next."</a> ";
    // Last Link
  	$url = $link.$sOp."screen=".($pages-1)."#result";
  	echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink' title='Go to the last page'><span dir='ltr'>&#187;</span></a>  ";
  }
}

// This function is used to get n characters of a certain string
function GetPartOfString($str, $n)
{
	if (strlen($str)>$n) {
		return substr(substr(($str), 0, $n), 0, strrpos(substr(($str), 0, $n), " "))." ...";
	}else{
		return $str;
	}
}

// This function is used to create a thumbnail of a certain picture
function CreateThumb($ImageName,$ThumbName,$new_w,$new_h)
{
	$path_parts = pathinfo($ImageName);
	$extension = strtolower($path_parts["extension"]);

	if (preg_match("/jpg|jpeg/",$extension)){$src_img=imagecreatefromjpeg($ImageName);}
	if (preg_match("/png/",$extension)){$src_img=imagecreatefrompng($ImageName);}
	if (preg_match("/gif/",$extension)){$src_img=imagecreatefromgif($ImageName);}
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);
	/*
	if ($old_x > $old_y)
	{
		$thumb_w=$new_w;
		$thumb_h=$old_y*($new_h/$old_x);
	}
	if ($old_x < $old_y)
	{
		$thumb_w=$old_x*($new_w/$old_y);
		$thumb_h=$new_h;
	}
	*/
	if ($old_x != $old_y)
	{
		$thumb_w=$new_w;
		$thumb_h=$old_y*($new_h/$old_x);
	}
	if ($old_x == $old_y)
	{
		$thumb_w=$new_w;
		$thumb_h=$new_h;
	}
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
	if (preg_match("/png/",$system[1]))
	{
		imagepng($dst_img,$ThumbName);
	}
	else if (preg_match("/gif/",$system[1]))
	{
		imagegif($dst_img,$ThumbName);
	}
	else
	{
		imagejpeg($dst_img,$ThumbName);
	}
	imagedestroy($dst_img);
	imagedestroy($src_img);
}

// This function is used to get the image width & height
function GetImageDetails($ImageName)
{
	$path_parts = pathinfo($ImageName);
	$extension = strtolower($path_parts["extension"]);

	if (preg_match("/jpg|jpeg/",$extension)){$src_img=imagecreatefromjpeg($ImageName);}
	if (preg_match("/png/",$extension)){$src_img=imagecreatefrompng($ImageName);}
	if (preg_match("/gif/",$extension)){$src_img=imagecreatefromgif($ImageName);}

	$ImageDetails = array();
	$ImageDetails[0] = imageSX($src_img);  // Image Width
	$ImageDetails[1] = imageSY($src_img);  // Image Height
	$ImageDetails[2] = $src_img;  				 // Image Source

	return $ImageDetails;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*              User Management (Roles & Permissions) Functions
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
// Gets splits the permissions mask into array
function GetPermissionsArray($PermissionsMask = '')
{
	$PermissionsArray = array();

	for($i=0; $i<strlen($PermissionsMask); $i++)
	{
		$PermissionsArray[$i] = substr($PermissionsMask,$i,1);
	}

	return $PermissionsArray;
}

/*
This function is used to do the following:
	- Register a session variable of a certain variable if not registered yet
	- Set this value of this session variable either to:
		* The value from the the post if exists
		* The default value
	- Returns the session value
*/
function LoadSessionValue($ObjectName, $VariableName, $SessionPrefix, $DefaultValue)
{
	$SessionVariable = $SessionPrefix . "_" . $VariableName;

	if (!isset($_POST[$VariableName]) && !isset($_SESSION[$SessionVariable]))
	{
		$_SESSION[$SessionVariable] = $DefaultValue;
	}
	else if (isset($_POST[$ObjectName]))
	{
		$_SESSION[$SessionVariable] = trim($_POST[$ObjectName]);
	}

	return $_SESSION[$SessionVariable];
}

/**************************selfURL and rootURL***************/
function selfURL()
{
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
	return $protocol."://".$_SERVER['HTTP_HOST'].$port.$_SERVER['REQUEST_URI'];
}
function strleft($s1, $s2)
{
	return substr($s1, 0, strpos($s1, $s2));
}
function rootURL()
{
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
	return $protocol."://".$_SERVER['SERVER_ADDR'].$port;
}

// Check if the page is accessed through http protocol then redirect it to https
function insureHTTPSAccess()
{
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
	if (empty($_SERVER["HTTPS"])) {
		exit("<script>window.location.href='https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."';</script>");
	}
}

function getRandomColor()
{
	mt_srand((double)microtime()*1000000);
  $c = '';
  while(strlen($c)<6){
  	$c .= sprintf("%02X", mt_rand(0, 255));
  }
  return $c;
}

// This function is used to retrieve the list of countries
function getCountryList()
{
	global $lang;
	global $dbLink;

	$query =
					"
						SELECT c_ID ISOCode, c_Name_".$lang['LANG']." Country, c_CallingCode CallingCode
						FROM country
						Where c_Enabled = 1
						ORDER BY Binary c_Name_".$lang['LANG'];

	$result = mysqli_query($dbLink, $query);
	return $result;
}

/*
// Other timzone related functions, to be used later
function getTimezoneOptions()
{
    $list = DateTimeZone::listAbbreviations();
    $idents = DateTimeZone::listIdentifiers();

    $data = $offset = $added = array();
    foreach ($list as $abbr => $info) {
        foreach ($info as $zone) {
            if ( ! empty($zone['timezone_id'])
                AND
                ! in_array($zone['timezone_id'], $added)
                AND
                  in_array($zone['timezone_id'], $idents)) {
                $z = new DateTimeZone($zone['timezone_id']);
                $c = new DateTime(null, $z);
                $zone['time'] = $c->format('H:i a');
                $data[] = $zone;
                $offset[] = $z->getOffset($c);
                $added[] = $zone['timezone_id'];
            }
        }
    }

    print_r($abbr);
    array_multisort($offset, SORT_ASC, $data);
    $options = array();
    foreach ($data as $key => $row) {
        $options[$row['timezone_id']] = "(".formatOffset($row['offset']).") "; // - " . $row['time'];
    }
	return $options;
}

function formatOffset($offset)
{
	$hours = $offset / 3600;
  $remainder = $offset % 3600;
  $sign = $hours > 0 ? '+' : '-';
  $hour = (int) abs($hours);
  $minutes = (int) abs($remainder / 60);

  if ($hour == 0 AND $minutes == 0) {
  	$sign = '';
  }
  return $sign . str_pad($hour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes,2, '0');
}

function getTimezoneDropdownList($selectedzone)
{
  echo '<select name="cmbUserTimeZone">';
  function timezonechoice($selectedzone)
  {
  	$options = getTimezoneOptions();
    $all = timezone_identifiers_list();

    $i = 0;
    foreach($all AS $zone) {
      $zone = explode('/',$zone);
      $zonen[$i]['continent'] = isset($zone[0]) ? $zone[0] : '';
      $zonen[$i]['city'] = isset($zone[1]) ? $zone[1] : '';
      $zonen[$i]['subcity'] = isset($zone[2]) ? $zone[2] : '';
      $i++;
    }

    asort($zonen);
    $structure = '';
    foreach($zonen AS $zone) {
      extract($zone);
      if($continent == 'Africa' || $continent == 'America' || $continent == 'Antarctica' || $continent == 'Arctic' || $continent == 'Asia' || $continent == 'Atlantic' || $continent == 'Australia' || $continent == 'Europe' || $continent == 'Indian' || $continent == 'Pacific') {
        if(!isset($selectcontinent)) {
          $structure .= '<optgroup label="'.$continent.'">'; // continent
        } elseif($selectcontinent != $continent) {
          $structure .= '</optgroup><optgroup label="'.$continent.'">'; // continent
        }

        if(isset($city) != ''){
          if (!empty($subcity) != ''){
            $city = $city . '/'. $subcity;
          }
          $structure .= "<option ".((($continent.'/'.$city)==$selectedzone)?'selected="selected "':'')." value=\"".($continent.'/'.$city)."\">".$options[($continent.'/'.$city)].str_replace('_',' ',$city)."</option>"; //Timezone
        } else {
          if (!empty($subcity) != ''){
            $city = $city . '/'. $subcity;
          }
          $structure .= "<option ".(($continent==$selectedzone)?'selected="selected "':'')." value=\"".$continent."\">".$continent."</option>"; //Timezone
        }

        $selectcontinent = $continent;
      }
    }
    $structure .= '</optgroup>';
    return $structure;
  }
  echo timezonechoice($selectedzone);
  echo '</select>';
}

function getTimezoneDDL($selectedzone)
{
	$utc = new DateTimeZone('UTC');
	$dt = new DateTime('now', $utc);

	echo '<select name="cmbUserTimeZone">';
	foreach(DateTimeZone::listIdentifiers() as $tz)
	{
    $current_tz = new DateTimeZone($tz);
    $offset =  $current_tz->getOffset($dt);
    $transition =  $current_tz->getTransitions($dt->getTimestamp(), $dt->getTimestamp());
    $abbr = $transition[0]['abbr'];

    echo '<option value="' .$tz. '"'.(($key==$selectedzone)?' selected="selected"':'').'>' .$tz. ' [' .$abbr. ' '. formatOffset($offset). ']</option>';
	}
	echo '</select>';
}
*/

function getTimezoneDDL($selectedTimezone)
{
	// Create an array listing the preferred time zones
	$preferredTimezonesList = array('Pacific/Honolulu' => '(GMT'.getTimezoneOffset("Pacific/Honolulu").') Hawaii',
		'America/Anchorage' => '(GMT'.getTimezoneOffset("America/Anchorage").') Alaska',
		'America/Los_Angeles' => '(GMT'.getTimezoneOffset("America/Los_Angeles").') Pacific Standard Time (PST)',
		'America/Denver' => '(GMT'.getTimezoneOffset("America/Denver").') Mountain Standard Time (MST)',
		'America/Chicago' => '(GMT'.getTimezoneOffset("America/Chicago").') Central Standard Time (CST)',
		'America/New_York' => '(GMT'.getTimezoneOffset("America/New_York").') Eastern Standard Time (EST)',
		'Atlantic/Reykjavik' => '(GMT'.getTimezoneOffset("Atlantic/Reykjavik").') Reykjavik',
		'Europe/London' => '(GMT'.getTimezoneOffset("Europe/London").') London',
		'Asia/Beirut' => '(GMT'.getTimezoneOffset("Asia/Beirut").') Beirut',
		'Asia/Riyadh' => '(GMT'.getTimezoneOffset("Asia/Riyadh").') Riyadh',
		'Asia/Dubai' => '(GMT'.getTimezoneOffset("Asia/Dubai").') Dubai');

	// Create an array listing all the time zones
	$allTimezonesList = array(
		'Pacific/Midway' => '(GMT'.getTimezoneOffset("Pacific/Midway").') Midway Island',
		'Pacific/Samoa' => '(GMT'.getTimezoneOffset("Pacific/Samoa").') Samoa',
		'Pacific/Honolulu' => '(GMT'.getTimezoneOffset("Pacific/Honolulu").') Hawaii',
		'America/Anchorage' => '(GMT'.getTimezoneOffset("America/Anchorage").') Alaska',
		'America/Los_Angeles' => '(GMT'.getTimezoneOffset("America/Los_Angeles").') Pacific Standard Time (PST)',
		'America/Tijuana' => '(GMT'.getTimezoneOffset("America/Tijuana").') Tijuana, Baja California',
		'America/Denver' => '(GMT'.getTimezoneOffset("America/Denver").') Mountain Standard Time (MST)',
		'America/Chihuahua' => '(GMT'.getTimezoneOffset("America/Chihuahua").') Chihuahua',
		'America/Mazatlan' => '(GMT'.getTimezoneOffset("America/Mazatlan").') Mazatlan',
		'America/Phoenix' => '(GMT'.getTimezoneOffset("America/Phoenix").') Arizona',
		'America/Regina' => '(GMT'.getTimezoneOffset("America/Regina").') Saskatchewan',
		'America/Tegucigalpa' => '(GMT'.getTimezoneOffset("America/Tegucigalpa").') Central America',
		'America/Chicago' => '(GMT'.getTimezoneOffset("America/Chicago").') Central Standard Time (CST)',
		'America/Mexico_City' => '(GMT'.getTimezoneOffset("America/Mexico_City").') Mexico City',
		'America/Monterrey' => '(GMT'.getTimezoneOffset("America/Monterrey").') Monterrey',
		'America/New_York' => '(GMT'.getTimezoneOffset("America/New_York").') Eastern Standard Time (EST)',
		'America/Bogota' => '(GMT'.getTimezoneOffset("America/Bogota").') Bogota',
		'America/Lima' => '(GMT'.getTimezoneOffset("America/Lima").') Lima',
		'America/Rio_Branco' => '(GMT'.getTimezoneOffset("America/Rio_Branco").') Rio Branco',
		'America/Indiana/Indianapolis' => '(GMT'.getTimezoneOffset("America/Indiana/Indianapolis").') Indiana (East)',
		'America/Caracas' => '(GMT'.getTimezoneOffset("America/Caracas").') Caracas',
		'America/Halifax' => '(GMT'.getTimezoneOffset("America/Halifax").') Atlantic Time (Canada)',
		'America/Manaus' => '(GMT'.getTimezoneOffset("America/Manaus").') Manaus',
		'America/Santiago' => '(GMT'.getTimezoneOffset("America/Santiago").') Santiago',
		'America/La_Paz' => '(GMT'.getTimezoneOffset("America/La_Paz").') La Paz',
		'America/St_Johns' => '(GMT'.getTimezoneOffset("America/St_Johns").') Newfoundland',
		'America/Argentina/Buenos_Aires' => '(GMT'.getTimezoneOffset("America/Argentina/Buenos_Aires").') Georgetown',
		'America/Sao_Paulo' => '(GMT'.getTimezoneOffset("America/Sao_Paulo").') Brasilia',
		'America/Godthab' => '(GMT'.getTimezoneOffset("America/Godthab").') Greenland',
		'America/Montevideo' => '(GMT'.getTimezoneOffset("America/Montevideo").') Montevideo',
		'Atlantic/South_Georgia' => '(GMT'.getTimezoneOffset("Atlantic/South_Georgia").') Mid-Atlantic',
		'Atlantic/Azores' => '(GMT'.getTimezoneOffset("Atlantic/Azores").') Azores',
		'Atlantic/Cape_Verde' => '(GMT'.getTimezoneOffset("Atlantic/Cape_Verde").') Cape Verde Is.',
		'Europe/Dublin' => '(GMT'.getTimezoneOffset("Europe/Dublin").') Dublin',
		'Europe/Lisbon' => '(GMT'.getTimezoneOffset("Europe/Lisbon").') Lisbon',
		'Europe/London' => '(GMT'.getTimezoneOffset("Europe/London").') London',
		'Africa/Monrovia' => '(GMT'.getTimezoneOffset("Africa/Monrovia").') Monrovia',
		'Atlantic/Reykjavik' => '(GMT'.getTimezoneOffset("Atlantic/Reykjavik").') Reykjavik',
		'Africa/Casablanca' => '(GMT'.getTimezoneOffset("Africa/Casablanca").') Casablanca',
		'Europe/Belgrade' => '(GMT'.getTimezoneOffset("Europe/Belgrade").') Belgrade',
		'Europe/Bratislava' => '(GMT'.getTimezoneOffset("Europe/Bratislava").') Bratislava',
		'Europe/Budapest' => '(GMT'.getTimezoneOffset("Europe/Budapest").') Budapest',
		'Europe/Ljubljana' => '(GMT'.getTimezoneOffset("Europe/Ljubljana").') Ljubljana',
		'Europe/Prague' => '(GMT'.getTimezoneOffset("Europe/Prague").') Prague',
		'Europe/Sarajevo' => '(GMT'.getTimezoneOffset("Europe/Sarajevo").') Sarajevo',
		'Europe/Skopje' => '(GMT'.getTimezoneOffset("Europe/Skopje").') Skopje',
		'Europe/Warsaw' => '(GMT'.getTimezoneOffset("Europe/Warsaw").') Warsaw',
		'Europe/Zagreb' => '(GMT'.getTimezoneOffset("Europe/Zagreb").') Zagreb',
		'Europe/Brussels' => '(GMT'.getTimezoneOffset("Europe/Brussels").') Brussels',
		'Europe/Copenhagen' => '(GMT'.getTimezoneOffset("Europe/Copenhagen").') Copenhagen',
		'Europe/Madrid' => '(GMT'.getTimezoneOffset("Europe/Madrid").') Madrid',
		'Europe/Paris' => '(GMT'.getTimezoneOffset("Europe/Paris").') Paris',
		'Africa/Algiers' => '(GMT'.getTimezoneOffset("Africa/Algiers").') West Central Africa',
		'Europe/Amsterdam' => '(GMT'.getTimezoneOffset("Europe/Amsterdam").') Amsterdam',
		'Europe/Berlin' => '(GMT'.getTimezoneOffset("Europe/Berlin").') Berlin',
		'Europe/Rome' => '(GMT'.getTimezoneOffset("Europe/Rome").') Rome',
		'Europe/Stockholm' => '(GMT'.getTimezoneOffset("Europe/Stockholm").') Stockholm',
		'Europe/Vienna' => '(GMT'.getTimezoneOffset("Europe/Vienna").') Vienna',
		'Europe/Minsk' => '(GMT'.getTimezoneOffset("Europe/Minsk").') Minsk',
		'Africa/Cairo' => '(GMT'.getTimezoneOffset("Africa/Cairo").') Cairo',
		'Europe/Helsinki' => '(GMT'.getTimezoneOffset("Europe/Helsinki").') Helsinki',
		'Europe/Riga' => '(GMT'.getTimezoneOffset("Europe/Riga").') Riga',
		'Europe/Sofia' => '(GMT'.getTimezoneOffset("Europe/Sofia").') Sofia',
		'Europe/Tallinn' => '(GMT'.getTimezoneOffset("Europe/Tallinn").') Tallinn',
		'Europe/Vilnius' => '(GMT'.getTimezoneOffset("Europe/Vilnius").') Vilnius',
		'Europe/Athens' => '(GMT'.getTimezoneOffset("Europe/Athens").') Athens',
		'Europe/Bucharest' => '(GMT'.getTimezoneOffset("Europe/Bucharest").') Bucharest',
		'Europe/Istanbul' => '(GMT'.getTimezoneOffset("Europe/Istanbul").') Istanbul',
		'Asia/Jerusalem' => '(GMT'.getTimezoneOffset("Asia/Jerusalem").') Jerusalem',
		'Asia/Amman' => '(GMT'.getTimezoneOffset("Asia/Amman").') Amman',
		'Asia/Beirut' => '(GMT'.getTimezoneOffset("Asia/Beirut").') Beirut',
		'Africa/Windhoek' => '(GMT'.getTimezoneOffset("Africa/Windhoek").') Windhoek',
		'Africa/Harare' => '(GMT'.getTimezoneOffset("Africa/Harare").') Harare',
		'Asia/Kuwait' => '(GMT'.getTimezoneOffset("Asia/Kuwait").') Kuwait',
		'Asia/Riyadh' => '(GMT'.getTimezoneOffset("Asia/Riyadh").') Riyadh',
		'Asia/Baghdad' => '(GMT'.getTimezoneOffset("Asia/Baghdad").') Baghdad',
		'Africa/Nairobi' => '(GMT'.getTimezoneOffset("Africa/Nairobi").') Nairobi',
		'Asia/Tbilisi' => '(GMT'.getTimezoneOffset("Asia/Tbilisi").') Tbilisi',
		'Europe/Moscow' => '(GMT'.getTimezoneOffset("Europe/Moscow").') Moscow',
		'Europe/Volgograd' => '(GMT'.getTimezoneOffset("Europe/Volgograd").') Volgograd',
		'Asia/Tehran' => '(GMT'.getTimezoneOffset("Asia/Tehran").') Tehran',
		'Asia/Dubai' => '(GMT'.getTimezoneOffset("Asia/Dubai").') Dubai',
		'Asia/Muscat' => '(GMT'.getTimezoneOffset("Asia/Muscat").') Muscat',
		'Asia/Baku' => '(GMT'.getTimezoneOffset("Asia/Baku").') Baku',
		'Asia/Yerevan' => '(GMT'.getTimezoneOffset("Asia/Yerevan").') Yerevan',
		'Asia/Yekaterinburg' => '(GMT'.getTimezoneOffset("Asia/Yekaterinburg").') Ekaterinburg',
		'Asia/Karachi' => '(GMT'.getTimezoneOffset("Asia/Karachi").') Karachi',
		'Asia/Tashkent' => '(GMT'.getTimezoneOffset("Asia/Tashkent").') Tashkent',
		'Asia/Kolkata' => '(GMT'.getTimezoneOffset("Asia/Kolkata").') Calcutta',
		'Asia/Colombo' => '(GMT'.getTimezoneOffset("Asia/Colombo").') Sri Jayawardenepura',
		'Asia/Katmandu' => '(GMT'.getTimezoneOffset("Asia/Katmandu").') Kathmandu',
		'Asia/Dhaka' => '(GMT'.getTimezoneOffset("Asia/Dhaka").') Dhaka',
		'Asia/Almaty' => '(GMT'.getTimezoneOffset("Asia/Almaty").') Almaty',
		'Asia/Novosibirsk' => '(GMT'.getTimezoneOffset("Asia/Novosibirsk").') Novosibirsk',
		'Asia/Rangoon' => '(GMT'.getTimezoneOffset("Asia/Rangoon").') Yangon (Rangoon)',
		'Asia/Krasnoyarsk' => '(GMT'.getTimezoneOffset("Asia/Krasnoyarsk").') Krasnoyarsk',
		'Asia/Bangkok' => '(GMT'.getTimezoneOffset("Asia/Bangkok").') Bangkok',
		'Asia/Jakarta' => '(GMT'.getTimezoneOffset("Asia/Jakarta").') Jakarta',
		'Asia/Brunei' => '(GMT'.getTimezoneOffset("Asia/Brunei").') Beijing',
		'Asia/Chongqing' => '(GMT'.getTimezoneOffset("Asia/Chongqing").') Chongqing',
		'Asia/Hong_Kong' => '(GMT'.getTimezoneOffset("Asia/Hong_Kong").') Hong Kong',
		'Asia/Urumqi' => '(GMT'.getTimezoneOffset("Asia/Urumqi").') Urumqi',
		'Asia/Irkutsk' => '(GMT'.getTimezoneOffset("Asia/Irkutsk").') Irkutsk',
		'Asia/Ulaanbaatar' => '(GMT'.getTimezoneOffset("Asia/Ulaanbaatar").') Ulaan Bataar',
		'Asia/Kuala_Lumpur' => '(GMT'.getTimezoneOffset("Asia/Kuala_Lumpur").') Kuala Lumpur',
		'Asia/Singapore' => '(GMT'.getTimezoneOffset("Asia/Singapore").') Singapore',
		'Asia/Taipei' => '(GMT'.getTimezoneOffset("Asia/Taipei").') Taipei',
		'Australia/Perth' => '(GMT'.getTimezoneOffset("Australia/Perth").') Perth',
		'Asia/Seoul' => '(GMT'.getTimezoneOffset("Asia/Seoul").') Seoul',
		'Asia/Tokyo' => '(GMT'.getTimezoneOffset("Asia/Tokyo").') Tokyo',
		'Asia/Yakutsk' => '(GMT'.getTimezoneOffset("Asia/Yakutsk").') Yakutsk',
		'Australia/Darwin' => '(GMT'.getTimezoneOffset("Australia/Darwin").') Darwin',
		'Australia/Adelaide' => '(GMT'.getTimezoneOffset("Australia/Adelaide").') Adelaide',
		'Australia/Canberra' => '(GMT'.getTimezoneOffset("Australia/Canberra").') Canberra',
		'Australia/Melbourne' => '(GMT'.getTimezoneOffset("Australia/Melbourne").') Melbourne',
		'Australia/Sydney' => '(GMT'.getTimezoneOffset("Australia/Brisbane").') Sydney',
		'Australia/Brisbane' => '(GMT'.getTimezoneOffset("Australia/Darwin").') Brisbane',
		'Australia/Hobart' => '(GMT'.getTimezoneOffset("Australia/Hobart").') Hobart',
		'Asia/Vladivostok' => '(GMT'.getTimezoneOffset("Asia/Vladivostok").') Vladivostok',
		'Pacific/Guam' => '(GMT'.getTimezoneOffset("Pacific/Guam").') Guam',
		'Pacific/Port_Moresby' => '(GMT'.getTimezoneOffset("Pacific/Port_Moresby").') Port Moresby',
		'Asia/Magadan' => '(GMT'.getTimezoneOffset("Asia/Magadan").') Magadan',
		'Pacific/Fiji' => '(GMT'.getTimezoneOffset("Pacific/Fiji").') Fiji',
		'Asia/Kamchatka' => '(GMT'.getTimezoneOffset("Asia/Kamchatka").') Kamchatka',
		'Pacific/Auckland' => '(GMT'.getTimezoneOffset("Pacific/Auckland").') Auckland',
		'Pacific/Tongatapu' => '(GMT'.getTimezoneOffset("Pacific/Tongatapu").') Nukualofa');

	echo '<select name="cmbTimezone" onchange="document.form.submit();">';
	foreach($preferredTimezonesList as $key => $value)
	{
		echo '<option title="' . $key . '" value="' . $key . '"' . (($selectedTimezone == $key) ? " selected" : "") . '>' . $value . '</option>' . "\n";
	}
	echo '<option disabled="disabled">----------------------------------------------</option>';
	foreach($allTimezonesList as $key => $value)
	{
		echo '<option title="' . $key . '" value="' . $key . '"' . ((($selectedTimezone == $key) && !array_key_exists($selectedTimezone, $preferredTimezonesList)) ? " selected" : "") . '>' . $value . '</option>' . "\n";
	}
	echo '</select>';
}

/*
 * This function is used to convert a certain date from the default MySQL server's timezone (GMT) to the user's timezone
 * It should be used in every page where there is a date to display.
 * NOTE: This function should not be used anymore in case we are retrieving dates from DB, because we are changing the timezone of MySQL server as per the user's timezone
 */
function getDateInUserTimezone($dateToConvert)
{
	global $userDateTimezone;

	$date = new DateTime($dateToConvert);
	$date->setTimeZone($userDateTimezone);
	return $date->format('Y-m-d H:i:s');
}

function getTimezoneOffset($DateTimeZone)
{ 

	
	if (!is_object($DateTimeZone))
	{
		$DateTimeZone = new DateTimeZone($DateTimeZone);
	}

	$date = new DateTime();
	$date->setTimeZone($DateTimeZone);
	$offset = $date->getOffset();
	$offsetHours   = (int)(abs($offset) / 3600);
	$offsetMinutes = (int) abs(round((abs($offset) - $offsetHours * 3600) / 60));
	$offsetString  = ($offset < 0 ? '-' : '+');
	$offsetString .= (strlen($offsetHours) < 2 ? '0' : '').$offsetHours;
	$offsetString .= ':';
	$offsetString .= (strlen($offsetMinutes) < 2 ? '0' : '').$offsetMinutes;

	return $offsetString;
	
}

function updateMySQLTimezone()
{
	global $dbLink;
	global $userDateTimezone;

	$offsetString = getTimezoneOffset($userDateTimezone);
	mysqli_query($dbLink, "SET time_zone='".$offsetString."'");
}


function ip_visitor_country($getip)
{
	$countryarray= array();
	if ($getip){
		$client  = $getip;
	} else {
		 $client  = @$_SERVER['HTTP_CLIENT_IP'];
	}	
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    $country  = "Unknown";

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $ip_data_in = curl_exec($ch); // string
    curl_close($ch);

    $ip_data = json_decode($ip_data_in,true);
    $ip_data = str_replace('&quot;', '"', $ip_data); // for PHP 5.2 see stackoverflow.com/questions/3110487/

    if($ip_data && $ip_data['geoplugin_countryName'] != null) {
        $countryarray['country'] = $ip_data['geoplugin_countryName'];
		$countryarray['country_code'] = $ip_data['geoplugin_countryCode'];
    }
	
    return $countryarray;
}
?>