<?php
error_reporting(E_ALL);
ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Reseller List";;

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	$SessionPrefix = 'Reseller Details';

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
		
	$Criteria['Email'] = (!empty($_GET['Email'])) ? $_GET['Email'] : LoadSessionValue('txtEmailSearch', 'Email', $SessionPrefix, '');
	$Criteria['ID'] 	 = LoadSessionValue('ID', 'ID', $SessionPrefix, '');
	
	$Criteria['Name']   = (!empty($_GET['Name'])) ? $_GET['Name'] : LoadSessionValue('txtName', 'Name', $SessionPrefix, '');
	$Criteria['Balance']   = (!empty($_GET['Balance'])) ? $_GET['Balance'] : LoadSessionValue('txtBalance', 'Balance', $SessionPrefix, '');
	$Criteria['City']   = (!empty($_GET['City'])) ? $_GET['City'] : LoadSessionValue('txtCity', 'City', $SessionPrefix, '');
	$Criteria['UserIP']   = (!empty($_GET['UserIP'])) ? $_GET['UserIP'] : LoadSessionValue('txtUserIP', 'UserIP', $SessionPrefix, '');
	// Search Criteria
	$Criteria['Phone'] = (!empty($_GET['Phone'])) ? $_GET['Phone'] : LoadSessionValue('txtPhone', 'Phone', $SessionPrefix, '');
	
	$SortFieldDefault = "ID"; $SortTypeDefault = "DESC";

	// Order By
	$ItemsPerPage = LoadSessionValue('cmbItemsPerPage', 'ItemsPerPage', $SessionPrefix, "40");
	$SortField = (isset($_POST['cmbSortField']) ? $_POST['cmbSortField'] : $SortFieldDefault);
	$SortType	 = (isset($_POST['rdSortType']) ? $_POST['rdSortType'] : $SortTypeDefault);

	

	// Date timestamp used for the Excel File Exporting Name
	$Timestamp = date('YmdHis');
?>





<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="export" value="1">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Reseller List</td>
				</tr>
				 <td height="30">
				  	<a href="add_reseller.php" class="link">Add New Reseller</a>
				  </td>
				<tr>
					<td valign="top" style="padding-top:10px;">
						<table border="0" cellpadding="3" cellspacing="0" width="380" class="table">
				  		<tr>
							  <td class="boxHeader">Filter Result</td>
							</tr>
							<tr>
								<td>
									<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="Silver">
										<tr id="IDRow">
											<td class="boxCaption" width="90">Name</td>
											<td class="boxContents">
												<input type="text" name="txtName" value="<?=$Criteria['Name'];?>" class="textbox"  onkeyup="NumbersOnly(logID);">
											</td>
										</tr>
										
										<tr id="CustomerRow">
											<td class="boxCaption">Email</td>
											<td class="boxContents">
												<input type="text" name="txtEmailSearch" value="<?=$Criteria['Email']?>" class="textbox">
											</td>
										</tr>
										
										<tr id="IDRow">
											<td class="boxCaption" width="90">Phone</td>
											<td class="boxContents">
												<input type="text" name="txtPhone" value="<?=$Criteria['Phone'];?>" class="textbox" onkeyup="NumbersOnly(Phone);">
											</td>
										</tr>
										
										<tr>
											<td class="boxCaption">IP</td>
											<td class="boxContents">
												<input type="text" name="txtUserIP" value="<?=$Criteria['UserIP'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr>
											<td class="boxCaption">SortBy</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td width="149">
															<select name='cmbSortField' size='1' style="width:140px;">
																<option value="ID" <? if ($SortField=="id") echo "SELECTED";?> id="IDOption">ID</option>
																<option value="name" <? if($SortField=="name") echo "SELECTED";?> id="UsernameOption">Name</option>
																<option value="email" <? if($SortField=="email") echo "SELECTED";?> id="UsernameOption">Email</option>
															</select>
														</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="ASC" <? if ($SortType=='ASC') echo "checked"; ?>></td>
														<td width="35">ASC</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="DESC" <? if ($SortType=='DESC') echo "checked"; ?>></td>
														<td width="35">DESC</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr class="boxContents">
											<td colspan="2" style="text-align:<?=$lang['RIGHT']?>;" >
												<table border="0" cellpadding="3" cellspacing="0">
													<tr>
														
														<td>
															<input type="button" name="btnReset" value="Reset" class="button" onclick="ResetcustomertxtSearchForm();">
														</td>
														<td><input type="submit" name="btnSearch" value="Search" class="button"></td>
													</tr>
												</table>
											</td>
										</tr>							
										
										
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? if ( (isset($_POST['btnSearch'])) || (isset($_POST['cmbTimezone'])) || (!empty($Criteria['Name']))|| (!empty($Criteria['Email'])) || (!empty($Criteria['ID']))|| (!empty($Criteria['Balance'])) || (!empty($Criteria['Phone']))|| (!empty($Criteria['City']))|| (!empty($Criteria['UserIP'])))  {  ?>
				<tr>
				  <td height="10"></td>
				</tr>
				<?
					// Preparing parameters for getList() method
					$customer->SortField 	= $SortField;
					$customer->SortType  	= $SortType;
					$customer->Criteria  	= $Criteria;
					
				?>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
							<?
							$result = $customer->getResellerList();
							$total_records = mysqli_num_rows($result);
							//$total_records -= 1; // Skip first row because it contains the fields headers

							if ($total_records > 0)
							{
						  	// Used for pagination
								$perpage = $ItemsPerPage;
								if (!isset($_GET['screen']))
								{
									$_SESSION[$SessionPrefix.'_Screen'] = (!isset($_SESSION[$SessionPrefix.'_Screen'])) ? 0 : $_SESSION[$SessionPrefix.'_Screen'];
								}
								else
								{
									$_SESSION[$SessionPrefix.'_Screen'] = $_GET['screen'];
								}
								$pages = ceil($total_records/$perpage);
								if ($_SESSION[$SessionPrefix.'_Screen'] >= $pages) $_SESSION[$SessionPrefix.'_Screen'] = 0;
								$screen  = $_SESSION[$SessionPrefix.'_Screen'];
								$start   = $screen*$perpage;
								$numItems = ($screen < $pages-1) ? ($start+$perpage) : $total_records;
								$customer->Start = $start;
								$customer->Num = $perpage;
								$result = $customer->getResellerList();

								
?>
								
						
							<tr>
								<td style="padding-bottom: 0px;">
									<table border="0" cellpadding="3" cellspacing="0" width="100%">
										<tr>
											<td width="1%" style="white-space: nowrap;">Items Per Page
												<select name='cmbItemsPerPage' size='1' style="width:40px;" onchange="submitForm();">
													<option value="5"  <?if ($ItemsPerPage=="5")  echo "SELECTED";?>>5</option>
													<option value="10" <?if ($ItemsPerPage=="10") echo "SELECTED";?>>10</option>
													<option value="20" <?if ($ItemsPerPage=="20") echo "SELECTED";?>>20</option>
													<option value="40" <?if ($ItemsPerPage=="40") echo "SELECTED";?>>40</option>
													<option value="100" <?if ($ItemsPerPage=="100") echo "SELECTED";?>>100</option>
												</select>
											</td>
											<? if ($total_records > $perpage) { ?>
									    <td>
									      <? Pagination($screen, 5, $pages, basename($_SERVER["SCRIPT_NAME"])); ?>
											</td>
											<? } ?>
											<td style="text-align:<?=$lang['RIGHT']?>;">
												<? echo "Displaying page ".($screen+1)." in ".$pages.", items ".($start+1)."-".$numItems." Of ".$total_records;?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
				  		<tr>
						
						
				  			<td>
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="5%">View</td>
											<td class="gridHeader" width="8%">ID</td>
											<td class="gridHeader" width="13%">Name</td>
											<td class="gridHeader" width="13%">Phone</td>
											<td class="gridHeader" width="13%">Email</td>
											<td class="gridHeader" width="13%">Balance</td>
											<td class="gridHeader" width="13%">City</td>
											<td class="gridHeader" width="13%">UserIP</td>
										</tr>
										<?
									  	$i=1;

											while ($row=mysqli_fetch_object($result))
											{
												
													$class = ($i%2==0) ? "gridItem" : "gridAltItem" ;
												$color = ($i%2==0) ? "#EBEBEB" : "#F2F2F2" ;
											
													
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#EEE2C8'" onMouseout="this.style.backgroundColor='<?=$color?>'"
										ondblclick="window.location.href='../admin/add_reseller.php?id=<?=$row->id?>';">
										<td title=Edit">
												<table border="0" cellpadding="2" cellspacing="0" width="100%">
													<tr>
														<td align="center"><a href="../admin/add_reseller.php?id=<?=$row->id?>"><img border="0" src="../Images/Icon_Details.gif" width="30px" height="30px"></a></td>
													</tr>
												</table>
											</td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->id ?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->name?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->phone?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->email?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->balance?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->city?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->userip?></td>
										</tr>
									<?	
											$i++;
										}
									?>
									</table>
								</td>
							</tr>
							<? } else { ?>
							<tr>
				  			<td>
				  				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
				  					
										<tr>
											<td class="gridHeader" width="8%">ID</td>
											<td class="gridHeader" width="8%">Name</td>
											<td class="gridHeader" width="7%">Phone</td>
											<td class="gridHeader" width="13%">Email</td>
											<td class="gridHeader" width="13%">Balance</td>
											<td class="gridHeader" width="13%">City</td>
											<td class="gridHeader" width="13%">UserIP</td>
										</tr>
									
										<tr>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
										</tr>
									</table>
								</td>
							</tr>			</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							
							
						</table>
					</td>
				</tr>
				<? }
			}?>
			</table>
		</td>
	</tr>
</table>
<? End_Response(); ?>