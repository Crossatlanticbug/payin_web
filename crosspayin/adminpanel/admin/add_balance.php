<?php


 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Add Balance";
	
	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	// Include the member class
	require_once("../Classes/virtualbalance.class.php");
	$vbalance = new virtualbalance();

	// Manage Page
	
	

	//------------------- Get user permissions on this page -------------------------------------------------------
	
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="AddEdit"))
	{
		$amount = $_POST['txtamount'];
		$balancetype = $_POST['btype'];
		$comment = $_POST['txtcomment'];
		$user = $_POST['reseller'];
		
		$rowreseller = $customer->getcustomerInfo($user);
		$resellertype = $rowreseller->resellertype;
		$Resellerdetails=$customer->getResellerType($resellertype);
		$Resellerrow=mysqli_fetch_object($Resellerdetails);
		$profitslab = $Resellerrow->profitlevel;
		$profitamount = ($amount*$profitslab)/100;
			
		if (!empty($id))
		{
			//$vbalance->updateBalanceinfo($id, $objbal);
		}
		// Add New Material Type
		else
		{	$objbal=  new stdclass();
			$objbal->amount=$amount;
			$objbal->user=$user ;
			$objbal->amounttype=$balancetype;
			$objbal->comment=$comment;
			$objbal->addedby=$_SESSION['UserID'];
			$vbalance->addBalance($objbal);
			
			if ($balancetype=="DR"){	
			$objintbal=  new stdclass();
			$objintbal->amount=number_format($profitamount, 2, '.', '');
			$objintbal->user=$user;
			$objintbal->amounttype='DR';
			$objintbal->comment='Interest' ;
			$objintbal->addedby=$_SESSION['UserID'];
			$vbalance->addBalance($objintbal);
			}
		}
		exit("<script>window.location.href='../admin/virtualbalance.php';</script>");
	}
	else
	{
		if (!empty($id))
		{
			// Check if Edit is allowed
			
			$result = $member->getUserInfo($id);
			if (mysqli_num_rows($result)>0)
			{
				$row = mysqli_fetch_object($result);
				$amount = stripslashes($row->amount);
				$amounttype = stripslashes($row->amounttype);
				$comment = stripslashes($row->comment);

			}
			$boxHeader = 	"Update Balance";
		}
		else
		{
			// Check if Add is allowed
			$boxHeader = 	"Add New Balance";
		}
	}

?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Virtual Balance</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="virtualbalance.php" class="link">Balance Detail List</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader"><?=$boxHeader;?></td>
							</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="3" cellspacing="1" bgcolor="Silver">
									<tr class="boxContents">
											<td><select name="reseller" style="height:18px;width:320px" class="textbox">
											<option value="">Select Reseller</option>
											<option value="0">Admin</option>
											<?php $result = $customer->getResellerList();
											    while ($row=mysqli_fetch_object($result))
												{ 
													
														if ($resellertype==$row->id) {$selcted="selected";} else {$selcted="";}
														echo "<option  value=".$row->id." ". $selcted. ">".$row->phone." ".$row->email." ".$row->name."</option>";
													
												}
											 ?>	
											</select>
											</td>
										</tr>
										
										<tr class="boxCaption">
											<td>Amount : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtamount" value="<?=$amount?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Balance Type : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><select name="btype" style="height:18px;width:320px" >
											<option value="CR" <?php if($amounttype=="CR"){echo "Selected";}?>>Credit</option>
											<option value="DR" <?php if($amounttype=="DR"){echo "Selected";}?>>Debit</option>
											</select>
											</td>
										</tr>
										<tr class="boxCaption">
											<td>Comment :</td>
										</tr>
										<tr class="boxContents">
											<td><textarea col="50" rows="10" name="txtcomment"  style="height:18px;width:320px" class="textbox"><?=$comment?></textarea></td>
										</tr>
											<tr class="boxContents">
											<td>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td height="20" align="RIGHT"><input type="button" name="btnCancel" value="Cancel" class="button" onclick="window.location.href='../admin/add_balance.php';"></td>
														<td width="4"></td>
														<td height="20" align="RIGHT" width="1"><input type="button" name="btnSave" value="Save" class="button" onclick="CheckBalanceInfo();"></td>
													</tr>
												</table>
											</td>
										</tr>
									
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>