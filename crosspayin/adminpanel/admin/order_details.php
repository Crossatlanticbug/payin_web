<?php

 	include_once("../Includes/template.inc.php");

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../Home/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	$OrderID = $_GET['id'];
	if ( !is_numeric($OrderID) ) exit("<script>window.location.href='../admin/order_index.php';</script>");

	// Prepare page title and header
	$page_title = "Recharge Order Detail [OrderID:" . $OrderID . "]";

	// Get order details/info
	$order_info = new order();
	$resultorder=$order_info->getRechargeOrderInfo($OrderID);
	$row=mysqli_fetch_object($resultorder);
	
	$operatordetails=$order_info->getOperatorName("$row->operatorid");
	
	require_once("../Classes/user.class.php");
	$customerReferrers = new user();
?>

<table border="0" cellpadding="8" cellspacing="0" width="100%">
	<tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader"><?=$page_title?></td>
				</tr>
				<tr>
					<td height="30">
						<a href="../admin/rechargeorders.php" class="link">Recharge Order List</a>
					</td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader" style="text-align:center;">Order Information</td>
							</tr>
							<tr>
								<td>
									<table border="0" cellpadding="4" cellspacing="1" width="100%" bgcolor="Silver">
										<tr>
											<td class="boxCaption1" width="110">Order ID</td>
											<td class="boxContents" ><?=$OrderID?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="90">Name</td>
											<td class="boxContents"><?=ucwords($row->name)?></td>
										</tr>
										<tr>
											<td class="boxCaption1">Phone No</td>
											<td class="boxContents"><?=$row->mobileno?></td>
										</tr>
										<tr>
											<td class="boxCaption1">Email</td>
											<td class="boxContents"><?=$row->email?></td>
										</tr>
										<tr>
											<td class="boxCaption1">Operator</td>
											<td class="boxContents"><?=isset($operatordetails->operatorname)?$operatordetails->operatorname:$row->operator;?></td>
										</tr>
										<tr>
											<td class="boxCaption1">Mobile Type</td>
											<td class="boxContents"><?=$row->mobiletype?></td>
										</tr>
										<tr>
											<td class="boxCaption1">Recharge Type</td>
											<td class="boxContents"><?=$row->rechargetype?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="110">Order Amount</td>
											<td class="boxContents" >INR <?=$row->amount?></td>
										</tr>
										
										<tr>
											<td class="boxCaption1" width="110">Transaction Number</td>
											<td class="boxContents" ><?=$row->transactionid?></td>
										</tr>
										
										
										
										
										<tr>
											<td class="boxCaption1" width="110">Recharge Key</td>
											<td class="boxContents" ><?=$row->rechargekey?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="110">Recharge Date</td>
											<td class="boxContents" ><?=$row->rechargedate?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="110">Payment</td>
											<td class="boxContents" ><?=$row->payment?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="110">Payment Status</td>
											<td class="boxContents" ><?=$row->paymentstatus?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="110">Recharge Status</td>
											<td class="boxContents" ><?=$row->rechargestatus?></td>
										</tr>
										<tr>
											<td class="boxCaption1" width="110">User Ip</td>
											<td class="boxContents" ><?=$row->userip?></td>
										</tr>
																
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>