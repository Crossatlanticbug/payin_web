<?php


 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Reseller User";
	
	global $logged_in;
   if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	require_once("../Classes/virtualbalance.class.php");
	$vbalance = new virtualbalance();
	
	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$name = ""; $username = ""; $password = ""; $enabled = "1";

	//------------------- Get user permissions on this page -------------------------------------------------------
	
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="AddEdit"))
	{
		$phone = $_POST['txtphone'];
		$email = $_POST['txtEmail'];
		$name = $_POST['txtName'];
		$password = $_POST['txtPassword'];
		$balance = $_POST['txtbalance'];
		$address = $_POST['txtaddress'];
		$city = $_POST['txtcity'];
		$state = $_POST['txtstate'];
		$zipcode = $_POST['txtzipcode'];
		$resellertype = $_POST['resellertype'];
		$enabled = (isset($_POST['chkEnabled'])) ? "1" : "0";
		$isreseller = ($_POST['customertype']);

		
		$objreseller=  new stdclass();
		$objreseller->phone=$phone;
		$objreseller->email=$email;
		$objreseller->name=$name;
		$objreseller->password=$password;
		$objreseller->address=$address;
		$objreseller->city=$city ;
		$objreseller->state=$state;
		$objreseller->zipcode=$zipcode;
		$objreseller->enabled=$enabled ;
		$objreseller->isreseller=$isreseller;
		$objreseller->resellertype=$resellertype;
		
		$Resellerdetails=$customer->getResellerType($resellertype);
		$Resellerrow=mysqli_fetch_object($Resellerdetails);
		$profitslab = $Resellerrow->profitlevel;
		$profitamount = ($balance*$profitslab)/100;
		
		$objreseller->balance=number_format(($balance+$profitamount), 2, '.', '');
			
		// Edit Material Type
		if (!empty($id))
		{
			$customer->updateResellerInfo($id, $objreseller);
			$boxHeader="<span style='color:green'>Record Updated Successfully</span>";
		}
		// Add New Material Type
		else
		{
			$numbercount=$customer->isresellerExist($phone,$email);
	
			if ($numbercount==0){
			
				$resellerid=$customer->addNewReseller($objreseller);
				
				$Resellerdetails=$customer->getResellerType($resellertype);
				$Resellerrow=mysqli_fetch_object($Resellerdetails);
				$profitslab = $Resellerrow->profitlevel;
				$profitamount = ($balance*$profitslab)/100;
				
				$objintbal=  new stdclass();
				
				$balancetype = 'DR';
				$comment = 'Interest' ;
				$objintbal->amount=number_format($profitamount, 2, '.', '');
				$objintbal->user=$resellerid;
				$objintbal->amounttype=$balancetype;
				$objintbal->comment=$comment;
				$objintbal->addedby=$_SESSION['UserID'];
				$vbalance->addBalance($objintbal);
				
				
				$balancetype = 'DR';
				$comment = 'Opening Balance' ;
				$objbal=  new stdclass();
				$objbal->amount=number_format(($balance), 2, '.', '');
				$objbal->user=$resellerid;
				$objbal->amounttype=$balancetype;
				$objbal->comment=$comment;
				$objbal->addedby=$_SESSION['UserID'];
				$vbalance->addBalance($objbal);
				
				$boxHeader="<span style='color:green'>Record Added Successfully</span>";
			} else {
				$boxHeader="<span style='color:red'>Already added</span>";
		}
		}	
	}
	else
	{
		if (!empty($id))
		{
			// Check if Edit is allowed
			
			$row = $customer->getcustomerInfo($id);
			$name = stripslashes($row->name);
			$email = stripslashes($row->email);
			$phone = stripslashes($row->phone);
			$address = stripslashes($row->address);
			$city = stripslashes($row->city);
			$state = stripslashes($row->state);
			$zipcode = stripslashes($row->zipcode);
			$enabled = $row->enabled;
			$isreseller = $row->isreseller;
			$resellertype = $row->resellertype;
			$boxHeader = 	"Update Reseller";
		}
		else
		{
			// Check if Add is allowed
			$boxHeader = 	"Add New Reseller";
		}
	}

?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Reseller</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="reseller.php" class="link">Reseller List</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader"><?=$boxHeader;?></td>
							</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="3" cellspacing="1" bgcolor="Silver">
										<tr class="boxCaption">
											<td>Name : </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtName" value="<?=$name?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Email : </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtEmail" value="<?=$email?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Phone No : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtphone" value="<?=$phone?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<?php if (empty($id)) {?>
										<tr class="boxCaption">
											<td>Password :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtPassword" value="<?=$password?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<? } ?>
										<tr class="boxCaption">
											<td>Customer Type :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><select name="customertype" style="height:18px;width:320px" class="textbox">
											<option value="">Select Customer Type</option>
											<option value="0" <?php if ($isreseller==0) { echo "selected";}?>>Customer</option>
											<option value="1" <?php if ($isreseller==1) { echo "selected";}?>>Reseller</option>
											<option value="2" <?php if ($isreseller==2) { echo "selected";}?>>Support</option>
											</select>
											</td>
										</tr>
										
										<tr class="boxCaption">
											<td>Reseller Type :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><select name="resellertype" style="height:18px;width:320px" class="textbox">
											<option value="">Select Reseller</option>
											<?php $result = $customer->getResellerTypeList();
											    while ($row=mysqli_fetch_object($result))
												{ 
													if ($row->status==1){
														if ($resellertype==$row->id) {$selcted="selected";} else {$selcted="";}
														echo "<option  value=".$row->id." ". $selcted. ">".$row->resellertype."</option>";
													}	
												}
											 ?>	
											</select>
											</td>
										</tr>
										
										<?php if (empty($id)) {?>
										<tr class="boxCaption">
											<td>Opening Balance :  </td>
										</tr>
										
										<tr class="boxContents">
											<td><input type="text" name="txtbalance" value="<?=$balance?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										
										<?php } ?>
										<tr class="boxCaption">
											<td>Address :  </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtaddress" value="<?=$address?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>City :  </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtcity" value="<?=$city?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
											<tr class="boxCaption">
											<td>state :  </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtstate" value="<?=$state?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										</tr>
											<tr class="boxCaption">
											<td>Zipcode :  </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtzipcode" value="<?=$zipcode?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxContents">
											<td>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														
														<td width="28">Enabled:</td>
														<td width="30" align="center"><input type="Checkbox" name="chkEnabled" value="<?=$enabled?>" <?=($enabled)? "checked" : "";?>></td>
														<td height="20" align="RIGHT"><input type="button" name="btnCancel" value="Reset" class="button" onclick="window.location.href='../admin/add_reseller.php';"></td>
														<td width="4"></td>
														<td height="20" align="RIGHT" width="1"><input type="button" name="btnSave" value="Save" class="button" onclick="CheckReselerInfo();"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>