<?php
error_reporting(E_ALL);
ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Coupon List";;

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	$SessionPrefix = 'Coupon';

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	
	$Criteria['FromDate']		     = LoadSessionValue('txtFromDate', 'FromDate', $SessionPrefix, '');
	$Criteria['ToDate'] 		     = LoadSessionValue('txtToDate', 'ToDate', $SessionPrefix, '');
	$Criteria['CouponName'] = (!empty($_GET['CouponName'])) ? $_GET['CouponName'] : LoadSessionValue('txtCouponSearch', 'CouponName', $SessionPrefix, '');
	$Criteria['Code']  	     = LoadSessionValue('txtCode', 'Code', $SessionPrefix, '');
	$Criteria['Amount']  	     = LoadSessionValue('txtAmount', 'Amount', $SessionPrefix, '');
	
	$Criteria['Description']   = LoadSessionValue('txtDescription', 'Description', $SessionPrefix, '');
	$Criteria['Location']  = LoadSessionValue('txtLocation', 'Location', $SessionPrefix, '');
	$Criteria['Status']   = (!empty($_GET['Status'])) ? $_GET['Status'] : LoadSessionValue('Status', 'Status', $SessionPrefix, '');
	
	$SortFieldDefault = "ID"; $SortTypeDefault = "DESC";
	

	if (!empty($_GET['ID']) && is_numeric($_GET['ID']))
	{
		$Criteria['ID']	= $_GET['ID'];
		$coupon->populateByID($Criteria['ID']);
		$Criteria['CouponName'] = $user->coupon_name;
	}
	else if (!empty($Criteria['CouponName']))
	{
		$user->populateByUsername($Criteria['CouponName']);
		$Criteria['ID'] = $user->id;
	}
	else
	{
		$Criteria['ID'] = "";
	}

	// Order By
	$ItemsPerPage = LoadSessionValue('cmbItemsPerPage', 'ItemsPerPage', $SessionPrefix, "40");
	$SortField = (isset($_POST['cmbSortField']) ? $_POST['cmbSortField'] : $SortFieldDefault);
	$SortType	 = (isset($_POST['rdSortType']) ? $_POST['rdSortType'] : $SortTypeDefault);

	

	// Date timestamp used for the Excel File Exporting Name
	$Timestamp = date('YmdHis');
?>





<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="export" value="1">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Coupon List</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="addcoupon.php" class="link">Add New Coupon</a>
				  </td>
				</tr>
				<tr>
					<td valign="top" style="padding-top:10px;">
						<table border="0" cellpadding="3" cellspacing="0" width="380" class="table">
				  		<tr>
							  <td class="boxHeader">Filter Result</td>
							</tr>
							<tr>
								<td>
									<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="Silver">
										<tr>
											<td class="boxCaption" >Start Date date</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td>From</td>
														<td><input type="text" id="txtFromDate" name="txtFromDate" value="<?=$Criteria['FromDate'];?>" class="textbox" style="width:80px;text-align:center;" readonly></td>
														<td width="2"></td>
														<td valign="middle"><img id="imgFromDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
														<td width="5"></td>
														<td width="15">To</td>
														<td><input type="text" id="txtToDate" name="txtToDate" value="<?=$Criteria['ToDate'];?>" class="textbox" style="width:80px;text-align:center;" readonly></td>
														<td width="2"></td>
														<td valign="middle"><img id="imgToDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="CustomerRow">
											<td class="boxCaption">Name</td>
											<td class="boxContents">
												<input type="text" name="txtCouponSearch" id="txtCouponSearch" autocomplete="off" value="<?=$Criteria['CouponName']?>">
												
											</td>
										</tr>
										<tr id="OrderIDRow">
											<td class="boxCaption" width="90">Code</td>
											<td class="boxContents">
												<input type="text" name="txtCouponCode" value="<?=$Criteria['Code'];?>" class="textbox" style="width:80px;" >
											</td>
										</tr>
										<tr id="OrderIDRow">
											<td class="boxCaption" width="90">Amount</td>
											<td class="boxContents">
												<input type="text" name="txtAmount" value="<?=$Criteria['Amount'];?>" class="textbox" style="width:80px;" >
											</td>
										</tr>
										<tr id="TransactionIDRow">
											<td class="boxCaption" width="90">Description</td>
											<td class="boxContents">
												<input type="text" name="txtDescription" value="<?=$Criteria['Description'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr id="DiscountCouponRow">
											<td class="boxCaption" width="90">Location</td>
											<td class="boxContents">
												<input type="text" name="txtLocation" value="<?=$Criteria['Location'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr>
											<td class="boxCaption" width="90">Status</td>
											<td class="boxContents">
												<select name='cmbStatusID' size='1' style="width:140px;">
													<option value="success" <? if ($Criteria['Status']=="1") echo "SELECTED"; ?>>Enable</option>
													<option value="Pending" <? if ($Criteria['Status']=="0") echo "SELECTED"; ?>>Disable</option>
												</select>
											</td>
										</tr>
										<tr>
											<td class="boxCaption">SortBy</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td width="149">
															<select name='cmbSortField' size='1' style="width:140px;">
																<option value="id" <? if ($SortField=="id") echo "SELECTED";?> id="OrderIDOption">ID</option>
																<option value="amount" <? if($SortField=="amount") echo "SELECTED";?>>Amount</option>
																<option value="paymentstatus" <? if ($SortField=="status") echo "SELECTED";?>> Status</option>
																<option value="name" <? if ($SortField=="coupon_name") echo "SELECTED";?> id="UsernameOption">Name</option>
													    </select>
														</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="ASC" <? if ($SortType=='ASC') echo "checked"; ?>></td>
														<td width="35">ASC</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="DESC" <? if ($SortType=='DESC') echo "checked"; ?>></td>
														<td width="35">DESC</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr class="boxContents">
											<td colspan="2" style="text-align:<?=$lang['RIGHT']?>;">
												<table border="0" cellpadding="3" cellspacing="0">
													<tr>
														
														<td>
															<input type="button" name="btnReset" value="Reset" class="button" onclick="ResetOrderSearchForm();">
														</td>
														<td><input type="submit" name="btnSearch" value="Search" class="button"></td>
													</tr>
												</table>
											</td>
										</tr>							
										
										
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? if ( (isset($_POST['btnSearch'])) || (isset($_POST['cmbTimezone'])) || (isset($_GET['screen'])) || (!empty($Criteria['coupon_name'])) || (!empty($Criteria['id'])) || (!empty($Criteria['coupon_code'])) || (!empty($Criteria['start_date'])) || (!empty($Criteria['end_date'])) || (!empty($Criteria['status'])) || (!empty($Criteria['amount'])) ) {  ?>
				<tr>
				  <td height="10"></td>
				</tr>
				<?
					// Preparing parameters for getList() method
					$coupon->SortField 	= $SortField;
					$coupon->SortType  	= $SortType;
					$coupon->Criteria  	= $Criteria;
					
				?>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
							<?
							$result = $coupon->getList();
							$total_records = mysqli_num_rows($result);
							//$total_records -= 1; // Skip first row because it contains the fields headers

							if ($total_records > 0)
							{
						  	// Used for pagination
								$perpage = $ItemsPerPage;
								if (!isset($_GET['screen']))
								{
									$_SESSION[$SessionPrefix.'_Screen'] = (!isset($_SESSION[$SessionPrefix.'_Screen'])) ? 0 : $_SESSION[$SessionPrefix.'_Screen'];
								}
								else
								{
									$_SESSION[$SessionPrefix.'_Screen'] = $_GET['screen'];
								}
								$pages = ceil($total_records/$perpage);
								if ($_SESSION[$SessionPrefix.'_Screen'] >= $pages) $_SESSION[$SessionPrefix.'_Screen'] = 0;
								$screen  = $_SESSION[$SessionPrefix.'_Screen'];
								$start   = $screen*$perpage;
								$numItems = ($screen < $pages-1) ? ($start+$perpage) : $total_records;
								$coupon->Start = $start;
								$coupon->Num = $perpage;
								$result = $coupon->getList();

								
?>
								
						
							<tr>
								<td style="padding-bottom: 0px;">
									<table border="0" cellpadding="3" cellspacing="0" width="100%">
										<tr>
											<td width="1%" style="white-space: nowrap;">Items Per Page
												<select name='cmbItemsPerPage' size='1' style="width:40px;" onchange="submitForm();">
													<option value="5"  <?if ($ItemsPerPage=="5")  echo "SELECTED";?>>5</option>
													<option value="10" <?if ($ItemsPerPage=="10") echo "SELECTED";?>>10</option>
													<option value="20" <?if ($ItemsPerPage=="20") echo "SELECTED";?>>20</option>
													<option value="40" <?if ($ItemsPerPage=="40") echo "SELECTED";?>>40</option>
													<option value="100" <?if ($ItemsPerPage=="100") echo "SELECTED";?>>100</option>
												</select>
											</td>
											<? if ($total_records > $perpage) { ?>
									    <td>
									      <? Pagination($screen, 5, $pages, basename($_SERVER["SCRIPT_NAME"])); ?>
											</td>
											<? } ?>
											<td style="text-align:<?=$lang['RIGHT']?>;">
												<? echo "Displaying page ".($screen+1)." in ".$pages.", items ".($start+1)."-".$numItems." Of ".$total_records;?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
				  		<tr>
						
						
				  			<td>
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="8%">ID</td>
											<td class="gridHeader" width="7%">Coupon Name</td>
											<td class="gridHeader" width="13%">Coupon Code</td>
											<td class="gridHeader" width="13%">Amount</td>
											<td class="gridHeader" width="13%">Start Date</td>
											<td class="gridHeader" width="13%">End Date</td>
											<td class="gridHeader" width="13%">Description</td>
											<td class="gridHeader" width="13%">Coupan Location</td>
											<td class="gridHeader" width="13%">Status</td>
										</tr>
										<?
									  	$i=1;

											while ($row=mysqli_fetch_object($result))
											{
												
													
											
													
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#EEE2C8'" onMouseout="this.style.backgroundColor='<?=$color?>'" >
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->id?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><b><?=$row->coupon_name ?></b></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->coupon_code?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><b><?=number_format($row->amount,2)?></b></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->start_date?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->end_date?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->description?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->coupon_location?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->rechargetype?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->status?></td>
										</tr>
									<?	
											$i++;
										}
									?>
									</table>
								</td>
							</tr>
							<? } else { ?>
							<tr>
				  			<td>
				  				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
				  					
										<tr>
											<td class="gridHeader" width="8%">ID</td>
											<td class="gridHeader" width="7%">Coupon Name</td>
											<td class="gridHeader" width="13%">Coupon Code</td>
											<td class="gridHeader" width="13%">Amount</td>
											<td class="gridHeader" width="13%">Start Date</td>
											<td class="gridHeader" width="13%">End Date</td>
											<td class="gridHeader" width="13%">Description</td>
											<td class="gridHeader" width="13%">Coupan Location</td>
											<td class="gridHeader" width="13%">Status</td>
										</tr>
									
										<tr>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
										</tr>
									</table>
								</td>
							</tr>			</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							
							
						</table>
					</td>
				</tr>
				<? }
			}?>
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">//<![CDATA[
	var cal = Calendar.setup({
  	onSelect: function(cal) { cal.hide() }
  });
  cal.manageFields("imgFromDate", "txtFromDate", "%Y-%m-%d");
  cal.manageFields("imgToDate", "txtToDate", "%Y-%m-%d");
//]]></script>

<? End_Response(); ?>