<?php


 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Reseller Type";
	
	global $logged_in;
   if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}


	
	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$name = ""; $username = ""; $password = ""; $enabled = "1";

	//------------------- Get user permissions on this page -------------------------------------------------------
	
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="AddEdit"))
	{
		$resellertype = $_POST['txtresellertype'];
		$profit = $_POST['txtprofit'];
		$enabled = (isset($_POST['chkEnabled'])) ? "1" : "0";
				
		$objreseller=  new stdclass();
		$objreseller->resellertype=$resellertype;
		$objreseller->profitlevel=$profit;
		$objreseller->enabled=$enabled ;
				
		// Edit Material Type
		if (!empty($id))
		{
			$customer->updateResellerType($id, $objreseller);
		}
		// Add New Material Type
		else
		{
			$resellerid=$customer->addResellerType($objreseller);
			
		}
		exit("<script>window.location.href='../admin/resellertype.php';</script>");
	}
	else
	{
		if (!empty($id))
		{
			// Check if Edit is allowed
			
			$result = $customer->getResellerType($id);
			$row = mysqli_fetch_object($result);
			$resellertype = stripslashes($row->resellertype);
			$profitlevel = stripslashes($row->profitlevel);
			$enabled = $row->status;
			$boxHeader = 	"Update Reseller Type";
		}
		else
		{
			// Check if Add is allowed
			$boxHeader = 	"Add New Reseller Type";
		}
	}

?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Reseller Type</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="reseller.php" class="link">Reseller Type List</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader"><?=$boxHeader;?></td>
							</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="3" cellspacing="1" bgcolor="Silver">
										<tr class="boxCaption">
											<td>Reseller Type : </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtresellertype" value="<?=$resellertype?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Profile Slab (%) : </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtprofit" value="<?=$profitlevel?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										
										<tr class="boxContents">
											<td>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														
														<td width="28">Enabled:</td>
														<td width="30" align="center"><input type="Checkbox" name="chkEnabled" value="<?=$enabled?>" <?=($enabled)? "checked" : "";?>></td>
														<td height="20" align="RIGHT"><input type="button" name="btnCancel" value="Cancel" class="button" onclick="window.location.href='../admin/add_reseller.php';"></td>
														<td width="4"></td>
														<td height="20" align="RIGHT" width="1"><input type="button" name="btnSave" value="Save" class="button" onclick="CheckReselerTypeInfo();"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>