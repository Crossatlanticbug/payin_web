<?php

 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Admin User";

	global $logged_in;
  if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../Home/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	
	// Include the member class
	require_once("../Classes/member.class.php");
	$member = new member();

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$error_msg = "";

	// This is used to delete a certain record
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="Delete"))
	{
		$member->deleteUser($id);
	}

	
?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Admin Users</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="user_manage.php" class="link">Add New User</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader">Users List</td>
							</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="5%">Edit</td>
											<td class="gridHeader" width="5%">Delete</td>
											<td class="gridHeader" width="7%">ID</td>
											<td class="gridHeader" width="25%">Name</td>
											<td class="gridHeader" width="25%">Username</td>
											<td class="gridHeader" width="8%">Enabled</td>
										
										</tr>
									 <?
										$result = $member->getUsers();
									  if (mysqli_num_rows($result)>0)
									  {
									  	$i=1;
											while ($row=mysqli_fetch_object($result))
											{
												$class = ($i%2==0) ? "gridItem" : "gridAltItem" ;
												$color = ($i%2==0) ? "#EBEBEB" : "#F2F2F2" ;
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#E5E5E5'" onMouseout="this.style.backgroundColor='<?=$color?>'"
										ondblclick="window.location.href='../Admin/user_manage.php?id=<?=$row->UserID?>';" title="Double click to edit this record">
										
											<td title=Edit">
												<table border="0" cellpadding="2" cellspacing="0" width="100%">
													<tr>
														<td align="center"><a href="user_manage.php?id=<?=$row->UserID?>"><img border="0" src="../Images/icon_edit.gif" width="30px" height="30px"></a></td>
													</tr>
												</table>
											</td>
										
											<td title="Delete">
												<table border="0" cellpadding="2" cellspacing="0" width="100%">
													<tr>

													<? if ( ($_SESSION['Username'] == $row->Username)
															 && (md5($_SESSION['Password']) == $row->Password) ) { ?>
														<td align="center" title="Deletion Not Allowed">NA</td>
													<? }else{ ?>
														<td align="center"><a href="#" onclick="ConfirmDeletion(<?=$row->UserID?>);"><img border="0" src="../Images/icon_delete.gif"></a></td>
													<? } ?>
													</tr>
												</table>
											</td>
										
											<td align="center"><?=$row->UserID?></td>
											<td><?=stripslashes($row->Name)?></td>
											<td><?=stripslashes($row->Username)?></td>
											<!--<td align="center"><?= ($row->IsAdmin == 1) ? 'Yes' : 'No'; ?></td>-->
											<td align="center"><?= ($row->Enabled == 1) ? '<img border="0" src="../Images/yes_icon.gif">' : '<img border="0" src="../Images/no_icon.gif">'; ?></td>
											
										</tr>
									<? 		$i++;
											}
										}else{
									?>
										<tr>
										
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">--------------</td>
											<td class="gridEmpty">----------------------</td>
											<td class="gridEmpty">---------</td>
										</tr>
									<? } ?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>