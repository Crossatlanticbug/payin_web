<?php


 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Admin User";
	
	global $logged_in;
  if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	// Include the member class
	require_once("../Classes/member.class.php");
	$member = new member();

	// Manage Page
	
	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$name = ""; $username = ""; $password = ""; $enabled = "1";

	//------------------- Get user permissions on this page -------------------------------------------------------
	
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="AddEdit"))
	{
		$name = $_POST['txtName'];
		$username = $_POST['txtUsername'];
		$password = $_POST['txtPassword'];
		$enabled = (isset($_POST['chkEnabled'])) ? "1" : "0";

		// Used in the old version
		 $email = $_POST['txtEmail'];
		// $isAdmin = (isset($_POST['chkAdmin'])) ? "1" : "0";
		// $user = new member($name, $username, $email, $isAdmin, $enabled);

		$user = new member($name, $username, $email, $password, $enabled);

		// Edit Material Type
		if (!empty($id))
		{
			$member->updateUserInfo($id, $user);
		}
		// Add New Material Type
		else
		{
			$member->addNewUser($user);
		}
		exit("<script>window.location.href='../admin/adminuser.php';</script>");
	}
	else
	{
		if (!empty($id))
		{
			// Check if Edit is allowed
			
			$result = $member->getUserInfo($id);
			if (mysqli_num_rows($result)>0)
			{
				$row = mysqli_fetch_object($result);
				$name = stripslashes($row->Name);
				$username = stripslashes($row->Username);
				$enabled = $row->Enabled;

				// Used in the old version
				 $email = stripslashes($row->Email);
				// $isAdmin = $row->IsAdmin;
			}
			$boxHeader = 	"Update User";
		}
		else
		{
			// Check if Add is allowed
			$boxHeader = 	"Add New User";
		}
	}

?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Users</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="adminuser.php" class="link">Users List</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader"><?=$boxHeader;?></td>
							</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="3" cellspacing="1" bgcolor="Silver">
										<tr class="boxCaption">
											<td>Name : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtName" value="<?=$name?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Username : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtUsername" value="<?=$username?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Password :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtPassword" value="<?=$password?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										
										<tr class="boxCaption">
											<td>Email : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtEmail" value="<?=$email?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
									
										<tr class="boxContents">
											<td>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														
														<td width="28">Enabled:</td>
														<td width="30" align="center"><input type="Checkbox" name="chkEnabled" value="<?=$enabled?>" <?=($enabled)? "checked" : "";?>></td>
														<td height="20" align="RIGHT"><input type="button" name="btnCancel" value="Cancel" class="button" onclick="window.location.href='../Admin/adminuser.php';"></td>
														<td width="4"></td>
														<td height="20" align="RIGHT" width="1"><input type="button" name="btnSave" value="Save" class="button" onclick="CheckUserInfo();"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>