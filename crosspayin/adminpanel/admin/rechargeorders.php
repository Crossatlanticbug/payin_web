<?php
error_reporting(E_ALL);
ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Recharge Order List";;

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	$SessionPrefix = 'RECHARGE';

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	
	$Criteria['FromDate']		     = LoadSessionValue('txtFromDate', 'FromDate', $SessionPrefix, '');
	$Criteria['ToDate'] 		     = LoadSessionValue('txtToDate', 'ToDate', $SessionPrefix, '');
	$Criteria['FromAmount']	     = LoadSessionValue('txtFromAmount', 'FromAmount', $SessionPrefix, '');
	$Criteria['ToAmount'] 	     = LoadSessionValue('txtToAmount', 'ToAmount', $SessionPrefix, '');
	$Criteria['Username'] = (!empty($_GET['Username'])) ? $_GET['Username'] : LoadSessionValue('txtCustomerSearch', 'Username', $SessionPrefix, '');
	$Criteria['OrderID']  	     = LoadSessionValue('txtOrderID', 'OrderID', $SessionPrefix, '');
	
	$Criteria['TransactionID']   = LoadSessionValue('txtTransactionID', 'TransactionID', $SessionPrefix, '');
	$Criteria['RechargeID']  = LoadSessionValue('txtRechargeID', 'RechargeID', $SessionPrefix, '');
	$Criteria['UserIP']   = (!empty($_GET['UserIP'])) ? $_GET['UserIP'] : LoadSessionValue('txtUserIP', 'UserIP', $SessionPrefix, '');
	// Search Criteria
	
	$Criteria['StatusID'] = (!empty($_GET['StatusID'])) ? $_GET['StatusID'] : LoadSessionValue('cmbStatusID', 'StatusID', $SessionPrefix, '');
	$Criteria['ResellerType'] = (!empty($_GET['ResellerType'])) ? $_GET['ResellerType'] : LoadSessionValue('cmbresellertype', 'StatusID', $SessionPrefix, '');
	$Criteria['RechargeStatus']		 = LoadSessionValue('cmbRechargeStatus', 'RechargeStatus', $SessionPrefix, '');
	$Criteria['RechargeType']		 = LoadSessionValue('cmbRechargeType', 'RechargeType', $SessionPrefix, '');
	$Criteria['PaymentStatus']		 = LoadSessionValue('cmbStatusID', 'PaymentStatus', $SessionPrefix, '');
	$Criteria['Payment']		 = LoadSessionValue('cmbpaymentID', 'Payment', $SessionPrefix, '');

	
	$SortFieldDefault = "OrderID"; $SortTypeDefault = "DESC";

	
		
		

	if (!empty($_GET['UserID']) && is_numeric($_GET['UserID']))
	{
		$Criteria['UserID']	= $_GET['UserID'];
		$user->populateByID($Criteria['UserID']);
		$Criteria['Username'] = $user->username;
	}
	else if (!empty($Criteria['Username']))
	{
		$user->populateByUsername($Criteria['Username']);
		$Criteria['UserID'] = $user->id;
	}
	else
	{
		$Criteria['UserID'] = "";
	}

	// Order By
	$ItemsPerPage = LoadSessionValue('cmbItemsPerPage', 'ItemsPerPage', $SessionPrefix, "40");
	$SortField = (isset($_POST['cmbSortField']) ? $_POST['cmbSortField'] : $SortFieldDefault);
	$SortType	 = (isset($_POST['rdSortType']) ? $_POST['rdSortType'] : $SortTypeDefault);

	

	// Date timestamp used for the Excel File Exporting Name
	$Timestamp = date('YmdHis');
?>





<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="export" value="1">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Recharge Order List</td>
				</tr>
				<tr>
					<td valign="top" style="padding-top:10px;">
						<table border="0" cellpadding="3" cellspacing="0" width="380" class="table">
				  		<tr>
							  <td class="boxHeader">Filter Result</td>
							</tr>
							<tr>
								<td>
									<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="Silver">
										<tr>
											<td class="boxCaption" >Order date</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td>From</td>
														<td><input type="text" id="txtFromDate" name="txtFromDate" value="<?=$Criteria['FromDate'];?>" class="textbox" style="width:80px;text-align:center;" readonly></td>
														<td width="2"></td>
														<td valign="middle"><img id="imgFromDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
														<td width="5"></td>
														<td width="15">To</td>
														<td><input type="text" id="txtToDate" name="txtToDate" value="<?=$Criteria['ToDate'];?>" class="textbox" style="width:80px;text-align:center;" readonly></td>
														<td width="2"></td>
														<td valign="middle"><img id="imgToDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="boxCaption" >Order Amount</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td>From</td>
														<td><input type="text" name="txtFromAmount" value="<?=$Criteria['FromAmount'];?>" class="textbox" style="width:80px;text-align:center;" onkeyup="NumbersOnly(txtFromAmount);"></td>
														<td width="2"></td>
														<td valign="middle" width="17"></td>
														<td width="5"></td>
														<td width="15">To</td>
														<td><input type="text" name="txtToAmount" value="<?=$Criteria['ToAmount'];?>" class="textbox" style="width:80px;text-align:center;" onkeyup="NumbersOnly(txtToAmount);"></td>
														<td width="2"></td>
														<td valign="middle" width="17"></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="CustomerRow">
											<td class="boxCaption">Customer</td>
											<td class="boxContents">
												<input type="text" name="txtCustomerSearch" id="txtCustomerSearch" autocomplete="off" value="<?=$Criteria['Username']?>">
												
											</td>
										</tr>
										<tr id="OrderIDRow">
											<td class="boxCaption" width="90">OrderID</td>
											<td class="boxContents">
												<input type="text" name="txtOrderID" value="<?=$Criteria['OrderID'];?>" class="textbox" style="width:80px;" onkeyup="NumbersOnly(txtOrderID);">
											</td>
										</tr>
										<tr id="TransactionIDRow">
											<td class="boxCaption" width="90">Transaction ID</td>
											<td class="boxContents">
												<input type="text" name="txtTransactionID" value="<?=$Criteria['TransactionID'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr id="DiscountCouponRow">
											<td class="boxCaption" width="90">Recharge ID</td>
											<td class="boxContents">
												<input type="text" name="txtRechargeID" value="<?=$Criteria['RechargeID'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr>
											<td class="boxCaption">IP</td>
											<td class="boxContents">
												<input type="text" name="txtUserIP" value="<?=$Criteria['UserIP'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr>
											<td class="boxCaption" width="90">Customer Type</td>
											<td class="boxContents">
												<select name="cmbresellertype" style="height:18px;width:320px" class="textbox">
												<option value="">Select Reseller</option>
												<?php $result = $customer->getResellerTypeList();
											    while ($row=mysqli_fetch_object($result))
												{ 
													if ($row->status==1){
														if ($resellertype==$row->id) {$selcted="selected";} else {$selcted="";}
														echo "<option  value=".$row->id." ". $selcted. ">".$row->resellertype."</option>";
													}	
												}
											 ?>	
											</select>
											</td>
										</tr>
										<tr>
											<td class="boxCaption" width="90">Order Status</td>
											<td class="boxContents">
												<select name='cmbStatusID' size='1' style="width:140px;">
													<option value="">All</option>
													<option value="success" <? if ($Criteria['PaymentStatus']=="success") echo "SELECTED"; ?>>Success</option>
													<option value="Pending" <? if ($Criteria['PaymentStatus']=="Pending") echo "SELECTED"; ?>>Pending</option>
													<option value="failure" <? if ($Criteria['PaymentStatus']=="failure") echo "SELECTED"; ?>>Failure</option>
												</select>
											</td>
										</tr>
										<tr>
											<td class="boxCaption" width="90">Payment</td>
											<td class="boxContents">
												<select name='cmbpaymentID' size='1' style="width:140px;">
													<option value="">All</option>
													<option value="WALLET" <? if ($Criteria['Payment']=="WALLET") echo "SELECTED"; ?>>WALLET</option>
													<option value="PAYU" <? if ($Criteria['Payment']=="PAYU") echo "SELECTED"; ?>>PAYU</option>
												</select>
											</td>
										</tr>
										<tr>
											<td class="boxCaption" width="90">Recharge Status</td>
											<td class="boxContents">
												<select name='cmbRechargeStatus' size='1' style="width:140px;">
													<option value="">All</option>
													<option value="Successfully completed" <? if ($Criteria['RechargeStatus']=="Successfully completed") echo "SELECTED"; ?>>Successfully completed</option>
													<option value="Pending" <? if ($Criteria['RechargeStatus']=="Pending") echo "SELECTED"; ?>>Pending</option>
												</select>
											</td>
										</tr>
										<tr id="ProductsRow1">
											<td class="boxCaption" width="90">Recharge Type</td>
											<td class="boxContents">
												<select name='cmbRechargeType' size='1' style="width:140px;" >
													<option value="">All</option>
													<option value="mobile" <? if ($Criteria['RechargeType']=="mobile") echo "SELECTED"; ?>>Mobile</option>
													<option value="dth" <? if ($Criteria['RechargeType']=="dth") echo "SELECTED"; ?>>DTH</option>
													<option value="datacard" <? if ($Criteria['RechargeType']=="datacard") echo "SELECTED"; ?>>Datacard</option>
												</select>
											</td>
										</tr>
										
										<tr>
											<td class="boxCaption">SortBy</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td width="149">
															<select name='cmbSortField' size='1' style="width:140px;">
																<option value="orderid" <? if ($SortField=="orderid") echo "SELECTED";?> id="OrderIDOption">OrderID</option>
																<option value="amount" <? if($SortField=="amount") echo "SELECTED";?>>OrderAmount</option>
																<option value="rechargedate" <? if ($SortField=="rechargedate") echo "SELECTED";?>>OrderDate</option>
																<option value="rechargetype" <? if ($SortField=="rechargetype") echo "SELECTED";?>>Recharge Type</option>
																<option value="paymentstatus" <? if ($SortField=="paymentstatus") echo "SELECTED";?>>Payment Status</option>
																<option value="name" <? if ($SortField=="name") echo "SELECTED";?> id="UsernameOption">Name</option>
													    </select>
														</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="ASC" <? if ($SortType=='ASC') echo "checked"; ?>></td>
														<td width="35">ASC</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="DESC" <? if ($SortType=='DESC') echo "checked"; ?>></td>
														<td width="35">DESC</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr class="boxContents">
											<td colspan="2" style="text-align:<?=$lang['RIGHT']?>;">
												<table border="0" cellpadding="3" cellspacing="0">
													<tr>
														
														<td>
															<input type="button" name="btnReset" value="Reset" class="button" onclick="ResetOrderSearchForm();">
														</td>
														<td><input type="submit" name="btnSearch" value="Search" class="button"></td>
													</tr>
												</table>
											</td>
										</tr>							
										
										
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? if ( (isset($_POST['btnSearch'])) || (isset($_POST['cmbTimezone'])) || (isset($_GET['screen'])) || (!empty($Criteria['Username'])) || (!empty($Criteria['UserID'])) || (!empty($Criteria['StatusID']))
							|| (!empty($Criteria['OrderID'])) || (!empty($Criteria['TransactionID'])) || (!empty($Criteria['DiscountCoupon'])) || (!empty($Criteria['StatusID'])) || (!empty($Criteria['UserIP'])))  {  ?>
				<tr>
				  <td height="10"></td>
				</tr>
				<?
					// Preparing parameters for getList() method
					$order->SortField 	= $SortField;
					$order->SortType  	= $SortType;
					$order->Criteria  	= $Criteria;
					
				?>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
							<?
							$result = $order->getList();
							$total_records = mysqli_num_rows($result);
							//$total_records -= 1; // Skip first row because it contains the fields headers

							if ($total_records > 0)
							{
						  	// Used for pagination
								$perpage = $ItemsPerPage;
								if (!isset($_GET['screen']))
								{
									$_SESSION[$SessionPrefix.'_Screen'] = (!isset($_SESSION[$SessionPrefix.'_Screen'])) ? 0 : $_SESSION[$SessionPrefix.'_Screen'];
								}
								else
								{
									$_SESSION[$SessionPrefix.'_Screen'] = $_GET['screen'];
								}
								$pages = ceil($total_records/$perpage);
								if ($_SESSION[$SessionPrefix.'_Screen'] >= $pages) $_SESSION[$SessionPrefix.'_Screen'] = 0;
								$screen  = $_SESSION[$SessionPrefix.'_Screen'];
								$start   = $screen*$perpage;
								$numItems = ($screen < $pages-1) ? ($start+$perpage) : $total_records;
								$order->Start = $start;
								$order->Num = $perpage;
								$result = $order->getList();

								
?>
								
						
							<tr>
								<td style="padding-bottom: 0px;">
									<table border="0" cellpadding="3" cellspacing="0" width="100%">
										<tr>
											<td width="1%" style="white-space: nowrap;">Items Per Page
												<select name='cmbItemsPerPage' size='1' style="width:40px;" onchange="submitForm();">
													<option value="5"  <?if ($ItemsPerPage=="5")  echo "SELECTED";?>>5</option>
													<option value="10" <?if ($ItemsPerPage=="10") echo "SELECTED";?>>10</option>
													<option value="20" <?if ($ItemsPerPage=="20") echo "SELECTED";?>>20</option>
													<option value="40" <?if ($ItemsPerPage=="40") echo "SELECTED";?>>40</option>
													<option value="100" <?if ($ItemsPerPage=="100") echo "SELECTED";?>>100</option>
												</select>
											</td>
											<? if ($total_records > $perpage) { ?>
									    <td>
									      <? Pagination($screen, 5, $pages, basename($_SERVER["SCRIPT_NAME"])); ?>
											</td>
											<? } ?>
											<td style="text-align:<?=$lang['RIGHT']?>;">
												<? echo "Displaying page ".($screen+1)." in ".$pages.", items ".($start+1)."-".$numItems." Of ".$total_records;?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
				  		<tr>
						
						
				  			<td>
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="8%">OrderDetails</td>
											<td class="gridHeader" width="7%">OrderID</td>
											<td class="gridHeader" width="13%">OrderAmount</td>
											<td class="gridHeader" width="13%">Date</td>
											<td class="gridHeader" width="13%">Name</td>
											<td class="gridHeader" width="13%">Mobile No</td>
											<td class="gridHeader" width="13%">Operator</td>
											<td class="gridHeader" width="13%">Recharge Type</td>
											<td class="gridHeader" width="13%">Payment</td>
											<td class="gridHeader" width="13%">Paymentstatus</td>
											<td class="gridHeader" width="12%">User IP</td>
											<td class="gridHeader" width="15%">Recharge Status</td>
										</tr>
										<?
									  	$i=1;

											while ($row=mysqli_fetch_object($result))
											{
												
												$class = ($i%2==0) ? "gridItem" : "gridAltItem" ;
												$color = ($i%2==0) ? "#EBEBEB" : "#F2F2F2" ;	
											
													
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#EEE2C8'" onMouseout="this.style.backgroundColor='<?=$color?>'"
												ondblclick="window.location.href='../admin/order_details.php?id=<?=$row->orderid?>';" title="Double click to view order details">
											<td title="Order Details">
												<table border="0" cellpadding="2" cellspacing="0" width="100%">
													<tr>
														<td style="text-align:center;"><a href="../admin/order_details.php?id=<?=$row->orderid?>"><img border="0" src="../Images/Icon_Details.gif"></a></td>
													</tr>
												</table>
											</td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->orderid?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><b><?=number_format($row->amount,2)?></b></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->rechargedate?></td>
											
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->name?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->mobileno?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->operator?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->rechargetype?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->payment?></td>
											
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->paymentstatus?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->userip?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->rechargestatus?></td>
										</tr>
									<?	
											$i++;
										}
									?>
									</table>
								</td>
							</tr>
							<? } else { ?>
							<tr>
				  			<td>
				  				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
				  					
											<tr>
											<td class="gridHeader" width="8%">OrderDetails</td>
											<td class="gridHeader" width="7%">OrderID</td>
											<td class="gridHeader" width="13%">OrderAmount</td>
											<td class="gridHeader" width="13%">Date</td>
											<td class="gridHeader" width="13%">Name</td>
											<td class="gridHeader" width="13%">Mobile No</td>
											<td class="gridHeader" width="13%">Operator</td>
											<td class="gridHeader" width="13%">Recharge Type</td>
											<td class="gridHeader" width="13%">Payment</td>
											<td class="gridHeader" width="13%">Paymentstatus</td>
											<td class="gridHeader" width="12%">User IP</td>
											<td class="gridHeader" width="15%">Recharge Status</td>
										</tr>
									
										<tr>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
										</tr>
									</table>
								</td>
							</tr>			</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							
							
						</table>
					</td>
				</tr>
				<? }
			}?>
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">//<![CDATA[
	var cal = Calendar.setup({
  	onSelect: function(cal) { cal.hide() }
  });
  cal.manageFields("imgFromDate", "txtFromDate", "%Y-%m-%d");
  cal.manageFields("imgToDate", "txtToDate", "%Y-%m-%d");
//]]></script>

<? End_Response(); ?>