<?php
error_reporting(E_ALL);
ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Reseller List";;

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	$SessionPrefix = 'Reseller Details';

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	
	

	// Date timestamp used for the Excel File Exporting Name
	$Timestamp = date('YmdHis');
?>





<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="export" value="1">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Reseller Type List</td>
				</tr>
				 <td height="30">
				  	<a href="add_resellertype.php" class="link">Add New Reseller Type</a>
				  </td>
				
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
							<?
							$result = $customer->getResellerTypeList();
							if (mysqli_num_rows($result)>0)
									  {
							?>					
							
				  		<tr>
						
						
				  			<td>
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="5%">Edit</td>
											<td class="gridHeader" width="13%">Id</td>
											<td class="gridHeader" width="13%">Type</td>
											<td class="gridHeader" width="13%">Profit Level</td>
											<td class="gridHeader" width="13%">Status</td>
											
										</tr>
										<?
									  	$i=1;
									
											while ($row=mysqli_fetch_object($result))
											{
												
													$class = ($i%2==0) ? "gridItem" : "gridAltItem" ;
												$color = ($i%2==0) ? "#EBEBEB" : "#F2F2F2" ;
											
													
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#EEE2C8'" onMouseout="this.style.backgroundColor='<?=$color?>'"
										ondblclick="window.location.href='../admin/add_resellertype.php?id=<?=$row->id?>';">
										<td title=Edit">
												<table border="0" cellpadding="2" cellspacing="0" width="100%">
													<tr>
														<td align="center"><a href="../admin/add_resellertype.php?id=<?=$row->id?>"><img border="0" src="../Images/Icon_Details.gif" width="30px" height="30px"></a></td>
													</tr>
												</table>
											</td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->id ?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->resellertype?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->profitlevel?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><? if ($row->status=="1") {echo "Enabled";} else {echo "Disabled";}?></td>
											
										</tr>
									<?	
											$i++;
										}
									?>
									</table>
								</td>
							</tr>
							<? } else { ?>
							<tr>
				  			<td>
				  				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
				  					
										<tr>
											<td class="gridHeader" width="5%">Edit</td>
											<td class="gridHeader" width="13%">Id</td>
											<td class="gridHeader" width="13%">Type</td>
											<td class="gridHeader" width="13%">Profit Level</td>
										</tr>
									
										<tr>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
										
										</tr>
									</table>
								</td>
							</tr>		
							
																
							
						</table>
					</td>
				</tr>
				<?
			}?>
			</table>
		</td>
	</tr>
</table>
<? End_Response(); ?>