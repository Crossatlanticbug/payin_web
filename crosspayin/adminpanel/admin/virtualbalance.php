<?php

 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Balance";

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	
	// Include the member class
	require_once("../Classes/virtualbalance.class.php");
	$vbalance = new virtualbalance();
	require_once("../Classes/customer.class.php");
	$customer = new customer();
	require_once("../Classes/member.class.php");
	$member = new member();

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$error_msg = "";

	// This is used to delete a certain record
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="Delete"))
	{
		$vbalance->deleteUser($id);
	}

	
?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Virtual Balance</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="add_balance.php" class="link">Add New Balance</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader">Transaction List</td>
							  <td class="boxHeader">Debit Amount:<?php echo $debitamount= $vbalance->getTotalAmount('DR');?></td>
							  <td class="boxHeader">Credit Amount:<?php echo $creditamount= $vbalance->getTotalAmount('CR');?></td>
							  <td class="boxHeader">Total Balance:<strong>INR <?php echo $creditamount - $debitamount?></strong></td>
						</tr>
						<?
							$result = $vbalance->getBalanceDetails();
							$total_records = mysqli_num_rows($result);
							//$total_records -= 1; // Skip first row because it contains the fields headers
							$ItemsPerPage = LoadSessionValue('cmbItemsPerPage', 'ItemsPerPage', $SessionPrefix, "40");
							if ($total_records > 0)
							{
						  	// Used for pagination
								$perpage = $ItemsPerPage;
								if (!isset($_GET['screen']))
								{
									$_SESSION[$SessionPrefix.'_Screen'] = (!isset($_SESSION[$SessionPrefix.'_Screen'])) ? 0 : $_SESSION[$SessionPrefix.'_Screen'];
								}
								else
								{
									$_SESSION[$SessionPrefix.'_Screen'] = $_GET['screen'];
								}
								$pages = ceil($total_records/$perpage);
								if ($_SESSION[$SessionPrefix.'_Screen'] >= $pages) $_SESSION[$SessionPrefix.'_Screen'] = 0;
								$screen  = $_SESSION[$SessionPrefix.'_Screen'];
								$start   = $screen*$perpage;
								$numItems = ($screen < $pages-1) ? ($start+$perpage) : $total_records;
								$vbalance->Start = $start;
								$vbalance->Num = $perpage;
								$result = $vbalance->getBalanceDetails();

								
?>
								
						
							<tr>
								<td style="padding-bottom: 0px;">
									<table border="0" cellpadding="3" cellspacing="0" width="100%">
										<tr>
											<td width="1%" style="white-space: nowrap;">Items Per Page
												<select name='cmbItemsPerPage' size='1' style="width:40px;" onchange="submitForm();">
													<option value="5"  <?if ($ItemsPerPage=="5")  echo "SELECTED";?>>5</option>
													<option value="10" <?if ($ItemsPerPage=="10") echo "SELECTED";?>>10</option>
													<option value="20" <?if ($ItemsPerPage=="20") echo "SELECTED";?>>20</option>
													<option value="40" <?if ($ItemsPerPage=="40") echo "SELECTED";?>>40</option>
													<option value="100" <?if ($ItemsPerPage=="100") echo "SELECTED";?>>100</option>
												</select>
											</td>
											<? if ($total_records > $perpage) { ?>
									    <td>
									      <? Pagination($screen, 5, $pages, basename($_SERVER["SCRIPT_NAME"])); ?>
											</td>
											<? } ?>
											<td style="text-align:<?=$lang['RIGHT']?>;">
												<? echo "Displaying page ".($screen+1)." in ".$pages.", items ".($start+1)."-".$numItems." Of ".$total_records;?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
				  		<tr>
						
				  		<tr>
				  			<td colspan="4">
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="5%">View</td>
											<td class="gridHeader" width="15%">Amount</td>
											<td class="gridHeader" width="10%">Type</td>
											<td class="gridHeader" width="15%">User Phone</td>
											<td class="gridHeader" width="15%">User email</td>
											<td class="gridHeader" width="15%">Date</td>
											<td class="gridHeader" width="15%">Comment</td>
											<td class="gridHeader" width="10%">Added By</td>
										
										</tr>
									 <?
										//$result = $vbalance->getBalanceDetails();
									  
									  	$i=1;
											while ($row=mysqli_fetch_object($result))
											{
												$class = ($i%2==0) ? "gridItem" : "gridAltItem" ;
												$color = ($i%2==0) ? "#EBEBEB" : "#F2F2F2" ;
												$customerdetail=$customer->getcustomerInfo($row->user);
												$addedbyresult=$member->getUserInfo($row->addedby);
												$addedbydetail = mysqli_fetch_object($addedbyresult);
																																		
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#E5E5E5'" onMouseout="this.style.backgroundColor='<?=$color?>'" >
										
											<td title=Edit">
												<table border="0" cellpadding="2" cellspacing="0" width="100%">
													<tr>
														<td align="center"><a href="viewbalancedetails.php?id=<?=$row->id?>"><img border="0" src="../Images/Icon_Details.gif" width="30px" height="30px"></a></td>
													</tr>
												</table>
											</td>
											<td align="center"><?php echo $row->amount?></td>
											<td align="center"><?php echo $row->amounttype?></td>
											<td align="center"><?php echo $customerdetail->phone?></td>
											<td align="center"><?php echo $customerdetail->email?></td>
											<td align="center"><?php echo $row->processdate?></td>
											<td align="center"><?php echo $row->comment?></td>
											<td align="center"><?php echo isset($addedbydetail->Username)?$addedbydetail->Username:'System';?></td>
											
											
										</tr>
									<? 		$i++;
											}
										}else{
									?>
										<tr>
										
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">------</td>
											<td class="gridEmpty">------</td>
											<td class="gridEmpty">------</td>
											<td class="gridEmpty">------</td>
										</tr>
									<? } ?>
									
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>