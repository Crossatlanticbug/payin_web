<?php


 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Reseller Request User";
	
	global $logged_in;
   if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	require_once("../Classes/virtualbalance.class.php");
	$vbalance = new virtualbalance();
	
	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$name = ""; $username = ""; $password = ""; $enabled = "1";

	//------------------- Get user permissions on this page -------------------------------------------------------
	
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="AddEdit"))
	{
		
		
		$name = $_POST['txtName'];
		$email = $_POST['txtEmail'];
		$phone = $_POST['txtphone'];
		$password = $_POST['txtPassword'];
		$company= $_POST['txtcompany'];
		$address = $_POST['txtaddress'];
		$balance = $_POST['txtbalance'];
		$resellertype = $_POST['resellertype'];
		$enabled = (isset($_POST['chkEnabled'])) ? "1" : "0";
		$isreseller = '1';
		
		
		$objreseller=  new stdclass();
		$objreseller->phone=$phone;
		$objreseller->email=$email;
		$objreseller->name=$name;
		$objreseller->password=$password;
		$objreseller->address=$address;
		$objreseller->city='' ;
		$objreseller->state='';
		$objreseller->zipcode='';
		$objreseller->enabled=$enabled ;
		$objreseller->isreseller=$isreseller;
		$objreseller->resellertype=$resellertype;
		
		$Resellerdetails=$customer->getResellerType($resellertype);
		$Resellerrow=mysqli_fetch_object($Resellerdetails);
		$profitslab = $Resellerrow->profitlevel;
		$profitamount = ($balance*$profitslab)/100;
		
		$objreseller->balance=number_format(($balance+$profitamount), 2, '.', '');
		
		$numbercount=$customer->isresellerExist($phone,$email);
	
			if ($numbercount==0){
				$resellerid=$customer->addNewReseller($objreseller);
				$resellerid=$customer->Updatepartnerflag($id);
				
				$objintbal=  new stdclass();
				
				$balancetype = 'DR';
				$comment = 'Interest' ;
				$objintbal->amount=number_format($profitamount, 2, '.', '');
				$objintbal->user=$resellerid;
				$objintbal->amounttype=$balancetype;
				$objintbal->comment=$comment;
				$objintbal->addedby=$_SESSION['UserID'];
				$vbalance->addBalance($objintbal);
				
				
				$balancetype = 'DR';
				$comment = 'Opening Balance' ;
				$objbal=  new stdclass();
				$objbal->amount=number_format(($balance), 2, '.', '');
				$objbal->user=$resellerid;
				$objbal->amounttype=$balancetype;
				$objbal->comment=$comment;
				$objbal->addedby=$_SESSION['UserID'];
				$vbalance->addBalance($objbal);
				
				
				
				exit("<script>window.location.href='../admin/resellerrequest.php';</script>");
			} else {
				$boxHeader="<span style='color:red'>Already added</span>";
				
			}
		
		
	}
	else
	{
		if (!empty($id))
		{
			// Check if Edit is allowed
			
			$row = $customer->getResellerRequestInfo($id);
			$name = stripslashes($row->name);
			$email = stripslashes($row->email);
			$phone = stripslashes($row->phoneno);
			$address = stripslashes($row->address);
			$company = stripslashes($row->company);
			$comment = stripslashes($row->comment);
			$partner = $row->partner;
			$boxHeader = 	"Update Reseller Request";
		}
		else
		{
			// Check if Add is allowed
			$boxHeader = 	"Add New Reseller Request";
		}
	}

?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Reseller</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="resellerrequest.php" class="link">Partner List</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							  <td class="boxHeader"><?=$boxHeader;?></td>
							</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="3" cellspacing="1" bgcolor="Silver">
										<tr class="boxCaption">
											<td>Name : </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtName" value="<?=$name?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Email : </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtEmail" value="<?=$email?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Phone No : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtphone" value="<?=$phone?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										
										<tr class="boxCaption">
											<td>Password :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtPassword" value="<?=$password?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										
										<tr class="boxCaption">
											<td>Company :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtcompany" value="<?=$company?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Address :  </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtaddress" value="<?=$address?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>comment :  </td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtcomment" value="<?=$comment?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Reseller Type :  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><select name="resellertype" style="height:18px;width:320px" class="textbox">
											<option value="">Select Reseller</option>
											<?php $result = $customer->getResellerTypeList();
											    while ($row=mysqli_fetch_object($result))
												{ 
													if ($row->status==1){
														if ($resellertype==$row->id) {$selcted="selected";} else {$selcted="";}
														echo "<option  value=".$row->id." ". $selcted. ">".$row->resellertype."</option>";
													}	
												}
											 ?>	
											</select>
											</td>
										</tr>
										<tr class="boxCaption">
											<td>Opening Balance :  </td>
										</tr>
										
										<tr class="boxContents">
											<td><input type="text" name="txtbalance" value="<?=$balance?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
									
										
										
										<tr class="boxContents">
											<td>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														
														<td width="28">Partner:</td>
														<td width="30" align="center"><input type="Checkbox" name="chkEnabled" value="<?=$enabled?>" <?=($enabled)? "checked" : "";?>></td>
														<?php if ($partner=="0"){ ?>
														<td height="20" align="RIGHT"><input type="button" name="btnCancel" value="Cancel" class="button" onclick="window.location.href='../admin/add_reseller.php';"></td>
														<td width="4"></td>
														<td height="20" align="RIGHT" width="1"><input type="button" name="btnSave" value="Save" class="button" onclick="CheckReselertypeInfo();"></td>
														<?php } else { ?>
														<td height="20" align="RIGHT">Already converted as Reseller</td>
														<?php } ?>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<? End_Response(); ?>