<?
global $lang;
global $language;
global $page_title;
global $CurrentPage;
global $buffer_contents;


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="ltr">
<head>
	<title><?=$page_title;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1256" />
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />

	<!-- General Scripts -->
	<script src="../Scripts/scripts.js" type="text/javascript" ></script>

	<!-- Used for the Color Picker -->
  <script language="JavaScript" src="../Scripts/colorpicker.js"></script>

	<!-- Used for the Calendar Script (New Version) -->
	<link rel="stylesheet" type="text/css" href="../JSCal2/css/jscal2.css" />
  <link rel="stylesheet" type="text/css" href="../JSCal2/css/border-radius.css" />
  <!-- <link rel="stylesheet" type="text/css" href="../JSCal2/css/gold/gold.css" />-->
  <script type="text/javascript" src="../JSCal2/js/jscal2.js"></script>
  <script type="text/javascript" src="../JSCal2/js/lang/en.js"></script>

	<!-- Web Style Sheets -->
	<link href="../Styles/style.css" rel="stylesheet" type="text/css"/>
</head>

<body>
	<form id="form" name="form" method="post" action="<?=$CurrentPage?>" enctype="multipart/form-data" class="decoration">
		<input type="hidden" name="useraction" value="browse"/>
		<input type="hidden" name="from" value="<?=$CurrentPage?>"/>
		<!-- Start ToolTip JavaScript -->
		<div id="dhtmltooltip"></div>
		<script src="../Scripts/tooltip_init.js" type="text/javascript"></script>
		<!-- End ToolTip Script -->
		<table border="0" cellspacing="0" cellpadding="0" width="100%" style="height:100%">
		  <!-- The Top part of the page -->
			<tr>
				<td width="100%" height="1" valign="top">
					<? include "../Includes/top.inc.php"; ?>
				</td>
	  	</tr>
	  	<tr>
				<td width="100%" height="2"></td>
	  	</tr>
	   	<tr>
	      <td width="100%" height="100%" valign="top" style="border:2px outset white;">
	        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="height:100%;">
	          <tr>
	          	<? if ($CurrentPage != "index.php") { ?>
	            <!-- The left part of the page -->
							<td width="190" height="100%" valign="top" style="border-<?=$lang['RIGHT']?>:1px outset Silver;">
	              <? include "../Includes/left.inc.php"?>
	            </td>
	            <? } ?>
	            <!-- The middle part of the page -->
	            <td height="100%" valign="top">
	    					<?= $buffer_contents ?>
	    				</td>
	          </tr>
	        </table>
	      </td>
			</tr>
		</table>
	</form>
</body>
</html>