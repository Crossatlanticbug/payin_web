<?php
	require_once("../Classes/session.class.php");
	$session = new session(true);
	
	require_once("../../Configurations/config.inc.php");	// Public site configurations


	// Send Charset to Browser
	if( ! headers_sent() )
	//  header("ContentType: text/html; charset=" . $lang['ENCODING']);

	global $soap_client;
	global $CurrentPage;
	global $buffer_contents;

	$CurrentPage = basename($_SERVER["SCRIPT_NAME"]);

	

	// Open connection to the local & PB Slave databases
	global $db;
	global $dbLink;

	require_once("../Classes/connection.class.php");
	$db = new connection("connection.cfg.php");
	$dbLink = $db->dbLink;

	// Get default currency
	global $Currency;
	$Currency = "INR";

	// Include the functions list
	require_once("../Functions/functions.php");




	  // Include the required classes
	require_once("../Classes/user.class.php");
	require_once("../Classes/country.class.php");
	require_once("../Classes/order.class.php");
	require_once("../Classes/developmentlog.class.php");
	require_once("../Classes/coupon.class.php");
	require_once("../Classes/customer.class.php");
	

	$user = new user();
	//$product = new product();
	$country = new country();
	$order = new order();
	$developmentlog = new developmentlog();
	$coupon = new coupon();
	$customer = new customer();
	//$orderStatus = new orderStatus();
	
	require_once("../main/check_login.php");

	global $logged_in;
	
	if ($logged_in == 0) {
	  require_once("../main/login.php");
	}

	
  function End_Response()
	{
	  global $buffer_contents;
		$buffer_contents = ob_get_contents();
		ob_end_clean();
		global $logged_in;
		include_once("template.php");
		global $db;
		if( $db) $db->dbclose();
		// ob_end_flush();
	  exit();
	}

	/*
	 * Start the output buffer
	 */
	ob_start();
?>