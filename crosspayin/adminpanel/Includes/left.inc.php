<?

global $language;
global $config;


require_once("../Classes/member.class.php");
$member = new member();



?>

<!-- Left Menu Section -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 100%;">
	<tr>
		<td valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			    <td height="20" class="sectionHeader">Menu</td>
				</tr>
				<tr>
					<td valign="top">
						<div id='ksm' class='outlookKSM' style='width:230px;' > 
							<ul class='ksmUL '>
								<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Admin User </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/adminuser.php' >Manage Users </span></a></li>
										</ul> 
									</div></div>
								</li>
																
								<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Manage Virtual Balance </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/virtualbalance.php' >Manage Balance </span></a></li>
										</ul> 
									</div></div>
								</li>
								<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Manage Reseller/Support </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/resellertype.php' >Manage Reseller Type</span></a></li>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/reseller.php' >Manage Reseller/Support </span></a></li>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/resellerrequest.php' >Become Reseller Request </span></a></li>
										</ul> 
									</div></div>
								</li>
									<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Customer </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/customer.php' >Manage Customer </span></a></li>
										</ul> 
									</div></div>
								</li>
								</li>
								<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Orders </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/rechargeorders.php' >Manage Recharge Orders </span></a></li>
										</ul> 
									</div></div>
								</li>
								
								<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Development Log </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/developmentlog.php' >Manage Development Log </span></a></li>

										</ul> 
									</div></div>
								</li>
								
								<li id='UMS' class='ksmLI ksmLevel0  ksmFirst'>
									<a class='ksmA ksmParent' ><span class='ksmIn'><span class='ksmLeft'> </span><span class='ksmRight'> </span><span class='ksmText'>Coupon </span></span> </span></a>
									<div class='ksmChildBox'><div class='ksmCBB'>
										<ul class='ksmUL'>
											<li id='ManageUsers' class='ksmLI ksmFirst'><a class='ksmA ksmChild ' href='../admin/coupon.php' >Manage Coupon </span></a></li>
										</ul> 
									</div></div>
								</li>
								
								
							</ul>
							
							
							
						</div>
							
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>