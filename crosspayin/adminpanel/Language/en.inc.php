<?php

global $lang;
global $config;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             General Use
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['ENCODING'] = 'windows-1256';
$lang['DIRECTION'] = 'ltr';
$lang['LEFT'] = 'Left';
$lang['RIGHT'] = 'Right';
$lang['LANG'] = 'En';

$lang['PerPage'] = 20;
$lang['PUBLICATIONS_IMAGE_PATH'] = "../../Images/Publications/";
$lang['EMAG_IMAGE_PATH'] = "../../Images/EMagazines/";
$lang['ARTICLES_DCOUMENTS_PATH'] = "../../PDF/EmagazineArticles/";
$lang['CONTENTS_DCOUMENTS_PATH'] = "../../PDF/PublicationContents/";

// Styles
$lang['S_FONT_SIZE'] = '10px';
$lang['M_FONT_SIZE'] = '11px';
$lang['B_FONT_SIZE'] = '12px';
$lang['H1_FONT_SIZE'] = '17px';
$lang['H2_FONT_SIZE'] = '13px';
$lang['FONT_FAMILY'] = 'Verdana';

// Special Styles
$lang['SPECIAL_SS_FONT_SIZE'] = '13px';
$lang['SPECIAL_S_FONT_SIZE']  = '14px';
$lang['SPECIAL_M_FONT_SIZE']  = '15px';
$lang['SPECIAL_B_FONT_SIZE']  = '16px';
$lang['SPECIAL_FONT_FAMILY']  = 'Calibri,Helvetica,Arial';

$lang['MAIN_BGCOLOR'] = '#F9F9F9';

// Date Format
$lang['DAY_OF_WEEK'] = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
$lang['MONTH'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$lang['DATE_FORMAT'] =  '%l, %d %F, %Y';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Common Texts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Language_Link'] = 'عربي';
$lang['Language'] = 'Ar';
$lang['English'] = 'English';
$lang['Arabic'] = 'عربي';
$lang['Home'] = 'Home';
$lang['Login'] = 'Login';
$lang['Account_Services'] = 'Account Services';
$lang['Yes'] = 'Yes';
$lang['No'] = 'No';
$lang['All'] = 'All';
$lang['ID'] = 'ID';
$lang['Name'] = 'Name';
$lang['Description'] = 'Description';
$lang['NameEn'] = 'Name';
$lang['NameAr'] = 'الاسم';
$lang['DescriptionEn'] = 'Description';
$lang['DescriptionAr'] = 'الشرح';
$lang['AllowedExtensions'] = 'Allowed Extensions';
$lang['Enabled'] = 'Enabled';
$lang['Disabled'] = 'Disabled';
$lang['Deleted'] = 'Deleted';
$lang['Availability'] = 'Availability';
$lang['Minutes'] = 'Minutes';
$lang['Cancel'] = 'Cancel';
$lang['Save'] = 'Save';
$lang['Display'] = 'Display';
$lang['Add'] = 'Add';
$lang['Edit'] = 'Edit';
$lang['Delete'] = 'Delete';
$lang['Upload'] = 'Upload';
$lang['Print'] = 'Print';
$lang['AddItem'] = 'Add Item';
$lang['Email'] = 'Email';
$lang['Facet'] = 'Facet #';
$lang['Tree'] = 'Tree';

$lang['Date'] = 'Date';
$lang['Type'] = 'Type';
$lang['MaterialType'] = 'Material Type';
$lang['TheLanguage'] = 'Language';
$lang['LangEn'] = 'English';
$lang['LangAr'] = 'Arabic';
$lang['DisplayURL'] = 'Display URL';
$lang['SiteURL'] = 'Site URL';
$lang['Location'] = 'Location';
$lang['Author'] = 'Author';
$lang['Title'] = 'Title';
$lang['OriginalTitle'] = 'OriginalTitle';
$lang['Abstract'] = 'Abstract';
$lang['Contents'] = 'Contents';
$lang['Priority'] = 'Priority';
$lang['DisplayOrder'] = 'Display Order';

$lang['DataEntryDate'] = 'Data Entry Date';
$lang['LastUpdated'] = 'Last Updated';
$lang['Attachments'] = 'Attachments';
$lang['Documents'] = 'Documents';
$lang['Manage_Attachments'] = 'Manage Attachments';
$lang['Manage_CC_Documents'] = 'Manage Credit Cards Documents';

$lang['From'] = 'From';
$lang['To'] = 'To';

$lang['Next'] = 'Next';
$lang['Previous'] = 'Previous';
$lang['Thereis'] = 'There is ';
$lang['Hit'] = 'Hit(s)';
$lang['Displaying'] = 'Displaying';
$lang['Of'] = 'of';

$lang['Search'] = 'Search';
$lang['All'] = 'All';
$lang['SortBy'] = 'Sort by';
$lang['ASC'] = 'ASC';
$lang['DESC'] = 'DESC';

$lang['FileName'] = 'File Name';
$lang['File'] = 'File';
$lang['Picture'] = 'Picture';

$lang['ShowHideAll'] = 'Show/Hide All';

$lang['NumOfMaterials'] = 'Num Of Materials';
$lang['Materials'] = 'Materials';

$lang['None'] = 'None';
$lang['Send'] = 'Send';

$lang['Keyword'] = 'Keyword';

$lang['Status'] = 'Status';
$lang['Pending'] = 'Pending';
$lang['Approved'] = 'Approved';
$lang['Denied'] = 'Denied';
$lang['Activated'] = 'Activated';
$lang['Expired'] = 'Expired';

$lang['ChooseFromList'] = '---- Choose From List ----';
$lang['Choose_Picture'] = 'Choose a picture';
$lang['Choose_File'] = 'Choose a file';
$lang['NoOptions'] = 'No Options';
$lang['NewValue'] = 'New Value';
$lang['ExistingOptions'] = 'Existing Options';
$lang['ExistingFile'] = 'Existing File';
$lang['User'] = 'User';
$lang['ItemsPerPage'] = 'Items per page:';
$lang['Upload_Picture'] = 'Upload Picture';
$lang['Features'] = 'Features';
$lang['ShowOnHomePage'] = 'Show On Home Page';
$lang['RemovePicture'] = 'Remove Picture';
$lang['RemoveFile'] = 'Remove File';

$lang['Category'] = 'Category';
$lang['Publisher'] = 'Publisher';
$lang['Edition'] = 'Edition';
$lang['Price'] = 'Price';
$lang['Quantity'] = 'Quantity';
$lang['Total'] = 'Total';
$lang['NumOfPages'] = 'Number of Pages';
$lang['Cover'] = 'Cover';
$lang['Dimension'] = 'Dimension';
$lang['Capacity'] = 'Capacity';
$lang['Weight'] = 'Weight';
$lang['Letter'] = 'Letter';

$lang['Upload_Picture'] = 'Upload Picture';
$lang['Upload_Publication_Contents'] = 'Upload Publication Contents';
$lang['Upload_Publication_Intro'] = 'Upload Publication Intro';
$lang['Features'] = 'Features';
$lang['Month'] = 'Month';
$lang['Year'] = 'Year';
$lang['Issue_Number'] = 'Issue Number';
$lang['Journal'] = 'Al Mustaqbal Al Arabi Journal';

$lang['Choose_Category'] = '---------------- Choose Category ----------------';
$lang['Choose_SubGroup']  = '---------------- Choose SubGroup ----------------';
$lang['Choose_Edition'] = '- Choose Edition -';
$lang['Choose_Author'] = '---------------- Choose Author ----------------';
$lang['Choose_Examiner']    = '---------------- Choose Examiner/Translator ----------------';
$lang['Choose_Publisher'] = '---------------- Choose Publisher ----------------';
$lang['Choose_Series'] = '-------------------------- Choose Series ------------------------';
$lang['Choose_Media'] = '-------------- Choose Media Type ---------------';
$lang['Choose_Journal'] = '---------------------- Choose Journal -----------------------';
$lang['Choose_Cover']  = '---- Choose Cover ----';
$lang['Choose_Dimesnion']  = '---- Choose Dimesnion ----';

$lang['Most_Sold'] = 'Most Sold';
$lang['SpecialOffer'] = 'Special Offer';
$lang['SpecialPrice'] = 'Special Price';
$lang['Gram'] = 'Gram';

$lang['DailySalesReport'] = 'Daily Sales Report';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Titles and Alt Messages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Pickup_Date'] = 'Click Here to Pick up the date';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Error Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Error_Page_Title'] = "Warning!!!";

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Validation Messages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Check_Name'] = 'You must enter the Name!!';
$lang['Check_NameEn'] = 'You must enter the Name in English!!';
$lang['Check_NameAr'] = 'You must enter the Name in Arabic!!';
$lang['Check_Username'] = 'You must enter the username!!';
$lang['Check_Password'] = 'You must enter the password!';
$lang['Confirm_Deletion'] = 'Are you sure you want to delete this record?';
$lang['Cannot_Delete_Material_Type'] = 'Can not delete material type because it is related to one or more materials!!';

$lang['Check_Material_Type'] = 'You must choose the Material Type!!';
$lang['Check_Language'] = 'You must choose the Language!!';
$lang['Check_Location'] = 'You must choose the Material Location!!';
$lang['Check_IssueDate'] = 'You must choose the Material Issue Date!!';
$lang['Check_RevisionDate'] = 'You must choose the Material Revision Date!!';
$lang['RevisionIssueDateViolation'] = 'Revision Date cannot be less than Issue Date!!';
$lang['Check_DisplayOrder'] = 'You must enter the Display Order!!';

$lang['Check_AttachmentFile'] = 'You must choose your attachment!!';
$lang['Check_AttachmentDate'] = 'You must enter the date of the attachment!!';
$lang['Check_Extension'] = 'The extension of the selected attachment is not allowed!!';
$lang['Check_PDFDocument'] = 'Only PDF documents are allowed!!';
$lang['Check_NewsletterTitle'] = 'You must enter the newsletter title!!';
$lang['Check_Date'] = 'You must choose the Date!!';
$lang['Check_URL'] = 'You must choose the URL!!';

$lang['Check_Title'] = 'You must enter the title!!';
$lang['Check_Description'] = 'You must enter the description!!';
$lang['Check_Picture'] = 'You must choose the picture!!';
$lang['Check_Document'] = 'You must choose the document!!';
$lang['Check_ReleaseFile'] = 'You must choose the release file!!';

$lang['Check_Subject'] = 'You must enter the Newsletter Subject!!';
$lang['Check_Message'] = 'You must enter the Newsletter Message!!';

$lang['No_Newsletter'] = 'There is no choosen newsletter to be sent!!';

$lang['Check_Category'] = 'You must supply a category!!';
$lang['Check_Price'] = 'You must enter a valid price!!';
$lang['Check_Price_Validity'] = 'Make sure you enter a correct and non-zero price!!';
$lang['Check_SpecialPrice'] = 'Make sure you enter a correct price greater than ';

$lang['Check_Number'] = 'Please enter a valid number!!';
$lang['Check_Issue_Number'] = 'You must supply a non-zero issue Number!!';
$lang['Check_Series'] = 'Upon entering a series number, you should also supply a series name';

$lang['Marquee_Requires_Enabled'] = 'To Display the news on Marquee, it should be enabled!!';
$lang['Issue_Number_Zero'] = 'Issue Number cannot be zero!!';

$lang['Check_Caption1'] = 'You must enter the first caption!!';
$lang['Check_Caption2'] = 'You must enter the second caption!!';
$lang['Check_Minutes']  = 'You must enter the number of minutes!!';


// Warnings
$lang['Enter_Email_Account'] = 'Please you must enter your e-mail account!!';
$lang['Email_Missing'] = 'Your email address is missing or incorrectly spelled check!!';
$lang['Invalid_Email'] = 'Your email address does not seem to be valid - check your spelling, including wrong use of commas, or a full point . at the end of the address!!';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Index Page (Login Area)
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Index_Page_Title'] = 'Login Page';
$lang['Welcome'] = 'Welcome';
$lang['UserType'] = 'User Type:';
$lang['LastLogin'] = 'Last Login:';
$lang['Log_Out'] = 'Log Out';
$lang['Username'] = 'Username';
$lang['Password'] = 'Password';
$lang['Admin'] = 'Administrator';
$lang['Normal_User'] = 'Normal User';
$lang['Forgot_Password'] = 'Forgot Your Password?';

// Error Messages
$lang['ERR_HACKING_ATTEMPT'] = 'Hacking Attempt!';
$lang['ERR_NOT_LOGGEDIN'] = 'Sorry you are not logged in, this area is restricted to registered members only!';
$lang['ERR_RESTRICTED'] = 'Sorry you are not allowed to access this page';
$lang['ERR_USER_DISABLED'] = 'Login failed!!! Your account is disabled';
$lang['ERR_INVALID_USER'] = 'Invalid User';

// Warnings
$lang['Enter_UserName'] = 'Please enter your UserName!';
$lang['Enter_Password'] = 'Please you must enter a password!';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Material Types Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Material_Type_Index_Title'] = 'Manage Material Types';
$lang['Add_Material_Type'] = 'Add New Material Type';
$lang['Update_Material_Type'] = 'Update Material Type';
$lang['Material_Type_List'] = 'Material Types List';


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Attachment Types Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Attachment_Type_Index_Title'] = 'Manage Attachment Types';
$lang['Add_Attachment_Type'] = 'Add New Attachment Type';
$lang['Update_Attachment_Type'] = 'Update Attachment Type';
$lang['Attachment_Type_List'] = 'Attachment Types List';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Users Management Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Users_Index_Title'] = 'Manage Users';
$lang['Add_New_User'] = 'Add New User';
$lang['Update_User'] = 'Update User Info';
$lang['Users_List'] = 'Users List';
$lang['Change_Password'] = 'Change Password';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Sitemap Management Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['SiteMap_Section_Title'] = 'Manage Site Map';
$lang['SiteMap'] = 'Site Map';
$lang['Materials_Priority'] = 'Show materials according to priority';
$lang['Materials_InsertDate'] = 'Show materials according to date';
$lang['Featured_Materials_Msg'] = 'Specify the number of materials to be shown under this item';
$lang['ItemProperties'] = 'Item Properties';
$lang['DisplayMode_Msg'] = 'Specify the display mode of the materials';
$lang['DisplayMode_Text'] = 'Show the whole information of each material';
$lang['DisplayMode_Links'] = 'Show the materials titles only';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Manage Materials Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Material_Index_Title'] = 'Manage Site Materials';
$lang['Add_New_Material'] = 'Add New Material';
$lang['Update_Material'] = 'Update Material Info';
$lang['Materials_List'] = 'Materials List';
$lang['General_Info'] = 'General Info';
$lang['Page_Contents'] = 'Page Contents';
$lang['Source'] = 'Source';
$lang['Filter_Results'] = 'Filter Results';

$lang['ShowDate'] = 'Show Date on page';
$lang['NoContents'] = 'No Contents';

$lang['Material_Preview_Title'] = 'Preview Material';
$lang['Material_Preview'] = 'Preview Material';
$lang['Back_Manage_Materials'] = 'Back to Manage Materials Page';



/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Attachments Management Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Attachments_Index_Title'] = 'Manage Attachments';
$lang['Add_New_Attachment'] = 'Add New Attachment';
$lang['Update_Attachment'] = 'Update Attachment Info';
$lang['Attachments_List'] = 'Attachments List';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Menu Setup Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Menu_Setup_Title'] = 'Menu Setup';
$lang['CheckUncheckAll'] = 'Check/Uncheck All';
$lang['MainLinks'] = 'Main Links';
$lang['MenuLeft'] = 'Left Menu';
$lang['MenuRight'] = 'Right Menu';
$lang['MenuTop'] = 'Top Menu';
$lang['MenuBottom'] = 'Bottom Menu';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                         Home Page Management Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['HomePage_Index_Title'] = 'Manage Home Page';
$lang['Add_HomePage_Entry'] = 'Add New Home Page Entry';
$lang['Update_HomePage_Entry'] = 'Update Home Page Entry';
$lang['HomePage_Entries_List'] = 'Home Page Entries List';
$lang['Material_Types'] = 'Material Types';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                   User Defined Controls Management Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['UserDefinedControls_Index_Title'] = 'Manage User Defined Controls Page';
$lang['Add_Control'] = 'Add New Control';
$lang['Update_Control'] = 'Update Control';
$lang['Controls_List'] = 'User Defined Controls List';
$lang['Manage_Control_Materials'] = 'Manage Control Materials';
$lang['Control_Materials_List'] = 'Control Materials List';
$lang['Update_Control_Material'] = 'Update Material';
$lang['Add_New_Control_Material'] = 'Add New Material';
//--------------------------------------------------------------------------
$lang['ControlMaterial_Kind_Desc'] = 'Specify control\'s materials kind';
$lang['ControlMaterial_Kind1'] = 'Independent material';
$lang['ControlMaterial_Kind2'] = 'Location of related materials';
$lang['ControlMaterial_Notes'] = 'You can\'t choose locations with light colors because they are not locations of related materials';


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                         Newsletter Management Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Newsletter_Index_Title'] = 'Manage Newsletter';
$lang['Add_Newsletter'] = 'Add New Newsletter';
$lang['Update_Newsletter'] = 'Update Newsletter';
$lang['Newsletters_List'] = 'Newsletters List';
$lang['Subject'] = "Subject";
$lang['IssuedDate'] = "Issued Date";
$lang['Message'] = "Message";

// Newsletter_Send Page
$lang['Newsletter_Send_Title'] = 'Send Newsletter';
$lang['Newsletters_Types'] = 'Newsletters [Not Sent/Sent]';
$lang['NotSent_Newsletters'] = 'Not Sent Newsletters';
$lang['Sent_Newsletters'] = 'Sent Newsletters';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Publication Category Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Publication_Category_Title'] = 'Manage Publication Categories';
$lang['Add_Publication_Category'] = 'Add New Publication Category';
$lang['Update_Publication_Category'] = 'Update Publication Category';
$lang['Publication_Category_List'] = 'Publication Categories List';
$lang['Publication_Category_Index_Type'] = 'Manage Publication Categories';
$lang['Cannot_Delete_Publication_Category'] = 'Cannot delete this publication category because it is related to one or more publications!';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Publication Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Publication_Title'] = 'Manage Publications';
$lang['Publication_Index_Type'] = 'Manage Publications';
$lang['Add_Publication'] = 'Add New Publication';
$lang['Publication_List'] = 'Publications List';
$lang['Media_Type'] = 'Media Type';
$lang['Summary'] = 'Summary';
$lang['Publication_Info'] = 'Publication Info';
$lang['Publication_Other_Info'] = 'Publication Other Info';
$lang['ShowOnHomePage'] = 'Show On HomePage';
$lang['WithPicture'] = 'With Picture';
$lang['WithWeight'] = 'With Weight';
$lang['Publication_Date'] = 'Publication Date';
$lang['Series_Info'] = 'Series Info';
$lang['Publication_General_Info'] = 'Publication General Info';
$lang['pub_media_name'] = 'Name_En';
$lang['Publication_Edition_Name'] = 'Name_En';
$lang['New_Publisher'] = 'Add New Publisher';
$lang['New_Author'] = 'Add New Author';
$lang['NoPublisher'] = 'There are currently no publishers';
$lang['NoAuthors'] = 'There are currently no authors';
$lang['Existing_Authors'] = 'Existing Authors';
$lang['Examiner'] = 'Examiner/Translator';
$lang['New_Examiner'] = 'Add New Examiner/Translator';
$lang['NoExaminers'] = 'There are currently no names';
$lang['Existing_Examiners'] = 'Existing Examiners/Translators';
$lang['NoSeries'] = 'There are currently no series';
$lang['New_Series'] = 'Add New Series';
$lang['Existing_Series'] = 'Existing Series';
$lang['Existing_Publishers'] = 'Existing Publishers';
$lang['Choose_Picture'] = 'Choose a picture';
$lang['Choose_File'] = 'Choose a file';
$lang['Update_Publication'] = 'Update Publication';
$lang['SubGroup'] = 'Subgroup';
$lang['New_SubGroup'] = 'Add New Subgroup';
$lang['NoSubGroups'] = 'There are currently no subgroups';
$lang['Existing_SubGroups'] = 'Existing Subgroups';
$lang['AvailableAseBook'] = 'Available as eBook';
$lang['FeaturedPublications'] = 'Featured Publications';
$lang['Featured'] = 'Featured';

//-----------------------------------------------------
$lang['New_Cover'] = 'Add New Cover';
$lang['NoCovers'] = 'There are currently no covers';
$lang['Existing_Covers'] = 'Existing Covers';
$lang['New_Dimension'] = 'Add New Dimension';
$lang['NoDimensions'] = 'There are currently no dimensions';
$lang['Existing_Dimensions'] = 'Existing Dimensions';

$lang['RemovePicture'] = 'Remove Picture';
$lang['RemoveFile'] = 'Remove File';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Electronic Magazine Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Emag_Index_Title'] = 'Manage Electronic Magazine';
$lang['Add_New_Emag'] = 'Add New Electronic Magazine';
$lang['Emag_List'] = 'Electronic Magazine List';
$lang['Emag_Date'] = 'Electronic Magazine Date';
$lang['Emag_General_Info'] = 'Electronic Magazine General Info';
$lang['ChooseJournal'] = 'Choose Journal';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Electronic Magazine Article Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Articles_Index_Title'] = 'Manage Electronic Magazine Articles';
$lang['Articles'] = 'Articles';
$lang['Manage_Articles'] = 'Manage Articles';
$lang['Articles_List'] = 'Articles List';
$lang['Update_Article'] = 'Update Article';
$lang['Add_Article'] = 'Add New Article';
$lang['Article_DocumentName'] = "Article Document Name";
$lang['Add_New_Article'] = "Add New Article";

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Latest News Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Latest_News_Index_Type'] = 'Manage Latest News';
$lang['Latest_News_List'] = 'Latest News Lists';
$lang['Add_Latest_News'] = 'Add News Entry';
$lang['Update_Latest_News'] = 'Update Latest News';
$lang['Show_On_Marquee'] = 'Show On Marquee';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Orders List Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Order_Index_Title'] = 'Reporting Module Interface';
$lang['PurchaseHistory'] = 'Purchase History';
$lang['OrderDetails'] = 'Order Details';
$lang['OrdersHistory'] = 'Orders History';
$lang['OrderID'] = 'Order ID';
$lang['TransactionID'] = 'Transaction ID';
$lang['_OrderDetails'] = 'Details';
$lang['_OrderID'] = 'Order ID';
$lang['OrderDate'] = 'Order Date';
$lang['OrderTime'] = 'Order Time';
$lang['OrderAmount'] = 'Amount';
$lang['AdditionalCredits'] = 'Additional Credits';
$lang['DiscountCoupon'] = 'Discount Coupon';
$lang['OrderStatus'] = 'Order Status';
$lang['OrderReportType'] = 'Report Type';
$lang['OrderPending'] = 'Pending';
$lang['OrderInProgress'] = 'InProgress';
$lang['OrderRefused'] = 'Refused';
$lang['OrderReversed'] = 'Reversed';
$lang['OrderCompleted'] = 'Completed';
$lang['Product'] = 'Product';
$lang['Payment'] = 'Payment';
$lang['BuyerName'] = 'Buyer Name';
$lang['Customer'] = 'Customer';
$lang['UnitPrice'] = 'Unit Price';
$lang['ExportExcel'] = 'Export to Excel';
$lang['ProductType'] = 'Product Type';
$lang['PaymentType'] = 'Payment Type';
$lang['DownloadExcel'] = 'Download Excel File';

$lang['DenialReason'] = 'Denial Reason';

$lang['Order_Details_Title'] = 'Order Details';
$lang['Order_Information'] = 'Order Information';
$lang['TotalWithShipping'] = 'Total With Shipping';

$lang['CustomerInformation'] = 'Customer Information';
$lang['Address'] = 'Address';
$lang['Country'] = 'Country';
$lang['City'] = 'City';
$lang['State'] = 'State';
$lang['ZipCode'] = 'Zip Code';
$lang['P.O.Box'] = 'P.O.Box';
$lang['Phone1'] = 'Phone 1';
$lang['Phone2'] = 'Phone 2';
$lang['Fax'] = 'Fax';
$lang['JoinDate'] = 'Join Date';

$lang['ShippingInformation'] = 'Shipping Information';
$lang['RecipientName'] = 'Recipient Name';
$lang['Customer_Type']='Customer Type';
$lang['AccountVerificationStatus'] = 'Account Verification Status';
$lang['LastVerifiedOn'] = 'Last Verified On';
$lang['Twostepverificationstatus'] = 'Two step verification status';
$lang['AutomatedBillingStatus'] = 'Automated Billing Status';
$lang['AutomatedBillingPaymentMethod'] = 'Automated Billing Payment Method';
$lang['LastAutomatedTransactionDate'] = 'Last Successful Automated Billing Transaction Date';
$lang['LastAutomatedTransactionAmount'] = 'Last Successful Automated Billing Transaction Amount';
$lang['AutomatedBillingActivationDate'] = 'Automated Billing Activation Date';
$lang['AutomatedBillingDeActivationDate'] = 'Automated Billing De-Activation Date';
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Subscribtions Pages Texts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['SubscribtionType1'] = 'Magazine Subscribtions';
$lang['SubscribtionType2'] = 'Supporter Subscribtions';
$lang['SubscribtionType3'] = 'Subscriber Subscribtions';
$lang['SubscribtionType4'] = 'Special Offer Subscribtions';

$lang['SubscribtionInformation'] = 'Information About Subscribtion No.';
$lang['SubscribtionDetails'] = 'Subscribtion Details';
$lang['SubscribtionID'] = 'Subscribtion ID';
$lang['SubscribtionDate'] = 'Subscribtion Date';
$lang['SubscribtionType'] = 'Subscribtion Type';
$lang['SubscribtionAmount'] = 'Subscribtion Amount';
$lang['SubscriberName'] = 'Subscriber Name';
$lang['PaymentMethod'] = 'Payment Method';

$lang['Annual'] = 'Annual Subscribtion';
$lang['LifeTime'] = 'Lifetime Subscribtion';
$lang['StartingDate'] = 'Starting Date';
$lang['EndingDate'] = 'Ending Date';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Member User Pages Texts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['MU_Index_Title'] = 'Customers List / Website Subscribers';
$lang['Keyword'] = 'Keyword';
$lang['Details'] = 'Details';
$lang['FName'] = 'First Name';
$lang['MName'] = 'Middle Name';
$lang['LName'] = 'Last Name';
$lang['TotalOrders'] = 'Total Orders';
$lang['AccountType'] = 'Account Type';
$lang['AccountStatus'] = 'Account Status';
$lang['AutomatedBillingStatus'] = 'Automated Billing Status';
$lang['AutomatedBilling'] = 'Automated Billing';
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Banner Pages Texts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Banner_Index_Title'] = 'Manage Banners';
$lang['Add_New_Banner'] = 'Add New Banner';
$lang['Update_Banner'] = 'Update Banner';
$lang['Banners_List'] = 'Banners List';
$lang['Banner_Location'] = 'Location';
$lang['Location_Left'] = 'Left';
$lang['Location_Right'] = 'Right';
$lang['Alt'] = 'Alt';
$lang['URL'] = 'URL';
$lang['Target'] = 'Target';
$lang['_blank'] = 'New Window';
$lang['_self'] = 'Same Window';
$lang['DisplayOrder'] = 'Display Order';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                         	Resellers Page Text
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Reseller'] = 'Reseller';
$lang['Reseller_Information'] = 'Reseller Information';
$lang['Resellers_Index_Title'] = 'Reseller Requests';
$lang['FullName'] = 'Full Name';
$lang['Notes'] = 'Notes';
$lang['Company'] = 'Company';
$lang['Mobile'] = 'Mobile';
$lang['Phone'] = 'Phone';
$lang['BusinessType'] = 'Business Type';
$lang['SalesAmount'] = 'Sales Amount';

$lang['Status'] = 'Status';
$lang['Pending'] = 'Pending';
$lang['Approved'] = 'Approved';
$lang['Denied'] = 'Denied';
$lang['Approve_Deny'] = 'Approve/Deny';
$lang['Reason'] = 'Denial Reason';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Web Statistics Texts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Web_Statistics_Index_Title'] = 'Website Statistics';
$lang['VisitedPage'] = 'Visited Page';
$lang['IP'] = 'IP';
$lang['PreviousURL'] = 'Previous URL';
$lang['VisitedURL'] = 'Visited URL';
$lang['DownloadPage'] = 'Download Page';
$lang['TotalVisits'] = 'Total Visits';
$lang['Visits'] = 'Visits';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                    User Management (Roles & Permissions)
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Role & User Roles Management Pages *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Role_Title'] = 'Manage Roles';
$lang['UserRoles_Title'] = 'Manage User Roles';
$lang['Role'] = 'Role';
$lang['Roles'] = 'Roles';
$lang['Roles_List'] = 'Roles List';
$lang['Add_New_Role'] = 'Add New Role';
$lang['Update_Role'] = 'Update Role';
$lang['Permissions'] = 'Permissions';
$lang['AssignRoles'] = 'Click here to specify the roles of this user';
$lang['AssignPermissions'] = 'Click here to specify the permissions of this role';
$lang['Cannot_Delete_Role'] = 'Can not delete this role because it is assigned to one or more users!!';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Role Permissions Management Page *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Role_Permissions_Title'] = 'Manage Role Permissions';
$lang['BackOffice_Permissions'] = 'BackOffice Permissions';
$lang['PublicSite_Permissions'] = 'Public Site Permissions';
$lang['Role_Permissions'] = '%s Permissions';
$lang['Other_Permissions'] = 'Other Permissions';
$lang['Allow'] = 'Allow';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                          Credit Card Details
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Orders_With_CC'] = 'Orders With Credit Cards';
$lang['CreditCard_Index_Title'] = 'Credit Cards List';
$lang['PreAuthorized_Index_Title'] = 'Pre-Authorized Transactions';
$lang['General_PreAuthorized_Index_Title'] = 'General Pre-Authorized Transactions';
$lang['Moneybookers_PreAuthorized_Index_Title'] = 'Moneybookers Pre-Authorized Transactions';
$lang['CreditCardNumber'] = 'CC Number';
$lang['CreditCardType'] = 'CC Type';
$lang['CreditCardStatus'] = 'CC Status';
$lang['HolderName'] = 'CC Holder Name';
$lang['IssuerBank'] = 'CC Issuer Bank';
$lang['ExpiryMonth'] = 'Expiry Month';
$lang['ExpiryYear'] = 'Expiry Year';
$lang['ExpiryDate'] = 'Exp. Date';
$lang['CVV2'] = 'CVV2';
$lang['PurchaseDate'] = 'Purchase Date';
$lang['CCCountry'] = 'CC Country';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Order Statuses Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Order_Status_Index_Title'] = 'Manage Order Statuses';
$lang['Add_Order_Status'] = 'Add New Order Status';
$lang['Update_Order_Status'] = 'Update Order Status';
$lang['Order_Status_List'] = 'Order Statuses List';
$lang['HasReasons'] = 'Has Reasons';
$lang['Order_Status_Reasons_Index_Title'] = 'Manage Order Statuses Reasons';
$lang['Add_Order_Status_Reason'] = 'Add New Order Status Reason';
$lang['Update_Order_Status_Reason'] = 'Update Order Status Reason';
$lang['Order_Status_Reasons_List'] = 'Order Statuses Reasons List';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Credit Card Documents Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['CC_Document_Types_Index_Title'] = 'Manage Credit Cards Documents Types';
$lang['Add_CC_Document_Type'] = 'Add New Credit Card Document Type';
$lang['Update_CC_Document_Type'] = 'Update Credit Card Document Type';
$lang['CC_Document_Types_List'] = 'Credit Card Document Types List';

$lang['CC_Documents_Index_Title'] = 'Manage Credit Cards Documents';
$lang['Add_New_CC_Document'] = 'Add New Credit Card Document';
$lang['Update_CC_Document'] = 'Update Credit Card Info';
$lang['CC_Documents_List'] = 'Credit Cards Documents List';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                            Reporting Modules Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang[$config['REPORT_VALUE']['POL']] = 'Purchase Orders List Report';
$lang[$config['REPORT_VALUE']['CAR']] = 'Cutomer Analysis Report';
$lang[$config['REPORT_VALUE']['TRDPP']] = 'Total Revenue per Payment-Product Report';
$lang['GeneralStats'] = 'General Stats';
$lang['TotalQuantity'] = 'Total Quantity';
$lang['TotalAmount'] = 'Total Amount';
$lang['GrandTotal'] = 'Grand Total';
$lang[$config['REPORT_VALUE']['VOICE']]='Voice Report';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                    News & Events Management
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Manage_News'] = 'Manage News & Events';
$lang['News_List'] = 'News & Events List';
$lang['Add_New_News'] = 'Add New News/Events';
$lang['Update_News'] = 'Update News/Events';
$lang['NewsID'] = 'News ID';
$lang['NewsInfo'] = 'News Info';
$lang['NewsType'] = 'News Type';
$lang['URLInfo'] = 'URL Info';
$lang['URL'] = 'URL';
$lang['Target'] = 'Target';
$lang['External'] = 'External';
$lang['_blank'] = 'New Window';
$lang['_self'] = 'Same Window';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                    Calling Plans Page
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Plan'] = 'Plan';
$lang['CallingPlans'] = 'Calling Plans';
$lang['CustomerCallingPlans'] = 'Customer\'s Calling Plans';
$lang['CustomerDIDPlans'] = 'Customer\'s DID Plans';
$lang['StartDate'] = 'Start Date';
$lang['EndDate'] = 'End Date';
$lang['ExpiredDate'] = 'Expired Date';
$lang['TotalMinutes'] = 'Total Minutes';
$lang['RemainingMinutes'] = 'Remaining Minutes';
$lang['UsedMinutes'] = 'Used Minutes';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                    Packages and Plans Pages
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Packages_Index_Title'] = 'Manage Packages';
$lang['HasMultiPlans'] = 'Multiple Plans';
$lang['Caption1'] = 'Caption 1';
$lang['Caption2'] = 'Caption 2';
$lang['IsFixedPrice'] = 'Fixed Price';
$lang['ColorTheme'] = 'Color Theme';
$lang['Add_Package'] = 'Add New Package';
$lang['Update_Package'] = 'Update Package';
$lang['Packages_List'] = 'Packages List';
$lang['Plans_Index_Title'] = 'Manage Plans';
$lang['NewPlan'] = 'New Plan';
$lang['Add_Plan'] = 'Add New Plan';
$lang['PackageName'] = 'Package Name';
$lang['I_VD_Plan'] = 'I_VD_Plan';
$lang['Add_Plan'] = 'Add New Plan';
$lang['Update_Plan'] = 'Update Plan';
$lang['Plans_List'] = 'Plans List';
$lang['Countries_Old'] = 'Already Assigned Countries';
$lang['Countries_New'] = 'Newly Assigned Countries';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                           Ukash Text
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['TransactionStatus'] = 'Transaction Status';
$lang['VoucherNumber'] = 'Voucher Number';
$lang['VoucherValue'] = 'Voucher Value';
$lang['VoucherCurrency'] = 'Voucher Currency';
$lang['RedemptionType'] = 'Redemption Type';
$lang['ChangeVoucherNumber'] = 'Change Voucher Number';
$lang['ChangeVoucherValue'] = 'Change Voucher Value';
$lang['ChangeVoucherExpiryDate'] = 'Change Voucher Expiry Date';
$lang['AmountAddedToBalance'] = 'Amount Added to Balance';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                           Conversions Text
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['CampaignName'] = 'Campaign Name';
$lang['ConversionName'] = 'Conversion Name';
$lang['NumberOfClicks'] = 'Number Of Clicks';
$lang['Amount'] = 'Amount';
$lang['Conversions_Index_Title']='Conversions Statistics';
$lang['GrpBy']='Group By';
$lang['Campaign']='Campaign';
$lang['Banner']='Banner';
$lang['BannerName']='Banner Name';

/*--------DID management----------*/
$lang['DID_Index_Title']= 'DID Number List';
$lang['DidNumber'] = 'DID Number';
$lang['DidArea'] = 'Area';
$lang['Did_Plan']='DID Plan';
$lang['Did_Plan_Name']='DID Plan Name';
$lang['Add_Package_Subscription']="Add New Subscription Plan";
$lang['Currency']="Currency";
$lang['Managed_By']="Managed By";
$lang['Description']="Description";
$lang['Subscription_Plan_Index_Title']="Subscription Plan";
$lang['Subscription_Plan_List']="Subscription Plan List";
$lang['General_Info']="General Info";
$lang['Invoice_Line_Description']="Invoice Line Description";
$lang['Charge_Suspended_Customers']="Charge Suspended Customers";
$lang['Subscription_Is_Activated']="Subscription Is Activated";
$lang['Activation_Fee']="Activation Fee";
$lang['Minimum_Subscription_Period']="Minimum Subscription Period";
$lang['Early_Cancellation_Penalty']="Early Cancellation Penalty";
$lang['Subscription_Charges_Applied']="Subscription Charges Applied";
$lang['Periods_in_Advance']="Periods in Advance";
$lang['Round_Charged_Amount']="Round Charged Amount";
$lang['Monthly_billing']="Monthly Billing";
$lang['BI_Monthly_billing']="BI Monthly Billing";
$lang['weekly_billing']="Weekly Billing";
$lang['Daily_billing']="Daily Billing ";
$lang['view_details']="View Details ";
$lang['ActiveDIDPlansDetails'] ="Active DID Plans Details"; //description
$lang['description'] ="Description";
$lang['activation_mode'] = "Activation Mode";
$lang['invoice_description']="Invoice Description";
$lang['activation_fee']="Activation Fee";
$lang['minimum_period']="Minimum Period";
$lang['cancel_penalty'] ="Cancel Penalty";
$lang['charge_model']="Charge Model";
$lang['rounding'] ="Rounding";
$lang['advance_periods']="Advance Periods";
$lang['tax_info']="Tax Info";
$lang['cancel_penalty_prorated']="Cancel Penalty Prorated"; 
$lang['multiple']="Multiple";
$lang['documentrequired']="Document Required";
$lang['documentrequiredlist']="Documents List";
$lang['documentstatus']="Document Status";
$lang['verifed']="Verify";
$lang['verifedrequired']="Not Verified";
$lang['slsactions']="SLS Action";
$lang['DID_Status'] = 'DID Status';
$lang['BilledDate'] = 'Billed To date';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Voice Statistics Texts
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Voice_Statistics_Index_Title'] = 'Voice Statistics';
$lang['GroupBy'] = 'Group By';
$lang['Description'] = 'Description';
$lang['AnsweredCalls'] = 'Answered Calls';
$lang['AnsweredDuration'] = 'Answered Duration';
$lang['TotalSeizures'] = 'Total Seizures';
$lang['TotalRevenue'] = 'Total Revenue';
$lang['ASR'] = 'ASR';
$lang['ACD'] = 'ACD';
$lang['Env'] = 'Environment';
$lang['AR-M'] = 'AR/M';
$lang['from'] ='From';
$lang['to']='To';
$lang['datetime']='Date/Time';
$lang['Hour']='Hour';
$lang['Country-Description'] = 'Country and Description';
$lang['Country-Description-Day'] = 'Country, Description and Day';
$lang['Country-Description-Day-Hour'] = 'Country, Description, Day and Hour';
$lang['Country-Description-Day-Vendor'] = 'Country, Description, Day and Vendor';
$lang['Country-Description-Day-Hour-Vendor'] = 'Country, Description, Day, Hour and Vendor';

$lang['Day'] = 'Day';
$lang['Hour'] = 'Hour';
$lang['Vendor'] = 'Vendor';
$lang['SubscriberIP'] = 'Subscriber IP';
$lang['DisconnectCause'] = 'Disconnect Cause';
$lang['Can_be_applied_more_than_once'] = 'Can be applied more than once';
$lang['Minimum_Subscription_Period'] = 'Minimum Subscription Period';

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
*                             Email Template Manage
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
$lang['Email_Template_Index_Title'] = "Notification Email Template";
$lang['ET_Index_Title'] = "Notification Email";
$lang['Notification_Value'] = "Notification value";
$lang['Notification_Type'] = "Notification Type";
$lang['Add_New_Notification'] = "Add New Notification";
$lang['Notification_Name'] = "Notification Name";
$lang['Notification_List'] = "Notification List";
$lang['Notification_Detail'] = "Notification Details";
$lang['Process_Step'] = "Process Step";
$lang['Notification_Detail'] = "Notification Details";
$lang['Notification_Reason'] = "Notification Reason";
$lang['From_Email'] = "From Email";
$lang['To_Email'] = "To Email";
$lang['CC_Email'] = "CC Email";
$lang['Bcc_Email'] = "BCC Email";
$lang['ReplyTo_Email'] = "Reply To Email";
$lang['Notification_Subject'] = "Notification Subject";
$lang['Notification_Body'] = "Notification Body";
$lang['Template_Path'] = "Template Path";
$lang['Parameters']= "Parameters";
?>