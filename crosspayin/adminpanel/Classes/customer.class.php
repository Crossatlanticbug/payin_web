<?

// Class Definition
class customer
{
	
	var $Criteria;
	var $SortField;
	var $SortType;
	var $Start = -1;
	var $Num = -1;
	/*
	 * Class constructor
	 */
  function __construct($ID = -1)
  {
  	if ((!empty($ID)) && ($ID != -1))
  	{
  		$this->ID = $ID;
			$this->getInfo();
		}
	}

	/*
	 * Retrieves order information
	 */
	function getInfo()
	{
		global $dbLink;

		$query  = "Select * From `customer` Where id = '" . $this->ID . "'";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);

   
  }

	
	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getList()
	{
		global $dbLink;
		global $config;
		global $lang;

		
		$statusIDFieldname = "customer.ID";
		$statusIDInnerJoin = '';
		$DiscountCouponInnerJoin = "";
		$DiscountCouponFieldName = "";

		$query = "Select * From `customer` where isreseller=0 ";
		
		// Name ------------------------------
		if($this->Criteria['Name'] != '') {
			$query .= "And (name Like '%" . $this->Criteria['Name'] . "%') ";
		}
		
		// Email ------------------------------
		if($this->Criteria['Email'] != '') {
			$query .= "And (email Like '%" . $this->Criteria['Email'] . "%') ";
		}
		
		// ID ------------------------------
		if($this->Criteria['ID'] != '') {
			$query .= "And (id = '" . $this->Criteria['ID'] . "') ";
		}
		
		// Phone ------------------------------
		if($this->Criteria['Phone'] != '') {
			$query .= "And (phone = '" . $this->Criteria['Phone'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['UserIP'] != '') {
			$query .= "And (userip = '" . $this->Criteria['UserIP'] . "') ";
		}
		
		// Balance ------------------------------
		if($this->Criteria['Balance'] != '') {
			$query .= "And (balance = '" . $this->Criteria['Balance'] . "') ";
		}
		
		// City ------------------------------
		if($this->Criteria['City'] != '') {
			$query .= "And (city = '" . $this->Criteria['City'] . "') ";
		}
		
		// Order By
		$query .= "ORDER BY ".$this->SortField . " " .$this->SortType . " ";

		// Limit Results
		if (($this->Start!=-1) && ($this->Num!=-1) ) {
			$query .= "LIMIT ".$this->Start.", ".$this->Num;
			}
		//echo($query);
		//die();	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_errno($dbLink)) { 
		echo $error = "MySQL error ".mysqli_errno($dbLink).": ".mysqli_error($dbLink)."\n<br>When executing:<br>\n$result\n<br>"; 
		}
		
		return $result;
	}

	
	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                         General Authorization Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the count of orders different from NetCommerce and having Status equal to "InProgress" available according to the given search criteria
	function getTotalGeneral($Criteria)
	{
		global $dbLink;
		global $language;

		$query  = "Select COUNT(*) From `customer` ";
		$query .= "Where 1=1 ";

		// User ID ------------------------------------------------------------
		if($Criteria['ID'] != '') {
			$query .= "And (id = " . $Criteria['ID'] . ") ";
		}
		// Name ------------------------------
		if($Criteria['Name'] != '') {
			$query .= "And (name = '" . $Criteria['Name'] . "') ";
		}
		// Email ------------------------------
		if($Criteria['Email'] != '') {
			$query .= "And (email = '" . $Criteria['Email'] . "') ";
		}
		// Phone ------------------------------
		if($Criteria['Phone'] != '') {
			$query .= "And (phone = '" . $Criteria['Phone'] . "') ";
		}
		// Balance ------------------------------
		if($Criteria['Balance'] != '') {
			$query .= "And (balance = '" . $Criteria['Balance'] . "') ";
		}
		// City ------------------------------
		if($Criteria['City'] != '') {
			$query .= "And (city = '" . $Criteria['City'] . "') ";
		}
	  // echo $query;
   	//$result = mysqli_result(mysqli_query($dbLink, $query), 0, 0);

   // return $result;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getListGeneral($Criteria, $SortField, $SortType, $Start = -1, $Num = -1)
	{
		global $dbLink;

		$query  = "Select ID, name, email, phone, userip, balance From customer Where 1=1 ";
		
		// User ID ------------------------------------------------------------
		if($Criteria['ID'] != '') {
			$query .= "And (id = " . $Criteria['ID'] . ") ";
		}
		// Name ------------------------------------------------------------
		if($Criteria['Name'] != '')
		{
			$query .= "And (name Like '%" . $Criteria['Name'] . "%') ";
		}
		// Email ------------------------------
		if($Criteria['Email'] != '') {
			$query .= "And (email = '" . $Criteria['Email'] . "') ";
		}
		// Balance ------------------------------
		if($Criteria['Email'] != '') {
			$query .= "And (balance = '" . $Criteria['Balance'] . "') ";
		}
		// Phone ------------------------------
		if($Criteria['Phone'] != '') {
			$query .= "And (city = '" . $Criteria['Phone'] . "') ";
		}
		// City ------------------------------
		if($Criteria['City'] != '') {
			$query .= "And (city = '" . $Criteria['City'] . "') ";
		}
		

		// Order By
		$query .= "Order By $SortField $SortType ";

		// Limit Results
		if (($Start!=-1) && ($Num!=-1)) {
     $query .= "Limit $Start,$Num";
    }

		// echo($query);

		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to delete a certain order
  function deletecustomer($ID)
  {
  	global $dbLink;

    $query  = "Delete From `customer` Where id = $ID";
    mysqli_query($dbLink, $query);
	}

	// This function is used to get the information of a certain order
	function getcustomerInfo($id)
	{
		global $dbLink;

		$query  = "Select * From `customer` Where id = '" . $id . "'";
		$result = mysqli_query($dbLink, $query);
		return $row = mysqli_fetch_object($result);
	}


	function getResellerList()
	{
		global $dbLink;
		global $config;
		global $lang;

		
		$statusIDFieldname = "customer.ID";
		$statusIDInnerJoin = '';
		$DiscountCouponInnerJoin = "";
		$DiscountCouponFieldName = "";

		$query = "Select * From `customer` where isreseller!=0 ";
		
		// Name ------------------------------
		if($this->Criteria['Name'] != '') {
			$query .= "And (name Like '%" . $this->Criteria['Name'] . "%') ";
		}
		
		// Email ------------------------------
		if($this->Criteria['Email'] != '') {
			$query .= "And (email Like '%" . $this->Criteria['Email'] . "%') ";
		}
		
		// ID ------------------------------
		if($this->Criteria['ID'] != '') {
			$query .= "And (id = '" . $this->Criteria['ID'] . "') ";
		}
		
		// Phone ------------------------------
		if($this->Criteria['Phone'] != '') {
			$query .= "And (phone = '" . $this->Criteria['Phone'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['UserIP'] != '') {
			$query .= "And (userip = '" . $this->Criteria['UserIP'] . "') ";
		}
		
		// Balance ------------------------------
		if($this->Criteria['Balance'] != '') {
			$query .= "And (balance = '" . $this->Criteria['Balance'] . "') ";
		}
		
		// City ------------------------------
		if($this->Criteria['City'] != '') {
			$query .= "And (city = '" . $this->Criteria['City'] . "') ";
		}
		
		// Order By
		if (($this->SortField) && ($this->SortType) ) {
		$query .= "ORDER BY ".$this->SortField . " " .$this->SortType . " ";
		}
		// Limit Results
		if (($this->Start!=-1) && ($this->Num!=-1) ) {
			$query .= "LIMIT ".$this->Start.", ".$this->Num;
			}
		//echo($query);
		//die();	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_errno($dbLink)) { 
		echo $error = "MySQL error ".mysqli_errno($dbLink).": ".mysqli_error($dbLink)."\n<br>When executing:<br>\n$result\n<br>"; 
		}
		
		return $result;
	}
	
	function addNewReseller($objreseller){
		
		global $dbLink;
		addslashes_object_vars($objreseller);

	
		// Get current date. Note that the date format for MySQL is Y-m-d (ex:2004-04-15)
		$creation_date = date("Y-m-d H:i:s");


		// Insert the information of the new created user into the database
		$query = "Insert Into `customer` (`phone`,`email`,`name`,`password`,`balance`,`address`,`city`,`state`,`zipcode`,`creationdate`,`enabled`,`isreseller`,`resellertype`) ";
		$query .= "Values ('$objreseller->phone','$objreseller->email','$objreseller->name','$objreseller->password','$objreseller->balance','$objreseller->address',";
		$query .= "'$objreseller->city','$objreseller->state','$objreseller->zipcode','$creation_date','$objreseller->enabled','$objreseller->isreseller','$objreseller->resellertype')";
		//echo $query;
		mysqli_query($dbLink, $query);
		return  mysqli_insert_id($dbLink);
	}
	
	
	function updateResellerInfo($id, $objreseller){
		
		global $dbLink;
		addslashes_object_vars($objreseller);

	
		// Get current date. Note that the date format for MySQL is Y-m-d (ex:2004-04-15)
		$creation_date = date("Y-m-d H:i:s");


		// Insert the information of the new created user into the database
		$query = "update `customer` set `phone`='$objreseller->phone',`email`='$objreseller->email',`name`='$objreseller->name'";
		$query .= ",`password`='$objreseller->password',`address`='$objreseller->address',`city`='$objreseller->city'";
		$query .= ",`state`='$objreseller->state',`zipcode`='$objreseller->zipcode',`enabled`='$objreseller->enabled' ";
		$query .= ",`resellertype`='$objreseller->resellertype',`isreseller`='$objreseller->isreseller' ";
		$query .= " where id='$id' ";

		mysqli_query($dbLink, $query);
		
	}
	
	function getResellerTypeList()
	{
		global $dbLink;

		$query  = "Select * From `resellertype` order by resellertype asc";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}
	
	function addResellerType($objreseller){
		
		global $dbLink;
		addslashes_object_vars($objreseller);

	
		// Get current date. Note that the date format for MySQL is Y-m-d (ex:2004-04-15)
		$creation_date = date("Y-m-d H:i:s");


		// Insert the information of the new created user into the database
		$query = "Insert Into `resellertype` (`resellertype`,`profitlevel`,`status`) ";
		$query .= "Values ('$objreseller->resellertype','$objreseller->profitlevel','$objreseller->enabled')";
		//echo $query;
		mysqli_query($dbLink, $query);
		return  mysqli_insert_id($dbLink);
	}
	
	function updateResellerType($id, $objreseller){
		
		global $dbLink;
		addslashes_object_vars($objreseller);

	
		// Get current date. Note that the date format for MySQL is Y-m-d (ex:2004-04-15)
		$creation_date = date("Y-m-d H:i:s");


		// Insert the information of the new created user into the database
		$query = "update `resellertype` set `resellertype`='$objreseller->resellertype',`profitlevel`='$objreseller->profitlevel',`status`='$objreseller->enabled'";
		$query .= " where id='$id' ";
		//echo $query;
		mysqli_query($dbLink, $query);
		
	}
	function getResellerType($id)
	{
		global $dbLink;

		$query  = "Select * From `resellertype` where id ='$id' ";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}
	
	function getResellerRequestList()
	{
		global $dbLink;
		global $config;
		global $lang;

		
		$statusIDFieldname = "customer.ID";
		$statusIDInnerJoin = '';
		$DiscountCouponInnerJoin = "";
		$DiscountCouponFieldName = "";

		$query = "Select * From `becomepartner` where 1=1 and isvalid='1' ";
		
		// Name ------------------------------
		if($this->Criteria['Name'] != '') {
			$query .= "And (name Like '%" . $this->Criteria['Name'] . "%') ";
		}
		
		// Email ------------------------------
		if($this->Criteria['Email'] != '') {
			$query .= "And (email Like '%" . $this->Criteria['Email'] . "%') ";
		}
		
		// ID ------------------------------
		if($this->Criteria['ID'] != '') {
			$query .= "And (id = '" . $this->Criteria['ID'] . "') ";
		}
		
		// Phone ------------------------------
		if($this->Criteria['Phone'] != '') {
			$query .= "And (phoneno = '" . $this->Criteria['Phone'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['UserIP'] != '') {
			$query .= "And (userip = '" . $this->Criteria['UserIP'] . "') ";
		}
		
				
				
		// Order By
		if (($this->SortField) && ($this->SortType) ) {
		$query .= "ORDER BY ".$this->SortField . " " .$this->SortType . " ";
		}
		// Limit Results
		if (($this->Start!=-1) && ($this->Num!=-1) ) {
			$query .= "LIMIT ".$this->Start.", ".$this->Num;
			}
		//echo($query);
		//die();	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_errno($dbLink)) { 
		echo $error = "MySQL error ".mysqli_errno($dbLink).": ".mysqli_error($dbLink)."\n<br>When executing:<br>\n$result\n<br>"; 
		}
		
		return $result;
	}
	
	function getResellerRequestInfo($id)
	{
		global $dbLink;

		$query  = "Select * From `becomepartner` Where id = '" . $id . "' and isvalid='1'";
		$result = mysqli_query($dbLink, $query);
		return $row = mysqli_fetch_object($result);
	}
	
	function isresellerExist($phone,$email)
	{
		global $dbLink;

		$query =  " SELECT * FROM customer WHERE phone = '".$phone."'  And enabled = '1'";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return mysqli_num_rows($result);
		}
		return 0;
	}
	
	function Updatepartnerflag($id){
		
		global $dbLink;
		// Insert the information of the new created user into the database
		$query = "update `becomepartner` set `partner`='1'";
		$query .= " where id='$id' ";
		//echo $query;
		mysqli_query($dbLink, $query);
		
	}

}

?>