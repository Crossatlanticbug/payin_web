<?

class trace
{
	var $log_email;
	var $log_file;

	var $log_level;
	var $message;
	var $message_type;
	var $message_prefix;

	function __construct()
	{
		global $config;

		$this->log_file  = $config['BACKOFFICE_LOG_FILE'];
		$this->log_email = $config['BACKOFFICE_LOG_EMAIL'];
	}

	function log()
	{
		// If message is to be appended to a log file
		if ($this->message_type == 3)
		{
			if (! $this->log_file){
				return;
			}

			$destination = $this->log_file;
		}
		else if ($this->message_type == 1)  // If message is to be sent to an email address
		{
			$destination = $this->log_email;
		}

		if (substr($this->message, -1) != "\n"){
				$this->message .= "\n";
		}

		$message = date('Y-m-d H:i:s') . " - " . $this->log_level . ' - ' . $this->getMessageLocation();

		
		if ($this->log_level == " ") {
			$message = $this->getMessageLocation().' | ';
		} else {
			$message = date('Y-m-d H:i:s') . " - " . $this->log_level . ' - ' . $this->getMessageLocation();
		}

		
		if (!empty($this->message_prefix))
		{
			$message .= trim($this->message_prefix) . " " . $this->message;
		}
		else
		{
			$message .= $this->message;
		}

		error_log($message, $this->message_type, $destination);

		// If message is to be sent to an email address, then append it also to the log file
		if ($this->message_type == 1) {
			error_log($message, 3, $this->log_file);
		}
	}

	function error($message, $message_type = 3)
	{
		$this->log_level = "ERROR";
		$this->message = $message;
		$this->message_type = $message_type;
		$this->log();
	}

	function warn($message, $message_type = 3)
	{
		$this->log_level = "WARNING";
		$this->message = $message;
		$this->message_type = $message_type;
		$this->log();
	}

	function info($message, $message_type = 3, $log_level = NULL)
	{
		//$this->log_level = "INFO";
		if ($log_level == NULL) {
			$this->log_level = "INFO";
		} else {
			$this->log_level = " ";
		}
		$this->message = $message;
		$this->message_type = $message_type;
		$this->log();
	}

	function debug($message, $message_type = 3)
	{
		$this->log_level = "DEBUG";
		$this->message = $message;
		$this->message_type = $message_type;
		$this->log();
	}

	function getMessageLocation()
	{
		// __FILE__.':'.__LINE__.'/'.__CLASS__.'->'.__FUNCTION__;
		$message_location = "";
		$arr = debug_backtrace();

		$file_name = basename($arr[2]['file']);
		if ($this->log_level != "INFO")
		{
			$message_location .= '['.$file_name.':'.$arr[2]['line'].']';
		}

		$pos = strrpos($file_name, ".class.php");
		// exit($file_name . ',' . $pos);
		if ($pos !== false) {
			if (isset($arr[3]['class'])) {
				$message_location .= '['.$arr[3]['class'].'->'.$arr[3]['function'].']';
			}else{
				$message_location .= '['.$arr[3]['function'].']';
			}
		}

		if (!empty($message_location)) $message_location .= ' - ';

		return $message_location;
	}

}

?>