<?

include_once("../Includes/template.inc.php");
// Include general functions
include_once("../Functions/functions.php");

// Class Definition
class member {

  var $Name;
	var $UserName;
	var $Password;
	 var $Email;
	// var $IsAdmin;
	var $Enabled;
	var $trace;


	// Constructor of the class
  //function member($Name='',$UserName='',$Email='', $IsAdmin=0, $Enabled=1)
  function __construct($Name='',$UserName='',$Email='',$Password='', $Enabled=1)
	{
		global $trace;

    $this->Name = $Name;
    $this->UserName = $UserName;
    $this->Password = $Password;
    $this->Email = $Email;
    //$this->IsAdmin = $IsAdmin;
    $this->Enabled = $Enabled;
	}

	// This function is used to authenticate user, if login information are valid
	// then return the userid of this member, else return 0.
  function loginUser($username, $password)
  {
  	global $dbLink;

    $query = "Select UserID From `user` Where (Username='$username') ";
    $query .= "And (Password='$password')";

		$result = mysqli_query($dbLink, $query);
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_object($result);
	
      return $row->UserID;
		}
    return 0;
	}

	// This function is used to set the last login date for a certain user
	function setLastLoginDate($userid)
  {
  	global $dbLink;

  	$today = date("y-m-d H:i:s");
    $query = "Update `user` Set LastLoginDate = '$today' Where UserID = $userid";
		mysqli_query($dbLink, $query);
	}

	// This function is used to set the timezone for a certain user
	function setTimezone($userid, $timezone)
  {
  	global $dbLink;

    $query = "Update `user` Set Timezone = '$timezone' Where UserID = $userid";
		mysqli_query($dbLink, $query);
	}

	// This function is used to retrieve the timezone of a certain user
  function getTimezone($userid)
  {
  	global $dbLink;

    $query = "Select Timezone From `user` Where UserID = '$userid'";
    $result = mysqli_query($dbLink, $query);
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_object($result);
      return $row->Timezone;
		}
    return "Europe/London";
	}


	// This function is used to retrieve the password of a certain username
  function getUserPassword($username)
  {
  	global $dbLink;

    $query = "Select Password From `user` Where Username = '$username'";
    $result = mysqli_query($dbLink, $query);
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_object($result);
      return $row->Password;
		}
    return 0;
	}

  // This function is used to retrieve the ID of a certain user
  function getUserID($username)
  {
  	global $dbLink;

    $query = "Select UserID From `user` Where Username = '$username'";
    $result = mysqli_query($dbLink, $query);
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_object($result);
      return $row->UserID;
		}
    return 0;
	}

	// This function is used to retrieve a list of all users
  function getUsers()
  {
  	global $dbLink;

    $query = "Select * From `user`";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to retrieve a list of the user account information.
  function getUserInfo($userid)
  {
  	global $dbLink;

    $query = "Select * From `user` Where UserID = $userid";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to generate an automatic random passwords of n characters length
  function createPassword($chars)
  {
	  $n = 0;
		$allowchars = '';
	  srand((double) microtime() * 1000000);
	  $allowchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*';
	  while(++$n < ($chars+1)) {
			$password .= $allowchars[rand(0, strlen($allowchars)-1)];
		}
	  return $password;
	}

	// This function is used to send the password of a certain user to his email
  function sendPassword($username ,$email ,$Name)
  {
  	global $dbLink;

    $password = $this->createPassword(6);
    $query = "Update `user` Set Password = '".md5($password)."' Where Username = '$username'";
    mysqli_query($dbLink, $query);

    // Prepare headers for sending an html mail
	  $headers  = "MIME-Version: 1.0\r\n";
	  $headers .= "Content-type: text/html; charset=windows-1256\r\n";
	  $headers .= "From:info@ids.com.lb\r\n";

    // This variable will be the email address of the lvl website
    $to = stripslashes($email);
    $subject = "Your Login Information";
    $message  = "Dear $Name,<br><br>";
    $message .= "This email is sent upon your request.<br><br>";
    $message .= "Your username is: $username<br>";
    $message .= "Your password is: $password<br><br>";
    $message .= "You requested your password on <b>".date("d/m/Y H:i:s")."</b><br>";
    $message .= "Remote IP: <font color='#CC0000'><b>".$_SERVER['REMOTE_ADDR']."</b></font><br>";

    //Send Password
    @mail($to, $subject, $message, $headers);
  }

	// This function is used to update the user's status
  function updateUserStatus($userid, $status)
  {
  	global $dbLink;

    $query = "Update `user` Set Enabled = $status Where UserID=$userid";
	  mysqli_query($dbLink, $query);
	}

	// This function is used to delete a certain user
  function deleteUser($userid)
  {
  	global $dbLink;

    $query = "Delete From `user` Where UserID=$userid";
	  mysqli_query($dbLink, $query);
	}

	// This function is used to update user's information
  function updateUserInfo($userid, $objuser)
  {
  	global $dbLink;

  	// Modification Date
  	$modification_date = date("Y-m-d H:i:s");

    // Add slashes to object variables before using in a query.
    addslashes_object_vars($objuser);
		// Update query
		$query  = "Update `user` Set ";
    //$query .= "Name = '$objuser->Name', Email = '$objuser->Email', ";
    $query .= "Name = '$objuser->Name', Email = '$objuser->Email',Username = '$objuser->UserName', ";
    $query .= "Password = '".md5($objuser->Password)."', Enabled = $objuser->Enabled, ";
    //$query .= "IsAdmin = $objuser->IsAdmin, ModificationDate = '$modification_date' ";
    $query .= "ModificationDate = '$modification_date' ";
    $query .= "Where UserID = $userid";

    mysqli_query($dbLink, $query);
	}

	// This function is used to create a new user
  function addNewUser($objuser)
  {
  	global $dbLink;

    // Add slashes to object variables before using in a query.
		addslashes_object_vars($objuser);

	
		// Get current date. Note that the date format for MySQL is Y-m-d (ex:2004-04-15)
		$creation_date = date("Y-m-d H:i:s");


		// Insert the information of the new created user into the database
    $query = "Insert Into `user` (Name,Email, Username, Password, CreationDate) ";
    $query .= "Values ('$objuser->Name','$objuser->Email','$objuser->UserName',";
    $query .= "'".md5($objuser->Password)."','$creation_date')";
    //echo $query;
    mysqli_query($dbLink, $query);
	}
}


	
	
?>