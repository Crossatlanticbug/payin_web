<?

// Class Definition
class order
{
	
	var $Criteria;
	var $SortField;
	var $SortType;
	var $Start = -1;
	var $Num = -1;
	/*
	 * Class constructor
	 */
  function __construct($OrderID = -1)
  {
  	if ((!empty($OrderID)) && ($OrderID != -1))
  	{
  		$this->OrderID = $OrderID;
			$this->getInfo();
		}
	}

	/*
	 * Retrieves order information
	 */
	function getInfo()
	{
		global $dbLink;

		$query  = "Select * From `rechargeorder` Where orderid = '" . $this->OrderID . "'";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);

   
  }

	
	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getList()
	{
		global $dbLink;
		global $language;
		global $config;
		global $lang;

		
		$statusIDFieldname = "order.StatusID";
		$statusIDInnerJoin = '';
		$DiscountCouponInnerJoin = "";
		$DiscountCouponFieldName = "";

		$query = "Select * From `rechargeorder` where 1=1 ";
		
		// Date Range ------------------------------------------------------
		if($this->Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(rechargedate, '%Y-%m-%d') >= '" . $this->Criteria['FromDate'] . "') ";
		}
		if($this->Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(rechargedate, '%Y-%m-%d') <= '" . $this->Criteria['ToDate'] . "') ";
		}
		// Amount Range ----------------------------------------------------
		if($this->Criteria['FromAmount'] != '') {
			$query .= "And (amount  >= " . $this->Criteria['FromAmount'] . ") ";
		}
		if($this->Criteria['ToAmount'] != '') {
			$query .= "And (amount <= " . $this->Criteria['ToAmount'] . ") ";
		}
		
		
	
		
		// IP Address ------------------------------
		if($this->Criteria['Username'] != '') {
			$query .= "And (name Like '%" . $this->Criteria['Username'] . "%') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['OrderID'] != '') {
			$query .= "And (orderid = '" . $this->Criteria['OrderID'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['TransactionID'] != '') {
			$query .= "And (transactionid = '" . $this->Criteria['TransactionID'] . "') ";
		}
		
		if($this->Criteria['RechargeID'] != '') {
			$query .= "And (rechargekey = '" . $this->Criteria['RechargeID'] . "') ";
		}
		
		
		// IP Address ------------------------------
		if($this->Criteria['UserIP'] != '') {
			$query .= "And (userip = '" . $this->Criteria['UserIP'] . "') ";
		}
		
		
		// Order Status ------------------------------
		if($this->Criteria['StatusID'] != '') {
			$query .= "And (paymentstatus = '" . $this->Criteria['PaymentStatus'] . "') ";
		}
		
		// Order Status ------------------------------
		if($this->Criteria['Payment'] != '') {
			$query .= "And (payment = '" . $this->Criteria['Payment'] . "') ";
		}
		// Order Status ------------------------------
		if($this->Criteria['RechargeStatus'] != '') {
			$query .= "And (rechargestatus = '" . $this->Criteria['RechargeStatus'] . "') ";
		}
		
		// Order Status ------------------------------
		if($this->Criteria['RechargeType'] != '') {
			$query .= "And (rechargetype = '" . $this->Criteria['RechargeType'] . "') ";
		}
		
		// Order By
		$query .= "ORDER BY ".$this->SortField . " " .$this->SortType . " ";

		// Limit Results
		if (($this->Start!=-1) && ($this->Num!=-1) ) {
			$query .= "LIMIT ".$this->Start.", ".$this->Num;
			}
		//echo($query);
		//die();	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_errno($dbLink)) { 
		echo $error = "MySQL error ".mysqli_errno($dbLink).": ".mysqli_error($dbLink)."\n<br>When executing:<br>\n$result\n<br>"; 
		}
		
		return $result;
	}

	
	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                         General Authorization Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the count of orders different from NetCommerce and having Status equal to "InProgress" available according to the given search criteria
	function getTotalGeneral($Criteria)
	{
		global $dbLink;
		global $language;

		$query  = "Select COUNT(*) From `order` ";
		$query .= "Where Language = '$language' And (Payment = 'MoneyBookers' Or Payment = 'cashU') And StatusID = 3 ";

		// Date Range ------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(PurchaseDate, '%Y-%m-%d') >= '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(PurchaseDate, '%Y-%m-%d') <= '" . $Criteria['ToDate'] . "') ";
		}
		// Amount Range ----------------------------------------------------
		if($Criteria['FromAmount'] != '') {
			$query .= "And (OrderAmount >= " . $Criteria['FromAmount'] . ") ";
		}
		if($Criteria['ToAmount'] != '') {
			$query .= "And (OrderAmount <= " . $Criteria['ToAmount'] . ") ";
		}
		// User ------------------------------------------------------------
		if($Criteria['UserID'] != '') {
			$query .= "And (UserID = " . $Criteria['UserID'] . ") ";
		}
		// Order ID ------------------------------
		if($Criteria['OrderID'] != '') {
			$query .= "And (OrderID = '" . $Criteria['OrderID'] . "') ";
		}

	  // echo $query;
   	//$result = mysqli_result(mysqli_query($dbLink, $query), 0, 0);

   // return $result;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getListGeneral($Criteria, $SortField, $SortType, $Start = -1, $Num = -1)
	{
		global $dbLink;
		global $language;

		$query  = "Select OrderID, OrderAmount, OrderDate, OrderTime, PurchaseDate, Payment, StatusID, ";
		$query .= "concat(mu_FName,' ',mu_LName) MemberName, mu_ID CustomerID, mu_Username Username ";
		$query .= "From `order`, member_user ";
		$query .= "Where UserID = mu_ID And Language = '$language' And (Payment = 'MoneyBookers' Or Payment = 'cashU') And StatusID = 3 ";

		// Date Range ------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(PurchaseDate, '%Y-%m-%d') >= '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(PurchaseDate, '%Y-%m-%d') <= '" . $Criteria['ToDate'] . "') ";
		}
		// Amount Range ----------------------------------------------------
		if($Criteria['FromAmount'] != '') {
			$query .= "And (OrderAmount >= " . $Criteria['FromAmount'] . ") ";
		}
		if($Criteria['ToAmount'] != '') {
			$query .= "And (OrderAmount <= " . $Criteria['ToAmount'] . ") ";
		}
		// User ------------------------------------------------------------
		if($Criteria['UserID'] != '') {
			$query .= "And (UserID = " . $Criteria['UserID'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['Username'] != '')
		{
			$query .= "And (mu_Username Like '%" . $Criteria['Username'] . "%') ";
		}
		// Order ID ------------------------------
		if($Criteria['OrderID'] != '') {
			$query .= "And (OrderID = '" . $Criteria['OrderID'] . "') ";
		}

		// Order By
		$query .= "Order By $SortField $SortType ";

		// Limit Results
		if (($Start!=-1) && ($Num!=-1)) {
     $query .= "Limit $Start,$Num";
    }

		// echo($query);

		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to delete a certain order
  function deleteOrder($OrderID)
  {
  	global $dbLink;

    $query  = "Delete From `order` Where OrderID = $OrderID";
    mysqli_query($dbLink, $query);
	}

	// This function is used to get the information of a certain order
	function getRechargeOrderInfo($orderid)
	{
		global $dbLink;

		//echo $query  = "SELECT * FROM `rechargeorder` ro LEFT JOIN customer c ON  ro.customerid=c.id WHERE OrderID =  '" . $orderid . "' ";
		 $query  = "SELECT * FROM `rechargeorder` WHERE OrderID =  '" . $orderid . "' ";
		$result = mysqli_query($dbLink, $query);
	
		return $result;
	}

	function getOperatorName($operatorid)
	{
		global $dbLink;

		$query  = "Select * From `operator` Where `status`=1 and  operatorid='".$operatorid."' ";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}

	

	
}

?>