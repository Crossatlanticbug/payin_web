<?
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Class		:	country
* Version :	1.0
* Date    :	21 July 2010
* Author  :	Darwish A. Khalil
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// Class Definition
class country
{
	var $ISOCode2;		// Country ISO Code 2
	var $ISOCode3;		// Country ISO Code 3
	var $Name;				// Country Name
	var $CallingCode; // Country Calling Code

	var $NamesArray  = array();  // Array of countries names
	var $CallingCodesArray = array();  // Array of countries calling codes

	/*
	 * Class constructor
	 */
  function __construct($ISOCode2 = -1)
  {
  	if ((!empty($ISOCode2)) && ($ISOCode2 != -1))
  	{
  		$this->ISOCode2 = $ISOCode2;
			$this->getInfo();
		}
		else
		{
			// Load countries names & calling codes into arrays
			$this->loadCountries();
		}
	}

	// Retrieves country information
	function getInfo()
	{
		global $dbLink;

		$query = "
							Select * From country
					 	  Where c_ID = $this->ISOCode2
					 	 ";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);

    // Set class data members
    $this->ISOCode3 = $row->c_ID_3;
    $this->Name = $row->c_Name_En;
    $this->CallingCode = $row->c_CallingCode;
  }

	// Retrieves a list of all countries
	function getList()
	{
		global $lang;
		global $dbLink;

		$query =
						"
							SELECT c_ID ISOCode2, c_Name_En, c_CallingCode CallingCode
							FROM country
							Where c_Enabled = 1
							ORDER BY Binary c_Name_En";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

  // Load countries names & calling codes into arrays
	function loadCountries()
	{
		$result = $this->getList();

		while($row = mysqli_fetch_object($result))
		{
			$this->NamesArray[$row->ISOCode2]  = $row->Name;
			$this->CallingCodesArray[$row->ISOCode2] = $row->CallingCode;
		}
	}
}
?>