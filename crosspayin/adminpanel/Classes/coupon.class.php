<?

// Class Definition
class coupon
{
	
	var $Criteria;
	var $SortField;
	var $SortType;
	var $Start = -1;
	var $Num = -1;
	/*
	 * Class constructor
	 */
  function __construct($id = -1)
  {
  	if ((!empty($id)) && ($id != -1))
  	{
  		$this->id = $id;
			$this->getInfo();
		}
	}

	/*
	 * Retrieves order information
	 */
	function getInfo()
	{
		global $dbLink;

		$query  = "Select * From `coupon` Where id = '" . $this->id . "'";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);

   
  }

	
	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getList()
	{
		global $dbLink;
		global $config;
		global $lang;

		
		$statusIDFieldname = "coupon.ID";
		$statusIDInnerJoin = '';
		$DiscountCouponInnerJoin = "";
		$DiscountCouponFieldName = "";

		$query = "Select * From `coupon` where 1=1 ";
		
		// Date Range ------------------------------------------------------
		if($this->Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(start_date, '%Y-%m-%d') = '" . $this->Criteria['FromDate'] . "') ";
		}
		if($this->Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(end_date, '%Y-%m-%d') = '" . $this->Criteria['ToDate'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['CouponName'] != '') {
			$query .= "And (coupon_name Like '%" . $this->Criteria['CouponName'] . "%') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['Code'] != '') {
			$query .= "And (coupon_code = '" . $this->Criteria['Code'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['Amount'] != '') {
			$query .= "And (amount = '" . $this->Criteria['Amount'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['Description'] != '') {
			$query .= "And (description = '" . $this->Criteria['Description'] . "') ";
		}
		
		
		// Order Status ------------------------------
		if($this->Criteria['Location'] != '') {
			$query .= "And (coupon_location = '" . $this->Criteria['Location'] . "') ";
		}
		
		// Order Status ------------------------------
		if($this->Criteria['Status'] != '') {
			$query .= "And (status = '" . $this->Criteria['Status'] . "') ";
		}
		
		// Order By
		$query .= "ORDER BY ".$this->SortField . " " .$this->SortType . " ";

		// Limit Results
		if (($this->Start!=-1) && ($this->Num!=-1) ) {
			$query .= "LIMIT ".$this->Start.", ".$this->Num;
			}
		//echo($query);
		//die();	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_errno($dbLink)) { 
		echo $error = "MySQL error ".mysqli_errno($dbLink).": ".mysqli_error($dbLink)."\n<br>When executing:<br>\n$result\n<br>"; 
		}
		
		return $result;
	}

	
	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                         General Authorization Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the count of orders different from NetCommerce and having Status equal to "InProgress" available according to the given search criteria
	function getTotalGeneral($Criteria)
	{
		global $dbLink;

		$query  = "Select COUNT(*) From `coupon` ";

		// Date Range ------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(start_date, '%Y-%m-%d') = '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(end_date, '%Y-%m-%d') = '" . $Criteria['ToDate'] . "') ";
		}
		
		// User ------------------------------------------------------------
		if($Criteria['ID'] != '') {
			$query .= "And (ID = " . $Criteria['ID'] . ") ";
		}
	  // echo $query;
   	//$result = mysqli_result(mysqli_query($dbLink, $query), 0, 0);

   // return $result;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getListGeneral($Criteria, $SortField, $SortType, $Start = -1, $Num = -1)
	{
		global $dbLink;
		global $language;

		$query  = "Select id, coupon_name, coupon_code, start_date, end_date, description, coupon_location, status, ";
		$query .= "From `coupon` WHERE 1=1 ";

		// Date Range ------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(start_date, '%Y-%m-%d') = '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(end_date, '%Y-%m-%d') = '" . $Criteria['ToDate'] . "') ";
		}
		// Amount Range ----------------------------------------------------
		if($Criteria['Amount'] != '') {
			$query .= "And (amount >= " . $Criteria['Amount'] . ") ";
		}
		// User ------------------------------------------------------------
		if($Criteria['CouponName'] != '') {
			$query .= "And (coupon_name = " . $Criteria['CouponName'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['CouponCode'] != '')
		{
			$query .= "And (coupon_code Like '%" . $Criteria['CouponCode'] . "%') ";
		}
		// Order ID ------------------------------
		if($Criteria['descri[Status'] != '') {
			$query .= "And (status = '" . $Criteria['Status'] . "') ";
		}

		// Order By
		$query .= "coupon By $SortField $SortType ";

		// Limit Results
		if (($Start!=-1) && ($Num!=-1)) {
     $query .= "Limit $Start,$Num";
    }

		// echo($query);

		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to delete a certain order
  function deleteCoupon($id)
  {
  	global $dbLink;

    $query  = "Delete From `coupon` Where id = $id";
    mysqli_query($dbLink, $query);
	}

	// This function is used to get the information of a certain order
	function getCouponInfo()
	{
		global $dbLink;

		$query  = "Select * From `coupon` Where id = '" . $this->id . "'";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	
	

	
}

?>