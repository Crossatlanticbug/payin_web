<?

// Class Definition
class developmentlog
{
	
	var $Criteria;
	var $SortField;
	var $SortType;
	var $Start = -1;
	var $Num = -1;
	/*
	 * Class constructor
	 */
  function __construct($ID = -1)
  {
  	if ((!empty($ID)) && ($ID != -1))
  	{
  		$this->ID = $ID;
			$this->getInfo();
		}
	}

	/*
	 * Retrieves order information
	 */
	function getInfo()
	{
		global $dbLink;

		$query  = "Select * From `development_log` Where id = '" . $this->ID . "'";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);

   
  }

	
	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getList()
	{
		global $dbLink;
		global $config;
		global $lang;

		
		$statusIDFieldname = "development_log.ID";
		$statusIDInnerJoin = '';
		$DiscountCouponInnerJoin = "";
		$DiscountCouponFieldName = "";

		$query = "Select * From `development_log` where 1=1 ";
		
		// Date Range ------------------------------------------------------
		if($this->Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(processdate, '%Y-%m-%d') >= '" . $this->Criteria['FromDate'] . "') ";
		}
		if($this->Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(processdate, '%Y-%m-%d') <= '" . $this->Criteria['ToDate'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['EmailID'] != '') {
			$query .= "And (emailid Like '%" . $this->Criteria['EmailID'] . "%') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['ID'] != '') {
			$query .= "And (ID = '" . $this->Criteria['ID'] . "') ";
		}
		
		// IP Address ------------------------------
		if($this->Criteria['UserIP'] != '') {
			$query .= "And (userip = '" . $this->Criteria['UserIP'] . "') ";
		}
		
		
		// Order Status ------------------------------
		if($this->Criteria['MobileNo'] != '') {
			$query .= "And (mobileno = '" . $this->Criteria['MobileNo'] . "') ";
		}
		
		// Order By
		$query .= "ORDER BY ".$this->SortField . " " .$this->SortType . " ";

		// Limit Results
		if (($this->Start!=-1) && ($this->Num!=-1) ) {
			$query .= "LIMIT ".$this->Start.", ".$this->Num;
			}
		//echo($query);
		//die();	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_errno($dbLink)) { 
		echo $error = "MySQL error ".mysqli_errno($dbLink).": ".mysqli_error($dbLink)."\n<br>When executing:<br>\n$result\n<br>"; 
		}
		
		return $result;
	}

	
	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                         General Authorization Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the count of orders different from NetCommerce and having Status equal to "InProgress" available according to the given search criteria
	function getTotalGeneral($Criteria)
	{
		global $dbLink;
		global $language;

		$query  = "Select COUNT(*) From `development_log` ";
		$query .= "Where 1=1 ";

		// Date Range ------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(processdate, '%Y-%m-%d') >= '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(processdate, '%Y-%m-%d') <= '" . $Criteria['ToDate'] . "') ";
		}
		// User ------------------------------------------------------------
		if($Criteria['ID'] != '') {
			$query .= "And (id = " . $Criteria['ID'] . ") ";
		}
		// Order ID ------------------------------
		if($Criteria['EmailID'] != '') {
			$query .= "And (emailid = '" . $Criteria['EmailID'] . "') ";
		}
		// Order ID ------------------------------
		if($Criteria['MobileNo'] != '') {
			$query .= "And (mobileno = '" . $Criteria['MobileNo'] . "') ";
		}
	  // echo $query;
   	//$result = mysqli_result(mysqli_query($dbLink, $query), 0, 0);

   // return $result;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

	// This function is used to retrieve a list of orders regarding a certain search criteria
	function getListGeneral($Criteria, $SortField, $SortType, $Start = -1, $Num = -1)
	{
		global $dbLink;
		global $language;

		$query  = "Select ID, paymentlog, rechargelog, mobileno, emailid, processdate, userip, pageurl, ";
		$query .= "From development_log ";
		$query .= "Where 1=1 ";

		// Date Range ------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(processdate, '%Y-%m-%d') >= '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(processdate, '%Y-%m-%d') <= '" . $Criteria['ToDate'] . "') ";
		}
		// User ------------------------------------------------------------
		if($Criteria['ID'] != '') {
			$query .= "And (id = " . $Criteria['ID'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['EmailID'] != '')
		{
			$query .= "And (emailid Like '%" . $Criteria['EmailID'] . "%') ";
		}
		// Order ID ------------------------------
		if($Criteria['MobileNo'] != '') {
			$query .= "And (mobileno = '" . $Criteria['MobileNo'] . "') ";
		}


		// Order By
		$query .= "Order By $SortField $SortType ";

		// Limit Results
		if (($Start!=-1) && ($Num!=-1)) {
     $query .= "Limit $Start,$Num";
    }

		// echo($query);

		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to delete a certain order
  function deletedevelopmentlog($ID)
  {
  	global $dbLink;

    $query  = "Delete From `development_log` Where id = $ID";
    mysqli_query($dbLink, $query);
	}

	// This function is used to get the information of a certain order
	function getdevelopmentlogInfo()
	{
		global $dbLink;

		$query  = "Select * From `development_log` Where id = '" . $this->ID . "'";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}	
}

?>