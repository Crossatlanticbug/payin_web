<?php

define("win_session_save_path","c:\\temp");
define("linux_session_save_path","/tmp");

class session
{

	var $_sess;
	var $_path;

  function __construct($start=false)
	{
    if($start){
      $this->_init_session();
    }
    if(is_array($this->_sess)){
      //foreach(array_keys($this->_sess) as $name => $value){
      foreach($this->_sess as $name => $value){
        //session_register($name);
        //echo $name ."=>". $value."<br>";
        $_SESSION[$name]=$value;
      }
    }
  }

  function _init_session()
	{
		$this->_path = ini_get('session.save_path');
    if(!is_dir($this->_path)){
      if(preg_match("/win/",strtolower(PHP_OS))){
				ini_set("session.save_path",win_session_save_path);
				@mkdir(win_session_save_path);
			}
      elseif(preg_match("/linux/",strtolower(PHP_OS))){
				ini_set("session.save_path",linux_session_save_path);
				@mkdir(linux_session_save_path,0700);
			}
		}
		//$this->set_session_limiter();
		//$this->set_session_expired(1);
		if(!isset($_SESSION))
		{
		try {
		   session_start();
		} catch(ErrorExpression $e) {
		   session_regenerate_id();
		   session_start();
		} 
    	
		}
		$this->_sess = &$_SESSION;
	}

  function close_session()
  {
    session_unset();
	}

  function destroy_session()
  {
    session_destroy();
	}

  function get_session_id()
  {
    return session_id();
	}

  function add_session_data($name,$value)
  {
    //session_register($name);
		$_SESSION[$name]=$value;
		 $this->_sess[$name] = $value;
	}

  function del_session_data($name)
  {
    session_unregister($name);
	}

  function edit_session_data($name,$value)
  {
    //session_register($name);
	 $_SESSION[$name]=$value;
		$this->_sess[$name] = $value;
 	}

  function get_session_value($name)
  {
    if(isset($this->_sess[$name]) && session_is_registered($name)){
      return $this->_sess[$name];
		}
	}

  function check_session($name)
  {
    return session_is_registered($name);
	}

  function get_session_list()
  {
    return array_keys($this->_sess);
	}

  function get_session_dir()
  {
    return $this->_path;
	}

  function get_session_count()
  {
		$i=0;
    if(is_dir($this->_path)){
      if($dir=opendir($this->_path)){
        while (false !== ($file = readdir($dir))) {
          if(preg_match("/sess_/i",$file)){
						$i++;
					}
				}
			}
		}
    return $i;
	}

  function set_session_expired($delay=30)
  {
    //session_cache_expire($delay);
	}

  function set_session_limiter($limit='nocache')
  {
    session_cache_limiter($limit);
	}
}

?>