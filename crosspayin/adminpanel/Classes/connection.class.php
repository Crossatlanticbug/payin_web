<?

// Class Definition
class connection {

	/*-*-*-*-* Private Class Variables *-*-*-*-*-*-*/
	// Connection Variables
	var $hostName; 			// Name of server
	var $dbLink; 				// Link to database
	var $dbName; 				// Name of databse
	var $dbUser; 				// Database user name
	var $dbPassword; 		// Database password
	var $dbPwd; 				// Password used or not?

	// Query Result Variables
	var $dbQryResult; 	// Result of last query
	var $dbResultLine; 	// One Result line of last query

	// Query Log Variables
	var $keepLog; 			// Should a log of all queries be kept
	var $logPath; 			// Path where log should be saved
	var $fullErr; 			// Display full or partial error
	var $rollBackSup; 	// Are you using tables with transactional support (ie. InnoDB)

	// Constructor of the class
	// Creates object and makes connection to database.
  function __construct($cfg_file = "connection.cfg.php")
  {
  	global $trace;
  	global $config;

		// Read config info from cfg file and save them in class variables
    require($cfg_file);
		$this->hostName = $host;
		$this->dbUser = $user;
		$this->dbPassword = $pwd;
		$this->dbName = $database;
		$this->keepLog = $keepLog;
		$this->logPath = $logPath;
		$this->fullErr = $fullErr;
		$this->rollBackSup = $rollBackSup;

		if (strlen($pwd) > 0) { $this->dbPwd = "Yes"; } else { $this->dbPwd = "No"; }
		// Creates connection to server and selects appropriate database
		$this->dbLink = mysqli_connect($this->hostName, $this->dbUser, $pwd);

		if (!$this->dbLink)
		{
			$trace->error("Could not connect to " . strtoupper($database) . " Database! [ERROR: " . mysqli_error($this->dbLink) . "]", 1);
		}
  	else
		{
    	mysqli_select_db($this->dbLink, $this->dbName);
			mysqli_query($this->dbLink, "SET CHARACTER SET 'cp1256'");
		}
	}

	// Close connection to the database
  function dbclose()
  {
    mysqli_close($this->dbLink);
	}

	// Executes a query ($qry).
  function make_query($qry)
  {
		$this->dbQryResult = mysqli_query($this->dbLink, $qry);

		// If required, write query to a log file
    if ($this->keepLog == 1) {
			$fp = fopen($this->logPath."connection.class.log", "a+");
      fputs($fp, date("r", time())." - ".$_SERVER['PHP_SELF']."<BR>");
      fputs($fp, $qry."<BR>");
      fclose($fp);
		}
    return $this->dbQryResult;
	}

	// Returns the resultset, one row at a time in an array
  function get_data()
  {
		$this->dbResultLine = mysqli_fetch_object($this->dbQryResult);
    return $this->dbResultLine;
	}

	// Number of rows in resultset
  function row_count()
  {
		$qtyLines = mysqli_num_rows($this->dbQryResult);
    return $qtyLines;
	}

	// Array with field names of resultset
  function get_fields()
  {
		$fldList = Array();
    for ($fldCntr = 0; $fldCntr < mysqli_num_fields($this->dbQryResult); $fldCntr++) {
			$fldList[$fldCntr] = mysqli_field_name($this->dbQryResult, $fldCntr);
		}
    return $fldList;
	}
}
?>