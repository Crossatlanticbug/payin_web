<?
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Class		:	admin
* Version :	1.0
* Date    :	09 September 2005
* Author  :	Darwish A. Khalil
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// Class Definition
class admin {

	// Constructor of the class
  function __construct() {

	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                         Material Types Page
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

	// This function is used to retrieve a list of all material types
  function getMaterialTypes($language)
  {
  	global $dbLink;

    $query  = "Select * From material_types Where Language = '$language' Order By TypeID";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to retrieve the information for a certain material type
  function getMaterialTypeInfo($id)
  {
  	global $dbLink;

    $query  = "Select * From material_types Where TypeID = $id";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to update the information of a certain material type
  function updateMaterialType($id, $name, $description, $language, $enabled)
  {
  	global $dbLink;

    $query  = "Update material_types Set ";
    $query .= "Name = '" . addslashes($name) . "', ";
    $query .= "Description = '" . addslashes($description) . "', ";
    $query .= "Language = '" . addslashes($language) . "', ";
    $query .= "Enabled = $enabled Where TypeID = $id";
    // echo $query;
    mysqli_query($dbLink, $query);
	}

	// This function is used to a new material type
  function addMaterialType($name, $description, $language, $enabled)
  {
  	global $dbLink;

    $query  = "Insert Into material_types (Name, Description, Language, Enabled) ";
    $query .= "Values ('".addslashes($name)."','".addslashes($description)."','";
    $query .= addslashes($language)."',$enabled)";
    mysqli_query($dbLink, $query);
	}

	// This function is used to check if this material type is used by another materials
  function isMaterialTypeUsed($id)
  {
  	global $dbLink;

    $query  = "Select MaterialID From material Where TypeID = $id";
    $result = mysqli_query($dbLink, $query);
    if (mysqli_num_rows($result) > 0)
    	return true;
		else
			return false;
	}

	// This function is used to delete a certain material type
  function deleteMaterialType($id)
  {
  	global $dbLink;

    $query  = "Delete From material_types Where TypeID = $id";
    mysqli_query($dbLink, $query);
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                         Attachment Types Page
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

	// This function is used to retrieve a list of all attachment types
  function getAttachmentTypes()
  {
  	global $dbLink;

    $query  = "Select * From attachment_types Order By TypeID";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to retrieve the information for a certain attachment type
  function getAttachmentTypeInfo($id)
  {
  	global $dbLink;

    $query  = "Select * From attachment_types Where TypeID = $id";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to update the information of a certain attachment type
  function updateAttachmentType($id, $name, $descriptionEn, $allowed_extensions, $enabled)
  {
  	global $dbLink;

    $query  = "Update attachment_types Set ";
    $query .= "Name = '" . addslashes($name) . "', ";
    $query .= "Description = '" . addslashes($descriptionEn) . "', ";
    $query .= "AllowedExtensions = '" . addslashes($allowed_extensions) . "', ";
    $query .= "Enabled = $enabled Where TypeID = $id";
    // echo $query;
    mysqli_query($dbLink, $query);
	}

	// This function is used to a new attachment type
  function addAttachmentType($name, $description, $allowed_extensions, $enabled)
  {
  	global $dbLink;

    $query  = "Insert Into attachment_types (Name, Description, AllowedExtensions, Enabled) ";
    $query .= "Values ('".addslashes($name)."','".addslashes($description)."','";
    $query .= addslashes($allowed_extensions)."',$enabled)";
    mysqli_query($dbLink, $query);
	}

	// This function is used to check if this attachment type is used by another attachment
  function isAttachmentTypeUsed($id)
  {
  	global $dbLink;

    $query  = "Select AttachmentID From attachment Where TypeID = $id";
    $result = mysqli_query($dbLink, $query);
    if (mysqli_num_rows($result) > 0)
    	return true;
		else
			return false;
	}

	// This function is used to a new attachment type
  function deleteAttachmentType($id)
  {
  	global $dbLink;

    $query  = "Delete From attachment_types Where TypeID = $id";
    mysqli_query($dbLink, $query);
	}

	// This function is used to get all extension types
  function getExtensionTypes()
  {
  	global $dbLink;

    $query  = "Select ID, Name From extension_types Where Enabled=1";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to get all extensions related to a certain type
  function getExtensions($id)
  {
  	global $dbLink;

  	if ($id != -1) {
    	$query  = "Select ID, Name From extensions Where TypeID=$id And Enabled=1";
    }else{
    	$query  = "Select ID, Name From extensions Where Enabled=1";
  	}
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	*                        Menu Setup Page
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the main links
  function getMainLinks()
  {
  	global $dbLink;
  	global $language;

    $query  = "Select Facet, Name, ";
    $query .= "MenuLeft, MenuRight, MenuTop, MenuBottom ";
    $query .= "From sitemap_section Where ParentID is NULL And Language = '$language' ";
    $query .= "Order By Facet";
    $result = mysqli_query($dbLink, $query);
    return $result;
	}

	// This function is used to save the menu setup
  function saveMenuSetup($Facet, $MenuLeft, $MenuRight, $MenuTop, $MenuBottom)
  {
  	global $dbLink;
  	global $language;

    $query  = "Update sitemap_section Set ";
    $query .= "MenuLeft = $MenuLeft, MenuRight = $MenuRight, ";
    $query .= "MenuTop = $MenuTop, MenuBottom = $MenuBottom ";
    $query .= "Where Facet Like '$Facet%' And Language = '$language'";
    // echo $query;
    mysqli_query($dbLink, $query);
	}

	// This function is used to retrieve the list of countries
	function getCountryList()
	{
		global $dbLink;
		global $language;

		$query  = "SELECT c_ID CountryID, c_Name_".$language." CountryName FROM country Where c_Enabled = 1 ";
		$query .= "ORDER BY Binary c_Name_".$language;
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

}

?>