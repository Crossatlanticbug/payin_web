<?php 
error_reporting(E_ALL);
ini_set("display_errors",1);
session_start();
include_once("../Includes/template.inc.php");
 
include_once("../Classes/api.class.php");
$apiprocess= new RechageApi();
include_once("../Classes/order.class.php");
$orderprocess= new Order();
$CurrentUser = &$_SESSION['CurrentUser'];


$payfor = isset($_REQUEST['optpayfor']) ?$_REQUEST['optpayfor'] :'';
$mobileno = isset($_REQUEST['txtmobileno']) ?$_REQUEST['txtmobileno'] :'';
$operator = isset($_REQUEST['cmboperator']) ?$_REQUEST['cmboperator'] :'';
$custacc = isset($_REQUEST['txtcustacc']) ?$_REQUEST['txtcustacc'] :'';
$custno = isset($_REQUEST['txtcustno']) ?$_REQUEST['txtcustno'] :'';
$amount = isset($_REQUEST['txtamount']) ?$_REQUEST['txtamount'] :'0';
$name = isset($_REQUEST['txtname']) ?$_REQUEST['txtname'] :'';
$email = isset($_REQUEST['txtemail']) ?$_REQUEST['txtemail'] :'';
$rechargetype = isset($_REQUEST['rechargetype']) ?$_REQUEST['rechargetype'] :'';
$paymenttype = isset($_REQUEST['paymenttype']) ?$_REQUEST['paymenttype'] :'';

if (!empty($mobileno)){
	$rechargevalue = new stdClass();
	$rechargevalue->payfor=$payfor;
	$rechargevalue->mobileno=$mobileno;
	$rechargevalue->operatorid=$operator;
	$rechargevalue->amount=$amount;
	$rechargevalue->rechargetype = $rechargetype;
	$rechargevalue->payment = $paymenttype; 
	$rechargevalue->paymentstatus ="pending"; 
	$rechargevalue->userip=$_SERVER['REMOTE_ADDR'];
	$rechargevalue->name=$name;
	$rechargevalue->email=$email;
	$rechargevalue->custacc=$custacc;
	$rechargevalue->custno=$custno;
	$rechargevalue->rechargestatus="Pending"; 
	$rechargevalue->customerid=isset($CurrentUser->id)?$CurrentUser->id:0; 
	$orderid = $orderprocess->AddRechargeOrder($rechargevalue);
	
	$_SESSION['ORDERID']=$orderid;
	
	if (!empty($orderid)){
		if ($paymenttype=="WALLET"){
			exit("<script>window.location.href='../wallet/success.php';</script>");
		} else {
			exit("<script>window.location.href='../payumoney/payuprocess.php';</script>");
		}
	} else {
		exit("<script>window.location.href='../site/error.php?err=errorprocess';</script>");
	}	
	
} else {
	exit("<script>window.location.href='../site/error.php?err=invalid';</script>");
}	

?>
<?  End_Response(); ?>