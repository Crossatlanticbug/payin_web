<?php 
session_start();
//error_reporting(E_ALL);
//ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();
?>

<div class="slider" id="slider">
<div id="mzs"></div>
	<div class="container">

			<div class="col-lg-5 col-sm-6">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="stars" class="btn btn-default tab-btn" href="#tab1" data-toggle="tab">
							<span class="fa fa-mobile" aria-hidden="true"></span>
							<div class="hidden-xs">Mobile</div>
						</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab2" data-toggle="tab">
							<span class="fa fa-television" aria-hidden="true"></span>
							<div class="hidden-xs">DTH</div>
						</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab3" data-toggle="tab">
							<span class="fa fa-signal" aria-hidden="true"></span>
							<div class="hidden-xs">Datacard</div>
						</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" id="following" class="btn btn-default tab-btn" href="#tab4" data-toggle="tab">
							<span class="fa fa-list-ol" aria-hidden="true"></span>
							<div class="hidden-xs">Others</div>
						</button>
					</div>
				</div>

				<div class="well">
					  <div class="tab-content">
						<div class="tab-pane fade in active" id="tab1">
							<form name="rechargeform"  Method="POST" id="rechargeform" onsubmit="return ActionRecharge();" action="../site/orderpreview.php">
								
								<div class="form-inline required" style="margin-bottom: 12px">
									<div class="form-group has-feedback ">
										<label class="input-group">
											<span class="input-group-addon">
												<input type="radio" name="optpayfor" id="optpayfors" value="prepaid" checked/>
											</span>
											<div class="form-control form-control-static">
												prepaid
											</div>
										</label>
									</div>
									<div class="form-group has-feedback">
										<label class="input-group">
											<span class="input-group-addon">
												<input type="radio" name="optpayfor" id="optpayfors" value="postpaid"  />
											</span>
											<div class="form-control form-control-static">
												postpaid
											</div>
										</label>
									</div>
								</div>


								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
										<input id="txtmobileno" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter Mobile Number">
								</div>
								
								<? $result = $main->getOperatorList('mobile');?>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
										<select id="cmboperators" type="text" class="form-control inp-field" name="cmboperator">
											<option value="" >Select Operator</option>
											<?php while($row =mysqli_fetch_assoc($result)) {
												echo "<option value=".$row['operatorid'].">".$row['operatorname']."</option>";
												
											 } ?>
											
										</select>
								</div>
								
								<? $circledata = $main->getCircle();?>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
										<select id="cmbcircles" type="text" class="form-control inp-field" name="cmbcircle">
											<option value="" >Select Circle</option>
											<?php while($rows =mysqli_fetch_assoc($circledata)) {
												echo "<option value=".$rows['circleid'].">".$rows['circle']."</option>";
												
											 } ?>
											
										</select>
								</div>
					
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
										<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
								</div>
									<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="mobile">
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Recharge Now</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
							<!--	<div class="form-group">
									<div style="">
										click here to <a href="#">	Browse Plans	</a>
									</div>
								</div>-->

						</form>

						</div>
						<div class="tab-pane fade in" id="tab2">
						 <form name="rechargedthform"  Method="POST" id="rechargedthform" onsubmit="return ActionRechargeDTH();" action="../site/orderpreview.php">

							

								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
										<input id="txtmobileno" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter DTH Number">
								</div>
								<? $result = $main->getOperatorList('dth');?>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
										<select type="text" class="form-control inp-field" name="cmboperator">
											<option value="" >Select Operator</option>
											<?php while($row =mysqli_fetch_assoc($result)) {
												echo "<option value=".$row['operatorid'].">".$row['operatorname']."</option>";
												
											 } ?>
											
										</select>
								</div>
								
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
										<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
								</div>
									<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="dth">
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Recharge Now</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
								

						</form>

						</div>
						<div class="tab-pane fade in" id="tab3">
						<form name="rechargedatacardform"  Method="POST" id="rechargedatacardform" onsubmit="return ActionRechargedatacard();" action="../site/orderpreview.php">

								
								<div class="form-inline required" style="margin-bottom: 12px">
										<div class="form-group has-feedback ">
										<label class="input-group">
											<span class="input-group-addon">
												<input type="radio" name="optpayfor" value="prepaid" checked/>
											</span>
											<div class="form-control form-control-static">
												prepaid
											</div>
										</label>
									</div>
									<div class="form-group has-feedback">
										<label class="input-group">
											<span class="input-group-addon">
												<input type="radio" name="optpayfor" value="postpaid" readonly />
											</span>
											<div class="form-control form-control-static">
												postpaid
											</div>
										</label>
									</div>

								

								</div>


								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
										<input id="txtmobilenodc" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter DataCard Number">
								</div>
								<? $result = $main->getOperatorList('datacard');?>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
										<select id="cmboperatordc" type="text" class="form-control inp-field" name="cmboperator">
											<option value="" >Select Operator</option>
											<?php while($row =mysqli_fetch_assoc($result)) {
												echo "<option value=".$row['operatorid'].">".$row['operatorname']."</option>";
												
											 } ?>
											
										</select>
								</div>
								
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
										<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
								</div>
									<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="datacard">
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Recharge Now</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
						

						</form>

						</div>
						<div class="tab-pane fade in" id="tab4">
						  <h3>This is tab 4</h3>
						</div>
					  </div>
				</div>
			</div>

			<div class="col-lg-7 col-sm-6">

						<div id="BannerCarousel" class="carousel slide text-right">
							<div class="pull-right">
								<ul style="margin:0px;" class="control-box pager">
									<li><a class="" href="#BannerCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li><a class="" href="#BannerCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
								</ul>
							</div>
							<div class="carousel-inner">

									   <div class="item">
												  <div class="row">
															<div class="col-sm-8">
																<img class="img-responsive" alt="car1" src="../Images/mob1.png">
															</div>
															<div class="col-sm-4">
																<h3> Mobile Recharge </h3>
																<h4>100% Cashback Offers</h4>
																<p>Recharge your mobile instantly to get the maximum value of your bucks. On each recharge, get assured cashback. </p>
																<a href="#" class="btn btn-default">Book Now</a>
															</div>
												   </div>
										  </div><!-- /Slide 1 --> 

										<div class="item">
										 
														<div class="row">
															<div class="col-sm-8">
																<img alt="car2" class="img-responsive" src="../Images/home-recharge.png">
															</div>
															<div class="col-sm-4">
																<h3> DTH </h3>
																<h4> Value for Money Offers </h4>
																<p>Pay DTH bill for any operator. Get guaranteed cashback instantly in your wallet after the payment of your bill.</p>
																<a href="#" class="btn btn-default">Book Now</a>
															</div>
												   </div>
										  </div><!-- /Slide 2 -->

										 <div class="item active">
														<div class="row">
															<div class="col-sm-8">
																<img alt="car3" class="img-responsive" src="../Images/mob3.png">
															</div>
															<div class="col-sm-4">
																<h3>Electricity / Datacard / Landline/Gas</h3>
																<h4>Save Your Pockets</h4>
																<p>Get cashback up to 50% on your first bill. Also get additional offers and credits into your account that can be cashed upon later.</p>
																<a href="#" class="btn btn-default">Book Now</a>
															</div>
												  </div>
										  </div><!-- /Slide 3 --> 
							</div>				  
						</div>		
			</div>

	</div>


</div>
  
<!-- //slider -->



<!--home-content-top starts from here-->
<section class="home-content-top">
  <div class="container">
    
    <h1 class="heading1">Welcome to Payin</h1>
    <div class="tabbable-panel margin-tops4 ">
		<div class="tabbable-line">
			<ul class="nav nav-tabs tabtop tabsetting">
				  <li class="active"> <a href="#tab_default_1" data-toggle="tab"> Mobile Recharge </a> </li>
				  <li> <a href="#tab_default_2" data-toggle="tab"> DTH Recharge </a> </li>
				  <li> <a href="#tab_default_3" data-toggle="tab"> Landline / Datacard </a> </li>
				  <li> <a href="#tab_default_4" data-toggle="tab"> Electricity / Gas Bill </a> </li>

				  <li> <a href="#tab_default_5" data-toggle="tab" class="thbada"> Become a Reseller</a> </li>
			</ul>

			<div class="tab-content margin-tops">
			  <div class="tab-pane active fade in" id="tab_default_1">
				<div class="col-md-4">
				  <div class="row"> <img src="../Images/home-mobile-operators.png" class="img-responsive"> </div>
				</div>
				<div class="col-md-8">
					  <h4 class="heading4">Online Pre-Paid or Post-Paid Mobile Recharge</h4>
					  <p class="para">Register with Payin for hassle-free mobile recharge. Just a click and your mobile recharge is done. Payin has won the trust of millions of hearts as it offers the various plans for different operators.  </p>
					  <p class="para">Covering major operators such as Airtel, Aircel, BSNL, Idea, Relaince, Tata Docomo, Vodafone etc., Payin keeps on updating the plans as offered by various operators. It is easy-to- use, trustworthy and money-saving. </p>

	  				  <h4 class="heading4">Offers/Cashback/Rewards/Coupons</h4>
					  <p class="para">For each prepaid or post paid recharge, you get cashback, rewards and coupons that can be redeemed on your next recharge.</p>
					  <p class="para">Each transaction at Payin is safe, secured and protected. We keep our user’s privacy the top most concern and thus offer the best solution. With quick response time, incredible performance and secured transactions, Payin is made for you.</p>

					  <a href="#">
						<div class="btns">View More <i class="fa fa-angle-right"></i></div>
					  </a> 
				</div>
			  </div>
			  <div class="tab-pane fade" id="tab_default_2">
				<div class="col-md-4">
				 <div class="row"> <img src="../Images/home-dth.png" class="img-responsive"> </div>
				</div>
				<div class="col-md-8">
	  				  <h4 class="heading4">Relish Your TV Watching Experience</h4>
					  <p class="para">Payin offers the user-friendly interface where DTH recharge is just few clicks away. To make your DTH recharge experience less time-consuming, we have come up with the quick recharge process.</p>
					  <p class="para">We do not want you to miss your favourite show and this is why we keep on reminding you the due date of DTH Recharge. Recharge your DTH with Payin in the least time possible. We keep updating the recharge plans as offered by operators to serve you the best. We have covered most of the DTH operators such as Airtel, Dish TV, Reliance DTH, Sun Direct and Tata Sky.</p>
					  
	  				  <h4 class="heading4">Offers/Rewards</h4>
					  <p class="para">With efficient DTH recharge process at Payin, you can relax to get the value for money. We ensure the quickest, simplest and protected transactions on every DTH recharge. With every DTH recharge, you get rewards and offers.</p>

					  <a href="#">
						<div class="btns">View More <i class="fa fa-angle-right"></i></div>
					</a> 
				  </div>
			  </div>
			  <div class="tab-pane fade" id="tab_default_3">
				<div class="col-md-4">
				 <div class="row"> <img src="../Images/home-recharge.png" class="img-responsive"> </div>
				</div>
				<div class="col-md-8">
				  <h4 class="heading4">Instant Bill Payments </h4>
				  <p class="para">Bills for electricity, datacards, landlines and gas are a huge headache. Visiting operator’s office, paying bills and then keeping the bill receipts seem to be cumbersome task. So, let’s switch to digital bill payments.</p>
				  <p class="para">Payin is one-stop solution for paying all utility bills wherein all the major state operators have been integrated. For all the future bill payments, pay through Payin and get unlimited offers such as cashbacks, offers and rewards.</p>
				  
				  <h4 class="heading4">Offers/Rewards</h4>
				  <p class="para">For every transaction, you get additional offers or rewards that can be redeemed on next transaction.</p>
				  <a href="#">
				  <div class="btns">View More <i class="fa fa-angle-right"></i></div>
				  </a> </div>
			  </div>
			  <div class="tab-pane fade" id="tab_default_4">
				<div class="col-md-4">
				 <div class="row"> <img src="../Images/home-happy.png" class="img-responsive"> </div>
				</div>
				<div class="col-md-8">
				  <h4 class="heading4">Instant Bill Payments </h4>
				  <p class="para">Bills for landlines and gas are a huge headache. Visiting operator’s office, paying bills and then keeping the bill receipts seem to be cumbersome task. So, let’s switch to digital bill payments.</p>
				  <p class="para">Payin is one-stop solution for paying all utility bills wherein all the major state operators have been integrated. For all the future bill payments, pay through Payin and get unlimited offers such as cashbacks, offers and rewards.</p>
				  <a href="#">
				  <div class="btns">View More <i class="fa fa-angle-right"></i></div>
				  </a> </div>
			  </div>
			  <div class="tab-pane fade" id="tab_default_5">
				<div class="col-md-4">
				  <div class="row"> <img src="../Images/home-Beach.png" class="img-responsive"> </div>
				</div>
				
				<div class="col-md-8">
				  <h4 class="heading4">Become a Reseller</h4>
				  <p class="para">Click on the Become a Reseller link below and begin taking advantage of the exclusive Reseller features, . </p>
				  <h4 class="heading4">Become a Reseller</h4>
				  <p class="para">Click on the Become a Reseller link below and begin taking advantage of the exclusive Reseller features, .  </p>
				  <a href="#">    <div class="btns">View More <i class="fa fa-angle-right"></i></div>     </a> 
				 </div>
				 
			  </div>
			</div>
      </div>
    </div>
	
  </div>
</section>
<!--home-content-top ends here--> 
<?  End_Response(); ?>