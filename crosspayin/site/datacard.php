<?php 
session_start();
//error_reporting(E_ALL);
//ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();
?>

<div class="slider" id="slider">

	<div class="container">

		<div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-3">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab3" data-toggle="tab">
							<span class="fa fa-signal" aria-hidden="true"></span>
							<div class="hidden-xs">Datacard</div>
						</button>
					</div>
					
				</div>

				<div class="well">
					  <div class="tab-content">
					<div class="tab-pane fade in active" id="tab3">
						<form name="rechargedatacardform"  Method="POST" id="rechargedatacardform" onsubmit="return ActionRechargedatacard();" action="../site/orderpreview.php">

								
								<div class="form-inline required" style="margin-bottom: 12px">
										<div class="form-group has-feedback ">
										<label class="input-group">
											<span class="input-group-addon">
												<input type="radio" name="optpayfor" value="prepaid" checked/>
											</span>
											<div class="form-control form-control-static">
												prepaid
											</div>
										</label>
									</div>
									<div class="form-group has-feedback">
										<label class="input-group">
											<span class="input-group-addon">
												<input type="radio" name="optpayfor" value="postpaid" readonly />
											</span>
											<div class="form-control form-control-static">
												postpaid
											</div>
										</label>
									</div>

								

								</div>


								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
										<input id="txtmobilenodc" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter DataCard Number">
								</div>
								<? $result = $main->getOperatorList('datacard');?>
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
										<select id="cmboperatorsdc" type="text" class="form-control inp-field" name="cmboperator">
											<option value="" >Select Operator</option>
											<?php while($row =mysqli_fetch_assoc($result)) {
												echo "<option value=".$row['operatorid'].">".$row['operatorname']."</option>";
												
											 } ?>
											
										</select>
								</div>
								
								<div style="margin-bottom: 12px" class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
										<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
								</div>
									<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="datacard">
								<button type="submit" class="btn btn-vk btn-block rch-btn" >Recharge Now</button>
								<hr style="margin-top:10px;margin-bottom:10px;" >
						</form>

						</div>
						
						
					
					  </div>
				</div>
			</div>

			
	</div>


</div>
  
<!-- //slider -->



<?  End_Response(); ?>
