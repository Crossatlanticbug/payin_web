<?php 
session_start();
//error_reporting(E_ALL);
//ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();
?>

<div class="slider" id="slider">

	<div class="container">
		<div class="row">
					<div class="col-lg-4 col-sm-6">
							<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<button type="button" id="stars" class="btn btn-default tab-btn" href="#tab1" data-toggle="tab">
										<span class="fa fa-mobile" aria-hidden="true"></span>
										<div class="hidden-xs">Mobile</div>
									</button>
								</div>
								
							</div>

							<div class="well">
								  <div class="tab-content">
									<div class="tab-pane fade in active" id="tab1">
										<form name="rechargeform" Method="POST" id="rechargeform" onsubmit="return ActionRecharge();" action="../site/orderpreview.php">

											
											<div class="form-inline required" style="margin-bottom: 12px">
													<div class="form-group has-feedback ">
													<label class="input-group">
														<span class="input-group-addon">
															<input type="radio" id="optpayfors" name="optpayfor" value="prepaid" checked/>
														</span>
														<div class="form-control form-control-static">
															prepaid
														</div>
													</label>
												</div>
												<div class="form-group has-feedback">
													<label class="input-group">
														<span class="input-group-addon">
															<input type="radio" id="optpayfors" name="optpayfor" value="postpaid" readonly />
														</span>
														<div class="form-control form-control-static">
															postpaid
														</div>
													</label>
												</div>
											</div>

											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
													<input id="txtmobileno" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter Mobile Number">
											</div>
											<? $result = $main->getOperatorList('mobile');?>
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
													<select id="cmboperators" type="text" class="form-control inp-field" name="cmboperator">
														<option value="" >Select Operator</option>
														<?php while($row =mysqli_fetch_assoc($result)) {
															echo "<option value=".$row['operatorid'].">".$row['operatorname']."</option>";
															
														 } ?>
														
													</select>
											</div>
											
											<? $circledata = $main->getCircle();?>
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
													<select id="cmbcircles" type="text" class="form-control inp-field" name="cmbcircle">
														<option value="" >Select Circle</option>
														<?php while($rows =mysqli_fetch_assoc($circledata)) {
															echo "<option value=".$rows['circleid'].">".$rows['circle']."</option>";
															
														 } ?>
														
													</select>
											</div>
											
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
													<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
											</div>
												<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="mobile">
											<button type="submit" class="btn btn-vk btn-block rch-btn" >Recharge Now</button>
											<hr style="margin-top:10px;margin-bottom:10px;" >
											<!--<div class="form-group">
												<div style="">
													click here to <a href="#">	Browse Plans	</a>
												</div>
											</div>-->

									</form>

									</div>
									
									
								
								  </div>
							</div>
					</div>


			<div class="col-lg-8">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="stars" class="btn btn-default tab-btn" href="#tab1" data-toggle="tab">
							<span class="fa fa-mobile" aria-hidden="true"></span>
							<div class="hidden-xs">Recharge Plan</div>
						</button>
					</div>	
				</div>
				
				<section class="section" style="padding:0px !important">
			
						<div class="well">			
									<div class="row">
									
										<div class="col-sm-12 sb_height">

											<a href="#" class="nav-tabs-dropdown btn btn-block btn-primary"></a>

											<ul id="nav-tabs-wrapper" class="nav nav-tabs nav-tabs-horizontal">
													<li class="active">
													<a href="#htab1" data-toggle="tab">Top Up</a>
													</li>
												
													<li><a href="#htab2" data-toggle="tab">Full Talktime</a></li>
													<li><a href="#htab3" data-toggle="tab">2G</a></li>
													<li><a href="#htab4" data-toggle="tab">3G/4G</a></li>
													<li><a href="#htab5" data-toggle="tab">SMS</a></li>
													<li><a href="#htab6" data-toggle="tab">Local/STD/ISD</a></li>
													<li><a href="#htab7" data-toggle="tab">Roaming</a></li>
													<li><a href="#htab8" data-toggle="tab">Other</a></li>
											</ul>

											<div class="tab-content plan">
													<div role="tabpanel" class="tab-pane fade in active" id="htab1">
														<div id="ResultTUP">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade" id="htab2">
														<div id="ResultFTT">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade in" id="htab3">
														<div id="Result2G">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade in" id="htab4">
														<div id="Result3G">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade in" id="htab5" >
														<div id="ResultSMS">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade in" id="htab6">
														<div id="ResultLSC">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade in" id="htab7">
														<div id="ResultRMG">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
													<div role="tabpanel" class="tab-pane fade in" id="htab7">
														<div id="ResultOTR">Enter 10 digit mobile number in given form on the left to view plans.</div>
													</div>
											</div>
										</div>
									</div>
						</div>

				</section>

			</div>

		<!--	//	profile		-->

		</div>
	</div>
</div>

  
<!-- //slider -->

<?  End_Response(); ?>
