<?php 
session_start();
error_reporting(E_ALL);
ini_set("display_errors",1);

include_once("../Includes/template.inc.php");
include_once("../Classes/customer.class.php");
$customer = new customer();
require ("../phpMailer/PHPMailerAutoload.php");
	$CurrentUser = &$_SESSION['CurrentUser'];
	$userid = $CurrentUser->id;
	$pass1 =$_POST['pass1'];
	$result= $customer->getchangepassword($userid,$pass1);
	
if ($result){
		
	//-------------------	 Mail Code 
		$name= isset($result->name) ?$result->name :'Customer';
	
		$to      = $result->email; // Send email to our user
		$subject = 'Reset password'; // Give the email a subject 
		$message=$config['EMAIL_HEADER'];
		$message.= "Dear ".$name.",<br><br>";
		$message.= "Your Reset password is mentioned below:<br><br>";
		$message.= "<strong>Password :  </strong>".$result->password."<br>";
		$message.=$config['EMAIL_FOOTER'];
			
		//Answer key attachment
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $config['SMTP']['HOST'];
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $config['SMTP']['PORT'];
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $config['SMTP']['USER'];
		//Password to use for SMTP authentication
		$mail->Password = $config['SMTP']['PASSWORD'];
		
		$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
		$mail->addReplyTo(''.$config['ADMIN_MAIL'].'', 'PAYIN SUPPORT');
		$mail->addAddress(''.$to.'');
		$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->IsHTML(true); 
		$mail->send();
		
		echo  "1";

} else {
		echo "0";
	
}	

?>
