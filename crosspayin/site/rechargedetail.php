<?php 
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");
	include_once("../Classes/order.class.php");
	$order = new order();
	global $dbLink;
	include_once("../Classes/api.class.php");
	$apiprocess= new RechageApi();

	$CurrentUser = &$_SESSION['CurrentUser'];
	if (!isset($CurrentUser->id)){
		exit("<script>window.location.href='../site/index.php';</script>");
	}
?>		
<style>
	.mainDIV{
		padding:60px 20px 20px 20px;
	}
</style>
<div class="mainDIV">
      <div class="row">
        <div class="col-xs-12">
		
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Payment/Recharge Detail</h3>
            </div>
			<div class="box-body">
				<div class='row'>
			<?php
				if(isset($_GET['ordid'])){
					$rechargeid = $_GET['ordid'];
					$condition="orderid=$rechargeid ";
					$rawdata= $order->Supportrechargeinfo($condition);
					if($rawdata!=""){
						$rowdata=mysqli_fetch_object($rawdata);	
			?>
						<div class='box-body no-padding'>
						  <table class='table table-striped' style='color:black;'>
							<tr>
							  <th style='width: 10px'>#</th>
							  <th>Field</th>
							  <th>Detail</th>
							</tr>
							
							<tr>
								<td>1.</td>
								<td>Name</td>
								<td><?=$rowdata->name?></td>
							</tr>
							
							<tr>
								<td>2.</td>
								<td>Email</td>
								<td><?=$rowdata->email?></td>
							</tr>
							
							<tr>
								<td>3.</td>
								<td>Phone</td>
								<td><?=$rowdata->mobileno?></td>
							</tr>
							
							<tr>
								<td>4.</td>
								<td>Operator</td>
								<td><?=$rowdata->operator?></td>
							</tr>
							
							<tr>
								<td>5.</td>
								<td>Mobile Type</td>
								<td><?=$rowdata->mobiletype?></td>
							</tr>
							
							<tr>
								<td>6.</td>
								<td>Recharge Type</td>
								<td><?=$rowdata->rechargetype?></td>
							</tr>
							
							<tr>
								<td>7.</td>
								<td>Amount</td>
								<td><?=$rowdata->amount?></td>
							</tr>
							
							<tr>
								<td>8.</td>
								<td>Transaction ID</td>
								<td><?=$rowdata->transactionid?></td>
							</tr>
							
							<tr>
								<td>9.</td>
								<td>Recharge Key</td>
								<td><?=$rowdata->rechargekey?></td>
							</tr>
							
							<tr>
								<td>10.</td>
								<td>Recharge Date</td>
								<td><?=$rowdata->rechargedate?></td>
							</tr>
							
							<tr>
								<td>11.</td>
								<td>Payment Mode</td>
								<td><?=$rowdata->payment?></td>
							</tr>
							
							<tr>
								<td>12.</td>
								<td>Payment Status</td>
								<td><?=$rowdata->paymentstatus?></td>
							</tr>
							
							<tr>
								<td>13.</td>
								<td>User IP</td>
								<td><?=$rowdata->userip?></td>
							</tr>
							
							<tr>
								<td>14.</td>
								<td>Recharge Status</td>
								<td><?=$rowdata->rechargestatus?></td>
							</tr>
							
						  </table>
						</div>
				<?	
						}else{
							$msg="No Data Found";
						}
					}
				?>	
						<div><?=isset($msg)?$msg:''?></div>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
<?  End_Response(); ?>
	