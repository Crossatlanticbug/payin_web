<?php 
session_start();
//error_reporting(E_ALL);
//ini_set("display_errors",1);
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();

include_once("../Classes/customer.class.php");
$customer = new customer();

$phone = isset($_REQUEST['txtmobileno'])?$_REQUEST['txtmobileno']:'';
$emailaddress = isset($_REQUEST['txtemailaddress'])?$_REQUEST['txtemailaddress']:'';
$password = isset($_REQUEST['txtpassword'])?$_REQUEST['txtpassword']:'';
$userip = $_SERVER['REMOTE_ADDR'];
if ((!$phone) && (!isset($_REQUEST['action']))){
	exit("<script>window.location.href='../site/index.php';</script>");
}
$status = true;
$postvalue = array();
if ($phone){
	$numbercount= $customer->isPhonenoExist($phone,$emailaddress);
	if ($numbercount==0)	{
		$randomno = GenerateRandomID(6,'NUMERIC');
		$mess="Your phone verification code is ".$randomno;
		$main->sendSms($phone,$mess);
		$postvalus = array('phone'=>$phone,'password' => $password,'email'=>$emailaddress,'userip'=>$userip,'enabled'=>0,'creationdate'=>date("Y-m-d H:i:s"));
		$customer->add($postvalus);
		$postcodevalus = array('phoneno'=>$phone,'code' => $randomno,'status'=>0,'date'=>date("Y-m-d H:i:s"));
		$customer->addNewConfirmation($postcodevalus);
		$displayphone = substr($phone, 0, 2)."******".substr($phone, -2,2);
	} else {
		$status = false;									
	}		
}	

if (isset($_REQUEST['action']) && ($_REQUEST['action']=="checkverificationcode")){
	
	$phoneno = $_REQUEST['phoneno'];
	$verificationcode = $_REQUEST['varificationcode'];
	$result= $customer->checkverificationcode($phoneno,$verificationcode);
	if ($result==1){
		$customer->updateverificationcode($phoneno,$verificationcode);
		$customer->updatestatus($phoneno);
		$_SESSION['UsernNo']=$phoneno;
		$cond = "phone='".$_REQUEST['phoneno']."'";
		$customerdata=$customer->getcustomerinfo($cond);
		$_SESSION['CurrentUser'] = $customerdata;
		echo "1";
	} else {
		echo "0";
	}	
	
	exit();
}	
?>

<div class="slider" id="slider">

	<div class="container">
	<div id="searchresult"></div>
		<div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-3">
				<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" id="favorites" class="btn btn-default tab-btn" href="#tab3" data-toggle="tab">
							<span class="fa fa-signal" aria-hidden="true"></span>
							<div class="hidden-xs">Registration Process</div>
						</button>
					</div>
					
				</div>

				<div class="well">
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab3">
						<?php if ($status) { ?>
						Please enter OPT received on your mobile no <?php echo $displayphone ;?>
							
									<div style="margin-bottom: 12px" class="input-group">
											<span class="input-group-addon"><i class="fa fa-user-secret" aria-hidden="true"></i></span>
											<input id="txtverificationcode" maxlength="6" type="tel" class="form-control inp-field" name="txtverificationcode" value="" placeholder="Enter Veification Code">
											<input id="txtphonno" type="hidden" class="form-control inp-field" name="txtphonno" value="<?php echo $phone ?>" >
									</div>
									<button type="submit" class="btn btn-vk btn-block rch-btn" onclick="Checkverificationcode();" >Submit</button>
									<hr style="margin-top:10px;margin-bottom:10px;" >
						
						<?php } else { ?>
						You are already registered Please click to <a href ="../site/forgotpassword.php">forgot password</a> or <a href ="../site/login.php">login</a>.
						<? } ?>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
<script type="text/javascript">
<!--

function Checkverificationcode(){

	var varificationcode =$('#txtverificationcode').val();
	var phonno =$('#txtphonno').val();
	
		if (varificationcode == "")
		{
			alert("Please enter 6 digit verification code!");
			$('#txtverificationcode').focus();
			return false;
		}
		var len = varificationcode.length;
		if(len !=6)
		{
			alert("Please enter 6 digit verification code!");
			$('#txtverificationcode').focus();
			return false;
		}
		
	var dataString = 'action=checkverificationcode&phoneno=' + phonno +'&varificationcode=' + varificationcode ;

	$.ajax({
		type: "POST",
		dataType: "text",
		url: "../site/registration.php",
		data: dataString,
		beforeSend: function() {
		$('#searchresult').html('<div class="success" style="text-align:"><img src="../Images/loading.gif" height="40"></div>').show('fast');
		},
		success: function(result){
			if(result=='1'){
				window.location.href='../account/myprofile.php'
			}else{
				$('#searchresult').html('<div class="Alert" style="margin-top:0px;">you have entered wrong verification code</div>');
			}
		}
	}); 
	
}
</script>
<?  End_Response(); ?>