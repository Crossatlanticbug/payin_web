<?php 
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");
	include_once("../Classes/api.class.php");
	$apiprocess= new RechageApi();
	include_once("../Classes/order.class.php");
	$orderprocess = new Order();
	include_once("../Classes/main.class.php");
	$main = new main();
	require ("../phpMailer/PHPMailerAutoload.php");
	global $config; 
	$CurrentUser = &$_SESSION['CurrentUser'];
	
	if(isset($_GET['ordid'])){
	$ORDERID = $_GET['ordid'];
	$condition="orderid=$ORDERID ";
	$rawdata= $orderprocess->Supportrechargeinfo($condition);
	
	if($rawdata!=""){
		$rowdata=mysqli_fetch_object($rawdata);	
		
		if(strtolower($rowdata->paymentstatus)=='success'){
			
			$status=$rowdata->paymentstatus;
			$firstname=$rowdata->name;
			$amount=$rowdata->amount;
			$txnid=$rowdata->transactionid;
			$email=$rowdata->email;
			$phone=$rowdata->mobileno;
			$rechargedate=$rowdata->rechargedate;
			
			$ordervalue="rechargestatus='refunded'";
			$orderprocess->UpdateRechargeOrder($ordervalue,$ORDERID) ; 	 
		
			$mess="We will Refund within 7 working days. Detail: OID-$ORDERID | TID-$txnid | AMT-$amount | DT-$rechargedate";
			$main->sendSms($phone,$mess);
			
			$name= isset($rowdata->name) ?$rowdata->name :'Customer';
			
			$to      = $rowdata->email; // Send email to our user
			$subject = 'Refund Request'; // Give the email a subject 
			$message=$config['EMAIL_HEADER'];
			$message.= "Dear ".ucfirst($name).",<br><br>";
			$message.= "We will refund your amount within 7 working days.<br><br>";
			$message.= "Please Find below details regarding the transaction:<br><br>";
			$message.= "<strong>Order No :</strong>".$ORDERID."<br>";
			$message.= "<strong>Transaction ID :</strong>".$txnid."<br>";
			$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
			$message.= "<strong>Payment Status :</strong>".$status."<br><br>";
			$message.= "<strong>Note :</strong>This request is reffered by ".$CurrentUser->name." (Support).<br>";
					 
			$message.=$config['EMAIL_FOOTER'];
				
			//Answer key attachment
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = $config['SMTP']['HOST'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $config['SMTP']['PORT'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $config['SMTP']['USER'];
			//Password to use for SMTP authentication
			$mail->Password = $config['SMTP']['PASSWORD'];
			
			$mail->setFrom(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addAddress(''.$to.'');
			$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->IsHTML(true); 
			$mail->send();
			if($mail->send()){
				$msg= "For Request: ";
				$msg.= "<strong>Order No :</strong>".$ORDERID."<br>";
				$msg.= "<strong>Transaction ID :</strong>".$txnid."<br>";
				$msg.= "<strong>Amount :</strong>INR ".$amount."<br>";
				$msg.= "<strong>Payment Status :</strong>".$status."<br><br>";
				$msg.= "Information Sent to ".$rowdata->name."(Customer)";
			}else{
				$msg= "We are unable to send mail. Please try again.";
			}
		}else{
			$msg= "Oreder Already Recharged Successfully ";
		}	
	}else{
		$msg= "Invalid Order ID";
	}
}
	
?>	
<div class='container' style='padding:50px 0;'><?php echo $msg ;?></div>
<?  End_Response(); ?>