<?php
	if(){
		$scriptname = "serviceData.go";
		$parameter="service_family=".$this->apisfutility;
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyutility."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		if (count($someArray) > 0) {
			$msg.="Recharge Success";
			$msg.=  "<table><thead><tr>
			<th>Detail</th>
			<th>Amount (Rs.)</th>
			<th>Validity (days)</th>
			<th>Select Plan</th>
			</tr></thead><tbody>";
			foreach ($someArray as $key => $value) {
				$radioid = (int)$key+1;
				$amount = $value["Amount"];
				$msg.= " <tr><td>" .$value["Detail"] . "</td> <td>" .$value["Amount"] . "</td> <td>" .$value["Validity"] . '</td><td><input type="radio" id="'.$radioid.'" name="PLANamt" onclick="PushAmount(id,'.$amount.')" ondblclick="Uncheck()"></td> </tr>';
			}
			$msg.=  "</tbody></table><br/>";

		}else{
			$msg.="Recharge Failed";
		}	
		echo $msg;
		
	}


?>

<html>
	<head>
		<title>Payin</title>
		<link rel="icon" type="image/png" href="../Images/favicon.png">
		<link href='//fonts.googleapis.com/css?family=Roboto:700,400&subset=latin' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css'>
		<link rel="stylesheet" href="../css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
			#MSGDIV{color:red;}
		</style>
	</head>
	<body>
		<div class="slider" id="slider">
	<div class="container">
		<div class="row">
			<div class="MSGDIV"> <?php isset($msg) ?$msg :''; ?></div>
					<div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-3">
							<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<button type="button" id="stars" class="btn btn-default tab-btn" href="#tab1" data-toggle="tab">
										<span class="fa fa-mobile" aria-hidden="true"></span>
										<div class="hidden-xs">Mobile</div>
									</button>
								</div>
								
							</div>

							<div class="well">
								  <div class="tab-content">
									<div class="tab-pane fade in active" id="tab1">
										<form name="rechargeform"  Method="POST" id="rechargeform" onsubmit="return ActionRecharge();" action="">

											
											<div class="form-inline required" style="margin-bottom: 12px">
												<div class="form-group has-feedback ">
													<label class="input-group">
														<span class="input-group-addon">
															<input type="radio" id="optpayfors" name="optpayfor" value="prepaid" checked/>
														</span>
														<div class="form-control form-control-static">
															prepaid
														</div>
													</label>
												</div>
												<div class="form-group has-feedback">
													<label class="input-group">
														<span class="input-group-addon">
															<input type="radio" id="optpayfors" name="optpayfor" value="postpaid" readonly />
														</span>
														<div class="form-control form-control-static">
															postpaid
														</div>
													</label>
												</div>
											</div>

											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
													<input id="txtmobileno" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter Mobile Number">
											</div>
											
											
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
													<select id="cmboperators" type="text" class="form-control inp-field" name="cmboperator">
														<option value="" >Select Operator</option>
														<option value="" >Airtel</option>
													</select>
											</div>
											
											
											
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
													<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
											</div>
											<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="mobile">
											<button type="submit" class="btn btn-vk btn-block rch-btn" >Recharge Now</button>
											<hr style="margin-top:10px;margin-bottom:10px;" >
											<!--<div class="form-group">
												<div style="">
													click here to <a href="#">	Browse Plans	</a>
												</div>
											</div>-->

									</form>

									</div>
									
									
								
								  </div>
							</div>
					</div>


			

		<!--	//	profile		-->

		</div>
	</div>
</div>

  
<!-- //slider -->
		
		</body>
	</html>
