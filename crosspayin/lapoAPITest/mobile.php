<?php
	session_start();
	include_once("../Includes/template.inc.php");

	include_once("../Classes/main.class.php");
	$main = new main();

	
	if(isset($_POST['submit'])){
		$username ="A02";
		//$password ="1v3Eq4SG";
		$password ="6363";
		$circle = $_POST['cmbcircle'];
		$operatorcode = $_POST['cmboperator'];
		$amount = $_POST['txtamount'];
		$phone = $_POST['txtmobileno'];
		// $referenceId = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$referenceId =GenerateRandomID(10);
		
		
		//$apiurl="http://www.payin.in/";
		$apiurl="http://api.payinrecharge.org/";
		$scriptname = "odata/RechargeByAPI";
		$parameter="Number=".$phone."&Amount=".$amount."&Opcode=".$operatorcode."&Circle=".$circle."&ReferenceId=".$referenceId;
		$authurl=$scriptname."?Username=".$username."&Password=".$password;
		$url=$apiurl.$authurl."&".$parameter;
		
		echo $url;
		
		
		$mysongs = simplexml_load_file($url);
		$json_string = json_encode($mysongs);
		$result_array = json_decode($json_string, TRUE);
		
		echo "<pre>";
		print_r($result_array);
		echo "</pre>";
		
		 $result = '';
		 $array  = ''; 
		 $ch = curl_init($url);
		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		 curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		 curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		 curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		 $result =  curl_exec($ch);
		 curl_close($ch);
		 $array = json_decode($result, true); 
		
		 echo '<pre>';
		 print_r($array);
		 echo '</pre>';
		
	}


?>

<html>
	<head>
		<title>Payin</title>
		<link rel="icon" type="image/png" href="../Images/favicon.png">
		<link href='//fonts.googleapis.com/css?family=Roboto:700,400&subset=latin' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css'>
		<link rel="stylesheet" href="../css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
			#MSGDIV{color:red;}
		</style>
	</head>
	<body>
		<div class="slider" id="slider">
	<div class="container">
		<div class="row">
			<div class="MSGDIV"> <?php isset($msg) ?$msg :''; ?></div>
					<div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-3">
							<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<button type="button" id="stars" class="btn btn-default tab-btn" href="#tab1" data-toggle="tab">
										<span class="fa fa-mobile" aria-hidden="true"></span>
										<div class="hidden-xs">Mobile</div>
									</button>
								</div>
								
							</div>

							<div class="well">
								  <div class="tab-content">
									<div class="tab-pane fade in active" id="tab1">
										<form name="rechargeform"  Method="POST" id="rechargeform" onsubmit="return ActionRecharge();" action="">

											
											<div class="form-inline required" style="margin-bottom: 12px">
												<div class="form-group has-feedback ">
													<label class="input-group">
														<span class="input-group-addon">
															<input type="radio" id="optpayfors" name="optpayfor" value="prepaid" checked/>
														</span>
														<div class="form-control form-control-static">
															prepaid
														</div>
													</label>
												</div>
												<div class="form-group has-feedback">
													<label class="input-group">
														<span class="input-group-addon">
															<input type="radio" id="optpayfors" name="optpayfor" value="postpaid" readonly />
														</span>
														<div class="form-control form-control-static">
															postpaid
														</div>
													</label>
												</div>
											</div>

											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
													<input id="txtmobileno" maxlength="12" type="tel" class="form-control inp-field" name="txtmobileno" value="" placeholder="Enter Mobile Number">
											</div>
											
											
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
													<select id="cmboperators" type="text" class="form-control inp-field" name="cmboperator">
														<option value="" >Select Operator</option>
														<option value="AC" >Aircel</option>
														<option value="AT" >Airtel</option>
														<option value="BTS" >BSNL</option>
														<option value="TDS" >Docomo</option>
														<option value="ID" >Idea</option>
														<option value="LP" >Loop </option>
														<option value="MTSP" >MTNL</option>
														<option value="RC" >Reliance CDMA </option>
														<option value="RG" >Reliance GSM </option>
														<option value="TI" >Tata Indicom </option>
														<option value="UNS" >Uninor  Mobile</option>
														<option value="VDS" >Videocon </option>
														<option value="VGC" >Virgin CDMA </option>
														<option value="VGG" >Virgin GSM </option>
														<option value="VF" >Vodafone </option>
													</select>
											</div>
											
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-podcast"></i></span>
													<select id="cmbcircles" type="text" class="form-control inp-field" name="cmbcircle">
														<option value="" >Select Circle</option>
														<option value="AP" >Andhra Pradesh </option>
														<option value="AS" >Assam </option>
														<option value="CH" >Chennai </option>
														<option value="DL" >Delhi</option>
														<option value="GJ" >Gujarat  </option>
														<option value="HR" >Haryana </option>
														<option value="HP" >Himachal Pradesh </option>
														<option value="JK" >Jammu & Kashmir </option>
														<option value="KT" >Karnataka </option>
														<option value="KR" >Kerala </option>
														<option value="KK" >Kolkata </option>
														<option value="MP" >Madhya Pradesh & Chhattisgarh </option>
														<option value="MH" >Maharashtra </option>
														<option value="MB" >Mumbai </option>
														<option value="NE" >North East </option>
														<option value="OR" >Orissa</option>
														<option value="PB" >Punjab </option>
														<option value="RJ" >Rajasthan </option>
														<option value="TN" >Tamilnadu </option>
														<option value="UPE" >Uttar Pradesh - East </option>
														<option value="UPW" >Uttar Pradesh – West </option>
														<option value="WB" >West Bengal</option>
													</select>
											</div>
											
											<div style="margin-bottom: 12px" class="input-group">
													<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
													<input id="txtamount" maxlength="12" type="tel" class="form-control inp-field" name="txtamount" value="" placeholder="Amount">
											</div>
											<input id="rechargetype" maxlength="12" type="hidden" class="form-control inp-field" name="rechargetype" value="mobile">
											<button type="submit" class="btn btn-vk btn-block rch-btn" name="submit" >Recharge Now</button>
											<hr style="margin-top:10px;margin-bottom:10px;" >
											<!--<div class="form-group">
												<div style="">
													click here to <a href="#">	Browse Plans	</a>
												</div>
											</div>-->

									</form>

									</div>
									
									
								
								  </div>
							</div>
					</div>


			

		<!--	//	profile		-->

		</div>
	</div>
</div>

  
<!-- //slider -->
		
		</body>
	</html>
