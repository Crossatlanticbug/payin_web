<?php 
$str="The saddest truth about America�s devastating opiate crisis is that it is pretty much legal.";
$keywords = preg_replace('/[^(\x20-\x7F)]*/','', $keywords);

$keywords = str_replace("\"", "", $keywords); // isolate quotes		
echo $str;
?>
<h1 style="text-align:center">Weekly Print Detail</h1>
&lt; td&gt;The recent sharp underperformance of the regional banks underscores the risk of a flatter yield curve.
<u></u>MIG Capital <u></u>
<table>
<tbody>
<tr>
<th>Name</th>
<th>Company Name</th>
<th>Print</th>
<th width="300px;">Title</th>
<th>Print Date</th>
<th>Publish Date</th>
</tr>
<tr>
<td>Min Choung</td>
<td>Centerbridge Partners L.P.</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Cameron Baynard</td>
<td>Highland Capital Management, L.P.</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Jeff Qiu</td>
<td>Credit Suisse Asset Mgmt. LLC ( NY ) </td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Kent Oram</td>
<td>Idaho Central Credit Union</td>
<td>Article</td>
<td>The Rise and Fall of Empires: When they look strongest, they are the weakest. Why? In a word, Hubris.</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Kent Oram</td>
<td>Idaho Central Credit Union</td>
<td>Article</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Terence Kim</td>
<td>Cross Ocean Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Peter Jones</td>
<td>Peter Jones</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Tim Beresford</td>
<td>Arrowpoint Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Maria Creixell</td>
<td>Centerbridge Partners L.P.</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Simon Pottinger</td>
<td>Franchise Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Michal Plotkowiak</td>
<td>The Newton Group</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Tom Danaher</td>
<td>Smith &amp; Williamson Investment Mangement</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 24, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Erik Svalander</td>
<td>Notger Capital</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Mark Limpert</td>
<td>Oaktree Capital -- Real Estate Group</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Christian Jensen</td>
<td>Dragoneer Investment Group, LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>David Bugatto</td>
<td>Alleghany Corporation</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>David Bugatto</td>
<td>Alleghany Corporation</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>David Lerner</td>
<td>David Lerner</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Jerry Gao</td>
<td>Future Fund</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Jack McIver</td>
<td>Future Fund</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Patrick Foley</td>
<td>Oaktree Capital -- Real Estate Group</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Alfred Fisher</td>
<td>Fisher &amp; Co</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Ryan Lee</td>
<td>Oaktree Capital Management, LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Greg Merage</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Rich Rosen</td>
<td>J. &amp; W. Seligman &amp; Company</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Suken Patel</td>
<td>Diamond Hill Capital Management, Inc.</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Michael Power</td>
<td>Investec Asset Management, LTD</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Daniel Burke</td>
<td>Westminster Research Associates</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Susan Anderson</td>
<td>Cannonball Capital Inc</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Heath Gray</td>
<td>FTI Consulting</td>
<td>Report</td>
<td>WHA T I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 2, 2017</td>
</tr>
<tr>
<td>Heath Gray</td>
<td>FTI Consulting</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Heath Gray</td>
<td>FTI Consulting</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Shipra Srivastava</td>
<td>Morgan Stanley</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>John Davitsky</td>
<td>Caldwell Investment Management, LTD</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Robert Kurman</td>
<td>Oaktree Capital Management, LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Cameron Addington</td>
<td>Centralis Capital</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Jim Henderson</td>
<td>Chilton Investment Company</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Thomas O&#39;Neill</td>
<td>D.F. Dent &amp; Co.</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Jay Ghiya</td>
<td>Oaktree Capital Management, LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Chad Hauser</td>
<td>VWK Capital Management, Inc</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Shane Grimaldi</td>
<td>LaunchPad Trading</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Carolyn Noll</td>
<td>T. Rowe Price</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Bill Gouldin</td>
<td>Morgan Stanley Glob al Wealth Management</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Daniel Johnson</td>
<td>Alaric Investments LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Terence Kim</td>
<td>Cross Ocean Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Terence Kim</td>
<td>Cross Ocean Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Marty Cheatham</td>
<td>Hoplite Capital Management, LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Kim Wafer</td>
<td>T. Rowe Price</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 23, 2017</td>
<td>March 23, 2017</td>
</tr>
<tr>
<td>Shawn Rubin</td>
<td>Morgan Stanley PWM</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 22, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Susan Anderson</td>
<td>Cannonball Capital Inc</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 21, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Vassilis Karatzas</td>
<td>Levant Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 21, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Vassilis Karatzas</td>
<td>Levant Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 21, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Erik Svalander</td>
<td>Notger Capital</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 20, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Ryan Lee</td>
<td>Oaktree Capital Management, LLC</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 20, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Shawn Rubin</td>
<td>Morgan Stanley PWM</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 20, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Simon Pottinger</td>
<td>Franchise Partners</td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 20, 2017</td>
<td>March 16, 2017</td>
</tr>
<tr>
<td>Jeff Qiu</td>
<td>Credit Suisse Asset Mgmt. LLC ( NY ) </td>
<td>Report</td>
<td>WHAT I LEARNED THIS WEEK</td>
<td>March 19, 2017</td>
<td>March 16, 2017</td>
</tr>
</tbody>
</table>
<footer style="background:#ecddc8;text-align:center;padding:20px"></footer></div>
</div>
</span>
</div></div></div>

</div><br></div>
</div><br></div>
