<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone',20)->nullable();
			$table->string('email',100);
			$table->string('name',100)->nullable();
			$table->string('password',70);
			$table->double('balance',10,2)->nullable();
			$table->string('address',100)->nullable();
			$table->char('countryid',2)->nullable();
			$table->string('city',40)->nullable();
			$table->string('state',21)->nullable();
			$table->string('zipcode',13)->nullable();
			$table->timestamp('creationdate')->nullable();
			$table->tinyInteger('enabled')->nullable();
			$table->tinyInteger('isreseller')->unsigned()->nullable();
			$table->string('userip',15)->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
