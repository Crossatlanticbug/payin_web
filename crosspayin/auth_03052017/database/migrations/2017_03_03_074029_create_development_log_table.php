<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevelopmentLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('development_log', function (Blueprint $table) {
            $table->increments('id');
            $table->text('paymentlog')->nullable();
            $table->text('rechargelog')->nullable();
			$table->bigInteger('mobileno')->nullable();
			$table->string('emailid',50)->nullable();
			$table->timestamp('processdate')->nullable();
			$table->string('userip',50)->nullable();
			$table->string('pageurl',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('development_log');
    }
}
