<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('UserID');
            $table->string('Username',50);
			$table->string('Password',50);
			$table->string('Name',100)->nullable();
			$table->string('Email',70)->nullable();
			$table->tinyInteger('IsAdmin');
			$table->timestamp('LastLoginDate')->nullable();
			$table->dateTime('CreationDate');
			$table->dateTime('ModificationDate')->nullable();
			$table->string('Timezone',70)->nullable();
			$table->tinyInteger('Enabled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
