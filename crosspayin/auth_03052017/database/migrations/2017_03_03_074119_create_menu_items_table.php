<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('ItemID');
            $table->string('ItemName_Ar',100)->nullable();
            $table->string('ItemName_En',100)->nullable();
			$table->string('PageLink',255)->nullable();
            $table->string('PageTarget',10)->nullable();
            $table->smallInteger('DisplayOrder')->nullable();
            $table->tinyInteger('IsForAdmin')->nullable();
            $table->tinyInteger('IsForNonAdmin')->nullable();
            $table->tinyInteger('Enabled')->nullable();
            $table->integer('PermissionID')->nullable();
			$table->string('KSMParentID',100)->nullable();
            $table->string('KSMID',100)->nullable();
			$table->tinyInteger('IsChild')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
