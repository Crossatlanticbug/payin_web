<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceOperatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_operator', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('code')->nullable();
			$table->string('short_name',50)->nullable();
			$table->string('state',50)->nullable();
            $table->integer('ciorcleid')->nullable();
			$table->string('circle',100)->nullable();
			$table->string('operator',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_operator');
    }
}
