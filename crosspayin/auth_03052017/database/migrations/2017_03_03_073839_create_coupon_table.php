<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
            $table->increments('id');
			$table->string('coupon_name',100)->nullable();
			$table->string('coupon_code',100)->nullable();
			$table->double('amount')->nullable();
			$table->timestamp('start_date');
			$table->timestamp('end_date');
			$table->text('description')->nullable();
			$table->string('coupon_location',250)->nullable();
			$table->Integer('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
