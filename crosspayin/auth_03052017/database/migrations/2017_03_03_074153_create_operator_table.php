<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operator', function (Blueprint $table) {
            $table->increments('id');
            $table->string('operatorname',50)->nullable();
            $table->string('rechargetype',50)->nullable();
            $table->string('operatortype',50)->nullable();
            $table->string('operatorcode',50)->nullable();
            $table->string('area',50)->nullable();
			$table->integer('status')->nullable();
			$table->integer('operatorid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operator');
    }
}
