<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRechargeorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rechargeorder', function (Blueprint $table) {
            $table->increments('orderid');
            $table->integer('customerid')->nullable();
            $table->string('name',50)->nullable();
            $table->string('email',100)->nullable();
			$table->bigInteger('mobileno')->nullable();
            $table->string('operator',100)->nullable();
            $table->string('mobiletype',100)->nullable();
            $table->string('rechargetype',100)->nullable();
            $table->double('amount',6,2)->nullable();
			$table->string('transactionid',100)->nullable();
			$table->string('rechargekey',100)->nullable();
            $table->timestamp('rechargedate')->nullable();
			$table->string('payment',100)->nullable();
			$table->string('paymentstatus',100)->nullable();
			$table->string('userip',50)->nullable();
			$table->string('rechargestatus',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rechargeorder');
    }
}
