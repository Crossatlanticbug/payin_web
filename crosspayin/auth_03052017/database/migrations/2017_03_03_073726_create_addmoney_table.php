<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddmoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addmoney', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customerid');
            $table->double('amount',10,2);
            $table->string('payment',50);
            $table->timestamp('processdate');
            $table->string('paymentstatus',50)->nullable();
            $table->string('transactionid',50)->nullable();
            $table->string('userip',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addmoney');
    }
}
