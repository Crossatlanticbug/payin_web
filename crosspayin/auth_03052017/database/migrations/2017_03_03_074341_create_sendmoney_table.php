<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendmoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendmoney', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('frommobile')->nullable();
            $table->bigInteger('tomobile')->nullable();
            $table->double('amount',10,2)->nullable();
            $table->timestamp('processdate');
            $table->string('transactionid',50)->nullable();
            $table->string('userip',50)->nullable();
            $table->string('payment',50)->nullable();
            $table->string('paymentstatus',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendmoney');
    }
}
