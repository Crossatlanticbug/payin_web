<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
			$table->increments('id');
            $table->char('c_ID',2);
            $table->string('c_ID_3',3);
			$table->string('c_CallingCode',5)->nullable();
			$table->string('c_Name_En',100)->nullable();
			$table->string('c_Name_Ar',100)->nullable();
			$table->tinyInteger('c_ShippingCostID')->nullable();
			$table->tinyInteger('c_Enabled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}
