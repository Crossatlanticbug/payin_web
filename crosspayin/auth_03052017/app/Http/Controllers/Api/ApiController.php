<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Addmoney AS Addmoney;
use App\Menu AS Menu;
use App\Customer AS Customer;

header('Content-type: application/json');	
header('Access-Control-Allow-Origin: *');


class ApiController extends Controller
{
	
	// protected $redirectTo = '/home';
	// public function __construct()
    // {
        // $this->middleware('guest');
    // }
	
	
	public function Login(){
		// echo csrf_token(); die();
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
                $response['status']="FALSE";   
                $response['code']="FAILURE";		 
                $response['message']='Not Acceptable Method';
                return response()->json($response);
                die();
            }
			$post=count($_POST);
			if(empty($post)){  
					$response['status']="FALSE";   
					$response['code']="FAILURE";		
					$response['message']='Not Found Any Params .';
					return response()->json($response);
					die();
			}
			if((!isset($_POST['username'])) OR empty($_POST['username'])){
				$response['status']="FALSE";   
                $response['code']="FAILURE";		 
                $response['message']='User Name should not be Blank.';
                return response()->json($response);
                die();
			}
			if((!isset($_POST['password'])) OR empty($_POST['password'])){
				$response['status']="FALSE";   
                $response['code']="FAILURE";		 
                $response['message']='Password should not be Blank.';
                return response()->json($response);
                die();
			}
			$username= $_POST['username'];
			$password= $_POST['password'];
		
		// $customer= Customer::whereRaw(array('password',"$password"), "AND phone='".$username."' OR email='".$username."' ' ")->first();
		$customer= Customer::whereRaw("(email = '".$username."' OR phone ='".$username."') AND  password ='".$password."' ")->first();
		if($customer==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Incorrect Username OR Password.';
			return response()->json($response);
			die();
		}
		if($customer->enabled==0){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='You are not autherised to login here.';
			return response()->json($response);
			die();
		}
		$res['json_data']['userid']=$customer->id;
		$res['json_data']['name']=$customer->name;
		$res['json_data']['email_id']=$customer->email;
		$res['json_data']['mobile_number']=$customer->phone;
		$res['json_data']['balance']=$customer->balance;
		$res['json_data']['address']=$customer->address;
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Login Successfull";
		return response()->json($res);
   }
   
   public function Logout(){
		
		return response()->json(['json_data'=>['STATUS'=>'TRUE','CODE'=>'SUCCESS','MESSAGE'=>'SUCCESS']]);
   }
   
   public function Menu(){
	   if($_SERVER['REQUEST_METHOD'] != "POST"){  
                $response['status']="FALSE";   
                $response['code']="FAILURE";		 
                $response['message']='Not Acceptable Method';
                return response()->json($response);
                die();
            }
			$post=count($_POST);
			if(empty($post)){
					$response['status']="FALSE";   
					$response['code']="FAILURE";		
					$response['message']='Not Found Any Params .';
					return response()->json($response);
					die();
			}
			if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
				$response['status']="FALSE";   
                $response['code']="FAILURE";		 
                $response['message']='User ID should not be Blank.';
                return response()->json($response);
                die();
			}
		$menu= Menu::where('enabled','1')->orderBy('index','asc')->get();
		$ar=array();
		foreach($menu as $menuvalue){
			$menulist=array(
				'id'=>"$menuvalue->id",
				'name'=>$menuvalue->name
			);
			$ar[]=$menulist;
		}
		$res['json_data']['menu_list']=$ar;
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Menu Successfull";
		return response()->json($res);
   }
   
   public function Addmoney(){
		array_push();
		$addmoney= Addmoney::find(1);
		return response()->json(['json_data'=>$addmoney]);
   }
}
