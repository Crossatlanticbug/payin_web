<?
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* File		:	functions.php
* Version :	1.0
* Date    :	18 January 2017
* Author  :	Neeraj Kumar
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


define("ALPHA_NUMERIC",1);
define("ALPHA",2);
define("NUMERIC",3);


function localizeDate($timestamp = -1, $format = '')
{
	global $lang;

	if ($format == '') {
		$format = $lang['DATE_FORMAT'];
	}

	if ($timestamp == -1) {
		$timestamp = time();
	}

	$date = preg_replace('@%[lL]@', $lang['DAY_OF_WEEK'][(int)strftime('%w', $timestamp)], $format);
	$date = preg_replace('@%[fF]@', $lang['MONTH'][(int)strftime('%m', $timestamp)-1], $date);

	return strftime($date, $timestamp);
}

// This function is used for the pagination
function Pagination($screen, $perpage, $pages, $link)
{
	global $lang;

	$prev = $lang['Previous'];
	$next = $lang['Next'];

	// Previous Link
	if ($screen > 0)
	{
		$url = $link.".php?screen=".($screen - 1)."#result";
		echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink'> ".$prev."</a>  ";
	}
	else
	{
		echo "  ".$prev." ";
	}
	// Pages links
	$first = $screen - ($screen % $perpage);
	$last = min($pages, $first + $perpage);
	for ($i=$first; $i<$last; $i++)
	{
		if ($i<9) $strZero = "0"; else $strZero = "";
		if ($i != $screen) {
			$url = $link.".php?screen=".$i."#result";
			echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink'> ".$strZero.($i+1)."</a> ";
		}else{
			echo "<span class='SelectedPage'>".$strZero.($i+1)."</span>";
		}
	}
	// Next Link
	if ($screen < $pages-1)
	{
		$url = $link.".php?screen=".($screen + 1)."";
		echo " <a href=\"javascript:SubmitFormAction('".$url."')\" class='PaginationLink'>".$next."</a> ";
	}
	else
	{
		echo " ".$next." ";
	}
}


function realRand($min=null, $max=null)
{
	if(is_null($min) == is_null($max))//means either both are null or both are not
	{

		// seed with microseconds
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float) $sec + ((float) $usec * 100000);
		srand($seed);
		if(is_null($min))
		return rand();
		return rand($min, $max);
	}

	echo "ERROR: invalid number of arguments to function realRand()";
}

function GenerateRandomID($chars, $type = NUMERIC)
{
	switch($type)
	{
		case ALPHA_NUMERIC:
			$allowchars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
			break;
		case ALPHA:
			$allowchars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			break;
		case NUMERIC:
		default:
			$allowchars = '0123456789';
	}

	// Length of character list
  $chars_length = (strlen($allowchars)-1);
  // Start our string
  $string = $allowchars[realRand(0, $chars_length)];
  // Generate random string
	for ($i = 1; $i < $chars; $i = strlen($string))
  {
  	// Grab a random character from our allowed characters list
    $r = $allowchars[realRand(0, $chars_length)];
    // Make sure the same two characters don't appear next to each other
    if ($r != $string[$i - 1]) $string .=  $r;
  }

	return $string;
}



?>