<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['username'])) OR empty($_POST['username'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='User Name should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['password'])) OR empty($_POST['password'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Password should not be Blank.';
			echo json_encode($response);
			die();
		}
		$username= $_POST['username'];
		$password= $_POST['password'];
	
		$customer= $apivalues->login($username,$password);
		if($customer==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Incorrect Username OR Password.';
			echo json_encode($response);
			die();
		}
		if($customer->enabled==0){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='You are not autherised to login here.';
			echo json_encode($response);
			die();
		}
		$res['json_data']['userid']=(int)$customer->id;
		$res['json_data']['name']=$customer->name;
		$res['json_data']['email_id']=$customer->email;
		$res['json_data']['mobile_number']=$customer->phone;
		$res['json_data']['balance']=$customer->balance;
		$res['json_data']['address']=$customer->address;
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Login Successful";
		echo json_encode($res);
	
?>