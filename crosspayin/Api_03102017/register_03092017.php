<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['name'])) OR empty($_POST['name'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Name should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['email'])) OR empty($_POST['email'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Email should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['phone'])) OR empty($_POST['phone'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Phone should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['password'])) OR empty($_POST['password'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Password should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$name= isset($_POST['name']) ?$_POST['name'] :'';
		$email=  isset($_POST['email_id']) ?$_POST['email_id'] :'';
		$phone=  isset($_POST['phone']) ?$_POST['phone'] :'';
		$password= $_POST['password'];
		
		$numbercount= $apivalues->isPhonenoExist($phone,$email);
		if((int)$numbercount>0){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='This Email OR Phone is already exist.';
			echo json_encode($response);
			die();
		}
		
		$randomno = GenerateRandomID(6,'NUMERIC');
		$mess="Your phone verification code is ".$randomno;
		$apivalues->sendSms($phone,$mess);
		$postvalus = array('name'=>$name,'phone'=>$phone,'password' => $password,'email'=>$email,'userip'=>"",'enabled'=>0,'creationdate'=>date("Y-m-d H:i:s"));
		$apivalues->add($postvalus);
		$postcodevalus = array('phoneno'=>$phone,'code' => $randomno,'status'=>0,'date'=>date("Y-m-d H:i:s"));
		$apivalues->addNewConfirmation($postcodevalus);		
		
		$res['json_data']['phone']=$phone;
		$res['json_data']['otp']=$randomno;
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Enter Verification Code to enable your account";
		echo json_encode($res);
	
?>