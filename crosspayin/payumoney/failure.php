<?php
session_start();
include_once("../Includes/template.inc.php");

include_once("../Classes/main.class.php");
$main = new main();

global $config; 
$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt=$config['PAYU'][$ENV]['SALT'];
$phone =$_POST["phone"];
include_once("../Classes/order.class.php");
$orderprocess = new Order();
require ("../phpMailer/PHPMailerAutoload.php");

if (!isset($_SESSION['ORDERID'])){
	exit("<script>window.location.href='../site/error.php?err=invalid';</script>");
}	

$result = $orderprocess->getRechargeOrderInfo($_SESSION['ORDERID']);

$ordervalue="transactionid='".$txnid."',paymentstatus='".$_POST["status"]."'";
		
$orderprocess->UpdateRechargeOrder($ordervalue,$_SESSION['ORDERID']) ;  	   

echo "<div class='container' style='padding:50px 0;'>";
echo "<h3>Your order status is ". $status .".</h3>";
echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
echo "<p><a href=".$config['HOST']."> Try Again</a></p>";
echo "</div>";

$phone=$result->mobileno;
$amount=floor($result->amount);
$mess="Your Order has been  failed. Plese try again";
$main->sendSms($phone,$mess);

$to= $result->email; // Send email to our user
$subject = 'Order Cancelled'; // Give the email a subject 
$message=$config['EMAIL_HEADER'];
$message.= "Dear ".$result->name.",<br><br>";
$message.= "Your order has been failed . Please try again.<br><br>";
$message.= "<strong>Order No :</strong>".$_SESSION['ORDERID']."<br>";
$message.= "<strong>Transaction ID :</strong>".$txnid."<br>";
$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
$message.= "<strong>Payment Status :</strong>".$status."<br>";
				 
$message.=$config['EMAIL_FOOTER'];
			
//Answer key attachment
$mail = new PHPMailer;
$mail->isSMTP();
$mail->Host = $config['SMTP']['HOST'];
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = $config['SMTP']['PORT'];
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication
$mail->Username = $config['SMTP']['USER'];
//Password to use for SMTP authentication
$mail->Password = $config['SMTP']['PASSWORD'];
$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
$mail->addAddress(''.$to.'');
$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
$mail->Subject = $subject;
$mail->Body = $message;
$mail->IsHTML(true); 
$mail->send();
	
?>
<!--Please enter your website homepagge URL -->


<?  End_Response(); ?>