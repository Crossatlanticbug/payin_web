<?php
session_start();
include_once("../Includes/template.inc.php");

global $config; 
$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
$merchantkey = $config['PAYU'][$ENV]['KEY'] ; 
$salt = $config['PAYU'][$ENV]['SALT'] ; 
$action = $config['PAYU'][$ENV]['URL'] ; 
$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

include_once("../Classes/order.class.php");
$orderprocess = new Order();

if (isset($_SESSION['ADDMONEYORDERID'])) { 
	$result = $orderprocess->getAddMoneyOrderInfo($_SESSION['ADDMONEYORDERID']);
	$amount = $result->amount;
	$cname = $result->name;
	$cemail = $result->email;
	$cmobileno = $result->mobileno;
	$productinfo = "Add Money  for Mobile No ".$result->mobileno;
	$udf1 = $_SESSION['ADDMONEYORDERID'];
	$udf2 ="Wallet";
} else if (isset($_SESSION['ORDERID'])) { 
	$result = $orderprocess->getRechargeOrderInfo($_SESSION['ORDERID']);
	$amount = $result->amount;
	$cname = $result->name;
	$cemail = $result->email;
	$cmobileno = $result->mobileno;
	$productinfo = "Recharge ".$result->rechargetype." ".$result->mobileno;
	$udf1 = $_SESSION['ORDERID'];
	$udf2 ="Recharge";
	
} else {
	exit("<script>window.location.href='../site/error.php?err=errorprocess';</script>");
}	
$posted['key']=$merchantkey;
$posted['txnid']=$txnid;
$posted['amount']=$amount;
$posted['firstname'] = $cname;
$posted['email']= $cemail;
$posted['phone']=$cmobileno;
$posted['productinfo'] = $productinfo;
$posted['surl']=$config['HOST']."/payumoney/success.php";
$posted['furl']=$config['HOST']."/payumoney/failure.php";
$posted['curl']=$config['HOST']."/payumoney/failure.php";
// $posted['service_provider'] =($config['TEST_ENV']) ? '' : 'payu_paisa';
$posted['service_provider'] =($config['TEST_ENV']) ? '' : '';
$posted['udf1'] =$udf1;
$posted['udf2'] =$udf2;


$hash = '';
// Hash Sequence
//$hashSequence = "key|txnid|amount|firstname|email|phone|productinfo|surl|furl|curl|service_provider";
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
$hashVarsSeq = explode('|', $hashSequence);
$hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }


$hash_string .= $salt;

$hash = strtolower(hash('sha512', $hash_string));
	

?>
<html>
  <head>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
     payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()">
    <div style="padding-left:200px"> <img src="../Images/loading.gif"></div>
   <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $merchantkey; ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
	  <input type="hidden" name="salt" value="<?php echo $salt ?>" />
      <input type="hidden" name="amount" value="<?php echo $posted['amount']; ?>" />
      <input type="hidden" name="firstname" id="firstname" value="<?php echo $posted['firstname']; ?>" />
      <input type="hidden" name="email" id="email" value="<?php echo $posted['email']; ?>" />
      <input type="hidden" name="phone" value="<?php echo $posted['phone']; ?>" />
	  <input type="hidden" name="productinfo" value="<?php echo $posted['productinfo']; ?>" />
      <input type="hidden" name="surl" value="<?php echo $posted['surl']; ?>"  />
      <input type="hidden" name="furl" value="<?php echo $posted['furl']; ?>"  />
	  <input type="hidden" name="service_provider" value="<?php echo $posted['service_provider'];?>"  />
      <input type="hidden" name="curl" value="<?php echo $posted['furl']; ?>" />
	  <input type="hidden" name="udf1" value="<?php echo $udf1; ?>" />
	  <input type="hidden" name="udf2" value="<?php echo $udf2; ?>" />
    </form>
  </body>
</html>