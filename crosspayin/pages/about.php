<?php 
session_start();
include_once("../Includes/template.inc.php");
?>


<div class="parallax"></div>

<div id="about">
    <div class="title-pg text-center">
      <h1>About</h1>
      <span>Fixed header and parallax scrolling background with a rhombus indicator</span>
	</div>
</div>

<section class="section content">
	<div class="container">
		<div class="col-sm-8 col-sm-offset-2">
			<h1 class="heading4">ABOUT US</h1>
			<p>Payin.in is India’s one of the leading recharge &amp; bill payment site, designed for customers convenience that delivers quick and safe online transaction portal to the customers across the world. Payin.in is a secure service provider who is focused on developing a strong technical core to make your experience with Payin more secure and reliable. We are aimed to serve you in such way that you don’t find need to go in hustle and stand in a long queue for your prepaid recharge &amp; bill payments. Payin.in make your recharge &amp; bill payment simple advantageous, with its exciting discount coupons, cashback and much more. So register yourself with Payin and enjoy our services.</p>
		</div>

		<div class="col-sm-8 col-sm-offset-2">
				<div class="tabbable tabs-left">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#Vision " data-toggle="tab">Vision </a></li>
							<li><a href="#Mission" data-toggle="tab">Mission</a></li>
						</ul>

						<div class="tab-content">

							<div class="tab-pane active" id="Vision">                
								<div class="vk-tab">
									<h1>OUR VISSION</h1>
									<p>“We aspire to provide services in a way that Payin become your optimal choice for everything.”</p>                 
								</div>
							</div>

							<div class="tab-pane" id="Mission"> 
								<div class="vk-tab">
									<h1>OUR MISSION</h1>
									<p>To work beyond the bounds to meet customer’s expectations of services in order to exceed our offerings by introducing new products &amp; services based on customers demand. Our aim is to conspire with endless merchants &amp; service providers to serve our customers endless services at their fingertips. We intend to create experiences which make us customer’s first choice in online market with our best customer services possible.</p>
								</div>
							</div>

						</div>
				</div>
		</div>
	</div>
</section>

<?  End_Response(); ?>