<?php 
session_start();
include_once("../Includes/template.inc.php");?>




<div class="parallax"></div>

<div id="policies">
    <div class="title-pg text-center">
      <h1>Payment Options</h1>
      <span>Fixed header and parallax scrolling background with a rhombus indicator</span>
	</div>
</div>

<section class="section ">
	<div class="container">
		<div class="policy-content">
		<div style="align:center"><b>FAQ</b></div>
		<p><b>How to create Payin account?</b></p>
		To create Payin account:
		<ul>
			<li>Register your mobile number and email address</li>
			<li>Create a password</li>
			<li>Enter One Time Password (OTP) sent to your mobile number</li>
			<li>Enter your First name and Last name</li>
			<li>Click create account, and you are registered with Payin</li>
		</ul>
		
		<p>How to add money in payinWallet?</p>
		<ul>
			<li>Login to your Payin account with your registered mobile number and password.</li>
			<li>Click on "Payin Wallet" icon </li>
			<li>Enter the amount you want to add to your Payin Wallet by clicking “Add Money”</li>
			<li>Select your option (Credit/Debit card, Net Banking ATM card) to add money</li>
			<li>You will be redirected to the payment options where you need to enter your card details and make payment. Once the Payment is successful, the recharge amount will be added to your PayinWallet.</li>
		</ul>
		
		<p>How will I know that the payment is successful?</p>
		You can check the confirmation in your order summary.

		<p>How can I send money to someone using Payin?</p>
		Sending money with Payin is quick and simple
		<ul>
			<li>Click on pay icon</li>
			<li>Enter mobile number whom you want to send money</li>
			<li>Enter the amount you are willing to send</li>
			<li>Mention the reason of sending money. This field is optional.
			<li>Enter send button</li>
			<li>With sufficient balance in your Payin Wallet money will be successfully sent</li>
		</ul>
		
		<p>What should I do if my amount is deducted but recharge failed?</p>
		If the amount is deducted and recharge failed, the amount of failed recharge will be refunded on your Payin Wallet within 48 hrs. But if the recharge is done by other options the bank will refund the amount on your bank account within 3-10 days.

		<p>I have forgotten my password, how do I get new one.</p>
		<ul>	
			<li>Click on "Log In/Sign Up"</li>
			<li>Click on "Forget Password" on login page</li>
			<li>Enter your registered mobile number or email address</li>
			<li>Enter the OTP sent on your mobile number or email address</li>
			<li>Enter new password and proceed</li>
			<li>Your password will be updated </li>


		<p>Is my account information safe?</p>
		All information transmitted between you and Payin is completely safe & secure. Never share your login credential with anyone.

		<p>I recharged on wrong number. How do I get it back?</p>
		Recharge once successful can not be reversed. We suggest you to contact your mobile operator for help with recharge.

		<p>How can I register online with payin as retailer/distributor?</p>
		Click on “Become a Reseller” and register yourself with Payin afterwards you can begin taking advantages of Reseller features.

		<p>What is the refund procedure of payin?</p>
		Payin has a Refund Policy, means any failed transaction will be refunded in your Payin Wallet within 48 hrs.

		<p>How can I submit complaint?</p>
		You can submit your complaint on info@payin.in and we will get back to you as quickly as possible.

		<p>Is payin safe for online transactions through debit/credit card or net banking?</p>
		The connection to Payin through the browser are 128-bit encrypted. Which means all the card details added while making online transaction will always be safe & secure. We always look forward to ensure the safe handling of sensitive information for both payments and recharges.

		<p>I did not receive the cash back.</p>
		Cash back will be credited to your Payin Wallet within 24 hours of a successful transaction.

		<p>I cancelled my order, how do I get the refund amount?</p>
		If the payment was made using Payin Wallet, we will refund the amount to the same. But, if the payment was made using debit/credit card or net banking, the money will be refund in your bank account.

		<p>Will I be charged any money if I pay using Debit/Credit card?</p>
		No extra money will be charged for using a specific payment mode.
		</div>
	</div>	
</section>
<?  End_Response(); ?>