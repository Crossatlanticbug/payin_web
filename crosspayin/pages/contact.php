<?php 
session_start();
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once("../Includes/template.inc.php");
global $config;
require ("../phpMailer/PHPMailerAutoload.php");

if(isset($_POST['submit']) && $_POST["captcha"]!="" && $_SESSION["code"]!=$_POST["captcha"])
{
	echo "<script>
	alert('Not a valid Captcha');
	</script>";
}
	$name = isset($_POST['txtname']) ?$_POST['txtname'] :'';
	$email = isset($_POST['txtemail']) ?$_POST['txtemail'] :'';
	$phone =  isset($_POST['txtphone']) ?$_POST['txtphone'] :'';
	$address = isset($_POST['txtaddress']) ?$_POST['txtaddress'] :'';
	$messages = isset($_POST['txtmessage']) ?$_POST['txtmessage'] :'';
	$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
if(isset($_POST['submit']) && isset($_POST["captcha"]) && $_POST["captcha"]!="" && $_SESSION["code"]==$_POST["captcha"])
{
		//-------------------	 Mail Code Start
		$to      = $email; // Send email to our user
		$subject = 'Contact us'; // Give the email a subject 
		$message=$config['EMAIL_HEADER'];
		$message.= "A user  ".$name." submitted the contact form:<br><br>";
		$message.= "<strong>Name :  </strong>".$name."<br>";
		$message.= "<strong>Email :  </strong>".$email."<br>";
		$message.= "<strong>Phone :  </strong>".$phone."<br>";
		$message.= "<strong>Address :  </strong>".$address."<br>";
		$message.= "<strong>Message :  </strong>".$messages."<br>";
		$message.= "<strong>User IP :  </strong>".$ip ."<br>";
		$message.=$config['EMAIL_FOOTER'];
			
		//Answer key attachment
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $config['SMTP']['HOST'];
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $config['SMTP']['PORT'];
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $config['SMTP']['USER'];
		//Password to use for SMTP authentication
		$mail->Password = $config['SMTP']['PASSWORD'];
		
		$mail->setFrom(''.$to.'', ''.$name.'');
		$mail->addReplyTo(''.$to.'', ''.$name.'');
		$mail->addAddress(''.$config['ADMIN_MAIL'].'', "PAYIN ADMIN");
		$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->IsHTML(true); 
		$mail->send();
		if($mail->send()){
			echo "<script>alert('Contact Mail Sent Successfully'); </script>";
			$name = '';
			$email = '';
			$phone =  '';
			$address = '';
			$messages = '';
		}else{
			echo "<script>alert('Please Try Again');</script>";
		}
}

?>
	<div id="about">
	
		<div class="parallax"></div>

		<div class="title-pg text-center">
		  <h1>Contact</h1>
		  <span>Fixed header and parallax scrolling background with a rhombus indicator</span>
		</div>
	</div>

<section class="section contact">
	<div class="container bootstrap snippets">
			<div class="row text-center padding50">

				<div class="col-sm-6">
				  <div class="contact-detail-box">
					<i class="fa fa-th fa-3x text-colored"></i>
					<h4>Get In Touch</h4>
						<address>
						5D/8E, Railway Road<br> 
						NIT -5, Near MCF Colony<br>
						Faridabad -121001, Haryana<br>
						<abbr title="Phone">P: </abbr>+ 91-129-2412255/56<br>
						E: <a href="mailto:info@payin.in" class="text-muted">info@payin.in</a>
						</address>
						
				  </div>
				</div><!-- end col -->
				
				
				<div class="col-sm-6">
				  <div class="contact-detail-box">
					<i class="fa fa-book fa-3x text-colored"></i>
					<h4>24x7 Support</h4>
					<abbr title="Phone">P:</abbr>18001802259
					<br>E: <a href="mailto:support@payin.in" class="text-muted">support@payin.in</a>
				  </div>
				</div><!-- end col -->

			</div>
      <!-- end row -->

			  <div class="row">
				<div class="col-sm-6">
					<div class="contact-map">					  
						<iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28017.356144717476!2d77.36904874087762!3d28.624681074913802!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ceff135b3084b%3A0x19ccb4e95c69306d!2sSector+63%2C+Noida%2C+Uttar+Pradesh!5e0!3m2!1sen!2sin!4v1487848709642" style="width:100%; border:5px solid #ED4C5F; height:300px;" allowfullscreen></iframe>  
					</div>
				</div><!-- end col -->

				<!-- Contact form -->
				<div class="col-sm-6">
						<div class="row">
								<span id="SuccessMsg"></span>  
								<div class="col-md-6">
									<form  name="contact_form" action="" method="POST" onsubmit="return validateForm()">
											<div class="form-group">
												<span class="glyphicon glyphicon-user" aria-hidden="true" ></span>
												<span class="sr-only"></span>
												<label for="inputName">Name</label>
												<input type="Name" class="form-control" name="txtname" id="inputNAME" placeholder="Please enter your full name" value="<?php echo $name ;?>" onblur="checkName()" autocomplete="off">
												<span style="color:red;" id="nameError"></span>  
											</div>
											
											<div class="form-group ">
												<span class="glyphicon glyphicon-earphone" aria-hidden="true" ></span>
												<span class="sr-only"></span>
												<label for="inputNumber">Phone</label>
												<input type="number" class="form-control" name="txtphone" id="inputNumber" placeholder="Phone Number" value="<?php echo $phone;?>" onblur="checkPhone()" autocomplete="off">
												<span style="color:red;" id="phoneError"></span>  
											</div>
								</div>
								
								<div class="col-md-6">
										  
										<div class="form-group">
											<span class="glyphicon glyphicon-envelope" aria-hidden="true" ></span>
											<span class="sr-only"></span>
											<label for="inputEmail">Email</label>
											<input type="email" class="form-control" name="txtemail" id="inputEmail" placeholder="Email" onblur="checkEmail()" value="<?php echo $email;?>" autocomplete="off">
											<span style="color:red;" id="emailError"></span>
											<span style="color:red;" id="emailErrors"></span>
										</div>
										<div class="form-group">
											<span class="glyphicon glyphicon-home" aria-hidden="true" ></span>
											<span class="sr-only"></span>
											<label for="inputAddress">Address</label>
											<input type="Address" class="form-control" id="inputAddress" name="txtaddress" placeholder="Full Address" value="<?php echo $address;?>" autocomplete="off">
										</div>
								</div>
						</div>

						<div class="row">
								<div class="col-md-12">
										<div class="form-group">
												<span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>
												<span class="sr-only"></span>
												<label for="inputMessage" for="message">Enter Your message</label>
												<textarea class="form-control" rows="5" id="message" name="txtmessage" placeholder="Enter your message"><?php echo $messages;?></textarea>
										</div>
								</div>
							</div>

<style>
		.input-group input{
			border:1px solid #ccc;
			min-height:40px;
		}
</style>
								<div class="row">

										<div class="col-md-12">
																<label for='inputCaptcha'> <i class="fa fa-lock" aria-hidden="true"></i> Enter the CAPTCHA code here :</label>
																<div class="input-group">
																	<input name="captcha" id="inputCaptcha" type="text" class="form-control" onblur="checkCaptcha()" autocomplete="off">
																	<span class="input-group-addon" style="background-color: #1656a5;">
																	<img src="captcha.php" />
																	</span>
																</div>
																<span style="color:red;" id="captchaError"></span>
										</div>
					
								</div> 
								<br>		
				
									<div class="form-group">
											<input type="submit" id="contactbtn" class="btn btn-success" name="submit" value="Send" />
									</div>




									</form>
							
						</div> 


				</div> <!-- end col -->
			</div> <!-- end row -->
    </div>
</section>
<script type="text/javascript">
	function validateForm(){
		var Name=document.getElementById("inputNAME").value;
		if (Name==null || Name==""){
			document.getElementById("nameError").innerHTML = "Please Enter Your Name";
			return false;
		}
		var Email=document.getElementById("inputEmail").value;
		if (Email==null || Email==""){
		   document.getElementById("emailError").innerHTML = "Please Enter Your Valid Email";
		   return false;
		}
		
		var Phone=document.getElementById("inputNumber").value;
		if (Phone==null || Phone==""){
		   document.getElementById("phoneError").innerHTML = "Please Enter Phone No.";
		   return false;
		}
		if (Phone.length!=10) {
			document.getElementById("phoneError").innerHTML = "Please Enter 10 Digit Phone No.";
			return false;
		}		
		
		var Captcha=document.getElementById("inputCaptcha").value;
		if (Captcha==null || Captcha==""){
			document.getElementById("captchaError").innerHTML = "Please Enter Valid Captcha";
		   return false;
		}	
	}	
	
	function checkName(){
		var Name=document.getElementById("inputNAME").value;
		if (Name==null || Name==""){
			document.getElementById("nameError").innerHTML = "Please Enter Your Name";
			return false;
		}
		document.getElementById("nameError").innerHTML = "";
		return false;
	}	
	
	function checkPhone(){
		var Phone=document.getElementById("inputNumber").value;
		if (Phone==null || Phone==""){
		   document.getElementById("phoneError").innerHTML = "Please Enter Phone No.";
		   return false;
		}else if (Phone.length!=10) {
			document.getElementById("phoneError").innerHTML = "Please Enter 10 Digit Phone No.";
			return false;
		}else{	
			document.getElementById("phoneError").innerHTML = "";
			return false;
		}
	}	
	
	function checkEmail(){
		var Email=document.getElementById("inputEmail").value;
		if (Email==null || Email==""){
		   document.getElementById("emailError").innerHTML = "Please Enter Your Valid Email";
		   return false;
		}
		document.getElementById("emailError").innerHTML = "";
		return false;
	}	
	
	function checkCaptcha(){
		var Captcha=document.getElementById("inputCaptcha").value;
		if (Captcha==null || Captcha==""){
			document.getElementById("captchaError").innerHTML = "Please Enter Captcha";
			return false;
		}
			document.getElementById("captchaError").innerHTML = "";
			return false;
	}	
	
</script>
<?  End_Response(); ?>