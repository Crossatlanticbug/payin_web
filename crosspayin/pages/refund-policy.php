<?php 
session_start();
include_once("../Includes/template.inc.php");?>

<div class="parallax"></div>

<div id="policies">
    <div class="title-pg text-center">
      <h1>Refund Policy</h1>
      <span>Fixed header and parallax scrolling background with a rhombus indicator</span>
	</div>
</div>

<section class="section ">

		<div class="container">
		
		
		
		
<p>payin.in is committed to process all refunds within 24 hours.</p>
<p>Please note that banks take 3-5 days to process the refunds.</p>


			<div class="panel-group" id="accordion1">
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h5 class="panel-title">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">Who can get a refund?</a>
					</h5>
				  </div>
				  <div id="accordion1_1" class="panel-collapse collapse in">
					<div class="panel-body">
					  <p>payin.in gives refund if payment is successful, and payment is credited to Payin balance but user is not able to get recharge.</p>
					</div>
				  </div>
				</div>
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h5 class="panel-title">
					  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">How to request a refund?</a>
					</h5>
				  </div>
				  <div id="accordion1_2" class="panel-collapse collapse">
					<div class="panel-body">
					  <p>Fill in the Customer Support form and mention clearly your current Payin balance and what problem you are facing with recharge. Please note that banks take 3-5 days to process the refunds.</p>
					</div>
				  </div>
				</div>
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h5 class="panel-title">
					  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">I made a payment, but my mobile did not get recharged. What to do?</a>
					</h5>
				  </div>
				  <div id="accordion1_3" class="panel-collapse collapse">
					<div class="panel-body">
					  <p>Login to , go to My Account and check your Payin balance. If your payment is reflecting in Payin balance, click on “Recharge” button against the cell number. If payment is not added to Payin balance, Fill in the Customer Support form and mention the transaction reference number you got from the payment. All such requests will be processed within 24 hours and payment will be added to Payin balance.</p>
					</div>
				  </div>
				</div>
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h5 class="panel-title">
					  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4">What if I have excess Payin balance but I don’t want to use it?</a>
					</h5>
				  </div>
				  <div id="accordion1_4" class="panel-collapse collapse">
					<div class="panel-body">
					  <p>According to RBI regulation, it is not possible to convert wallet amount into currency, therefore, we cannot refund unless there is a problem with recharge.</p>
					</div>
				  </div>
				</div>
			  </div>
		</div>

</section>

<?  End_Response(); ?>