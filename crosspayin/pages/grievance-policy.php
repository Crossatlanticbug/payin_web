<?php 
session_start();
include_once("../Includes/template.inc.php");?>

<div class="parallax"></div>

<div id="policies">
    <div class="title-pg text-center">
      <h1>Refund Policy</h1>
      <span>Fixed header and parallax scrolling background with a rhombus indicator</span>
	</div>
</div>

<section class="section ">

		<div class="container">

				<div class="policy-content">
					<b>Preamble</b>
					<p>One Payin Systems Private Limited. (the Company) is engaged in the following business segment<p>
						<h3>SEGMENT A:</h3>
						<p>Semi-Closed prepaid Wallet business: Issuance of multipurpose prepaid Payin Wallet as a payment option alternative to cash for large segment of population in the country that is unable to use e-payment / m-payment facility since they do not have debit/credit cards.</p>
						<p>(Semi-Closed prepaid Payin Wallet business is governed by the PSS Act and the RBI Guidelines).</p>
						<p>The Company offers safe and easy payment option to every customer for seamless transactions across M-Commerce and E-Commerce domains. The Company has introduced multi-purpose prepaid Payin Wallet (known as "Payin eWallet") which can be used to purchase various goods and services Online/On-mobile/IVRS from affiliated merchants based on "anywhere-anytime" concept which would result in increased sales for all affiliated merchants and safety and convenience for the customer. The Company is helping business organizations to expand markets by supporting sales channels convergence with its payments options.</p>
						<p>As a service provider industry, customer service and customer satisfaction are the prime concerns of the Company and the object of this policy is to minimize instances of customer complaints through proper service delivery and review mechanism and prompt redressal of various types of Customer Grievance Redressal Policy customer complaints.</p>
						<p>The Company is in the business of issuance of Semi-Closed prepaid Payin Wallet and is authorised by the Reserve Bank of India (RBI) to operate a Payment System. It is governed by the Payment and Settlement Systems Act, 2007("the PSS Act"), Regulations made under and the Issuance and Operation of Prepaid Payment Instruments in India (Reserve Bank) Directions, 2009 ("the RBI Guidelines") laid down by the RBI.</p>
						<p>As required under the Clause 12.2 of the said RBI Guidelines, the Company has developed a procedure for promptly attending to grievances of the customers in respect of various issues pertaining to Payin Wallet as online payment mode. This is done by setting up an internal two tier system in the form of "Customer Support" and a grievance redressal mechanism in the form of "Customers Grievance Redressal Committee", as hereinafter provided.</p>
						<ol class="list-roman">
							<li>This Policy is called the "Customers Grievance Redressal Policy".</li>
							<li>It shall apply to the business of the Company in India.</li>
							<li>It shall come into force from 1st day of December, 2013</li>
							<li>It shall apply only to Semi-closed prepaid Payin Wallet business as mentioned under point 3 above.</li>
							<li>It is available on the website of the Company </li>
						</ol>				
				</div>

				<div class="policy-content">
						<h3>Section 1 - Definitions</h3>
						<ol class="list-lowercase">
							<li>	'Company' shall mean One Payin Systems Private Limited. which is carrying on the business of issuance of Prepaid Payment Instruments, payment processing, payment collection and related services by facilitating payment solutions to the Customer for buying goods and services through any digital/electronic medium.	</li>
							<li>	'Complainant' shall mean the Customer who has a Grievance.	</li>
							<li>	'Customer' shall mean the holder and/or user of Payin Wallet and/or any of the system participants of the Company.	</li>
							<li>	'Grievance' shall mean communication in any form by a customer that expresses dissatisfaction about an action or lack of action by, or about the standard of service of the Company and/or its representative, in relation to use of Payin Wallet.	</li>
							<li>	'Payin Wallet' shall mean the activated and valid Semi-closed prepaid wallet and all variants of the same issued by the Company.	</li>
							<li>	'Payment System' means a system that enables payment to be effected between a payer and a beneficiary involving clearing, payment or settlement service or all of them but Customer Grievance Redressal Policy does not include a stock exchange.	</li>
							<li>	'Redressal' shall mean the final disposal of the Grievance of the Complainant by the Company.	</li>
							<li>	'System Participant' shall mean Bank or any other person participating in a payment system and includes the system provider as per the PSS Act.	</li>
							<li>	'System Provider' shall mean a person who operates and authorizes payment system.	</li>
							<li>	'Week' shall mean consecutive seven Working Days.	</li>
							<li>	'Working Day' shall mean any day (other than Sunday & Public Holiday) on which the Company's Corporate Office is open for business.	</li>
						</ol>
				</div>

				<div class="policy-content">
						<h3>Section 2 - Principles Governing Company's policy</h3>
						<p>The Company's policy on grievance redressal is governed by the following principles:<p>
						<ol class="list-lowercase">
							<li>    Customer shall be treated fairly at all times.</li>
							<li>    Complaints raised by customers shall be attended with courtesy and in time.</li>
							<li>    Customers shall be fully informed of avenues for grievance redressal within the organization and their right to approach the Customers Grievance Redressal Committee in case they are not fully satisfied with the response of the Customer Support.</li>
						</ol>
						<p>The Company's Officer-in-Charge of Customer Support must work in good faith keeping in mind this policy of the Company</p>					
				</div>

				<div class="policy-content">
						<h3>	Section 3 - Process to handle Customer Grievances	</h3>
						<ol class="list-lowercase">
							<li>	Grievances by the holder and/or user of Payin Wallet:	</li>
						</ol>
						
						<p>		3.1 Two Tier Grievance Redressal System	</p>
						<p>		The Company has established customer grievances redressal machinery functioning at two levels:	</p>
						<ol>
							<li>a. Customer Support & </li>
							<li>b. Customers Grievance Redressal Committee</li>
						</ol>
						<p>		The Company has a Customer Support facility (0800 Hrs to 2300 Hrs Monday to Sunday) for effective resolution for operational issues and all the grievances referred to it.</p>
						<p>		For quick reference, the contact details are provided herein below:	</p>
						<p>   - Customer Support: PayinHelpdesk	</p>
						<p>   - Customer Care Number - 09466446644	</p>
				</div>
				
				<div class="policy-content">
						<h3>	Function and Authority	</h3>
						<p>		The Customer Support is responsible for the resolution of queries of all customers. The Officer-in-Charge of Customer Support is responsible for ensuring resolution of all queries received at Customer Support to the customer's satisfaction. 	</p>
				</div>
				
				<div class="policy-content">
						<h3>	Constitution	</h3>
						<p>		The customers who are not satisfied with the resolution of their queries by the Customer Support can approach the Customers Grievance Redressal Committee ("the Committee") for redressing their grievances. The Committee shall consist of two members out of whom one will be designated by the Company and the other would be an independent person who has fairly requisite knowledge of the Laws, who is not office-bearer, employee, director or any other person/s who has/have any interest of whatsoever nature in the Company. 	</p>
				</div>
				
				
				
				<div class="policy-content">
						<h3>	Function and Authority	</h3>
						<p>The functions of the Committee are as under:</p>
						<ol>
							<li>	a. The Committee will address the grievance of the Customer if he/she is not satisfied with the decision of the Customer Support.</li>
							<li>	b. The Committee will be responsible for ensuring timely and effective implementation of all regulatory requirements regarding customer service.</li>
							<li>	c. The Committee shall have right to ask for all records from the Customer Support and the customer.</li>
							<li>	d. The Committee will look into the simplification of procedures and practices prevailing in the Company with a view to safeguarding the interests of customers of the Company.</li>
							<li>	e. The Committee will review the regulations and procedures prescribed by RBI for customer service and whether the same are adopted in spirit and intent by the Company and make suitable recommendations for rationalization of the same.</li>
							<li>	f. The Committee will review the practice and procedures prevalent in prepaid payment solutions industry and take necessary corrective action on an on-going basis.</li>
							<li>	g. The Committee will endeavour to proactively advice the Customer Support on pending complaints.</li>
						</ol>

						<h3>Grievance Redressal Procedure</h3>
						<ol>
							<li>	a. A grievance may be communicated by the Complainant to the Committee as per the convenience of the Complainant in physical or electronic mode, in the form provided in schedule 'A' hereto to PayinHelpdesk	</li>
							<li>    b. Upon receipt of a grievance, the concerned officer shall enter the details thereof in the Grievance Redressal Register.	</li>
							<li>    c. All grievances received shall be acknowledged within three working days from the receipt of grievance by the Committee.	</li>
							<li>    d. The Committee shall resolve every grievance within 21 working days from the date of receipt of the grievance.	</li>
						</ol>

						<h3>Final Redressal and Closure of Grievance</h3>
						<p>Grievance shall be treated as finally redressed and closed in any of the following circumstances:</p>
						
						<ol>
							<li>   a. Where the Complainant has communicated his acceptance of the Company's decision on redressal of grievance communicated by Customer Support	</li>
						</ol>
							   or	
						<ol start="2">
							<li>b. Where the Complainant has not communicated his acceptance of the Company's decision, within 1 (ONE) months from the date of communication of decision by the Customer Support or the Committee, as the case may be.	</li>
						</ol>
						
						
						<p>Implementation of the decision</P>
						
						<p>The Management shall take all necessary steps to implement the decision of the Committee.</P>

						<p>Grievances by a person other than the holder and/or user of Payin Wallet (System Participant):</P>
						
						<p>Grievances between System Participants in respect of any matter connected with the operation of the payment system shall be resolved in accordance with the relevant provisions of the Payment and Settlement Systems Act, 2007, as may be amended from time to time.</P>
				</div>
				

				<div class="policy-content">

				<h3>	APPLICATION TO COMPANY BY CUSTOMER FOR REDRESSAL OF GRIEVANCE	</h3>

<p>(All fields are mandatory)</p>
<p>Date</p>
<p>To,</p>
<p>Customers Grievance Redressal Committee</p>
<p>ONE PAYINSYSTEMS PRIVATE LIMITED.</p>
<p>2nd Floor, Orchid Centre, Golf Course Road, Sector 53, Gurgaon,</p>
<p>HR 122002</p>

	<ol>
		<li>NAME OF THE CUSTOMER</li>
		<li>FULL ADDRESS OF THE CUSTOMER ( With Email Id and Mobile N0 )</li>
		<li>Payin Wallet ID</li>
		<li>DETAILS OF THE GRIEVANCE,
		(If space is not sufficient, please enclose separate sheet)</li>
		<li>DATE OF ORIGINAL INTIMATION OF GRIEVANCE BY THE CUSTOMER TO THE CUSTOMER SUPPORT</li>
		<li>REMEDY PROVIDED BY THE CUSTOMER SUPPORT, IF ANY
		(If remedy has been provided, please enclose relevant communication from the Customer Care Centre)</li>
		<li>LIST OF DOCUMENTS ENCLOSED
		(Please enclose copies of any relevant documents which support the facts giving rise to the Grievance)</li>
		<li>DECLARATION
				<ol>
					<li>I/ We, the customer/s herein declare that:</li>
						<ol>
							<li>the information furnished hereinabove is true and correct; </li>
								and
							<li>I/ We have not concealed or misrepresented any fact stated hereinabove and the documents submitted herewith.</li>
						</ol>
					<li>The present Grievance has been intimated to Committee in the prescribed form and manner prescribed by the Company and I/We am/are not satisfied by the remedy provided by the Customer Support</li>
					OR
					<li>no remedy was provided within a period of (__) days/weeks/months from the date of original intimation.</li>
					<li>The subject matter of the present Grievance has never been submitted to the Company by me or by any one of us or by any of the parties concerned with the subject matter to the best of my/our knowledge.</li>
					<li>The subject matter of my/our Grievance has not been settled by the Company/ Customer Support in any previous proceedings.</li>
					<li>The subject matter of my/our Grievance has not been decided by any competent authority/court/arbitrator and is not pending before any such authority/court/arbitrator.</li>
				</ol>
		</li>
	</ol>


<p>Yours faithfully</p>
<p>(Signature)</p>
<p>(Customer's name in block letter) </p>

</div>

				<div class="policy-content">
						<h3>	NOMINATION		</h3>
						<p>If the customer wants to nominate his representative to appear and make submissions on his behalf before the Customers Grievance Redressal Committee the following declaration should be submitted:</p>
						<p>I/We the above named customer hereby nominate Shri/Smt. , who is not an Advocate and whose address is as my/our REPRESENTATIVE in the proceedings and confirm that any statement, acceptance or rejection made by him/her shall be binding on me/us.</p>
						<p>He/ She has signed below in my presence.</p>
						<p>(Signature of Representative)</p>
						<p>(Signature of Customer)</p>
						<p> We will be glad to assist you</p>
				</div>

	</div>

</section>

<?  End_Response(); ?>