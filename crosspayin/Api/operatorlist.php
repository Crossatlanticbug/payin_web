<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/api.class.php");
	$apivalues = new api();	

	include_once("../Api/model/rechargeapi.class.php");
	$rechargevalues = new rechargeapi();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		
		if((!isset($_POST['phone'])) AND (!isset($_POST['recharge_type'])) ){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Please Enter Phone Or Recharge Type.';
			echo json_encode($response);
			die();
		}
		
		if(isset($_POST['phone']) AND isset($_POST['recharge_type'])){
			$ar=array();
			
			if($_POST['phone']!=""){
				$shotno=$_POST['phone'];
				$result=  $rechargevalues->GetMobileInfo($shotno);
				$operatorname = $apivalues->getOperatorName($result);
	
				if($result!="" || $operatorname!=""){
					$operatorlist1=array(
					'id'=>(int)$result,
					'name'=>trim($operatorname->operatorname));
					$ar[]=$operatorlist1;
				}
			}
			$results = $rechargevalues->getOperatorList('mobile');			
			while($row =mysqli_fetch_assoc($results)){
				
				$operatorlist=array(
					'id'=>(int)$row['operatorid'],
					'name'=>trim($row['operatorname'])
				);
				if ($_POST['phone']!="" && $operatorlist1['id']!=$row['operatorid']){
					$ar[]=$operatorlist;
				}
				if($_POST['phone']=="")	$ar[]=$operatorlist;
			}
			
			$res['json_data']['operator_list']=$ar;
			$res['json_data']['status']="TRUE";
			$res['json_data']['code']="SUCCESS";
			$res['json_data']['message']="Operator list Successful";
			echo json_encode($res);
			die();
		}
		if(isset($_POST['recharge_type']) AND !isset($_POST['phone'])){
			if((!isset($_POST['phone'])) AND (!isset($_POST['recharge_type'])) ){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Please Enter Recharge Type.';
				echo json_encode($response);
				die();
			}
			$type=$_POST['recharge_type'];
			$result = $rechargevalues->getOperatorList($type);
			if($result==""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Invalid User ID.';
				echo json_encode($response);
				die();
			}
			
			$ar=array();
			while($row =mysqli_fetch_assoc($result)){
				$operatorlist=array(
					'id'=>(int)$row['operatorid'],
					'name'=>trim($row['operatorname'])
				);
				$ar[]=$operatorlist;
			}
			$res['json_data']['operator_list']=$ar;
		}
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Operator List Successful";
		echo json_encode($res);
	
?>