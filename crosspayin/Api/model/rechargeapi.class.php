<?

class RechargeApi 
{
	function __construct()
    {
      	global $config; 
		$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
		$this->apiurl = $config['API'][$ENV]['URL'] ; 
		$this->goid = $config['API'][$ENV]['GOID']; 
		$this->apikeyprepaid = $config['API'][$ENV]['MOBILE']['PREPAID']['APIKEY']; 
		$this->apikeypostpaid = $config['API'][$ENV]['MOBILE']['POSTPAID']['APIKEY']; 
		$this->apikeydth = $config['API'][$ENV]['DTH']['APIKEY']; 
		$this->apikeydatacard = $config['API'][$ENV]['DATACARD']['APIKEY']; 
		$this->rtype = $config['API'][$ENV]	['RTYPE'];
		$this->apimode = $config['API'][$ENV]['APIMODE'];
	}
	
	function GetMobileInfo($mobile)
	{
		$scriptname = "findMsisdnInfo.go";
		$parameter="msisdn=$mobile";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyprepaid."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		$operatorcode= isset($array['operator_code']) ?$array['operator_code'] :'';
		return $operatorcode;
		
    }

	function getOperatorList($type)
	{
		global $dbLink;

		$query  = "Select * From `operator` Where `status`=1 and rechargetype='".strtoupper($type)."' order by operatorname asc";
		$result = mysqli_query($dbLink, $query);
		
		return $result;
	}

    function MobileRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {
		$scriptname = "serviceTrans.go";
		$parameter="msisdn=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeyprepaid."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
		
    }
	
	function MobileRechargePostPaid($mobile,$vendor_code,$amount,$txnid,$rechargetype)
    {
		$scriptname = "billTrans.go";
		$parameter="msisdn=$mobile&vendor_code=$vendor_code&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeypostpaid."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function DataCardRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {	
		$scriptname = "serviceTrans.go";
		$parameter="msisdn=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeydatacard."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
		
    }
	
	function DTHRecharge($mobile,$operatorcode,$amount,$txnid,$rechargetype)
    {
		$scriptname = "serviceTrans.go";
		$parameter="custid=$mobile&operator_code=$operatorcode&amount=$amount&client_trans_id=$txnid&service_family=$rechargetype";		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$this->apikeydth."&rtype=".$this->rtype."&apimode=".$this->apimode;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		
		return $array;
    }
	
	function GetCircle($mobile,$apikey)
	{
		if($apikey=="prepaid")	$apikey = $this->apikeyprepaid;
		if($apikey=="postpaid")	$apikey = $this->apikeypostpaid;
		
		$scriptname = "findMsisdnInfo.go";
		$parameter="msisdn=$mobile";
		$authurl=$scriptname."?goid=".$this->goid."&apikey=".$apikey."&rtype=".$this->rtype;
		$url=$this->apiurl.$authurl."&".$parameter;
		// echo $url;
		
		$result = '';
		$array  = ''; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CAINFO, "GoCAcert.pem"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); // Operators Time Out is 300 sec varies from operator to operator. 
		$result =  curl_exec($ch);
		curl_close($ch);
		$array = json_decode($result, true); 
		$circle_code= isset($array['circle_code']) ?$array['circle_code'] :'';
		return $circle_code;
		
    }
	
	function GetPlan($operatorCode,$type,$circle)
	{	
		$ch = curl_init();
		$apikey= 172803667719761;
		//TUP=> Top-up Recharge, FTT=> Full Talk-time Recharge, 2G=> 2G Data Recharge, 3G=> 3G/4G Data Recharge, SMS=> SMS Pack Recharge, LSC=> Local/STD/ISD Call Recharge, OTR=> Other Recharge, RMG=> National/International Roaming Recharge
		
		$myurl = "https://joloapi.com/api/findplan.php?userid=payin&key=$apikey&opt=$operatorCode&cir=$circle&typ=$type&max=&amt=&type=json";
		// echo $myurl;
	
		$msg= '';
		curl_setopt ($ch, CURLOPT_URL, $myurl);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 30);
		$jsonxx = curl_exec($ch);
		$curl_error = curl_errno($ch);
		curl_close($ch);
		$someArray = json_decode($jsonxx, true);
		return $someArray;
	}
	
}

?>