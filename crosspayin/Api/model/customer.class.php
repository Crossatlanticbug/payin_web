<?
error_reporting(E_ALL);
ini_set("display_errors",1);

// Class Definition
class customer
{

	var $id;
	var $fName;
	var $mName;
	var $lName;
	var $username;
	var $password;
	var $email;
	var $address;
	var $countryID;
	var $countryName;
	var $city;
	var $state;
	var $zip;
	var $phone1;
	var $fax;
	var $creationDate;
	var $enabled;
	var $userIP;
	
	function __construct()
	{
		
	}

	function add($postvalues)
	{
		global $dbLink;
	
		$CreationDate = date("Y-m-d H:i:s");

		$query = "INSERT INTO customer set ";
		foreach ($postvalues as $key=>$value){
			$query .= "$key='$value'";
			$query .= ",";
		}	

		$query = substr($query, 0, -1); 
		 //exit($query);
		mysqli_query($dbLink, $query);
		$this->id = mysqli_insert_id($dbLink);
	}	
	
	function addNewConfirmation($postdata)
	{
		global $dbLink;
	
		$CreationDate = date("Y-m-d H:i:s");
		$query =" INSERT INTO customercode set ";
		foreach ($postdata as $key=>$value){
			$query .= "$key='$value'";
			$query .= ",";
		}	
		$query = substr($query, 0, -1); 
		mysqli_query($dbLink, $query);
	}
	
	function isPhonenoExist($phone,$email)
	{
		global $dbLink;

		$query =  " SELECT * FROM customer WHERE phone = '".$phone."' or  email = '".$email."' And enabled = '1'";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return mysqli_num_rows($result);
		}
		return 0;
	}

	function checkverificationcode($phone,$verificationcode)
	{
		global $dbLink;

		$query =  " SELECT * FROM customercode WHERE phoneno = '$phone' and code = '$verificationcode' And status = '0' and DATE(`date`) = '".date("Y-m-d")."'";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return mysqli_num_rows($result);
		}
		return 0;
	}
	
	function updateverificationcode($phone,$verificationcode)
	{
		global $dbLink;

		$query =  " update customercode set status = '1' WHERE phoneno = '$phone' and code = '$verificationcode'";
		$result = mysqli_query($dbLink, $query);
	
	}
	
	function updatestatus($phone)
	{
		global $dbLink;

		$query =  " update customer set enabled = '1' WHERE phone = '$phone'";
		$result = mysqli_query($dbLink, $query);
	
	}
	// Login customer
	function checkaccountlogin($where,$pass)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE ".$where." AND `password` = '".$pass."' and  enabled =1";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}

	function checkdestinationacount($destinationmo)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE phone = '".$destinationmo."' AND  enabled =1";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	
	function getcustomerinfo($cond)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE ".$cond." and  enabled =1";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	
	function getforgotpassword($email)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE email='".$email."' AND enabled =1  ";
		 
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	
	function getchangepassword($userid,$pass1)
	{
		global $dbLink;

		$query ="UPDATE customer SET password='".$pass1."' WHERE id='".$userid."' ";
		 
		if (mysqli_query($dbLink, $query))
		{
			$querys ="SELECT *  FROM customer WHERE id='".$userid."' ";
		 
			$result = mysqli_query($dbLink, $querys);
			if (mysqli_num_rows($result)>0)
			{
				$row = mysqli_fetch_object($result);
				return $row;
			}
			return false;
		}
		return false;
	}
	
	function updateprofile($userid,$postupdatevalues)
	{
		global $dbLink;
		
		$query =  " update customer set `name` = '".$postupdatevalues->name."',`gender`='".$postupdatevalues->gender."',`email`='".$postupdatevalues->email."',`address`='".$postupdatevalues->address."',
		`city`='".$postupdatevalues->city."',`state`='".$postupdatevalues->state."',`zipcode`='".$postupdatevalues->zipcode."'
		 WHERE id = '$userid'";
		$result = mysqli_query($dbLink, $query);
	
	}
	
	function getbalance($userid)
	{
		global $dbLink;

		$query ="SELECT * FROM customer WHERE id=$userid and  enabled =1";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return ;
	}
	
	
}

?>