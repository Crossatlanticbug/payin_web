<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	require ("../phpMailer/PHPMailerAutoload.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}

		if(isset($_POST['accounttype'])=="FB" OR isset($_POST['accounttype'])=="GPLUS"){
			if(((!isset($_POST['email_id'])) OR empty($_POST['email_id'])) AND ((!isset($_POST['phone'])) OR empty($_POST['phone']))){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Please Enter Phone/Email ID';
				echo json_encode($response);
				die();
			}

			$name= isset($_POST['name']) ?$_POST['name'] :'';
			$email=  isset($_POST['email_id']) ?$_POST['email_id'] :'';
			$phone=  isset($_POST['phone']) ?$_POST['phone'] :'';
			$accountid=  isset($_POST['accountid']) ?$_POST['accountid'] :'';
			$accounttype= isset($_POST['accounttype']) ?$_POST['accounttype'] :'';
			$token=  isset($_POST['token']) ?$_POST['token'] :'';
			$imageurl=  isset($_POST['imageurl']) ?$_POST['imageurl'] :'';
			
			if($email=="")	$cond= $phone;	else $cond= $email;
			$customer= $apivalues->GetAccountDetails($cond);
			
			if($customer == ""){
				$password = GenerateRandomID(12,'NUMERIC');
				$postvalus = array('name'=>$name,'email'=>$email,'phone'=>$phone,'password'=>$password,'accountid'=>"$accountid",'accounttype'=>"$accounttype",'token'=>"$token",'imgurl'=>"$imageurl",'enabled'=>1,'creationdate'=>date("Y-m-d H:i:s"));
				$apivalues->AccountRegister($postvalus);
				
				$customer= $apivalues->GetAccountDetails($email,$accounttype);
				if($customer==""){
					$response['status']="FALSE";   
					$response['code']="FAILURE";		 
					$response['message']='Something is wrong!.';
					echo json_encode($response);
					die();
				}
				
				$name = $customer->name;
				$to      = $customer->email; // Send email to our user
				$subject = 'Payin Password'; // Give the email a subject 
				$message=$config['EMAIL_HEADER'];
				$message.= "Dear ".isset($name)?$name:'Customer'.",<br><br>";
				$message.= "You have registered successfully. Your password is given below:<br><br>";
				$message.= "<strong>Password :</strong>".$customer->password."<br>";					 
				$message.=$config['EMAIL_FOOTER'];
					
				//Answer key attachment
				$mail = new PHPMailer;
				$mail->isSMTP();
				$mail->Host = $config['SMTP']['HOST'];
				//Set the SMTP port number - likely to be 25, 465 or 587
				$mail->Port = $config['SMTP']['PORT'];
				//Whether to use SMTP authentication
				$mail->SMTPAuth = true;
				//Username to use for SMTP authentication
				$mail->Username = $config['SMTP']['USER'];
				//Password to use for SMTP authentication
				$mail->Password = $config['SMTP']['PASSWORD'];
				
				$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
				$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
				$mail->addAddress(''.$to.'');
				$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
				$mail->Subject = $subject;
				$mail->Body = $message;
				$mail->IsHTML(true); 
				$mail->send();

			}
			
			if($customer->imgurl != "")	$imgurl = $customer->imgurl;
			if($customer->imgurl == "")	$imgurl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]//development/Images/user.png";
			
			$res['json_data']['userid']=(int)$customer->id;
			$res['json_data']['name']=trim($customer->name);
			$res['json_data']['email_id']=trim($customer->email);
			$res['json_data']['phone']=trim($customer->phone);
			$res['json_data']['balance']=trim($customer->balance);
			$res['json_data']['gender']=trim($customer->gender);
			$res['json_data']['zipcode']=trim($customer->zipcode);
			$res['json_data']['address']=trim($customer->address);
			$res['json_data']['imgurl']= trim($imgurl);
			
		}else{
			if((!isset($_POST['username'])) OR empty($_POST['username'])){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='User Name should not be Blank.';
				echo json_encode($response);
				die();
			}
			if((!isset($_POST['password'])) OR empty($_POST['password'])){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Password should not be Blank.';
				echo json_encode($response);
				die();
			}
			$username= $_POST['username'];
			$password= $_POST['password'];
		
			$customer= $apivalues->login($username,$password);
			if($customer==""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Incorrect Username OR Password.';
				echo json_encode($response);
				die();
			}
			if($customer->enabled==0){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='You are not autherised to login here.';
				echo json_encode($response);
				die();
			}
			
			if($customer->imgurl != "")	$imgurl = $customer->imgurl;
			if($customer->imgurl == "")	$imgurl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/development/Images/user.png";
			
			$res['json_data']['userid']=(int)$customer->id;
			$res['json_data']['name']=trim($customer->name);
			$res['json_data']['email_id']=trim($customer->email);
			$res['json_data']['phone']=trim($customer->phone);
			$res['json_data']['balance']=trim($customer->balance);
			$res['json_data']['gender']=trim($customer->gender);
			$res['json_data']['zipcode']=trim($customer->zipcode);
			$res['json_data']['address']=trim($customer->address);
			$res['json_data']['imgurl']= trim($imgurl);
		}
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Login Successful";
		echo json_encode($res);
	
?>