<?php
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	include_once("../Api/model/customer.class.php");
	$customerdata = new customer();
	
	include_once("../Api/model/order.class.php");
	$orderdata = new order();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='User ID should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['action'])) OR empty($_POST['action'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Action should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$userid= $_POST['userid'];
		$action= $_POST['action'];
		$cond= "id=".$userid;
		$results= $customerdata->getcustomerinfo($cond);	
		if($results==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Invalid User ID.';
			echo json_encode($response);
			die();
		}
		if($action=="profile"){
			$res['json_data']['userid']=(int)$results->id;
			$res['json_data']['name']=trim($results->name);
			$res['json_data']['email_id']=trim($results->email);
			$res['json_data']['phone']=trim($results->phone);
			$res['json_data']['balance']=trim($results->balance);
			$res['json_data']['address']=trim($results->address);
		}
		
		if($action=="recharge"){
			$resultr= $orderdata->getrechargeinfo($userid);
			$ar=array();
			if($resultr==""){
				$res['json_data']['recharge_list']=$ar;
				$res['json_data']['status']="FALSE";   
				$res['json_data']['code']="FAILURE";
				$res['json_data']['message']="Customer Information Not Found";
				echo json_encode($res);
				die();
			}
			
			while($recharge = mysqli_fetch_object($resultr)){
				$list=array(
					'id'=>(int)$recharge->orderid,
					'name'=>trim($recharge->name),
					'email'=>trim($recharge->email),
					'mobileno'=>trim($recharge->mobileno),
					'mobiletype'=>trim($recharge->mobiletype),
					'rechargetype'=>trim($recharge->rechargetype),
					'amount'=>$recharge->amount,
					'transactionid'=>trim($recharge->transactionid),
					'rechargekey'=>trim($recharge->rechargekey),
					'rechargedate'=>trim($recharge->rechargedate),
					'paymentstatus'=>trim($recharge->paymentstatus),
					'rechargestatus'=>trim($recharge->rechargestatus)
				);
				$ar[]=$list;
			}
			$res['json_data']['recharge_list']=$ar;
			$msg= "Customer Information Successful";
		}
		
		if($action=="addmoney"){
			$resultr= $orderdata->addmoneyinfo($userid);
			$ar=array();
			if($resultr==""){
				$res['json_data']['recharge_list']=$ar;
				$res['json_data']['status']="FALSE";   
				$res['json_data']['code']="FAILURE";
				$res['json_data']['message']="Add Money Information Not Found";
				echo json_encode($res);
				die();
			}
			
			while($addmoney = mysqli_fetch_object($resultr)){
				$list=array(
					'id'=>(int)$addmoney->id,
					'amount'=>trim($addmoney->amount),
					'processdate'=>trim($addmoney->processdate)
				);
				$ar[]=$list;
			}
			$res['json_data']['addmoney_list']=$ar;
			$msg= "Add Money Information Successful";
		}
		
		if($action=="sendmoney"){
			$phone=$results->phone;
			$cond="frommobile='".$phone."'";
			$resultr= $orderdata->sendmoneyinfo($cond);
			$ar=array();
			if($resultr==""){
				$res['json_data']['recharge_list']=$ar;
				$res['json_data']['status']="FALSE";   
				$res['json_data']['code']="FAILURE";
				$res['json_data']['message']="Send Money Information Not Found";
				echo json_encode($res);
				die();
			}
			
			while($sendmoney = mysqli_fetch_object($resultr)){
				$list=array(
					'id'=>(int)$sendmoney->id,
					'tomobile'=>trim($sendmoney->tomobile),
					'amount'=>trim($sendmoney->amount),
					'processdate'=>trim($sendmoney->processdate)
				);
				$ar[]=$list;
			}
			$res['json_data']['sendmoney_list']=$ar;
			$msg= "Send Money Information Successful";
		}
		
		if($action=="receivedmoney"){
			$phone=$results->phone;
			$cond="tomobile='".$phone."' ";
			$resultr= $orderdata->sendmoneyinfo($cond);
			$ar=array();
			if($resultr==""){
				$res['json_data']['recharge_list']=$ar;
				$res['json_data']['status']="FALSE";   
				$res['json_data']['code']="FAILURE";
				$res['json_data']['message']="Send Money Information Not Found";
				echo json_encode($res);
				die();
			}
			
			while($sendmoney = mysqli_fetch_object($resultr)){
				$list=array(
					'id'=>(int)$sendmoney->id,
					'frommobile'=>trim($sendmoney->frommobile),
					'amount'=>trim($sendmoney->amount),
					'processdate'=>trim($sendmoney->processdate)
				);
				$ar[]=$list;
			}
			$res['json_data']['received_list']=$ar;
			$msg= "Received Money Information Successful";
		}
		
		$res['json_data']['userid']=(int)$results->id;
		$res['json_data']['balance']=trim($results->balance);
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']=$msg;
		echo json_encode($res);
	
?>