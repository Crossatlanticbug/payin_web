<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='User ID should not be Blank.';
			echo json_encode($response);
			die();
		}
		$userid= $_POST['userid'];
		$results= $apivalues->getmenu($userid);
		if($results==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Invalid User ID.';
			echo json_encode($response);
			die();
		}
		$ar=array();
		while($menu = mysqli_fetch_object($results)){
			$menulist=array(
				'id'=>(int)$menu->id,
				'name'=>trim($menu->name)
			);
			$ar[]=$menulist;
		}
		$res['json_data']['menu_list']=$ar;
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Menu Successful";
		echo json_encode($res);
	
?>