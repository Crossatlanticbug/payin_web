<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/customer.class.php");
	$customer = new customer();
	
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='User ID should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$userid= isset($_POST['userid']) ?$_POST['userid'] :'';
		$email= isset($_POST['email_id']) ?$_POST['email_id'] :'';
		$phone= isset($_POST['phone']) ?$_POST['phone'] :'';
		$condition="id!=$userid";
		$cond = "id=".$userid;
		$results = $customer->getcustomerinfo($cond);
		
		if($results==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Invalid User ID';
			echo json_encode($response);
			die();
		}
		
		if($phone!="" AND $email!="")	$condition.=" AND (phone='".$phone."' OR email='".$email."') ";
		if($phone!="" AND $email=="")	$condition.=" AND phone='".$phone."' ";
		if($phone=="" AND $email!="")	$condition.=" AND email='".$email."' ";	
		
		if($phone!="" OR $email!=""){
			$rowdata = $apivalues->isExistAccount($condition);
			if($rowdata!=""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Email/Phone is Already exist';
				echo json_encode($response);
				die();
			}
		}
		if($_POST['phone']!=""){
			// if($results->phone==$phone){
				// $response['status']="FALSE";   
				// $response['code']="FAILURE";		 
				// $response['message']='This is your previous no. use another';
				// echo json_encode($response);
				// die();
			// }
			if($results->phone!=$phone){
				$randomno = GenerateRandomID(6,'NUMERIC');
				$mess="Your phone verification code is ".$randomno;
				$apivalues->sendSms($phone,$mess);
				
				$postcodevalus = array('phoneno'=>$phone,'code' => $randomno,'status'=>0,'date'=>date("Y-m-d H:i:s"));
				$apivalues->addNewConfirmation($postcodevalus);		
			}
		}
		
		$postupdatevalues = new stdclass();
		$postupdatevalues->name= isset($_POST['name']) ?$_POST['name'] :'';
		$postupdatevalues->email= isset($_POST['email_id']) ?$_POST['email_id'] :'';
		$postupdatevalues->phone='';
		$postupdatevalues->gender= isset($_POST['gender']) ?$_POST['gender'] :'';
		$postupdatevalues->address= isset($_POST['address']) ?$_POST['address'] :'';
		$postupdatevalues->city= isset($_POST['city']) ?$_POST['city'] :'';
		$postupdatevalues->state= isset($_POST['state']) ?$_POST['state'] :'';
		$postupdatevalues->zipcode= isset($_POST['zipcode']) ?$_POST['zipcode'] :'';
		$customer->updateprofile($userid,$postupdatevalues);
		
		$result = $customer->getcustomerinfo($cond);
		$imgurl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/development/Images/user.png";
		
		$res['json_data']['userid']=(int)$userid;
		$res['json_data']['name']=$result->name;
		$res['json_data']['phone']=$phone;
		if($results->phone!=$phone)	$res['json_data']['otp']=$randomno;
		$res['json_data']['email_id']=$result->email;
		$res['json_data']['gender']=trim($result->gender);
		$res['json_data']['city']=$result->city;
		$res['json_data']['state']=$result->state;
		$res['json_data']['zipcode']=$result->zipcode;
		$res['json_data']['address']=$result->address;
		$res['json_data']['balance']=$result->balance;
		$res['json_data']['imgurl']= trim($imgurl);
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Profile Updated Successfully";
		echo json_encode($res);
	
?>