<?php 
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");

	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/rechargeapi.class.php");
	$apivalues = new RechargeApi();
		
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}

		if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='User ID should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['operatorcode'])) OR empty($_POST['operatorcode'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Operator Code should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['type'])) OR empty($_POST['type'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Recharge Type should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['circle'])) OR empty($_POST['circle'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Circle should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$userid = $_POST['userid'];
		$operatorcode = $_POST['operatorcode'];
		$type = $_POST['type'];
		$circle = $_POST['circle'];

		$result = $apivalues->GetPlan($operatorcode,$type,$circle);

		if($result==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Plan Not Found.';
			echo json_encode($response);
			die();
		}
		
		$ar=array();
		foreach ($result as $key => $value) {
			$planlist=array(
				'id'=>(int)$key+1,
				'detail'=>trim($value["Detail"]),
				'amount'=>trim($value["Amount"]),
				'validity'=>trim($value["Validity"]),
			);
			$ar[]=$planlist;
		}
			
		$res['json_data']['plan_list']=$ar;
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Recharge plan Successful";
		echo json_encode($res);
?>
