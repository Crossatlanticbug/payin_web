<?php 
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	
	include_once("../Api/model/order.class.php");
	$orderprocess= new Order();
	
	include_once("../Classes/main.class.php");
	$main= new main();

	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/rechargeapi.class.php");
	$apivalues = new RechargeApi();
		
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}

		if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='User ID should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$userid = $_POST['userid'];
		if((isset($_POST['phone'])) AND  !empty($_POST['phone'])){
			
			if((!isset($_POST['phone'])) OR empty($_POST['phone'])){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Phone should not be Blank.';
				echo json_encode($response);
				die();
			}
			
			if((!isset($_POST['optpayfor'])) OR empty($_POST['optpayfor'])){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Operator Pay For should not be Blank.';
				echo json_encode($response);
				die();
			}
			
			$phone = $_POST['phone'];
			$apikey = $_POST['optpayfor'];
			$circle_code = $apivalues->GetCircle($phone,$apikey);

			if($circle_code==""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Circle Not Found.';
				echo json_encode($response);
				die();
			}
			
			$circledata = $main->getCircle();
			if($circledata==""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Circle Not Found.';
				echo json_encode($response);
				die();
			}
			
			$ar=array();
			while($rows =mysqli_fetch_assoc($circledata)){
				if($rows['circleid']==$circle_code){
					$circlecode =$rows['circleid'];
					$circlename =$rows['circle'];
					break;
				}
			}
			
			$res['json_data']['id']=(int)$circlecode;
			$res['json_data']['name']=trim($circlename);
		
		}else{
			
			$circledata = $main->getCircle();
			if($circledata==""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Circle Not Found.';
				echo json_encode($response);
				die();
			}
			
			$ar=array();
			while($rows =mysqli_fetch_assoc($circledata)){
				$circlelist=array(
					'id'=>(int)$rows['circleid'],
					'name'=>trim($rows['circle'])
				);
				$ar[]=$circlelist;
			}
			$res['json_data']['circle_list']=$ar;
		}
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Operator List Successful";
		echo json_encode($res);

?>
