<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	require ("../phpMailer/PHPMailerAutoload.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		
		if((!isset($_POST['email_id'])) OR empty($_POST['email_id'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Email ID should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$email= $_POST['email_id'];
		
		$result= $apivalues->getforgotpassword($email);

		if ($result==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Invalid Email ID/Phone.';
			echo json_encode($response);
			die();
		}
		if($result->email){
			$name= $result->name;
			$to      = $result->email; // Send email to our user
			$subject = 'Forgot Password'; // Give the email a subject 
			$message=$config['EMAIL_HEADER'];
			$message.= "Dear ".ucfirst($name).",<br><br>";
			$message.= "Your password is mentioned  below:<br><br>";
			$message.= "<strong>password :  </strong>".$result->password."<br>";
			$message.=$config['EMAIL_FOOTER'];
				
			//Answer key attachment
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = $config['SMTP']['HOST'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $config['SMTP']['PORT'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $config['SMTP']['USER'];
			//Password to use for SMTP authentication
			$mail->Password = $config['SMTP']['PASSWORD'];
			
			$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
			$mail->addReplyTo(''.$config['ADMIN_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addAddress(''.$to.'');
			$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->IsHTML(true); 
			$mail->send();
		}
		if($result->phone){
			
			$mess="Your Password is ".$result->password;
			$apivalues->sendSms($result->phone,$mess);
		}
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Password Sent";
		echo json_encode($res);
	
?>