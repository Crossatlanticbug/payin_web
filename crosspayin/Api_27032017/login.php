<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}

		if(isset($_POST['accounttype'])=="FB" OR isset($_POST['accounttype'])=="GPLUS"){
			if(((!isset($_POST['email_id'])) OR empty($_POST['email_id'])) AND ((!isset($_POST['phone'])) OR empty($_POST['phone']))){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Please Enter Phone/Email ID';
				echo json_encode($response);
				die();
			}

			$name= isset($_POST['name']) ?$_POST['name'] :'';
			$email=  isset($_POST['email_id']) ?$_POST['email_id'] :'';
			$phone=  isset($_POST['phone']) ?$_POST['phone'] :'';
			$accountid=  isset($_POST['accountid']) ?$_POST['accountid'] :'';
			$accounttype= isset($_POST['accounttype']) ?$_POST['accounttype'] :'';
			$token=  isset($_POST['token']) ?$_POST['token'] :'';
			$imageurl=  isset($_POST['imageurl']) ?$_POST['imageurl'] :'';
			
			if($email=="")	$cond= $phone;	else $cond= $email;
			$customer= $apivalues->GetAccountDetails($cond,$accounttype);
			
			if($customer == ""){
				$postvalus = array('name'=>$name,'email'=>$email,'phone'=>$phone,'accountid'=>"$accountid",'accounttype'=>"$accounttype",'token'=>"$token",'imgurl'=>"$imageurl",'enabled'=>1,'creationdate'=>date("Y-m-d H:i:s"));
				$apivalues->AccountRegister($postvalus);
				
				$customer= $apivalues->GetAccountDetails($email,$accounttype);
				if($customer==""){
					$response['status']="FALSE";   
					$response['code']="FAILURE";		 
					$response['message']='Something is wrong!.';
					echo json_encode($response);
					die();
				}
			}
			$res['json_data']['userid']=(int)$customer->id;
			$res['json_data']['name']=trim($customer->name);
			$res['json_data']['email_id']=trim($customer->email);
			$res['json_data']['phone']=trim($customer->phone);
			$res['json_data']['balance']=trim($customer->balance);
			$res['json_data']['address']=trim($customer->address);
			
			/*$res['json_data']['userid']=1;
			$res['json_data']['name']="Rohit Tiwari";
			$res['json_data']['email_id']="111rohittiwari@gmail.com";
			$res['json_data']['phone']="7836951028";
			$res['json_data']['balance']="0.00";
			$res['json_data']['address']="New Delhi";*/
			
		}else{
			if((!isset($_POST['username'])) OR empty($_POST['username'])){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='User Name should not be Blank.';
				echo json_encode($response);
				die();
			}
			if((!isset($_POST['password'])) OR empty($_POST['password'])){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Password should not be Blank.';
				echo json_encode($response);
				die();
			}
			$username= $_POST['username'];
			$password= $_POST['password'];
		
			$customer= $apivalues->login($username,$password);
			if($customer==""){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='Incorrect Username OR Password.';
				echo json_encode($response);
				die();
			}
			if($customer->enabled==0){
				$response['status']="FALSE";   
				$response['code']="FAILURE";		 
				$response['message']='You are not autherised to login here.';
				echo json_encode($response);
				die();
			}
			$res['json_data']['userid']=(int)$customer->id;
			$res['json_data']['name']=trim($customer->name);
			$res['json_data']['email_id']=trim($customer->email);
			$res['json_data']['phone']=trim($customer->phone);
			$res['json_data']['balance']=trim($customer->balance);
			$res['json_data']['address']=trim($customer->address);
		}
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Login Successful";
		echo json_encode($res);
	
?>