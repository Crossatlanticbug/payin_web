<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	
	include_once("../Api/model/order.class.php");
	$orderprocess= new Order();
	
	include_once("../Classes/main.class.php");
	$main= new main();
	
	include_once("../Classes/customer.class.php");
	$customerdata= new customer();
	
	include_once("../Api/model/api.class.php");
	$apivalues = new api();	
	
	require ("../phpMailer/PHPMailerAutoload.php");
	
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/rechargeapi.class.php");
	$apiprocess = new rechargeapi();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
	if($_SERVER['REQUEST_METHOD'] != "POST"){  
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Not Acceptable Method';
		echo json_encode($response);
		die();
	}
	$post=count($_POST);
	if(empty($post)){  
		$response['status']="FALSE";   
		$response['code']="FAILURE";		
		$response['message']='Not Found Any Params .';
		echo json_encode($response);
		die();
	}
	if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='User ID should not be Blank.';
		echo json_encode($response);
		die();
	}
	if((!isset($_POST['result'])) OR empty($_POST['result'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Result should not be Blank.';
		echo json_encode($response);
		die();
	}
		
		$userid =  $_POST['userid'];
		$result=$_POST["result"];
		$datas= urldecode($_POST["result"]);
		$json_data= json_decode($datas);
		
		$txnid= $json_data->txnid;
		$ORDERID= $json_data->udf1;
		$amount=  $json_data->amount;
		$name=  $json_data->firstname;
		$email=  $json_data->email;
		$type=  $json_data->udf2;
		$status=  $json_data->status;
	
	if($type=="Wallet")	{
		$rowdata= $orderprocess->GetAddMoneyinfo($ORDERID);
	}	else	{
		$rowdata= $orderprocess->getorderinfo($ORDERID);
		$phone= $rowdata->mobileno; 
	}
	
	// print_r($rowdata);die();
	$logvalue = new stdClass();
	if($type!="Wallet")		$logvalue->paymentlog=$datas;
	$logvalue->rechargelog= '';
	$logvalue->mobileno= $rowdata->mobileno;
	$logvalue->emailid= $rowdata->email; 
	$logvalue->userip= '';
	$logvalue->pageurl= '';
	$logid=$orderprocess->AddDevelopmentLog($logvalue);
		
	$ordervalue="transactionid='".$txnid."',paymentstatus='".$status."'";
	
	if($json_data->status=="failure"){
		$response['status']="TRUE";   
		$response['code']="SUCCESS";		 
		$response['message']="Payment Failed";
		echo json_encode($response);
		die();
	}
	
	if ($type=="Wallet") {
			$orderprocess->UpdateAddMoneyOrder($ordervalue,$ORDERID) ;  
			$orderprocess->UpdateBalance($amount,$rowdata->customerid);
			$balancetype = 'DR';
			$comment = 'Add Money In Wallet' ;
			$objbal=  new stdclass();
			$objbal->amount=$amount;
			$objbal->user=$rowdata->customerid;
			$objbal->amounttype=$balancetype;
			$objbal->comment=$comment;
			$objbal->addedby='';
			$orderprocess->UpdateVirtualBalance($objbal);
		} else {
			$orderprocess->UpdateRechargeOrder($ordervalue,$ORDERID) ; 	 
		}

		$msg = "<h3>Thank You. Your order status is ". $status .".</h3>";
        $msg .= "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";
        $msg .= "<h4>We have received a payment of Rs. " . $amount . ". </h4>";
			
		if ($type=="Wallet") {
			$result = $orderprocess->getAddMoneyOrderInfo($ORDERID);
		} else {
			$result = $orderprocess->getRechargeOrderInfo($ORDERID);
			$operatorid=$result->operatorid;
			$operatorname = $apivalues->getOperatorName($operatorid);
		}
		
		$randomno = GenerateRandomID(20);
		$phone=$result->mobileno;
		$amount=floor($result->amount);
		$mess="Your Order has been  successfully placed with transaction id ".$txnid.". ";
		if ($type=="Wallet") {
			$mess .="Your current balance is INR ". $result->balance;
		}		
		
		$main->sendSms($phone,$mess);
		
		$to      = $result->email; // Send email to our user
		$subject = 'Order Confirmation'; // Give the email a subject 
		$message=$config['EMAIL_HEADER'];
		$message.= "Dear ".$result->name.",<br><br>";
		$message.= "A order has been successfully submitted. Please Find below details regarding the transaction:<br><br>";
		$message.= "<strong>Order No :</strong>".$ORDERID."<br>";
		$message.= "<strong>Transaction ID :</strong>".$txnid."<br>";
		$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
		if ($type=="Wallet") {
			$message.= "<strong>Balance :</strong>INR ".$result->balance."<br>";
		}
		$message.= "<strong>Payment Status :</strong>".$status."<br>";
				 
		$message.=$config['EMAIL_FOOTER'];
			
		//Answer key attachment
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = $config['SMTP']['HOST'];
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = $config['SMTP']['PORT'];
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $config['SMTP']['USER'];
		//Password to use for SMTP authentication
		$mail->Password = $config['SMTP']['PASSWORD'];
		
		$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
		$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
		$mail->addAddress(''.$to.'');
		$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->IsHTML(true); 
		$mail->send();
		unset($_SESSION['ADDMONEYORDERID']);
		
		if ($type=="Recharge") {
			$rechargetype=$result->rechargetype;
			
			$mobiletype= $result->mobiletype;
				If (!empty($phone)){
	/*-------------------------		Mobile Recharge	Start	--------------------------------*/
			
					If ($rechargetype == "Mobile"){
						
						//Pre-paid Mobile Recharge
						if($mobiletype=="Prepaid")	{
							$operatorcode=$result->operatorid;
							$apiresponse= $apiprocess->MobileRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][strtolower($rechargetype)][strtolower($mobiletype)]);
							
							if (isset($apiresponse['ErrorCode'])){
								$response['status']="FALSE";   
								$response['code']="FAILURE";		 
								$response['message']=$apiresponse['Message'];
								echo json_encode($response);
								die();
							}
							
						}
						
						//Post-Paid Mobile Recharge
						if($mobiletype=="Postpaid")	{
							$vendor_code=$result->operatorid;
							$apiresponse= $apiprocess->MobileRechargePostPaid($phone,$vendor_code,$amount,$randomno,$config['API']['SF'][strtolower($rechargetype)][strtolower($mobiletype)]);
							if (isset($apiresponse['ErrorCode'])){
								$response['status']="FALSE";   
								$response['code']="FAILURE";		 
								$response['message']=$apiresponse['Message'];
								echo json_encode($response);
								die();
							}
						}
						
						$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
						$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
						
						if ($codestatus==true AND $apiresponse['response_code'] == 0){
							
							if($mobiletype=="Prepaid"){
								$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
								$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
								$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
								$main->sendSms($phone,$mess);
								$to      = $result->email; // Send email to our user
								$subject = 'Recharge Confirmation'; // Give the email a subject 
								$message=$config['EMAIL_HEADER'];
								$message.= "Dear ".$result->name.",<br><br>";
								$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
								$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
								$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
								$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
								$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
									 
								$message.=$config['EMAIL_FOOTER'];
								//Answer key attachment
								$mail = new PHPMailer;
								$mail->isSMTP();
								$mail->Host = $config['SMTP']['HOST'];
								//Set the SMTP port number - likely to be 25, 465 or 587
								$mail->Port = $config['SMTP']['PORT'];
								//Whether to use SMTP authentication
								$mail->SMTPAuth = true;
								//Username to use for SMTP authentication
								$mail->Username = $config['SMTP']['USER'];
								//Password to use for SMTP authentication
								$mail->Password = $config['SMTP']['PASSWORD'];
							
								$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
								$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
								$mail->addAddress(''.$to.'');
								$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
								$mail->Subject = $subject;
								$mail->Body = $message;
								$mail->IsHTML(true); 
								$mail->send();
							}
				
							if($mobiletype=="Postpaid")	{
								$rechargevalue="rechargekey='".$apiresponse['trans_id']."',rechargestatus='".$apiresponse['status']."'";
								$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	 
								
								$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
								$main->sendSms($phone,$mess);
								$to      = $result->email; // Send email to our user
								$subject = 'Recharge Confirmation'; // Give the email a subject 
								$message=$config['EMAIL_HEADER'];
								$message.= "Dear ".$result->name.",<br><br>";
								$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
								$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
								$message.= "<strong>Transaction ID :</strong>".$apiresponse['trans_id']."<br>";
								$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
								$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
								$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
								$message.= "<strong>Recharge Date :</strong>".$apiresponse['datetime']."<br>";
									 
								$message.=$config['EMAIL_FOOTER'];
								//Answer key attachment
								$mail = new PHPMailer;
								$mail->isSMTP();
								$mail->Host = $config['SMTP']['HOST'];
								//Set the SMTP port number - likely to be 25, 465 or 587
								$mail->Port = $config['SMTP']['PORT'];
								//Whether to use SMTP authentication
								$mail->SMTPAuth = true;
								//Username to use for SMTP authentication
								$mail->Username = $config['SMTP']['USER'];
								//Password to use for SMTP authentication
								$mail->Password = $config['SMTP']['PASSWORD'];
							
								$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
								$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
								$mail->addAddress(''.$to.'');
								$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
								$mail->Subject = $subject;
								$mail->Body = $message;
								$mail->IsHTML(true); 
								$mail->send();
							}
						}
					}
				}
	/*-------------------------		Mobile Recharge	End		--------------------------------*/
	
	/*-------------------------		Data Card Recharge Start	--------------------------------*/
					If ($rechargetype == "datacard"){
						$operatorcode=$result->operatorid;
						$apiresponse= $apiprocess->DataCardRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][$rechargetype]);
					
						if (isset($apiresponse['ErrorCode'])){
							$msg= $apiresponse['Message'];
							$codestatus=false;
						}

						$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
						$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
					
					if ($codestatus==true AND $apiresponse['response_code'] == 0){
								$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
								$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
								unset($_SESSION['ORDERID']);
								
								$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
								$main->sendSms($phone,$mess);
								$to      = $result->email; // Send email to our user
								$subject = 'Recharge Confirmation'; // Give the email a subject 
								$message=$config['EMAIL_HEADER'];
								$message.= "Dear ".$result->name.",<br><br>";
								$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
								$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
								$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
								$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
								$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
									 
								$message.=$config['EMAIL_FOOTER'];
								//Answer key attachment
								$mail = new PHPMailer;
								$mail->isSMTP();
								$mail->Host = $config['SMTP']['HOST'];
								//Set the SMTP port number - likely to be 25, 465 or 587
								$mail->Port = $config['SMTP']['PORT'];
								//Whether to use SMTP authentication
								$mail->SMTPAuth = true;
								//Username to use for SMTP authentication
								$mail->Username = $config['SMTP']['USER'];
								//Password to use for SMTP authentication
								$mail->Password = $config['SMTP']['PASSWORD'];
							
								$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
								$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
								$mail->addAddress(''.$to.'');
								$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
								$mail->Subject = $subject;
								$mail->Body = $message;
								$mail->IsHTML(true); 
								$mail->send();
							}
						}
	/*-------------------------		Data Card Recharge	End		--------------------------------*/
	
	/*-------------------------		DTH Recharge Start	--------------------------------*/
				If ($rechargetype == "dth"){
					$operatorcode=$result->operatorid;
					$apiresponse= $apiprocess->DTHRecharge($phone,$operatorcode,$amount,$randomno,$config['API']['SF'][$rechargetype]);
					if (isset($apiresponse['ErrorCode'])){
						$msg= $apiresponse['Message'];
						$codestatus=false;
					}
			
					$logvalue="rechargelog = '".json_encode($apiresponse)."'" ;
					$orderprocess->UpdateDevelopmentLog($logvalue,$logid);
					
					if ($codestatus==true AND $apiresponse['response_code'] == 0){
							$rechargevalue="rechargekey='".$apiresponse['client_trans_id']."',rechargestatus='".$apiresponse['status']."'";
							$resp= $orderprocess->UpdateRechargeOrder($rechargevalue,$ORDERID) ;  	   
							unset($_SESSION['ORDERID']);
							
							$mess="Your ".ucfirst($rechargetype)." has been successfully recharged with amount ".$amount;
							$main->sendSms($phone,$mess);
							$to      = $result->email; // Send email to our user
							$subject = 'Recharge Confirmation'; // Give the email a subject 
							$message=$config['EMAIL_HEADER'];
							$message.= "Dear ".$result->name.",<br><br>";
							$message.= "Your ".ucfirst($rechargetype)." has been successfully recharged. Please Find below details :<br><br>";
							$message.= "<strong>".ucfirst($rechargetype)." No :</strong>".$phone."<br>";
							$message.= "<strong>Recharge key :</strong>".$apiresponse['client_trans_id']."<br>";
							$message.= "<strong>Amount :</strong>INR ".$amount."<br>";
							$message.= "<strong>Recharge Status :</strong>".$apiresponse['status']."<br>";
								 
							$message.=$config['EMAIL_FOOTER'];
							//Answer key attachment
							$mail = new PHPMailer;
							$mail->isSMTP();
							$mail->Host = $config['SMTP']['HOST'];
							//Set the SMTP port number - likely to be 25, 465 or 587
							$mail->Port = $config['SMTP']['PORT'];
							//Whether to use SMTP authentication
							$mail->SMTPAuth = true;
							//Username to use for SMTP authentication
							$mail->Username = $config['SMTP']['USER'];
							//Password to use for SMTP authentication
							$mail->Password = $config['SMTP']['PASSWORD'];
						
							$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
							$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
							$mail->addAddress(''.$to.'');
							$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
							$mail->Subject = $subject;
							$mail->Body = $message;
							$mail->IsHTML(true); 
							$mail->send();
						}
					}
	/*-------------------------		DTH Recharge End	--------------------------------*/
			}
			
		$res['json_data']['orderid']=(int)$ORDERID;
		$res['json_data']['amount']=(string)$amount;
		$res['json_data']['firstname']=trim($name);
		$res['json_data']['email']=trim($email);
		$res['json_data']['phone']=trim($phone);
		if ($type=="Recharge") {	$res['json_data']['operator_name']=trim($operatorname->operatorname);	}
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Response Successful";
		echo json_encode($res);
	
?>