<?
error_reporting(E_ALL);
ini_set("display_errors",1);

class api
{

	var $id;
	var $fName;
	var $mName;
	var $lName;
	var $username;
	var $password;
	var $email;
	var $address;
	var $countryID;
	var $countryName;
	var $city;
	var $state;
	var $zip;
	var $phone1;
	var $fax;
	var $creationDate;
	var $enabled;
	var $userIP;
	
	function __construct()
	{
		
	}

	/*-------------		Register Start	--------------*/
	function add($postvalues)
	{
		global $dbLink;
	
		$CreationDate = date("Y-m-d H:i:s");

		$query = "INSERT INTO customer set ";
		foreach ($postvalues as $key=>$value){
			$query .= "$key='$value'";
			$query .= ",";
		}	

		$query = substr($query, 0, -1); 
		 //exit($query);
		mysqli_query($dbLink, $query);
		$this->id = mysqli_insert_id($dbLink);
	}	
	
	function addNewConfirmation($postdata)
	{
		global $dbLink;
	
		$CreationDate = date("Y-m-d H:i:s");
		$query =" INSERT INTO customercode set ";
		foreach ($postdata as $key=>$value){
			$query .= "$key='$value'";
			$query .= ",";
		}	
		$query = substr($query, 0, -1); 
		mysqli_query($dbLink, $query);
	}
	
	function isPhonenoExist($phone,$email)
	{
		global $dbLink;

		$query =  " SELECT * FROM customer WHERE (phone = '".$phone."' or  email = '".$email."') And enabled = '1' ";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return mysqli_num_rows($result);
		}
		return 0;
	}
	
	function checkverificationcode($phone,$verificationcode)
	{
		global $dbLink;

		$query =  " SELECT * FROM customercode WHERE phoneno = '$phone' and code = '$verificationcode' And status = '0' and DATE(`date`) = '".date("Y-m-d")."'";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return mysqli_num_rows($result);
		}
		return 0;
	}
	
	function updateverificationcode($phone,$verificationcode)
	{
		global $dbLink;

		$query =  " update customercode set status = '1' WHERE phoneno = '$phone' and code = '$verificationcode'";
		$result = mysqli_query($dbLink, $query);
	
	}
	
	function updatestatus($phone)
	{
		global $dbLink;

		$query =  " update customer set enabled = '1' WHERE phone = '$phone'";
		$result = mysqli_query($dbLink, $query);
	
	}
	/*-------------		Register End	--------------*/

	/*-------------		Register Google+ And Facebook Account Start	--------------*/
	function AccountRegister($postvalues)
	{
		global $dbLink;
	
		$CreationDate = date("Y-m-d H:i:s");

		$query = "INSERT INTO customer set ";
		foreach ($postvalues as $key=>$value){
			$query.= "$key='$value'";
			$query.= ",";
		}	
		$query = substr($query, 0, -1); 
		// exit($query);
		mysqli_query($dbLink, $query);
		$this->id = mysqli_insert_id($dbLink);
	}	
	/*-------------		Register Google+ And Facebook Account End	--------------*/

	/*-------------		Get Account Details Of Google+ And Facebook Account Start	--------------*/
	function GetAccountDetails($email,$accounttype)
	{
		global $dbLink;
		
		$query ="SELECT *  FROM customer WHERE 1=1 AND (email='".$email."' OR phone='".$email."') AND accounttype='".$accounttype."' AND enabled =1  ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	/*-------------		Get Account Details Of Google+ And Facebook Account End	--------------*/

	/*-------------		Send SMS Start	--------------*/
	function sendSms($mobilenumbers,$mess)
	{ 
		global $config;
		
		$user=$config['SMS']['USER'];	 //your username
		$password=$config['SMS']['PASSWORD']; //your password 
		$senderid=$config['SMS']['SENDERID']; //Your senderid
		$url=$config['SMS']['URL'];
			
		$message = urlencode($mess);

		$ch = curl_init();
		if (!$ch){die("Couldn't initialize a cURL handle");}
		$ret = curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"username=$user&password=$password&type=0&dlr=1&destination=$mobilenumbers&source=$senderid&message=$message");
		$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ;

		$curlresponse = curl_exec($ch);// execute
		
		
		if(curl_errno($ch))
			echo 'curl error : '. curl_error($ch);
		if (empty($ret)) {
			// some kind of an error happened
			die(curl_error($ch));
			curl_close($ch); // close cURL handler
		} else {
			$info = curl_getinfo($ch);
			curl_close($ch); // close cURL handler
		   
		 }
	}
	/*-------------		Send SMS End	--------------*/

	/*-------------		Login Start	--------------*/
	function login($username,$password)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE (email='".$username."' OR phone='".$username."') AND password=BINARY '".$password."' AND (accounttype ='MANUAL' OR accounttype ='') AND enabled =1  ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	/*-------------		Login End	--------------*/

	/*-------------		After Verification Return User Data Start	--------------*/
	function Verificationdata($username)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE phone='".$username."' AND enabled =1  ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	/*-------------		After Verification Return User Data END	--------------*/
	
	/*-------------		Menu Start	--------------*/
	function getmenu($userid)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE id='".$userid."' AND enabled =1  ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$querys ="SELECT * FROM menu WHERE enabled =1 ORDER BY menu.index ASC ";
			$results = mysqli_query($dbLink, $querys);
			return $results;
		}
		return false;
	}
	/*-------------		Menu END	--------------*/
	
	/*-------------		Forgot PASSWORD Start	--------------*/
	function getforgotpassword($email)
	{
		global $dbLink;

		$query ="SELECT *  FROM customer WHERE email='".$email."' OR phone='".$email."' AND enabled =1  ";
		 
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	/*-------------		Forgot PASSWORD END		--------------*/
	
	/*-------------		Get Operator By operatorid Start	--------------*/
	function getOperatorName($operatorid)
	{
		global $dbLink;

		$query  = "Select * From `operator` Where `status`=1 and  operatorid='".$operatorid."' ";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row;
		}
		return false;
	}
	/*-------------		Get Operator By operatorid END	--------------*/
	
	/*-------------		Retrieves order information Start	--------------*/
	function getRechargeOrderInfo($OrderID)
	{
		global $dbLink;

		echo $query  = "Select * From `rechargeorder` Where orderid = '" . $OrderID . "'";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		
		$this->mobileno = $row->mobileno;
		$this->operator = $row->operator;
		$this->mobiletype = $row->mobiletype;
		$this->amount = $row->amount;
		$this->transactionid = $row->transactionid;
		$this->rechargedate = $row->rechargedate;
		$this->payment = $row->payment;
		$this->paymentstatus = $row->paymentstatus;
		$this->userip = $row->userip;
		return $row;
	}
	/*-------------		Retrieves order information END		--------------*/

}

?>