<?php 
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	
	include_once("../Api/model/order.class.php");
	$orderprocess= new Order();
	
	global $config;	
	$dbLink = $db->dbLink;

	include_once("../Api/model/customer.class.php");
	$customer = new customer();

	include_once("../Classes/main.class.php");
	$main = new main();
	
	require ("../phpMailer/PHPMailerAutoload.php");

	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
	if($_SERVER['REQUEST_METHOD'] != "POST"){  
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Not Acceptable Method';
		echo json_encode($response);
		die();
	}
	$post=count($_POST);
	if(empty($post)){  
		$response['status']="FALSE";   
		$response['code']="FAILURE";		
		$response['message']='Not Found Any Params .';
		echo json_encode($response);
		die();
	}

	if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='User ID should not be Blank.';
		echo json_encode($response);
		die();
	}
	
	if((!isset($_POST['amount'])) OR empty($_POST['amount'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Amount should not be Blank.';
		echo json_encode($response);
		die();
	}
	
	if((!isset($_POST['phone'])) OR empty($_POST['phone'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Phone No should not be Blank.';
		echo json_encode($response);
		die();
	}
	
	if((!isset($_POST['tomobile'])) OR empty($_POST['tomobile'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='To Mobile No should not be Blank.';
		echo json_encode($response);
		die();
	}
	
	$userid = $_POST['userid'];
	$amount = $_POST['amount'] ;
	$phone = $_POST['phone'];
	$sendmobileno =$_POST['tomobile'];
		
		$sendercond = "phone='".$phone."'";
		$senderinfo=$customer->getcustomerinfo($sendercond);
		$senderbalance = $senderinfo->balance;
		
		if($senderbalance<$amount){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']="Your wallet balance is less than amount!";
			echo json_encode($response);
			die();
		}
		
		if($phone==$sendmobileno){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']="Please Enter Another Registered Mobile No!";
			echo json_encode($response);
			die();
		}
		
		$destinationnodetails= $customer->checkdestinationacount($sendmobileno);
		
		if (!isset($destinationnodetails->id)) {
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']="Please Enter Valid Mobile No!";
			echo json_encode($response);
			die();
		}
		
		$sendmoneyvalue = new stdClass();
		$sendmoneyvalue->frommobile = $phone;
		$sendmoneyvalue->tomobile = $sendmobileno;
		$sendmoneyvalue->amount = $amount;
		$sendmoneyvalue->payment = "WALLET";
		$sendmoneyvalue->transactionid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);;
		$sendmoneyvalue->userip = "";
		$sendmoneyvalue->paymentstatus = "success"; 
		$orderid = $orderprocess->SendMoney($sendmoneyvalue);
		$orderprocess->updateSenderMoney($phone,$amount);
		$sendercond = "phone='".$phone."'";
		$senderinfo=$customer->getcustomerinfo($sendercond);
		$mess="Your Order has been  successfully placed with transaction id ".$sendmoneyvalue->transactionid .". Your current balance is INR ". $senderinfo->balance;
		$main->sendSms($phone,$mess);
			
			$name= isset($senderinfo->name)?$senderinfo->name:'Customer';
			$to      = $senderinfo->email; // Send email to our user
			$subject = 'Amount Sent'; // Give the email a subject 
			$message=$config['EMAIL_HEADER'];
			$message.= "Dear ".ucfirst($name).",<br><br>";
			$message.= "Your order has been successfully submitted. Please Find below details regarding the transaction:<br><br>";
			$message.= "<strong>Order No : </strong>".$orderid."<br>";
			$message.= "<strong>Transaction ID : </strong>".$sendmoneyvalue->transactionid."<br>";
			$message.= "<strong>Receiver Mobile : </strong>".$sendmoneyvalue->tomobile."<br>";
			$message.= "<strong>Amount : </strong> INR ".$amount."<br>";
			$message.= "<strong>Balance : </strong> INR ".$senderinfo->balance."<br>";
			$message.= "<strong>Payment Status : </strong>success<br>";
					 
			$message.=$config['EMAIL_FOOTER'];
				
			//Answer key attachment
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = $config['SMTP']['HOST'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $config['SMTP']['PORT'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $config['SMTP']['USER'];
			//Password to use for SMTP authentication
			$mail->Password = $config['SMTP']['PASSWORD'];
			
			$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
			$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addAddress(''.$to.'');
			$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->IsHTML(true); 
			$mail->send();	
		
		$orderprocess->updateReceiverMoney($sendmobileno,$amount);
		$receivercond = "phone='".$sendmobileno."'";
		$receiverinfo=$customer->getcustomerinfo($receivercond);
		$mess="You have received payment INR ".$amount." from ".$sendmoneyvalue->frommobile.". Your current balance is INR ". $receiverinfo->balance;
		$main->sendSms($receiverinfo->phone,$mess);
		
			$name= isset($receiverinfo->name)?$receiverinfo->name:'Customer';
			$to      = $receiverinfo->email; // Send email to our user
			$subject = 'Amount Received'; // Give the email a subject 
			$message=$config['EMAIL_HEADER'];
			$message.= "Dear ".ucfirst($name).",<br><br>";
			$message.= "You have received payment INR ".$amount." from ".$sendmoneyvalue->frommobile.". Your current balance is INR ". $receiverinfo->balance;
						 
			$message.=$config['EMAIL_FOOTER'];
				
			//Answer key attachment
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = $config['SMTP']['HOST'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $config['SMTP']['PORT'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;
			//Username to use for SMTP authentication
			$mail->Username = $config['SMTP']['USER'];
			//Password to use for SMTP authentication
			$mail->Password = $config['SMTP']['PASSWORD'];
			
			$mail->setFrom(''.$config['NOREPLY'].'', 'PAYIN');
			$mail->addReplyTo(''.$config['SUPPORT_MAIL'].'', 'PAYIN SUPPORT');
			$mail->addAddress(''.$to.'');
			$mail->AddBCC($config['SUPPORT_MAIL'], "PAYIN SUPPORT");
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->IsHTML(true); 
			$mail->send();	
			
			
			$sendmoneyvalue= $orderprocess->getsenderorderinfo($orderid);
			$msg = "<h3>Thank You. Your order status is success.</h3>";
			$msg .= "<h4>Your Transaction ID for this transaction is ".$sendmoneyvalue->transactionid.".</h4>";
			$msg .= "<h4>You have sent amount INR " . $sendmoneyvalue->amount . " to ". $sendmoneyvalue->tomobile."</h4>";
		
		
			$res['json_data']['orderid']=(int)$orderid;
			$res['json_data']['txnid']=$sendmoneyvalue->transactionid;
			$res['json_data']['amount']=$sendmoneyvalue->amount;
			$res['json_data']['tomobile']=$sendmoneyvalue->tomobile;
		
			$res['json_data']['status']="TRUE";
			$res['json_data']['code']="SUCCESS";
			$res['json_data']['message']="Send money Successfully";
			echo json_encode($res);


?>
