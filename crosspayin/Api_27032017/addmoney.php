<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	
	include_once("../Api/model/order.class.php");
	$orderprocess= new Order();

	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/model/api.class.php");
	$apivalues = new api();
	
	$ENV = ($config['TEST_ENV']) ? 'TEST' : 'LIVE';
	$merchantkey = $config['PAYU'][$ENV]['KEY'] ;
	$salt = $config['PAYU'][$ENV]['SALT'] ; 
	$action = $config['PAYU'][$ENV]['URL'] ; 
	$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
	if($_SERVER['REQUEST_METHOD'] != "POST"){  
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Not Acceptable Method';
		echo json_encode($response);
		die();
	}
	$post=count($_POST);
	if(empty($post)){  
		$response['status']="FALSE";   
		$response['code']="FAILURE";		
		$response['message']='Not Found Any Params .';
		echo json_encode($response);
		die();
	}

	if((!isset($_POST['userid'])) OR empty($_POST['userid'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='User ID should not be Blank.';
		echo json_encode($response);
		die();
	}
	
	if((!isset($_POST['amount'])) OR empty($_POST['amount'])){
		$response['status']="FALSE";   
		$response['code']="FAILURE";		 
		$response['message']='Amount should not be Blank.';
		echo json_encode($response);
		die();
	}
	
	$userid = isset($_POST['userid']) ?$_POST['userid'] :'';
	$amount = isset($_POST['amount']) ?$_POST['amount'] :'';

	if (!empty($amount)){
		$addmoneyvalue= new stdclass();
		$addmoneyvalue->customerid = $userid;
		$addmoneyvalue->amount = $amount;
		$addmoneyvalue->payment = "PAYU";
		$addmoneyvalue->paymentstatus = "pending"; 
		$addmoneyvalue->transactionid = "";
		$addmoneyvalue->userip = "";
		
		$orderid = $orderprocess->AddMoney($addmoneyvalue);
		
		if (!empty($orderid)){
			$result = $orderprocess->getAddMoneyOrderInfo($orderid);
			$amount = $result->amount;
			$cname = $result->name;
			$cemail = $result->email;
			$cmobileno = $result->mobileno;
			$productinfo = "Add Money  for Mobile No ".$result->mobileno;
			$udf1 = $orderid;
			$udf2 ="Wallet";
		} else {
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Sorry to proceed, please try again!.';
			echo json_encode($response);
			die();
		}
		
	}

	$posted['key']=$merchantkey;
		$posted['txnid']=$txnid;
		$posted['amount']=$amount;
		$posted['firstname'] = $cname;
		$posted['email']= $cemail;
		$posted['phone']=$cmobileno;
		$posted['productinfo'] = $productinfo;
		$posted['surl']=$config['HOST']."/payumoney/success.php";
		$posted['furl']=$config['HOST']."/payumoney/failure.php";
		$posted['curl']=$config['HOST']."/payumoney/failure.php";
		$posted['service_provider'] =($config['TEST_ENV']) ? '' : 'payu_paisa';
		// $posted['service_provider'] =($config['TEST_ENV']) ? '' : '';
		$posted['udf1'] =$udf1;
		$posted['udf2'] =$udf2;


		$hash = '';
		// Hash Sequence
		//$hashSequence = "key|txnid|amount|firstname|email|phone|productinfo|surl|furl|curl|service_provider";
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		$hashVarsSeq = explode('|', $hashSequence);
		$hash_string = '';	
		foreach($hashVarsSeq as $hash_var) {
		  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
		  $hash_string .= '|';
		}

		$hash_string .= $salt;

		$hash = strtolower(hash('sha512', $hash_string));


		
		$res['json_data']['orderid']=(int)$orderid;
		$res['json_data']['key']=$posted['key'];
		$res['json_data']['txnid']=$posted['txnid'];
		$res['json_data']['amount']=$posted['amount'];
		$res['json_data']['firstname']=trim($posted['firstname']);
		$res['json_data']['email']=$posted['email'];
		$res['json_data']['phone']=$posted['phone'];
		$res['json_data']['productinfo']=$posted['productinfo'];
		$res['json_data']['surl']=$posted['surl'];
		$res['json_data']['furl']=$posted['furl'];
		$res['json_data']['curl']=$posted['curl'];
		$res['json_data']['service_provider']=$posted['service_provider'];
		$res['json_data']['udf1']=$posted['udf1'];
		$res['json_data']['udf2']=$posted['udf2'];
		$res['json_data']['hash']=$hash;
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Pay to add money in your wallet";
		echo json_encode($res);
	
?>