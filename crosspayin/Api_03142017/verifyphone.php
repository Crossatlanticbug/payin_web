<?php
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	require_once("../Configurations/config.inc.php");
	require_once("../Functions/functions.php");
	require_once("../Classes/connection.class.php");
	$db = new connection("../Classes/connection.cfg.php");
	global $config;	
	$dbLink = $db->dbLink;
	include_once("../Api/api.class.php");
	$apivalues = new api();
	
	header('Content-type: application/json');	
	header('Access-Control-Allow-Origin: *');
	
		if($_SERVER['REQUEST_METHOD'] != "POST"){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Not Acceptable Method';
			echo json_encode($response);
			die();
		}
		$post=count($_POST);
		if(empty($post)){  
			$response['status']="FALSE";   
			$response['code']="FAILURE";		
			$response['message']='Not Found Any Params .';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['otp'])) OR empty($_POST['otp'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='OTP should not be Blank.';
			echo json_encode($response);
			die();
		}
		if((!isset($_POST['phone'])) OR empty($_POST['phone'])){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Phone should not be Blank.';
			echo json_encode($response);
			die();
		}
		
		$otp= $_POST['otp'];
		$phone= $_POST['phone'];
		
		$result= $apivalues->checkverificationcode($phone,$otp);
		if ($result==0){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Invalid OTP.';
			echo json_encode($response);
			die();
		}	
		
		$apivalues->updateverificationcode($phone,$otp);
		$apivalues->updatestatus($phone);
		
		$apivaluesdata= $apivalues->Verificationdata($phone);
		
		if($apivaluesdata==""){
			$response['status']="FALSE";   
			$response['code']="FAILURE";		 
			$response['message']='Vefification Failed.';
			echo json_encode($response);
			die();
		}
		
		$res['json_data']['userid']=(int)$apivaluesdata->id;
		$res['json_data']['name']=$apivaluesdata->name;
		$res['json_data']['email_id']=$apivaluesdata->email;
		$res['json_data']['phone']=$apivaluesdata->phone;
		$res['json_data']['balance']=$apivaluesdata->balance;
		$res['json_data']['address']=$apivaluesdata->address;
		
		$res['json_data']['status']="TRUE";
		$res['json_data']['code']="SUCCESS";
		$res['json_data']['message']="Welcome TO Payin";
		echo json_encode($res);
	
?>