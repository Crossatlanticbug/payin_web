<?php
	session_start();
	include_once("../Includes/template.inc.php");

	include_once("../Classes/order.class.php");
	$orderprocess = new Order();

	if (!isset($_SESSION['senderorderid'])){
		exit("<script>window.location.href='../site/index.php';</script>");
	}	
		$senderid=	$_SESSION['senderorderid'];
		$sendmoneyvalue= $orderprocess->getsenderorderinfo($senderid);

		$msg = "<h3>Thank You. Your order status is success.</h3>";
		$msg .= "<h4>Your Transaction ID for this transaction is ".$sendmoneyvalue->transactionid.".</h4>";
		$msg .= "<h4>You have sent amount INR " . $sendmoneyvalue->amount . " to ". $sendmoneyvalue->tomobile."</h4>";

		unset($_SESSION['senderorderid']); 
		
?>
<div class='container' style='padding:50px 0;'><?php echo $msg ;?></div>
<?  End_Response(); ?>