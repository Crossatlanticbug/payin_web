<?
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * Class		:	user
* Version :	1.0
* Date    :	12 April 2006
* Author  :	Darwish Khalil
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// Member Reward Types Constants
define("REFERRER_COMMISSION",1);
define("REFERRED_COMMISSION",2);
define("RETROACTIVE_COMMISSION",3);

// Member Verification/Confirmation Types Constants
define("VERIFICATION_EMAIL",1);
define("VERIFICATION_SMS",2);
define("VERIFICATION_VOICE",3);

class user 
{
	var $id;
	var $fName;
	var $mName;
	var $lName;
	var $username;
	var $password; 
	var $email;
	var $address;
	var $countryID;
	var $countryISOCode3;
	var $countryName;
	var $callingCode;
	var $city;
	var $state;
	var $zip;
	var $pobox;
	var $phone1;
	var $phone2;
	var $fax;
	var $creationDate;
	var $enabled;
	var $i_account = -1;    // The corresponding account id in the Accounts table in porta_billing database
	var $balance;
	var $isReseller;	// A flag to check if this account is a reseller or not
	var $parentID = 0; 		// The account id of the owner of this account (reseller account) in case this account belongs to a reseller
	var $batchID; // Batch ID of the reseller's sub-account
	var $userIP;
	var $retroactiveNextDate;  // The date used to indicate when this reseller will get his retroactive commissions
	var $verified;

	// The reference ID of the user that indicates how did he/she knows about SoftCall
	var $referenceID;
	// If $referenceID = 104 (Friend's Recommendation), $referrerID will be used to store the referrer SoftCall Account ID
	// to be used in referral & rewards programs
	var $referrerID;
	var $referralID; // ID of the referral that converts this referred party into a SoftCall customer
	var $isRewardsAllowed;

	// Added to be used while switching to either PAYG or MCP
	var $i_product;
	var $i_routing_plan;

	var $trace;

	function member_user($fName='', $mName='', $lName='', $username='', $password='', $email='',
			$address='', $countryID=-1, $city='', $state='', $zip='', $pobox='', $phone1='', $phone2='', $fax='', $enabled=false)
	{
		$this->fName = $fName;
		$this->mName = $mName;
		$this->lName = $lName;
		$this->username = $username;
		$this->password = $password;
		$this->email = $email;
		$this->address = $address;
		$this->countryID = $countryID;
		$this->city = $city;
		$this->state = $state;
		$this->zip = $zip;
		$this->pobox = $pobox;
		$this->phone1 = $phone1;
		$this->phone2 = $phone2;
		$this->fax = $fax;
		$this->enabled = $enabled;
	}

	//private utility method
	function populateFromRowObject($row, &$memberUser)
	{
		global $country;

		$memberUser->id = $row->mu_ID;
		$memberUser->fName = $row->mu_FName;
		$memberUser->mName = $row->mu_MName;
		$memberUser->lName = $row->mu_LName;
		$memberUser->username = $row->mu_Username;
		$memberUser->password = $row->mu_Password;
		$memberUser->email = $row->mu_Email;
		$memberUser->address = $row->mu_Address;
		$memberUser->countryID = $row->mu_Country_ID;
		$memberUser->city = $row->mu_City;
		$memberUser->state = $row->mu_State;
		$memberUser->zip = $row->mu_ZipCode;
		$memberUser->pobox = $row->mu_POBOX;
		$memberUser->phone1 = $row->mu_Phone1;
		$memberUser->phone2 = $row->mu_Phone2;
		$memberUser->fax = $row->mu_Fax;
		$memberUser->enabled = $row->mu_Enabled;
		$memberUser->creationDate = $row->mu_Creation_Date;
		$memberUser->i_account = $row->i_account;
		$memberUser->isReseller = $row->mu_IsReseller;
		$memberUser->parentID = $row->mu_ParentID;
		$memberUser->batchID = $row->mu_BatchID;
		$memberUser->referenceID = $row->mu_ReferenceID;
		$memberUser->referrerID = $row->mu_ReferrerID;
		$memberUser->referralID = $row->mu_ReferralID;
		$memberUser->isRewardsAllowed = $row->mu_IsRewardsAllowed;
		$memberUser->userIP = $row->mu_UserIP;
		$memberUser->retroactiveNextDate = $row->mu_RetroactiveNextDate;

		$memberUser->countryISOCode3 = $this->getCountryISO3Digits($row->mu_Country_ID);

		$memberUser->countryName = $country->NamesArray[$row->mu_Country_ID];
		$memberUser->callingCode = $country->CallingCodesArray[$row->mu_Country_ID];

		$memberUser->verified = $this->isVerified();
	}

	//populates the current user by providing his/her ID
	function populateByID($id)
	{
		global $dbLink;

		$query = "SELECT * FROM member_user WHERE mu_ID = $id";
		$result = mysqli_query($dbLink, $query);
		if ($result){
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_object($result);
			$this->populateFromRowObject($row, $this);
			return true;
		}
		}
		return false;
	}

	//populates the current user by providing his/her Email
	function populateByEmail($email)
	{
		global $dbLink;

		$query = "SELECT * FROM member_user WHERE LTRIM(RTRIM(mu_Email)) = LTRIM(RTRIM('$email'))";
		$result = mysqli_query($dbLink, $query);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_object($result);
			$this->populateFromRowObject($row, $this);
			return true;
		}
		return false;
	}

	//populates the current user by providing his/her Username
	function populateByUsername($username)
	{
		global $dbLink;
		$username = LTRIM(RTRIM($username));
		$query = "SELECT * FROM member_user WHERE LTRIM(RTRIM(mu_Username)) = '$username'";

		$result = mysqli_query($dbLink, $query);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_object($result);
			$this->populateFromRowObject($row, $this);
			return true;
		}
		return false;
	}

	// This function is used to get the references list to fill the "How did you hear about us" dropdown list in the registration page
	function getRefenercesList()
	{
		global $dbLink;

		$query = "SELECT * From member_reference Where Enabled = 1 Order By Name";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to retrieve the matching country in the 3 Digits ISO Code System
	function getCountryISO3Digits($CountryID)
	{
		global $lang;
		global $dbLink;

		$query = "SELECT c_ID_3 CountryISOCode3 FROM country Where c_ID = '$CountryID'";
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		return $row->CountryISOCode3;
		/*if (mysqli_num_rows($result)>0) {
		 return mysqli_result($result, 0, 0);
		}else{
		return '';
		}*/
	}

	// Update retroactive next date
	function updateRetroactiveNextDate()
	{
		global $dbLink;

		$nextDate = date("Y-m-d", mktime(0, 0, 0, date("m")+1, date("d"), date("Y")));
		$query  = "Update `member_user` Set mu_RetroactiveNextDate = '$nextDate' Where mu_ID = '$this->id'";
		mysqli_query($dbLink, $query);
	}

	// Update the customer's type (mu_IsReseller = 1 => Reseller, mu_IsReseller = 0 => Normal Customer)
	function updateType($Value)
	{
		global $dbLink;

		$query = "Update member_user Set mu_IsReseller = $Value Where mu_ID = " .  $this->id;
		mysqli_query($dbLink, $query);
	}

	// Update the customer's status (mu_Enabled = 1 => Active Account, mu_Enabled = 0 => Blocked/Inactive Account)
	function updateStatus($Value)
	{
		global $dbLink;

		$query = "Update member_user Set mu_Enabled = $Value Where mu_ID = " .  $this->id;
		mysqli_query($dbLink, $query);
	}

	// Update account status on P1 (blocked = 'Y' => Blocked/Disabled account, blocked = 'N' => Enabled/Active account)
	function updateStatusOnP1($Value)
	{
		global $trace;

		require_once("connection.class.php");
		$pb_masterdb = new connection("pb_connection.master.cfg.php");
		$pb_masterdbLink = $pb_masterdb->dbLink;

		$blocked = ($Value) ? 'N' : 'Y';
		$query = "Update Accounts Set blocked = '$blocked' Where i_account = " . $this->i_account;
		$trace->info('query = ' . $query);
		try
		{
			mysqli_query($pb_masterdbLink, $query);
		}
		catch (Exception $e)
		{

			$trace->error('Caught Exception' . $e->getMessage(),1);
		}
		$pb_masterdb->dbclose();
	}

	// Block customer on P1 only
	function blockOnP1()
	{
		// block the account on P1 DB
		$this->updateStatusOnP1(0);
	}

	// Block customer on both SoftCall website and P1
	function completelyBlock()
	{
		// block the account on SoftCall DB
		$this->updateStatus(0);

		// block the account on P1 DB
		$this->updateStatusOnP1(0);
	}

	// Unblock customer on P1 only
	function unblockOnP1()
	{
		// unblock the account on P1 DB
		$this->updateStatusOnP1(1);
	}

	// Unblock customer on both SoftCall website and P1
	function unblock()
	{
		// unblock the account on SoftCall DB
		$this->updateStatus(1);

		// unblock the account on P1 DB
		$this->updateStatusOnP1(1);
	}

	// Set the customer's type as Normal Customer
	function makeCustomer()
	{
		$this->updateType(0);
	}

	// Set the customer's type as Reseller
	function makeReseller()
	{
		$this->updateType(1);
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                   Functions used in Orders Pages
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get a list of all member users (Buyers or Subscribers)
	function getMemberUsers()
	{
		global $dbLink;

		$query  = "Select mu_ID UserID, concat(mu_FName,' ',mu_LName) MemberName, mu_Username Username ";
		$query .= "From member_user Where mu_Enabled = 1 ";
		$query .= "Order By mu_Username, concat(mu_FName,' ',mu_LName)";
		// echo $query;
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to get a list of selected member users (Buyers or Subscribers)
	function getMemberUsersBySelection($text)
	{
		global $dbLink;

		$query  =
		"
		Select mu_ID UserID, concat(mu_FName,' ',mu_LName) CustomerName, mu_Username Username
		From member_user
		Where mu_Enabled = 1
		And mu_FName Like '$text%' Or mu_Username Like '$text%'
		Order By mu_Username, concat(mu_FName,' ',mu_MName,' ',mu_LName)
		";
		// echo $query;
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to get a list of customers who placed orders
	function getCustomersList()
	{
		global $dbLink;

		$query  =
		"
				Select Distinct mu_ID UserID, mu_Username Username, concat(mu_FName,' ',mu_LName) CustomerName, Count(OrderID) As TotalOrders
				From member_user MU Inner Join `order` O On MU.mu_ID = O.UserID
				Group By mu_ID
				Order By mu_Username, concat(mu_FName,' ',mu_LName)
				";
		// echo $query;
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to get the information of a certain user
	function getMemberUserInfo($UserID)
	{
		global $dbLink;
		global $lang;

		$query  = "Select concat(mu_FName,' ',mu_MName,' ',mu_LName) CustomerName, ";
		$query .= "mu_Username Username, mu_Password Password, mu_Email Email, mu_Address Address, ";
		$query .= "mu_City City, mu_State State, mu_ZipCode ZipCode, mu_POBOX POBox, mu_Country_ID CountryISO, ";
		$query .= "c_Name_".$lang['LANG']." Country, mu_Phone1 Phone1, ";
		$query .= "mu_Phone2 Phone2, mu_Fax Fax, mu_Creation_Date JoinDate, mu_Enabled Enabled, i_account ";
		$query .= "From member_user, country ";
		$query .= "Where mu_Country_ID = c_ID And mu_ID = $UserID";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

		function getMemberUserInfoByAccountNumber($UserID)
	{
		global $dbLink;
		global $lang;

		 $query  = "Select mu_ID,concat(mu_FName,' ',mu_MName,' ',mu_LName) CustomerName, ";
		$query .= "mu_Username Username, mu_Password Password, mu_Email Email, mu_Address Address, ";
		$query .= "mu_City City, mu_State State, mu_ZipCode ZipCode, mu_POBOX POBox, mu_Country_ID CountryISO, ";
		$query .= "c_Name_".$lang['LANG']." Country, mu_Phone1 Phone1, ";
		$query .= "mu_Phone2 Phone2, mu_Fax Fax, mu_Creation_Date JoinDate, mu_Enabled Enabled, i_account ";
		$query .= "From member_user, country ";
		$query .= "Where mu_Country_ID = c_ID And i_account = $UserID";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}
	// This function is used to get the shipping address of a certain user
	function getShippingAddress($AddressID)
	{
		global $dbLink;
		global $lang;

		$query  = "Select RecipientName, Address, City, c_Name_".$lang['LANG']." Country ";
		$query .= "From member_addressbook, country ";
		$query .= "Where CountryID = c_ID And AddressID = $AddressID";
		// echo($query);
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                   Functions used in Member Users Pages
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the count of members registered available according to the given search criteria
	function getRegisteredMembersCount($Criteria)
	{
	
		
		global $dbLink;

		$query  = "Select COUNT(*) From `member_user` ";
		// $query .= "Where 1=1 And (mu_ParentID Is NULL OR mu_ParentID = 0) ";
		if(($Criteria['IsAutoEnabled'] != '') && ($Criteria['IsAutoEnabled'] != '-1'))
		{	
			$query .= ", automated_billing ";
		}
		$query .= "Where 1=1 ";

		// Member User ID ------------------------------------------------------------
		if($Criteria['MemberID'] != '')
		{
			$query .= "And (mu_ID = " . $Criteria['MemberID'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['Username'] != '')
		{
			$query .= "And (mu_Username Like '%" . $Criteria['Username'] . "%') ";
		}
		// Keyword ------------------------------
		if($Criteria['Keyword'] != '')
		{
			$query .= "And ( ";
			$query .= "(mu_FName Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_MName Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_LName Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Username Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Password Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Email Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Address Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_City Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_State Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_ZipCode Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_POBOX Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Phone1 Like '%" . $Criteria['Keyword'] . "%') ";
			$query .= ") ";
		}
		// Country ID ------------------------------------------------------------
		if($Criteria['CountryID'] != '')
		{
			$query .= "And (mu_Country_ID = '" . $Criteria['CountryID'] . "') ";
		}
		// IsReseller ------------------------------------------------------------
		if($Criteria['IsReseller'] != '')
		{
			$query .= "And (mu_IsReseller = '" . $Criteria['IsReseller'] . "') ";
		}
		// IsEnabled ------------------------------------------------------------
		if($Criteria['IsEnabled'] != '')
		{
			$query .= "And (mu_Enabled = '" . $Criteria['IsEnabled'] . "') ";
		}

		if(($Criteria['IsAutoEnabled'] != '') && ($Criteria['IsAutoEnabled'] != '-1'))
		{
			$query .= "And (automated_billing.Status = '" . $Criteria['IsAutoEnabled'] . "')  And (automated_billing.UserID=mu_ID ) ";
		}
		
		if(($Criteria['IsAutoEnabled'] == '-1'))
		{
			$query .= "And mu_ID NOT IN (select UserID from automated_billing) ";
		}
		// echo $query;
		//$result = mysqli_result(mysqli_query($dbLink, $query), 0, 0);

		//return $result;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

	// This function is used to retrieve a list of registered member regarding a certain search criteria
	function getRegisteredMembers($Criteria, $SortField, $SortType, $Start = -1, $Num = -1)
	{
		global $dbLink;
		global $lang;

		$CountryName = "c_Name_" . $lang['LANG'];

		$query  = "Select mu_ID MemberID, mu_FName FName, mu_MName MName, mu_LName LName, mu_Country_ID CountryISO,i_account,";
		$query .= "concat(mu_FName,' ',mu_MName,' ',mu_LName) CustomerName, mu_Username UserName, mu_Password Password, ";
		$query .= "mu_Email Email, mu_Phone1 Phone, mu_Creation_Date JoinDate, $CountryName CountryName, mu_Enabled IsEnabled, mu_IsReseller IsReseller, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID) TotalOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 1) PendingOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 2) RefusedOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 3) InProgressOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 4) SuspiciousOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 5) AuthorizedOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 6) DeclinedOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 7) ReversedOrders, ";
		$query .= "(Select Count(*) From `order` Where UserID =  mu_ID And StatusID = 8) CancelledOrders ";
		$query .= "From member_user, country ";
		if(($Criteria['IsAutoEnabled'] != '') && ($Criteria['IsAutoEnabled'] != '-1'))
		{	
			$query .= ", automated_billing ";
		}
		$query .= "Where mu_Country_ID = c_ID ";

		// Member User ID ------------------------------------------------------------
		if($Criteria['MemberID'] != '')
		{
			$query .= "And (mu_ID = " . $Criteria['MemberID'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['Username'] != '')
		{
			$query .= "And (mu_Username Like '%" . $Criteria['Username'] . "%') ";
		}
		// Keyword --------------------------------------------------------------
		if($Criteria['Keyword'] != '')
		{
			$query .= "And ( ";
			$query .= "(mu_FName Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_MName Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_LName Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Username Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Password Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Email Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Address Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_City Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_State Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_ZipCode Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_POBOX Like '%" . $Criteria['Keyword'] . "%') OR ";
			$query .= "(mu_Phone1 Like '%" . $Criteria['Keyword'] . "%') ";
			$query .= ") ";
		}
		// Country ID ------------------------------------------------------------
		if($Criteria['CountryID'] != '')
		{
			$query .= "And (mu_Country_ID = '" . $Criteria['CountryID'] . "') ";
		}
		// IsReseller ------------------------------------------------------------
		if($Criteria['IsReseller'] != '')
		{
			$query .= "And (mu_IsReseller = '" . $Criteria['IsReseller'] . "') ";
		}
		/*if($Criteria['Customer_Type'] !='')
		{
			$query .="AND ode.`UserID`=`member_user`.`mu_ID` AND ode.`OrderID`= od.`OrderID` AND od.`Type`='" . $Criteria['Customer_Type'] ."' GROUP BY ode.`UserID`";
		
		}*/
		// IsEnabled ------------------------------------------------------------
		if($Criteria['IsEnabled'] != '')
		{
			$query .= "And (mu_Enabled = '" . $Criteria['IsEnabled'] . "') ";
		}

		if(($Criteria['IsAutoEnabled'] != '') && ($Criteria['IsAutoEnabled'] != '-1'))
		{
			$query .= "And (automated_billing.Status = '" . $Criteria['IsAutoEnabled'] . "')  And (automated_billing.UserID=mu_ID )  ";
		}
		
		if(($Criteria['IsAutoEnabled'] == '-1'))
		{
			$query .= "And mu_ID NOT IN (select UserID from automated_billing) ";
		}
		// Order By
		// $BINARY = ($SortField!="mu_ID") ? "BINARY" : "";

		if (($SortField == "mu_Enabled") || ($SortField == "mu_IsReseller"))
		{
			$query .= "Order By $SortField $SortType, mu_ID $SortType ";
		}
		else
		{
			$query .= "Order By $SortField $SortType ";
		}
		
		// Limit Results
		if (($Start!=-1) && ($Num!=-1)) {
			$query .= "Limit $Start,$Num";
		}

		// echo($query);

		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to delete a certain order
	function deleteMemberUser($MemberID)
	{
		global $dbLink;

		$query  = "Delete From `member_user` Where mu_ID = $MemberID";
		mysqli_query($dbLink, $query);
	}

	// This function is used to retrieve the list of countries
	function getCountryList()
	{
		global $dbLink;
		global $lang;

		$query  = "SELECT c_ID CountryID, c_Name_".$lang['LANG']." CountryName FROM country Where c_Enabled = 1 ";
		$query .= "ORDER BY Binary c_Name_".$lang['LANG'];
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used to retrieve the customer username
	function getMemberUsername($MemberID)
	{
		global $dbLink;

		$query  = "Select mu_Username Username From `member_user` Where mu_ID = $MemberID";
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		return $row->Username;
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                         Member Plan Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// This function is used to get the the info of the Calling Plan
	function getUserPlanInfo($UserPlanID)
	{
		global $dbLink;

		$query  = "Select * From member_plan Where UserPlanID = $UserPlanID";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}
 
	// Add the purchased plans for this user
	function addPlan($PlanID = -1, $PlanName = '', $i_vd_plan = -1, $OrderID = -1, $IsProcessed = 1, $i_product = '')
	{
		global $dbLink;

		require_once("plan.class.php");
		$plan = new plan();
		// changes done on 15 september, 2016 to set end date as per plans (20 or 30days)
		$valid_for = $plan->getValidity($PlanID);

		$StartDate = date("Y-m-d H:i:s");
		$EndDate = date('Y-m-d H:i:s', strtotime("+$valid_for days"));//date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m")+1, date("d"), date("Y")));	
		
		$query =
		"
				INSERT INTO member_plan (UserID, PlanID, PlanName, i_vd_plan, StartDate, EndDate, OrderID, IsProcessed, i_product)
				Values (".$this->id.", $PlanID, '$PlanName', $i_vd_plan, '$StartDate', '$EndDate', '$OrderID', $IsProcessed, $i_product)
				";

		// $this->trace->info("addPlan Query:" . $query);
		mysqli_query($dbLink, $query);
		return mysqli_insert_id($dbLink);
	}

	// Add the purchased DID for this user
	function addDID($AreaCodeID = -1, $Area = '', $OrderID = -1, $IsProcessed = 1)
	{
		global $dbLink;

		$StartDate = date("Y-m-d H:i:s");
		$EndDate = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m")+1, date("d"), date("Y")));

		$query =
		"
				INSERT INTO member_did (UserID, AreaCodeID, Area, StartDate, EndDate, OrderID, IsProcessed)
				Values (".$this->id.", $AreaCodeID, '$Area', '$StartDate', '$EndDate', '$OrderID', $IsProcessed)
				";

		// $this->trace->info("addDID Query:" . $query);
		mysqli_query($dbLink, $query);
		return mysqli_insert_id($dbLink);
	}

	// Activate a certain plan
	function activatePlan($UserPlanID)
	{
		global $dbLink;

		// Since there must be only one active plan at the same time, then we should deactivate any active plan
		$query = "Update member_plan Set CurrentlyActive = 0 Where CurrentlyActive = 1 And UserID = " .  $this->id;
		mysqli_query($dbLink, $query);

		$query = "Update member_plan Set CurrentlyActive = 1 Where UserPlanID = $UserPlanID";
		mysqli_query($dbLink, $query);
	}

	// This function is used to check if this user has any active plans
	function hasActivePlan()
	{
		global $dbLink;

		$query  = "Select UserPlanID From member_plan Where UserID = " . $this->id . " And CurrentlyActive = 1 And Expired = 0";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0) {
			return true;
		}
		return false;
	}

	// This function is used get the number of total calling plans for a specific user
	function getCallingPlansTotalNumber()
	{
		global $dbLink;

		$query  =	"Select count(*) as TotalCallingPlans From member_plan MP Where MP.UserID = " . $this->id . " ";
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		return $row->TotalCallingPlans;
	}

	// This function is used get the current active calling plan
	function getCurrentCallingPlan()
	{
		global $dbLink;

		$query  =
		"
				Select MP.UserPlanID, MP.PlanID, MP.StartDate, MP.EndDate, MP.OrderID,
				PP.Name PlanName, PP.Minutes TotalMinutes, PP.i_vd_plan, PP.Description, PP.PackageID
				From member_plan MP
				Inner Join package_plan PP On MP.PlanID = PP.PlanID
				Where MP.UserID = " . $this->id . "
						And MP.CurrentlyActive = 1 And MP.Expired = 0
						Order By MP.StartDate, PlanName
						";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used get the inactive and not expired calling plans
	function getInActiveCallingPlans()
	{
		global $dbLink;

		$query  =
		"
				Select MP.UserPlanID, MP.PlanID, MP.StartDate, MP.EndDate,
				PP.Name PlanName,  PP.Minutes TotalMinutes, PP.i_vd_plan, PP.Description, PP.PackageID
				From member_plan MP
				Inner Join package_plan PP On MP.PlanID = PP.PlanID
				Where MP.UserID = " . $this->id . "
						And MP.CurrentlyActive = 0 And MP.Expired = 0
						Order By PlanName, MP.StartDate
						";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// This function is used get the expired calling plans
	function getExpiredCallingPlans()
	{
		global $dbLink;

		$query  =
		"
				Select MP.UserPlanID, MP.StartDate, MP.EndDate, MP.UsedMinutes, MP.ExpiredDate,
				PP.Name PlanName, PP.Minutes TotalMinutes, PP.i_vd_plan, PP.Description, PP.PackageID
				From member_plan MP
				Inner Join package_plan PP On MP.PlanID = PP.PlanID
				Where MP.UserID = " . $this->id . "
						And MP.Expired = 1
						Order By MP.StartDate DESC, PlanName
						";
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	// Set a certain plan as expired
	function setPlanAsExpired($UserPlanID, $UsedMinutes)
	{
		global $dbLink;

		$this->trace->info("Set UserPlanID = $UserPlanID as expired");
		$ExpiredDate = date("Y-m-d H:i:s");
		$query = "UPDATE member_plan SET Expired = 1, CurrentlyActive = 0, ExpiredDate = '$ExpiredDate', UsedMinutes = $UsedMinutes Where UserPlanID = $UserPlanID";
		mysqli_query($dbLink, $query);
	}

	// Set a certain did as expired
	function setDIDAsExpired($OrderID)
	{
		global $dbLink;

		$this->trace->info("Set DIDs as expired for order $OrderID");
		$ExpiredDate = date("Y-m-d H:i:s");
		$query = "UPDATE member_did SET Expired = 1, ExpiredDate = '$ExpiredDate' Where OrderID = '$OrderID'";
		mysqli_query($dbLink, $query);
	}

	// deactivate current active plan
	function deactivateCurrentActivePlan()
	{
		global $dbLink;

		$query = "Update member_plan Set CurrentlyActive = 0 Where CurrentlyActive = 1 And UserID = " .  $this->id;
		mysqli_query($dbLink, $query);
	}

	/*
	 * Checks if this user has any plans that have been activated since no more than 1 week ago
	*/
	function isComplyWithServiceCancelationConditions()
	{
		global $dbLink;

		$query  =
		"
				Select UserPlanID From member_plan
				Where UserID = " . $this->id . " And Expired = 0
						And DATE_ADD(StartDate, INTERVAL 7 DAY) >= NOW();
						";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0) {
			return true;
		}
		return false;
	}

	// This function is used to prepare the volume discount plan of a certain purshaced plan
	// by adding a new record in the Account_VD_Counters table in PB database
	function prepareVolumeDiscountPlan($UserPlanID)
	{
		global $dbLink;
		global $trace;
		//-----------------------------------------------------------------------------------------------------
		$result = $this->getUserPlanInfo($UserPlanID);
		$row = mysqli_fetch_object($result);
		$i_vd_plan = $row->i_vd_plan;
		$period_from = $row->StartDate;
		$period_to = $row->EndDate;

		require_once("pb_access.class.php");
		$pb_access = new pb_access($this->i_account, $i_vd_plan);

		// Check if this customer has used his calling plan before
		if (!$pb_access->hasAccountVolumeDiscountCounter())
		{
			$trace->info("This customer doesn't have a volume discount plan added in PB database, so add it.");
			// If not used before, then add a new record counter in the Account_VD_Counters table
			$pb_access->addAccountVolumeDiscountCounter($period_from, $period_to);
		}
		else
		{
			$trace->info("This customer already has a volume discount plan to the same destination group");
		}
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                         After Successful Payment Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	function acquirePurchasedServices($OrderID = -1, $PayPalTransactionID = "")
	{
		global $dbLink;
		global $trace;
		global $config;

		// Include the member class
		require_once("plan.class.php");
		require_once("order.class.php");
		require_once("pb_account.class.php");
		require_once("pb_did.class.php");
		require_once("did_areacode.class.php");
		require_once("discount_coupon.class.php");
		require_once("package_plan_did.class.php");
		require_once("did.class.php");
		require_once("unaccomplished_orders.class.php");

		$plan = new plan();
		$pb_did = new pb_did();
		$did_areacode = new did_areacode();
		$discount_coupon = new discount_coupon();
		$package_plan_did = new package_plan_did();
		$unaccomplished_orders = new unaccomplished_orders();
		$did = new did();
		
		// Get the order details for the purchased order
		$order = new order($OrderID);
		$dateTimeObj    =   new DateTime();
        $nowTime    =   $dateTimeObj->format('Y-m-d H:i:s'); 
		
		// Setting unaccomplished orderID for any failure that may occur
		$unaccomplished_orders->OrderID = $OrderID;
		
		//*********Changes for New Year, Christmas offers *********/
		$purchaseDate = $order->PurchaseDate;
		$festiveCredit = 0;

		$trace->message_prefix = "[AcquirePurchasedServices][OrderID=$OrderID] ";

		if ( ($order->StatusID == INPROGRESS) || ($order->StatusID == SUSPICIOUS) )
		{
			if (!$this->populateByID($order->UserID)) {
				$trace->error("Not existing customer [UserID=$UserID]");
				// Inserting order into a special table `unaccomplished_orders` that will retry to give the user's
				// his purchased services
				//$unaccomplished_orders->add();
				return false;
			}

			$pb_account = new pb_account($this->i_account);
			// $pb_account->i_account = $this->i_account;
			// $pb_account->id = $this->username;

			$trace->info("Getting all the purchased products for this customer order");
			// Get all products purchased in this order
			$PurchasedCredits = 0; $UserPlanID = 0; $UserPlanToActivate = 0; $i_vd_plan = -1; $PurchasedDID = false;
			$result = $order->getOrderDetails();
			$product_info = array();
			while ($row=mysqli_fetch_object($result))
			{
				$ProductID = $row->ProductID;
				$ProductType = $row->Type;
				$Quantity = $row->Quantity;

				/********************************************************************************
				 * If this product is a monthly calling plan then add it to the member_plan table
				*********************************************************************************/
				if ($ProductType == "MCP")
				{
					$plan->PlanID = $ProductID;
					$ProductName = $plan->getName();
					$i_vd_plan = $plan->getPBVolumeDiscountPlan();
					$i_product = $plan->getProductID($ProductID);
					// changes done on 15 september, 2016 to set end date as per plans (20 or 30days)
					$valid_for = $plan->getValidity($ProductID);

					$trace->info("Adding ".$Quantity." plan(s) (PlanID=" . $ProductID . "," . $ProductName . ") to this user account in member_plan table");
					for($i=1; $i<=$Quantity; $i++)
					{
						$IsProcessed = ($order->IsRecurring) ? 0 : 1;

						// Check if there are any assigned DIDs to this plan
						/*
						 * Retrieve a list of DIDs from the package_plan_did filtered by this PlanID
						* Then loop through the returned result set and assign each DID to this customer
						* Here you should also add the DIDs to member_did table (create a method in this class called addDID)
						* similarly to what has been done above $this->addPlan ....
						*/

						$package_plan_did->PlanID = $ProductID;
						$resultDIDs = $package_plan_did->getPackagePlanDIDs();

						if (mysqli_num_rows($resultDIDs) > 0)
						{
							while($row = mysqli_fetch_object($resultDIDS))
							{
								// Retrieve a new DID to be set as an alias to this account
								$did_areacode->AreaCodeID = $row->AreaCodeID;
								$did_areacode->populateByID();

								$pb_did->area_code = $did_areacode->AreaCode;
								$pb_did->limit = 1;
								$pb_did->getNumberList();

								for($i=0; $i < count($pb_did->number_list); $i++)
								{
									$did_number_info = $pb_did->number_list[$i];
									if (!$pb_account->addAlias($did_number_info->number))
									{
										$trace->error("Failed to add a new alias to this account!", 1);
										// Inserting order into a special table `unaccomplished_orders` that will retry to give the user's
										// his purchased services
										$unaccomplished_orders->add();
										return false;
									}
									else
									{
										$trace->info("DID = $did_number_info->number has been added successfuly to account");
										$this->addDID($did_areacode->AreaCodeID, $did_areacode->Area, $OrderID, $IsProcessed);
									}
								}
							}
							$PurchasedDID = true;
						}

						$plan_package_id = $plan->getPackageID();
						$trace->info("Package Id=".$plan_package_id);
						$ifSamePackagePlanExist = $plan->ifSamePackagePlanExist($order->UserID, $plan_package_id);

						// Please always keep this specific part (which is writing on our local DB) after all SOAP related code part
						// In a way that if the SOAP connection fails, the process will stop and no data woul enter our local DB.
						// Add this plan to the user who purchased this product
						//$UserPlanID  = $this->addPlan($ProductID, $ProductName, $i_vd_plan, $OrderID, $IsProcessed);
						$UserPlanID  = $this->addPlan($ProductID, $ProductName, $i_vd_plan, $OrderID, $IsProcessed,$i_product);

						// add plan details to user's portaone account
						$StartDate = date("Y-m-d H:i:s");
						$EndDate = date('Y-m-d H:i:s', strtotime("+$valid_for days"));//date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m")+1, date("d"), date("Y")));	
					
						/* If product addon already exist list on 05 august 2015 */

						if (count($pb_account->assigned_addons)) {
							// get free plan details
							$free_plan_query = "SELECT * FROM package_plan WHERE i_vd_plan = '".$config['PB']['i_vd_plan']."' ORDER BY PlanID DESC LIMIT 1";

							$trace->info("free plan query=".$free_plan_query);
				
							$free_plan_result = mysqli_query($dbLink, $free_plan_query);
							$free_plan = mysqli_fetch_object($free_plan_result);
							foreach ($pb_account->assigned_addons as $key => $val) {
								/*$product_info[] = array('i_product' => $val->i_product, 'i_vd_plan' => $val->i_vd_plan);*/	
								if ($free_plan->i_product != $val->i_product) {
									$product_info[] = array('i_product' => $val->i_product);
								}
							} 
						}
						/* end of adding product addon */

						/*$product_info[]= array('i_product' => $i_product, 'i_vd_plan' => $i_vd_plan,'addon_effective_from' => date("c", strtotime($StartDate)), 'addon_effective_to' => date("c", strtotime($EndDate)));*/						
						
						if (!$ifSamePackagePlanExist) {
							$trace->info("Same Pacakge Plan not exist.");
							$product_info[]= array('i_product' => $i_product, 'addon_effective_from' => date("c", strtotime($StartDate)), 'addon_effective_to' => date("c", strtotime($EndDate)),'override_product_vd_plan'=>'N');
						} else {
							$trace->info("Same Pacakge Plan already exist.");
						}
						//$product_info[]= array('i_product' => $i_product);

						/*if ($i == 1)
						{
							$UserPlanToActivate = $UserPlanID;
							$this->prepareVolumeDiscountPlan($UserPlanID);
						}*/
					}
				}
				else if ($ProductType == "PAYG")
				{
					$PurchasedCredits = $PurchasedCredits + $row->Amount;
					$validDate = strtotime($purchaseDate);
					$startDate = strtotime($config['FESTIVE_OFFER_START']);
					$expDate = strtotime($config['FESTIVE_OFFER_END']);
					if($validDate >= strtotime('2013-12-24 00:00') && $validDate <= strtotime('2013-12-25 23:59')){
						$festiveCredit = $festiveCredit + $row->Amount * 0.5;
					}
					
					if($validDate >= $startDate && $validDate <= $expDate)
					{
						$festiveCredit = $festiveCredit + $row->Amount * 0.5;
					}
					} else if (($ProductType == "DID") || ($ProductType == "DID_RESTORE")){
				
				$trace->info("Start DID Process after Payment confirmation");
				
				$resultuser=$this->getMemberUserInfo($order->UserID);
				$rowuser=mysqli_fetch_object($resultuser);
				
				$diduserEmail = $rowuser->Email;	
				$didusername = ucwords($rowuser->CustomerName);
				$diduseriaccount = $rowuser->i_account;
					
				$did->updateOrderIdInMemberDetails($OrderID);
				
				$trace->info("Update OrderOId $OrderID  In DID Membor Table");
				
				$result=$did->selectdidMemberDetails($OrderID);
				$count_did= mysqli_num_rows($result);
				if ($count_did>0)
				{
				while ($row=mysqli_fetch_object($result))
				{
					$mu_did_doc_id = $row->mu_did_doc_id;
					$mu_did_details_req_id = $row->mu_did_details_req_id;
					$did_mu_prefered_country_iso = $row->did_mu_prefered_country_iso;
					$did_mu_prefered_country_prefix = $row->did_mu_prefered_country_prefix;
					$did_mu_prefered_city_id = $row->did_mu_prefered_city_id;
					$did_mu_prefered_city_prefix = $row->did_mu_prefered_city_prefix;
					$did_mu_prefered_city_name = $row->did_mu_prefered_city_name;
					$did_mu_prefered_i_subscription_period = $row->did_mu_prefered_i_subscription_period;
					$did_mu_prefered_i_subscription = $row->did_mu_prefered_i_subscription;
					$did_parent_mu_did_details_req_id = $row->parent_mu_did_details_req_id;
					
					$resultdata=$did->getSubscriptiondate($did_mu_prefered_i_subscription);
					$rowdata=mysqli_fetch_object($resultdata);
					$invoice_description =$rowdata->invoice_description;
					$fee=$rowdata->selling_price;
					if ($mu_did_doc_id=='0'){
							$processflag="";	
						$trace->info("Start DID process  OrderOId $OrderID");
						
						if 	($ProductType == "DID"){	
						$didResult=$did->generateDid($diduseriaccount,$did_mu_prefered_country_iso,$did_mu_prefered_city_prefix,$did_mu_prefered_i_subscription_period,$did_mu_prefered_city_id);

						$didNumber = $didResult->did_number;
						$customer_billed_to =$didResult->did_expire_date_gmt;
						} else {
							
							$resultprerestore=$did->getdiddetailswithparent($did_parent_mu_did_details_req_id);
							$rowprerestore=mysqli_fetch_object($resultprerestore);
							$i_did_number_billing=$rowprerestore->i_did_number_billing;
							$iaccountrenew=$rowprerestore->i_account;
							$did_mu_prefered_i_subscription=$rowprerestore->i_subscription_id;
							$i_did_number=$rowprerestore->i_did_number;
							
							$resultchkservice = $did->didServiceDetail($diduseriaccount,$rowprerestore->order_id);
							if ($resultchkservice->did_number==""){
							
							//if ($rowprerestore->did_expire_date_gmt<=$nowTime){
								$did_restore=$did->restoreDid($diduseriaccount,$rowprerestore->did_number, $rowprerestore->did_period);
								if ($did_restore->error==0){
									$resultservice = $did->didServiceDetail($diduseriaccount,$rowprerestore->order_id);
									$didResult->result=$resultservice->result;
									$didResult->country_name=$resultservice->country_name;
									$didResult->country_iso=$resultservice->country_iso;
									$didResult->city_name=$resultservice->city_name;
									$didResult->city_prefix=$resultservice->city_prefix;
									$didResult->city_id=$rowprerestore->city_id;
									$didResult->did_number=$resultservice->did_number;
									$didResult->did_status=$resultservice->did_status;
									$didResult->did_timeleft=$resultservice->did_timeleft;
									$didResult->did_expire_date_gmt=$resultservice->did_expire_date_gmt;
									$didResult->order_id=$resultservice->order_id;
									$didResult->city_prefix=$resultservice->order_status;
									$didResult->did_mapping_format=$resultservice->did_mapping_format;
									$didResult->did_setup=$resultservice->did_setup;
									$didResult->did_monthly=$resultservice->did_monthly;
									$didResult->did_period=$resultservice->did_period;
									$didResult->prepaid_balance=$resultservice->prepaid_balance;
									$didResult->autorenew_enable=$rowprerestore->autorenew_enable;
								}	 else{
									$did->unaccomplishprocess($OrderID,$didNumber,'7');
									$trace->info("DID Not Restore for OrderOId $OrderID and DID Number $rowprerestore->did_number",1);	
								}		
								
							} else {
								$did_renewresult=$did->ReneworderAuto($diduseriaccount,$rowprerestore->did_number,$rowprerestore->did_period);
								$pb_account1 = new pb_account($iaccountrenew);
								if ($pb_account1->id){
									$processflag="new";
								} else {
									$processflag="";
								}	
								if ($did_renewresult){
									//$processflag="new";
									$addi_account=$iaccountrenew;
									$didNumber = $did_renewresult->did_number;
									$customer_billed_to =$did_renewresult->did_expire_date_gmt;
									$didResult =$did_renewresult;
									$didResult->country_iso=$rowprerestore->country_iso;
									$didResult->city_prefix=$rowprerestore->city_prefix;
									$didResult->city_id=$rowprerestore->city_id;
									$didResult->autorenew_enable=$rowprerestore->autorenew_enable;
								} else{
									$did->unaccomplishprocess($OrderID,$didNumber,'6');
									$trace->info("DID Not Renew for OrderOId $OrderID and DID Number $rowprerestore->did_number",1);	
								}	
							}
							
						}	
						$trace->info("Generate DID $didNumber for OrderOId $OrderID and DID Detail ID $mu_did_details_req_id");							
									
						$process = false;
						
						if (!empty($didNumber)){
							if ($processflag==""){
							$Blocked = "Y";
							$addAliasInPorta=$pb_account->addDidAlias($didNumber,$diduseriaccount,$Blocked);
							if ($addAliasInPorta->i_account) {
								$process = true;
								$trace->info("pb_account Alias Add User ID $addAliasInPorta->i_account");
							} else {
								$process = false;
								$did->unaccomplishprocess($OrderID,$didNumber,'1');
								$trace->info("Error in add did Alias. Data added in unaccomplish table",1);
								
							}
							
							$i_did_number=$pb_account->addDidNumberp1($didNumber,$invoice_description,$fee,$did_mu_prefered_country_iso,$did_mu_prefered_city_name);
							if ($i_did_number) {
								$process = true;
								$trace->info("pb_account added data in DID Number table with ID  $i_did_number");
							} else {
								$process = false;
								$did->unaccomplishprocess($OrderID,$didNumber,'2');
								$trace->info("Error in add data in DID Number table.Data added in unaccomplish table",1);
							}
														
							$i_did_number_billing=$pb_account->addDidNumberBillingp1($i_did_number,$customer_billed_to);
							if ($i_did_number_billing) {
								$process = true;
								$trace->info("pb_account added data in DID Number Billing table with ID  $i_did_number_billing");
							} else {
								$process = false;
								$did->unaccomplishprocess($OrderID,$didNumber,'3');
								$trace->info("Error in add data in DID Number Billing table.Data added in unaccomplish table",1);
							}
							} else {
								
								if($pb_account->UpdateDidNumberBillingp1($i_did_number_billing,$customer_billed_to)){
								
									$process = true;
									$trace->info("pb_account updated data in DID Number Billing table with ID  $i_did_number_billing");
								} else {
									$process = false;
									$did->unaccomplishprocess($OrderID,$didNumber,'7');
									$trace->info("Error in add data in DID Number Billing table.Data added in unaccomplish table",1);
								}
							}	
							
							
							$datadid = array();
							if ($processflag==""){
							$datadid['iaccount']=$addAliasInPorta->i_account;
							} else {
								$datadid['iaccount']=$addi_account;
							}	
							$datadid['imaster_account']=$diduseriaccount;
							$datadid['isubscription_id']=  $did_mu_prefered_i_subscription;
							$datadid['mu_did_details_req_id'] =   $mu_did_details_req_id;
							$datadid['i_did_number']   =   $i_did_number;
							$datadid['i_did_number_billing'] =   $i_did_number_billing;
							$datadid['customer_billed_to_date'] =   $customer_billed_to;
							
							$resultid=$didResult=$did->InsetDIdDetails($datadid,$didResult);
							if ($resultid) {
								$process = true;
								$trace->info("added DID data in Final Table with resultid $resultid");
							} else {
								$process = false;
								$did->unaccomplishprocess($OrderID,$didNumber,'4');
								$trace->info("Error in add data in final Table in SFTC.Data added in unaccomplish table",1);
							}
							
							$did->updateflagafterDIdDetails($mu_did_details_req_id);
							$trace->info("Updated Flag after add final DID data");
							if ($ProductType == "DID_RESTORE"){
								$did->restoredOLdDID($did_parent_mu_did_details_req_id);
								$trace->info("Closed Old plan for $did_parent_mu_did_details_req_id");
							}	
							if ($process){
								$from = $config['EMAIL']['DID']['SLS']; 
								$to	= $diduserEmail;	
								$name = $didusername;
												
								$this->sendEmailToUserCustomer($from, $to, $name, $order->UserID, $did_mu_prefered_country_iso, $diduseriaccount, $didNumber,$did_mu_prefered_city_name);
								//$did->unaccomplishprocess($OrderID,$didNumber,'5');
								$trace->info("No notification sent to customer for DID Number $didNumber and  OrderOId $OrderID.",1);
							} else {
								
								$trace->info("DID Process unaccomplishprocess for DID Number $didNumber and  OrderOId $OrderID. Please Troubleshoot",1);
							}			
						} else {
							$did->unaccomplishprocess($OrderID,$didNumber,'0');
							$trace->info("DID Not Generated for OrderOId $OrderID and DID Detail ID $mu_did_details_req_id",1);							
						}	
					} else {
						$from=$config['EMAIL']['DID']['SLS']; 
						$to=$config['EMAIL_TO_SUPPORT'];
						$name = $didusername;
						$this->sendEmailToSLS($from, $to, $name,  $order->UserID, $did_mu_prefered_country_iso, $this->i_account, $OrderID, $did_mu_prefered_i_subscription,$did_mu_prefered_city_name);
					}
			 }
			} else {
				$trace->error("Error in selecting DID DATA for OrderOId $OrderID",1);	
			}		
				}
			}

			if (count($product_info)) {
				$addon = $pb_account->updateAddonList($this->i_account, $product_info);
				if ($addon) {
					$trace->info("The plan has been added successfully to user's portaone account. Account ID = " . $this->i_account);
				}
			}
			/****************************************************************************************************************
			 * If there are many calling plans purchased in this order, and there is no current active plans for this customer
			* Because there must be only one activate plan, therefore we should activate one of those purchased plans
			* Here in this workflow we took the first purchased calling plan (but sure the user can switch to his desired plan)
			*****************************************************************************************************************/
			/*if ($UserPlanToActivate !=0)
			{
				if (!$this->hasActivePlan())
				{
					$trace->info("Activating " . $ProductName . " plan (UserPlanID = " . $UserPlanToActivate . ") for this customer");
					$this->activatePlan($UserPlanToActivate);

					// Update account in porta_billing, and check if it has been updated successfully
					$trace->info("Update i_vd_plan = " . $i_vd_plan . " in PortaBilling Accounts table");

					$pb_account_info->i_account = $this->i_account;
					$pb_account_info->i_vd_plan = $i_vd_plan;
					$pb_account_info->i_product = $config['PB']['I_MCP_PRODUCT'];
					$pb_account_info->i_routing_plan = $config['PB']['I_MCP_ROUTING_PLAN'];

					if (!$pb_account->update($pb_account_info))
					{
						$trace->error("Failed to execute PB update method!");
					}
				}
			}
			*/
			
			// If the customer has purchased credits than add it to his balance in PortaBilling Accounts table
			if ($PurchasedCredits !=0)
			{
				$trace->info("Adding purchased credits ($" . $PurchasedCredits . ") to this customer in the PortaBilling Accounts table");
				$options['OrderID'] = $OrderID;
				$options['purchaseDate'] = $purchaseDate; 
				if (!$pb_account->update_balance($PurchasedCredits, "Purchase", $options))
				{
					$trace->error("Failed to execute PB update_balance method!");
					// Inserting order into a special table `unaccomplished_orders` that will retry to give the user's
					// his purchased services
					$unaccomplished_orders->add();
					return false;
				}
				
				if($festiveCredit > 0)
				{
					// Preparing Item array that contains all the order details info
					$Item 						= array();
					$Item['ID'] 			= "207"; // Reseller Additional Credit Product
					$Item['Quantity'] = 1;
					$Item['Price']    = $festiveCredit;
					$Item['SubTotal'] = $festiveCredit;
					$Item['Type']     = "PAYGP";
					
					// Adding a single order details in 'order_details' table
					$order->addNewItem($Item, $OrderID);
					if (!$pb_account->update_balance($festiveCredit, "FestivalAdditionalCredit"))
					{
						$trace->error("Failed to add additional credits ($festiveCredit USD) to this User!", 1);
					}
					else
					{
						$trace->info("Festival Promotion: Additional credits ($festiveCredit USD) has been added to $this->username!", 1);
					}
					// Update this order by setting the AdditionalCredits value
					$order->AdditionalCredits = $order->AdditionalCredits + $festiveCredit;
					$order->updateAdditionalCredits();
				}

				if (($this->isReseller) && ($pb_account->i_product != $config['PB']['MASTER_I_PRODUCT']))
				{
					// Special case for the reseller danybousaad (If order amount is greater than $500 then give him a discount value of %10)
					if (($this->username == "danybousaad") && ($PurchasedCredits >= 500))
					{
						$DiscountValue = 10;
					}
					else
					{
						// Get discount percentage value from reseller_discount_level table
						$DiscountValue = $this->getDiscountValue($PurchasedCredits);
					}

					if ($DiscountValue>0)
					{
						$AdditionalCredits = ($DiscountValue * $PurchasedCredits)/100;

						// Preparing Item array that contains all the order details info
						$Item 						= array();
						$Item['ID'] 			= "202"; // Reseller Additional Credit Product
						$Item['Quantity'] = 1;
						$Item['Price']    = $AdditionalCredits;
						$Item['SubTotal'] = $AdditionalCredits;
						$Item['Type']     = "PAYGP";

						// Adding a single order details in 'order_details' table
						$order->addNewItem($Item, $OrderID);

						if (!$pb_account->update_balance($AdditionalCredits, "ResellerAdditionalCredit"))
						{
							$trace->error("Failed to add additional credits ($AdditionalCredits USD) to this Reseller!", 1);
						}
						else
						{
							$trace->info("Reseller Promotion: Additional credits ($AdditionalCredits USD) has been added to $this->username!", 1);
						}

						// Update this order by setting the AdditionalCredits value
						$order->AdditionalCredits = $order->AdditionalCredits + $AdditionalCredits;
						$order->updateAdditionalCredits();
					}
				}
			}

			// If the customer has purchased DID(s) then update his account to set (UM Enabled = yes, Hide CLI = no)
			if ($PurchasedDID)
			{
				// Update account in porta_billing, and check if it has been updated successfully
				$trace->info("Update um_enabled = yes in PortaBilling Accounts table");

				$pb_account_info->i_account = $this->i_account;
				$pb_account_info->um_enabled = 'Y';

				if (!$pb_account->update($pb_account_info))
				{
					$trace->error("Failed to enable UM feature for $pb_account_info->id, please enable it manually in PortaBilling Administration Portal", 1);
				}
				else
				{
					$trace->info("UM feature for $pb_account_info->id has been enabled successfuly");
				}
			}

			try
			{
				// Discount Coupon Codes Management --------------------------------------------------------
				// Check if this Order has a discount coupon code
				if ($discount_coupon->ID = $order->hasDiscountCoupon())
				{
					$discount_coupon->populateByID();
					$trace->info("This customer used the following discount coupon code = $discount_coupon->Code");

					// Check if this discount coupon code is attached to a specific product
					if ($arr = $discount_coupon->isForSpecificProduct())
					{
						// Loop through returned result set
						$ProductAmount = 0;
						while ($row=mysqli_fetch_object($arr))
						{
							// Get the Total Amount of this specific purchased product to apply the discount coupon on it
							$ProductAmount += $order->getProductAmount($row->ProductID);
						}
						$AdditionalCredits = ($ProductAmount * $discount_coupon->Discount)/100;
					}
					else
					{
						// $AdditionalCredits = ($PurchasedCredits * $discount_coupon->Discount)/100;
						$AdditionalCredits = ($order->OrderAmount * $discount_coupon->Discount)/100;
					}
					$trace->info("Additional Credits = $AdditionalCredits");

					// Preparing Item array that contains all the order details info
					$Item 						= array();
					$Item['ID'] 			= "201"; // Pay As you Go Promotion Product
					$Item['Quantity'] = 1;
					$Item['Price']    = $AdditionalCredits;
					$Item['SubTotal'] = $AdditionalCredits;
					$Item['Type']     = "PAYGP";

					// Adding a single order details in 'order_details' table
					$order->addNewItem($Item, $OrderID);
					$trace->info("Added new PAYGP item");

					// Give this additional credit to this customer
					$options["CouponCode"] = $discount_coupon->Code;
					$AdditionalCredits = number_format($AdditionalCredits, 2, '.', '');
					if (!$pb_account->update_balance($AdditionalCredits, "DiscountCoupon", $options))
					{
						$trace->error("[DiscountCouponCode=$discount_coupon->Code][DiscountValue=$discount_coupon->Discount]: Failed to add additional credits ($AdditionalCredits USD) to this $this->username!", 1);
					}
					else
					{
						$trace->info("[DiscountCouponCode=$discount_coupon->Code][DiscountValue=$discount_coupon->Discount]: Additional credits ($AdditionalCredits USD) has been added to $this->username!", 1);
					}

					// Update this order by setting the AdditionalCredits value
					$order->AdditionalCredits = $order->AdditionalCredits + $AdditionalCredits;
					$order->updateAdditionalCredits();
				}
				// End Discount Coupon Codes Management ----------------------------------------------------

				// Referral Rewards Management -------------------------------------------------------------
				// Check if this customer is already referred by someone and allowed to get rewards, and hence provide him and his referrer with the additional credit assigned to this referral (if any)
				if (($this->isRewardsAllowed) && (!empty($this->referralID)))
				{
					$this->executeReferralRewards($OrderID);
				}
				// End Referral Rewards Management ---------------------------------------------------------

				// Check if this is a reseller and RetroactiveNextDate is empty, then set it
				if (($this->isReseller) && (empty($this->retroactiveNextDate)))
				{
					$trace->info("Retroactive next date has been updated");
					$this->updateRetroactiveNextDate();
				}

				// If payment done through PayPal then update the paypal_transactions table by setting the corresponding transaction to processed
				if (!empty($PayPalTransactionID))
				{
					// Set this transaction as processed
					$trace->info("Set this transaction=".$PayPalTransactionID." as Processed");
					$this->setAsProcessed($PayPalTransactionID);
				}
			}
			catch(Exception $e)
			{
				$trace->error('Caught Exception' . $e->getMessage(),1);
				return false;
			}
		}
		else
		{
			$trace->warn("[StatusID=".$order->StatusID."] Trying to acquire the services more than once.");
			return false;
		}

		return true;
	}

	// Set this transaction as processed
	function setAsProcessed($txn_id)
	{
		global $dbLink;

		$query = "Update paypal_transactions Set Processed = 1 Where txn_id = '$txn_id'";
		mysqli_query($dbLink, $query);
	}

	// This function is used to get the discount value of a certain amount
	function getDiscountValue($OrderAmount)
	{
		global $dbLink;
		global $trace;

		$query = "SELECT Discount DiscountValue FROM reseller_discount_level WHERE $OrderAmount BETWEEN CreditFrom AND CreditTo";
		$result = mysqli_query($dbLink, $query);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_object($result);
			return $row->DiscountValue;
		}
		return 0;
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                         Reseller Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// Checks if there are any accounts assigned to this reseller
	function hasAccounts()
	{
		global $dbLink;

		$query = "SELECT COUNT(*) FROM member_user WHERE mu_ParentID = $this->i_account";
		//$count = mysqli_result(mysqli_query($dbLink, $query), 0, 0);
		//return $count;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

	// This function is used to get a list of all reseller's accounts
	function getAccountsList()
	{
		global $dbLink;

		$query  =
		"
		Select mu_ID UserID, mu_Username Username, mu_Password Password, mu_Enabled IsEnabled, mu_Creation_Date JoinDate, i_account
		From member_user
		Where mu_ParentID = $this->i_account
		Order By JoinDate DESC, Username ASC
		";
		// echo $query;
		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                    Referral Program Related Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// Retrieves the total number of times the referred customer earned credit from a specific referral
	function getReferredEarnedCount()
	{
		global $dbLink;

		$query = "SELECT COUNT(*) Total FROM member_reward Where UserID = $this->id And ReferralID = $this->referralID";
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		return $row->Total;
	}

	// Retrieves the total number of times the referrer customer earned credit on a specific referral from a specific referred party
	function getReferrerEarnedCount()
	{
		global $dbLink;

		$query = "SELECT COUNT(*) Total FROM member_reward Where UserID = $this->referrerID And ReferralID = $this->referralID And ReferredID = $this->id";
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		return $row->Total;
	}

	// Inserts details about the reward earned by this customer
	function addRewardDetails($rewardDetails)
	{
		global $dbLink;

		$UserID = ($rewardDetails['TypeID'] == REFERRER_COMMISSION) ? $this->referrerID : $this->id;
		$ReferralID = ($rewardDetails['TypeID'] == RETROACTIVE_COMMISSION) ? 'NULL' : ((empty($this->referralID)) ? 'NULL' : $this->referralID);
		$EarnedDate = date("Y-m-d");

		$query =
		"
		Insert Into member_reward (UserID, EarnedCredit, OrderID, ReferralID, ReferredID, TypeID, EarnedDate)
		Values($UserID, " . $rewardDetails['EarnedCredit'] . ", "
				. ((empty($rewardDetails['OrderID'])) ? 'NULL' : $rewardDetails['OrderID']) . ", " . ($ReferralID) . ", "
						. ((empty($rewardDetails['ReferredID'])) ? 'NULL' : $rewardDetails['ReferredID']) . ", "
								. $rewardDetails['TypeID'] . ", '$EarnedDate')
								";
		mysqli_query($dbLink, $query);
	}

	// This function is used to provide referrer and reffered customers their additional credit that they are entitled for due to a certain referral
	function executeReferralRewards($OrderID)
	{
		global $dbLink;
		global $trace;
		global $config;

		// Include required classes
		require_once("order.class.php");
		require_once("pb_account.class.php");
		require_once("referral_offer.class.php");
		require_once("member_referral.class.php");

		$trace->message_prefix = trim($trace->message_prefix) . "[REFERRAL_REWARDS] ";

		try
		{
			// Get the order details for this purchased order
			$order = new order($OrderID);

			// Get the details of the referral assigned to this referred customer
			$member_referral = new member_referral($this->referralID);
			// The below is needed in case the referral is WOM (ReferralID = 1), where in this case ReferrerID = 0, so we must set the correct ReferrerID
			if (($member_referral->ID == RT_WOM) || (empty($member_referral->ReferrerID)))
			{
				$member_referral->ReferrerID = $this->referrerID;
			}

			// Get the referral offer's details
			$referral_offer = new referral_offer($member_referral->OfferID);

			// Check if this referred customer deserves any rewards --------------------------------------
			$trace->info("Check if this referred customer deserves any rewards");
			if (!empty($referral_offer->ReferredRewardID))
			{
				// Check if this referred customer is entitled to any more additional credit
				$trace->info("Check if this referred customer is entitled to any more additional credit");
				if ((empty($referral_offer->ReferredRewardMaxEarn)) || ($this->getReferredEarnedCount() < $referral_offer->ReferredRewardMaxEarn))
				{
					// Provide the deserved additional credit to this referred customer
					if ($referral_offer->ReferredRewardTypeID == REWARDTYPE_PERCENTAGE)
					{
						$AdditionalCredits = ($order->OrderAmount * $referral_offer->ReferredRewardValue)/100;
					}
					else
					{
						$AdditionalCredits = $referral_offer->ReferredRewardValue;
					}
					$trace->info("Provide the deserved referral additional credit ($AdditionalCredits USD) to this referred customer");

					// Preparing Item array that contains all the order details info
					$Item 						= array();
					$Item['ID'] 			= PRODUCT_REFERRAL_ADDITIONAL_CREDIT;
					$Item['Quantity'] = 1;
					$Item['Price']    = $AdditionalCredits;
					$Item['SubTotal'] = $AdditionalCredits;
					$Item['Type']     = "PAYGP";

					// Adding a single order details in 'order_details' table
					$order->addNewItem($Item, $OrderID);

					// Update the balance of the referred customer
					$pb_account = new pb_account($this->i_account);
					if (!$pb_account->update_balance($AdditionalCredits, "ReferralAdditionalCredit"))
					{
						$trace->error("Failed to add referral additional credits ($AdditionalCredits USD) to $this->username!", 1);
					}
					else
					{
						$trace->info("Referred Customer Promotion: Additional credits ($AdditionalCredits USD) has been added to $this->username!", 1);
					}

					// Update this order by setting the AdditionalCredits value
					$order->AdditionalCredits = $order->AdditionalCredits + $AdditionalCredits;
					$order->updateAdditionalCredits();

					// Insert referral earned credit into member_reward table
					$rewardDetails = array();
					$rewardDetails['EarnedCredit'] = $AdditionalCredits;
					$rewardDetails['OrderID'] 		 = $OrderID;
					$rewardDetails['TypeID'] 			 = REFERRED_COMMISSION;

					$trace->info("Insert referred customer earned credit into DB");
					$this->addRewardDetails($rewardDetails);
				}
			}

			// Check if the referrer customer deserves any rewards ---------------------------------------
			$trace->info("Check if the referrer customer deserves any rewards");
			if (!empty($referral_offer->ReferrerRewardID))
			{
				// Check if the referrer customer is entitled to any more BONUS Credits
				$trace->info("Check if the referrer customer is entitled to any more BONUS Credits");
				if ((empty($referral_offer->ReferrerRewardMaxEarn)) || ($this->getReferrerEarnedCount() < $referral_offer->ReferrerRewardMaxEarn))
				{
					// Retrieve referrer information
					$referrer = new user();
					$referrer->populateByID($this->referrerID);

					// Provide the deserved additional credit to the referrer customer
					if ($referral_offer->ReferrerRewardTypeID == REWARDTYPE_PERCENTAGE)
					{
						$BonusCredits = ($order->OrderAmount * $referral_offer->ReferrerRewardValue)/100;
					}
					else
					{
						$BonusCredits = $referral_offer->ReferrerRewardValue;
					}
					$trace->info("Provide the deserved referral BONUS Credits ($BonusCredits USD) to the referrer $referrer->username");

					// Update the balance of the referrer customer
					$pb_account = new pb_account($referrer->i_account);
					if (!$pb_account->update_balance($BonusCredits, "ReferralBonusCredit"))
					{
						$trace->error("Failed to add referral BONUS Credits ($BonusCredits USD) to the referrer $referrer->username!", 1);
						return false;
					}
					else
					{
						$trace->info("Referrer Customer Promotion: BONUS Credits ($BonusCredits USD) has been added to the referrer $referrer->username!", 1);
					}

					// Insert referral earned credit into member_reward table
					$rewardDetails = array();
					$rewardDetails['EarnedCredit'] = $BonusCredits;
					$rewardDetails['OrderID'] 		 = $OrderID;
					$rewardDetails['ReferredID'] 	 = $this->id;
					$rewardDetails['TypeID'] 			 = REFERRER_COMMISSION;

					$trace->info("Insert referrer customer earned BONUS Credits into DB");
					$this->addRewardDetails($rewardDetails);

					// Add a new earn activity for this referral
					$member_referral->addEarnActivity($this->id, $BonusCredits);

					// Increase the total number of times the referrer earned a reward from this referral
					$member_referral->increaseEarned();

					// Update the total earned amount of this referral
					$member_referral->updateTotalEarned($BonusCredits);

					// Inform referrer (by email) about the added bonus credits
					$this->informReferrerAboutRewards($this->referrerID, $BonusCredits);
				}
			}
			return true;
		}
		catch(Exception $e)
		{
			$trace->error('Caught Exception' . $e->getMessage(),1);
			return false;
		}
	}

	// This function is used to send an email to the referrer informing him about the bonus credits added to his balance
	function informReferrerAboutRewards($ReferrerID, $BonusCredits)
	{
		global $trace;
		global $config;

		// Retrieve referrer information
		$referrer = new user();
		$referrer->populateByID($ReferrerID);

		// Prepare headers for sending html mails
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=windows-1256\r\n";
		$headers .= "From: " . $config['EMAIL_FROM'] . " \r\n";
		$headers .= "BCC: " . $config['EMAIL_DEVELOPMENT'] . " \r\n";

		$ReferrerName = ucwords($referrer->fName . " " . $referrer->lName);
		$subject = "SoftCall - Referral Earned BONUS Credits (Total = $".$BonusCredits.")";
		$to = $referrer->email;

		$message =
		'
				<html>
				<head>
				<link href="https://www.softcall.me/Styles/old/style.css" rel="stylesheet" type="text/css">
				</head>
				<body leftmargin="0" topmargin="0" class="en">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td class="Success">
				Dear <span class="sTitle">'.$ReferrerName.' ('.$referrer->username.')</span>,<br/><br/>

						This is to inform you that we have added <u>$'.$BonusCredits.' BONUS Credits</u> to your balance as a reward on one of your referrals.<br/><br/>

								Keep sharing SoftCall with your friends, and earn more BONUS Credits!<br/><br/>

								There are no limits on the number of referrals you can make!<br/>
								There are no limits on how much you can earn!<br/><br/>

								You can access your <a href="'.$config['WEB_URL'].'/Account/refer_friend.php">Referrals Dashboard</a> to know more information about all your referrals stats.<br/><br/>

										If you encounter any problem or require further information, do not hesitate to contact our support team at <a href="mailto:support@softcall.me">support@softcall.me</a>
										or go to <a href="'.$config['WEB_URL'].'/Home/support.php">SoftCall Support</a> and ask for help.<br/><br/>

												Sincerely yours,<br/>
												SoftCall Sales Department
												</td>
												</tr>
												</table>
												</body>
												</html>
												';

		// Send Message
		$message = stripslashes(trim($message));
		if(@preg_match("/win/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $headers);
		}
		elseif(@preg_match("/linux/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $config['EMAIL_DEVELOPMENT']);
			//mail($to, $subject, $message, $headers);
		}

		$trace->info("Referral Earned BONUS Credits announcement has been successfully sent to the Referrer's email!");
	}

	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                    Registration Workflow Related Functions
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
	// Checks if this user is verified or not
	function isVerified()
	{
		global $dbLink;

		$query = "SELECT SUM(Verified) Verified FROM member_confirmation WHERE Username LIKE '$this->username'";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row->Verified;
		}
		return false;
	}

	// Retrieves the Confirmation/PIN Code for a specific customer on a specific verification type
	function getConfirmationCode($type)
	{
		global $dbLink;

		$query =
		"
		SELECT ConfirmationCode
		FROM member_confirmation
		WHERE Username = '$this->username' And TypeID = '$type'
		";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
			return $row->ConfirmationCode;
		}
		return false;
	}

	// Retrieves the Verification Info for a specific customer
	function getVerificationInfo($verified = false)
	{
		global $dbLink;

		$query =
		"
		SELECT mc.ConfirmationCode VerificationCode, mc.TypeID, mct.Name VerificationType, mc.VerificationDate
		FROM member_confirmation mc
		INNER JOIN member_confirmation_type mct ON mc.TypeID = mct.ID
		WHERE Username = '$this->username'
		";
		if ($verified)
		{
			$query .= "AND Verified = 1";
		}
		else
		{
			$query .= "ORDER BY mc.ID DESC LIMIT 1";
		}
		$result = mysqli_query($dbLink, $query);
		return $result;
	}
		function getProductType()
		{
		global $dbLink;
		$query = "SELECT od.`Type` productlist FROM order_details od WHERE od.`Type`!='' GROUP BY od.`Type`";
		$result = mysqli_query($dbLink, $query);
		return $result;
		
		}
		
	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	 *                   Functions used in DID Pages
	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
                
   function getDIDPlansTotalNumber($userId)
	{
		global $dbLink;

		//$query  ="Select count(*) as TotalDIDPlans FROM `did_member_subscription` DMS Where DMS.mu_ID = " . $this->id . " ";
	//	echo $query="SELECT COUNT(*) as TotalDIDPlans FROM `Accounts` acc,`DID_Numbers` dn WHERE acc.`id`= dn.`number` AND acc.`i_master_account`='$userId'";
	
		$query="SELECT COUNT(*) AS TotalDIDPlans FROM did_member_api_result_object WHERE `did_member_api_result_object`.`mu_did_details_req_id` IN
('SELECT `mu_did_details_req_id` FROM did_member_details WHERE mu_ID = ".$userId."')";
		$result = mysqli_query($dbLink, $query) or die("Query failed getDIDPlansTotalNumber: " .mysqli_error()); 
		$row = mysqli_fetch_object($result);
		return $row->TotalDIDPlans;
	}
	
function displayDIDUserdocs($userId,$i_subcriptionPlan,$Order_id,$mu_did_details_req_id)
{
	global $dbLink;
	//$query = "SELECT * FROM did_member_subscription_attachment WHERE mu_ID = '$userId' "; 
	 $query="SELECT * FROM `did_member_details` amd, `did_member_subscription_attachment` dmsa
	WHERE amd.`mu_did_doc_id`=dmsa.`mu_did_doc_id` AND dmsa.`mu_ID`=$userId AND amd.`did_mu_prefered_i_subscription`='$i_subcriptionPlan' AND dmsa.`is_valid`=0   AND amd.`order_id`='$Order_id' AND dmsa.`flag`=1 AND  amd.`mu_did_details_req_id`='$mu_did_details_req_id'";
	$result = mysqli_query($dbLink, $query) or die("Query failed displayDIDUserdocs: " .mysqli_error()); 
	
    return $result;
}

function downloaddiddocs($filename)
{
$updir = 'https://www.softcall.me/Testing/did_docs/'; // change the path to fit your websites document structure
	if($_GET['download_file'])
	{
	 $fullPath = $updir.$_GET['download_file'];
	if ($fd = fopen ($fullPath, "r")) {
    $fsize = filesize($fullPath);
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]);
    switch ($ext) {
        case "pdf":
        header("Content-type: application/pdf"); // add here more headers for diff. extensions
        header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a download
        break;
		 case "png":
        header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'.$path_parts["basename"].'"');
	header('Content-Length: ' . filesize($file));
	readfile($file);
        break;
        default;
        header("Content-type: application/octet-stream");
        header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
    }
    header("Content-length: $fsize");
    header("Cache-control: private"); //use this to open files directly
    while(!feof($fd)) {
        $buffer = fread($fd, 200000);
        echo $buffer;
    }
}
}


}

// Email function to send the email from SLS to Did User

	function informDIDUserbyemail($from,$To,$from_subject,$userID,$email_messages,$did_number)
	{
	

		global $trace;
		global $config;

		// Retrieve referrer information
		$referrer = new user();
		$referrer->populateByID($userID);

		// Prepare headers for sending html mails
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=windows-1256\r\n";
		$headers .= "From: " . $from . " \r\n";
		$headers .= "BCC: " . $config['EMAIL_DEVELOPMENT'] . " \r\n";
		$ReferrerName = ucwords($referrer->fName . " " . $referrer->lName);
		$ste_url_link="http://".$config['HOST'];
		$subject = "SoftCall-".ucwords($from_subject);
		$to = $To;
		$message =
		'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SoftCall - $subject </title>
<style type="text/css">
table td {
	border-collapse: collapse;
	font-family: Verdana;
}
.active font{
	color: #FF0000;
}
</style>
</head>
<body style="margin:0; padding:0; background:#e8e8e8;">
<table width="100%" bgcolor="#e8e8e8" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!--| Main table -->
    <table width="546" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#e8e8e8">
        <tr>
        <!-- start top empty row -->
          <td height="30" align="center" valign="middle">&nbsp;</td>
        <!-- end top empry row -->
		</tr>
        <!-- Start main row -->
          <td align="center" valign="middle" style="border:1px solid #53c2eb;">
          	<table border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" width="100%">
            <tr>
            	<!-- start logo row -->
            	<td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;"><a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank"><font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Email_temp/logo.gif" alt="SoftCall" /></font></td> 
                <!-- end logo row -->
			 </tr>
			<tr>
			  	<!-- start logo row -->
            	<td align="left" valign="middle" height="36" style="padding-left:17px;"><font color="#6d6d6d" face="Verdana" style="font-size:12px;">
						Dear <span class="sTitle">'.$ReferrerName.'</span>,</td>
						</tr>
						<tr>
                  	<td  align="left" valign="middle" height="100" style="padding-left:17px; padding-bottom:10px; padding-top:10px;">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr><td valign="middle" align="left" style="padding-right:10px;"> 
				<font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
		'.$email_messages.'</br>
				</font>
			
        <font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
			
		<br> Thank you for using SoftCall. </br>
		<br>&nbsp;&nbsp;&nbsp; </br>
If you face any issue or need help, please contact our customer support by sending an e-mail to <a href="mailto:support@softcall.me">support@softcall.me</a> or by using our <a href="'.$ste_url_link.'/Home/index.php?emailLiveLink=true">live-help chat</a>.
            </td>
                          
<td valign="middle" align="right"><a target="_blank" title="Live help" href="http://tr.im/4q545"><img width="48" alt="Live Help" src="'.$ste_url_link.'/Engines/mailTemplate/sftc_images/live-help.gif"></a></td>
</tr></tbody></table> </td></tr><!-- end second row -->
            </tr>
             <tr>
            	<!-- start second row -->
            	<td  align="left" valign="middle" height="50" style="padding-left:17px; padding-bottom:10px;"><font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:24px;">Best Regards,<br />
<b>SoftCall Support <br></b></font><br/>
<img src="'.$ste_url_link.'/Email_temp/logo_signature.gif" alt="SoftCall" />
</td> 
                <!-- end second row -->
				
            </tr>
			
            <tr>
             	<!-- start third row -->
<!-- 2 Sept 2016 start -->

<td align="left" valign="top" style="padding-top:7px; padding-bottom:7px; border-top:1px solid #53c2eb;">
                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="94%">                    	
                        <tr>
                        	<!-- Start Promotions  -->
                      ';
						$groupplanid = explode(",",$config['PLAN_GROUP_ID']);
						foreach ($groupplanid as $vplanid){
							$PlanID =$vplanid;
							$plan = new plan($PlanID);
							$plan_result = $plan->getInfo();
							$PlanName 	= $plan->Name;
							$PlanDesc 	= $plan->Description;
							$PlanPrice	= $plan->Price;
							$PlanMinutes= number_format($plan->Minutes);
							$ShowPlanMinutes= $plan->ShowMinutes;
							$i_vd_plan  = $plan->i_vd_plan;
							$CountryISO = strtolower($plan->CountryISO);
							//$CountryName= $plan->CountryName;
							$CountryName= explode(",",$plan->CountryName);
							$ShowMinutes = $plan->ShowMinutes;
							
						     $message .= '<td align="left" valign="top" width="41%" style="padding-top:5px;">
                        	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                            	<tr>
                                <td align="left" valign="top" width="21%"><img src="'.$ste_url_link.'/Flags/32/'.$CountryISO.'.png" alt='.$PlanName.' /></td>
                                <td align="left" valign="top" width="79%">
                                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    	
										
										<tr><td align="left" valign="top"><font color="#004a90" face="Verdana" 
										style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">'.$PlanName .'</font></td></tr>
                                        <tr><td align="left" valign="top"><font color="#333333" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; 

line-height:11px;">'.$ShowMinutes.' Minutes for</font><font color="#25a4d3" face="Verdana" style="font-size:12px; mso-line-height-rule:exactly; line-height:17px;"><b> 

$'.$PlanPrice.'</b></td></tr>
                                        <tr><td align="left" valign="bottom" style="padding-top:3px;"><a href="'.$ste_url_link.'/Purchase/purchase.php" style="text-

decoration:none;" title="Buy Now" target="_blank"><font color="#018494" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:17px;"><img 

src="'.$ste_url_link.'/Email_temp/arrow.gif" alt="" /> Buy Now</font></a></td></tr>
                                    </table>
                                </td>
                                </tr>
                            </table>
                        </td>
                     	';
						}
                        
                        $message .='
						
						<!-- END Promotions  -->
						<td align="center" valign="top" width="17%">
                        <!--start Free Test Calls -->
                        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                        <tr><td align="center" valign="top"><a href="'.$ste_url_link.'/Home/membership.php?mode=AddUser" title="Free Test Calls"><font color="#004a90" 

face="Verdana" style="font-size:13px; mso-line-height-rule:exactly; line-height:11px;"><img src="'.$ste_url_link.'/Email_temp/free-call.gif" alt="Free Test Call" 

/></font></a></td></tr>
                        <tr><td align="left" valign="top"><font color="#605d5d" face="Arial" style="font-size:10px; mso-line-height-rule:exactly; line-

height:11px;">*Upon first sign-up</font></td></tr>
						</table>
                        <!-- End Free Test Calls -->
                        </td>
                        </tr>
                    </table>	
                </td>   
                
<!-- 2 Sept 2016 ends -->
                <!-- end third row -->
 
            </tr>
            
            
            
            <tr>
        <!-- Start Footer -->
              <td bgcolor="#45beeb" valign="middle" height="82"><table width="97%" cellspacing="0" cellpadding="0" border="0" align="right" >
                <tr>
                  <td height="26" align="left" valign="middle" width="83%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Support</b></font></td>
                  <td align="left" valign="middle" width="17%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Follow Us</b></font></td>
                  </tr>
                <tr>
                  <td width="444" align="left" valign="middle"><font face="Verdana" color="#12769b" style="font-size:11px; line-height:21px;"><a href="'.$ste_url_link.'/Home/forgotpassword.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Lost Password">Lost Password</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/index.php?emailLiveLink=true" style=" text-decoration:none; color:#12769b;" target="_blank" title="Live Help">Live Help</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/support_form.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Contact Us">Contact Us</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/membership.php?mode=UpdateUser" style=" text-decoration:none; color:#12769b;" target="_blank" title="Account Settings">Account Settings</a> <br/>
                    <a href="'.$ste_url_link.'/Home/material.php?id=112" style=" text-decoration:none; color:#12769b;" target="_blank" title="Privacy Policy">Privacy Policy</a> &nbsp;  | &nbsp; <a href="'.$ste_url_link.'/Home/material.php?id=111" style=" text-decoration:none; color:#12769b;" target="_blank" title="Terms Of Service">Terms Of Service </a></font></td>
                  <td align="left" width="100" valign="middle"><table width="96%" cellspacing="0" cellpadding="0" border="0" align="center" >
                    <tr>
                      <td width="43" height="42" align="left" valign="middle"><a href="http://www.facebook.com/SoftCall" style="text-decoration:none; color:#767373;" title="Facebook" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/fb.gif" alt="Facebook" border="0" width="26" height="26" /></font></a></td>
                      <td align="left" valign="middle"><a href="http://www.twitter.com/SoftCallME/" style="text-decoration:none;" title="Twitter" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/twitter.gif" alt="Twitter" border="0" width="26" height="26" /></font></a></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <!-- End Footer -->
            </tr>            
          	</table>
          </td>
        <!--- End Main Row -->  	
        </tr>
        <tr><td height="36" valign="middle" align="center"><font color="#6d6d6d" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:18px;">Copyright &copy; 2014 Cloud Communications & Computing Corp., All rights reserved.</font></td></tr>        
      </table>
      <!--end main table -->
      </td>
  </tr>
</table>
</body>
</html>';
			// Send Message
		
	$message = stripslashes(trim($message));
		if(preg_match("/win/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $headers);
			return true;
		}
		elseif(preg_match("/linux/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message,$config['EMAIL_DEVELOPMENT']);
			//mail($to, $subject, $message, $headers);
			return true;
		}
		$trace->info("Referral Earned BONUS Credits announcement has been successfully sent to the Referrer's email!");
		
	}
	
	function Upadate_did_member_did_status($mu_id,$mu_did_doc_id)
	{
	global $dbLink;
	$query="UPDATE `did_member_details` SET `did_status`='1' WHERE `mu_ID`='$mu_id' and mu_did_doc_id='$mu_did_doc_id'";
	$result=mysqli_query($dbLink,$query) or die("Query failed Upadate_did_member_did_status: " .mysqli_error());
	return $result;
	}
	
	function Upadate_did_member_subscription_attachment($mu_id,$mu_did_doc_id,$value)
	{
	global $dbLink;
	$query="UPDATE `did_member_details` SET `did_status`='{$value}' WHERE `mu_ID`='$mu_id' and mu_did_doc_id='$mu_did_doc_id'";
	$result=mysqli_query($dbLink, $query) or die("Query failed Upadate_did_member_subscription_attachment: " .mysqli_error());
	return $result;
	}
/*	
function update_did_member_subscription_attachment($mu_did_doc_id,$fileType,$mu_did_doc_id_uncheck,$UserID,$doc_select_type,$orderstatus)
{
global $dbLink;
$query ="UPDATE did_member_subscription_attachment SET documents_name_type= '";
for($j=0;$j<count($fileType);$j++)
{
$file_type=explode("+",$fileType[$j]);

$val=explode("##",$doc_select_type);
foreach ($val as $key => $value) 
{
$doc_select_type=explode("_",$value);
if(!empty($doc_select_type[0]))
{
$val=$file_type[0].'-'.$doc_select_type[0].'--1'.';';
$query .= "$val";
}
}
}
if(!empty($mu_did_doc_id_uncheck))
{
$query .="',is_valid='2' , flag='0' WHERE `mu_ID`=$UserID AND `mu_did_doc_id`='$mu_did_doc_id_uncheck[0]'";  
}
else
{
$query .="',is_valid='1' , flag='0' WHERE `mu_ID`=$UserID AND `mu_did_doc_id`='$mu_did_doc_id[0]'";  
}

$result=mysqli_query($dbLink, $query);
 return $result;
}
*/

function update_did_member_subscription_attachment_test($mu_did_doc_id,$fileType,$mu_did_doc_id_uncheck,$UserID,$doc_select_type,$orderstatus)
{
global $dbLink;
$query ="UPDATE did_member_subscription_attachment SET documents_name_type= '";
$file_type = array();
for($j=0;$j<count($fileType);$j++)
{
$file_type[]=explode("+",$fileType[$j]);
}
$val=explode("##",$doc_select_type); //	Passport_124+0##UtilityBill_124+1##IDProof_124+2##
$j=0;
$valid_check = 0;
foreach ($val as $key => $value) 
{
$doc_select_type=explode("_",$value);
if(!empty($doc_select_type[0]))
{
$val=$file_type[$j][0].'-'.$doc_select_type[0].'--1'.'_'.$mu_did_doc_id_uncheck[$key]; 
if ($mu_did_doc_id_uncheck[$key]== 'Valid') {
	$valid_check++;
}
$query .= "$val".";";
}
$j = $j+1;

}
//}
if ($valid_check == 3) {
	$query .="',sls_approve_doc_date = NOW()";
} else {
	$query .="',sls_reject_doc_date = NOW()";
}
$query .=",is_valid=1, flag='0' WHERE `mu_ID`=$UserID AND `mu_did_doc_id`=trim('$mu_did_doc_id')"; 

$result=mysqli_query($dbLink, $query) or die("Query failed update_did_member_subscription_attachment_test: " .mysqli_error()); 
if($result)
{
$this->Upadate_did_member_did_status($UserID,$mu_did_doc_id);
}
return $result;

}



function getSubscriptionDetailsByUser($i_master_account)
{
global $dbLink;
$query="SELECT dma.`did_number`,dma.i_subscription_id,dmd.`order_id`,dsdc.`invoice_description`,dma.`activation_date`,dma.`did_expire_date_gmt`,dma.`customer_billed_to_date` 
FROM did_member_api_result_object dma, did_member_details dmd,
did_subscription_document_controller dsdc
WHERE dsdc.`i_subscription`=dma.`i_subscription_id` AND dsdc.`i_subscription`= dmd.`did_mu_prefered_i_subscription`	
 AND dma.`mu_did_details_req_id`= dmd.`mu_did_details_req_id`
AND dma.`i_master_account`='$i_master_account'  GROUP BY dma.`order_id`";

$result = mysqli_query($dbLink, $query) or die("Query failed getSubscriptionDetailsByUser: " .mysqli_error()); 
return $result;
}

function getdidstatus($UserID,$doc_id)
{
global $dbLink;
$query="SELECT * FROM `did_member_details` amd, `did_member_subscription_attachment` dmsa, `did_subscription_document_controller` dsdc,
`did_member_api_result_object` dmar WHERE amd.`mu_did_doc_id`=dmsa.`mu_did_doc_id` AND amd.`did_mu_prefered_i_subscription` = dsdc.`i_subscription` 
AND dmar.`mu_did_details_req_id`=amd.`mu_did_details_req_id`
AND dmsa.`mu_ID`= ".$UserID." AND amd.`mu_did_doc_id`=".$doc_id." ORDER BY dmsa.`submit_date`";
$result = mysqli_query($dbLink, $query) or die("Query failed getdidstatus : " .mysqli_error()); 
return $result;

}

function getdidstatus_update($did_number,$UserID)
{
global $dbLink;
$query="UPDATE `did_member_api_result_object` SET `did_status`='1' WHERE did_number=".$did_number."  AND `did_status`='-1'";
$result = mysqli_query($dbLink, $query) or die("Query failed getdidstatus_update : " .mysqli_error()); 
return $result;

}
function getSubscriptionNameByUserIMasterAccount($i_master_account)
{
global $pb_dbLink;
$query="SELECT SUBS.`i_subscription`,SUBS.`name`,ASUB.`start_date`,ASUB.`activation_date` FROM `Account_Subscriptions` AS ASUB,`Subscriptions` AS SUBS 
WHERE ASUB.`i_subscription`=SUBS.`i_subscription` AND ASUB.`i_account`='$i_master_account'";
$result = mysqli_query($pb_dbLink, $query) or die("Query failed getSubscriptionNameByUserIMasterAccount: " .mysqli_error()); 
return $result;


}

function getSubscriptionClosedDetails($i_master_account)
{
global $pb_dbLink;
 $query="SELECT * FROM `Account_Subscriptions` AS ASUB,`Subscriptions` AS SUBS 
WHERE ASUB.`i_subscription`=SUBS.`i_subscription` AND ASUB.`finish_date` !='' AND ASUB.`i_account`='$i_master_account'";
$result = mysqli_query($pb_dbLink, $query) or die("Query failed getSubscriptionClosedDetails: " .mysqli_error()); 
return $result;
}
function getSubscriptionDetailsByUserDocuments1($i_master_account)
{
global $pb_dbLink;
  $querys="SELECT acc.`id` AS AccountNumber,acc.`login`,acc.`i_account`,acc.`i_master_account` AS Masteraccount,dn.`number` AS DIDNumber,
accs.`i_subscription` AS subscriptionid,subs.`name`, accs.`start_date` AS StartDate , accs.`finish_date` AS EndDate,accs.`billed_to` AS BillingDate

FROM `Accounts` acc,
`DID_Numbers` dn,
`Account_Subscriptions` accs,
`Subscriptions` subs

WHERE acc.`id`= dn.`number`AND acc.`i_master_account`=accs.`i_account` 
AND accs.`i_subscription`=subs.`i_subscription`

AND acc.`i_master_account`='$i_master_account'";
$results = mysqli_query($pb_dbLink, $querys) or die("Query failed getSubscriptionDetailsByUserDocuments1: " .mysqli_error()); 
return $results;
}
function getSubscriptionDetailsByUserDocuments($i_master_account,$number)
{
global $pb_dbLink;
 $querys="SELECT acc.`id` AS AccountNumber,acc.`login`,acc.`i_account`,acc.`i_master_account` AS Masteraccount,dn.`number` AS DIDNumber,
accs.`i_subscription` AS subscriptionid,subs.`name`, accs.`start_date` AS StartDate , accs.`finish_date` AS EndDate,accs.`billed_to` AS BillingDate

FROM `Accounts` acc,
`DID_Numbers` dn,
`Account_Subscriptions` accs,
`Subscriptions` subs

WHERE acc.`id`= dn.`number`AND acc.`i_master_account`=accs.`i_account` 
AND accs.`i_subscription`=subs.`i_subscription`

AND acc.`i_master_account`='$i_master_account' AND dn.`number`='$number'";
$results = mysqli_query($pb_dbLink, $querys) or die("Query failed getSubscriptionDetailsByUserDocuments: " .mysqli_error()); 
return $results;
}
//display document required grid for DID plans  


	function getdid_member_api_result_object($did_number)
	{
		global $dbLink;
		$query  ="SELECT * FROM did_member_api_result_object WHERE `did_number`='{$did_number}' AND flag=1 ";
		$result = mysqli_query($dbLink, $query) or die("Query failed getdid_member_api_result_object: " .mysqli_error());
		return $result;
	}
	
	
	
	 function getpendingdidstatus($UserID)
	 {
	 global $dbLink;
 $query  =" SELECT * FROM `did_member_details` dmd ,`did_member_subscription_attachment` dmsa ,`did_subscription_document_controller` dsdc 
WHERE dmd.`mu_id`='{$UserID}' AND dmd.`mu_did_doc_id`=dmsa.`mu_did_doc_id` AND  dmd.`did_mu_prefered_i_subscription`=dsdc.`i_subscription`
AND dmd.did_status='1'";
	 $result = mysqli_query($dbLink, $query) or die("Query failed getpendingdidstatus: " .mysqli_error());
	 return $result;
	 }
	 
	function getpendingDIDPlansforgenerate($UserID)
	{
	global $dbLink;
		 /*$query  =
		"
		SELECT * 
		FROM did_member_details
		WHERE mu_id = ".$UserID." AND flag = 1
		";*/
		
		$query  ="SELECT * 
		FROM did_member_api_result_object
		WHERE `did_member_api_result_object`.`mu_did_details_req_id` 
		IN 
		('SELECT `mu_did_details_req_id` FROM did_member_details WHERE mu_ID = ".$UserID."') and flag=1
		";
		$result = mysqli_query($dbLink, $query) or die("Query failed getpendingDIDPlansforgenerate: " .mysqli_error());
		return $result;
	}
	
	function getActiveDIDPlansOrder($UserID)
	{
		global $dbLink;
	/*echo	$query  =
		"
		SELECT * 
		FROM did_member_api_result_object
		WHERE  `did_member_api_result_object`.`mu_did_details_req_id` 
		IN 
		('SELECT `mu_did_details_req_id` FROM did_member_details WHERE mu_ID = ".$UserID."')
		";
		$result = mysqli_query($dbLink, $query) or die("Query failed getActiveDIDPlansOrder: " .mysqli_error());
		return $result;*/
		$query= "SELECT * FROM did_member_api_result_object  dmaro,`did_member_details` mddr WHERE 
dmaro.mu_did_details_req_id= mddr.`mu_did_details_req_id` AND mu_ID='$UserID'";
$result = mysqli_query($dbLink, $query) or die("Query failed getActiveDIDPlansOrder: " .mysqli_error());
		return $result;
	}
	
	
	function getInActiveDIDPlanscounts($UserID)
	{
		global $dbLink;
		$query  =
		"
		SELECT count(dsdc.`i_subscription`) as TotalPendingDIDPlans FROM `did_member_details` amd, `did_member_subscription_attachment` dmsa,`did_subscription_document_controller` dsdc
		WHERE amd.`mu_did_doc_id`=dmsa.`mu_did_doc_id`
		AND amd.`did_mu_prefered_i_subscription` = dsdc.`i_subscription`  AND  dmsa.is_valid=0 
		AND dmsa.`mu_ID`=  " . $UserID .  "
		Order By dmsa.`submit_date`
		";
		
		
		
		//$query =" SELECT * FROM  `Subscriptions` Subs WHERE Subs.`i_subscription`='$SubscriptionId' ";
		
		
		
		$result = mysqli_query($dbLink, $query) or die("Query failed getInActiveDIDPlanscounts: " .mysqli_error());
		$row = mysqli_fetch_object($result);
		return $row->TotalPendingDIDPlans;
		
	}
	
	function getSubscriptionCloseNameByUserIMasterAccountActive($i_master_account)
	{
	global $dbLink;
	/*$query="SELECT SUBS.`i_subscription`,SUBS.`name`,ASUB.`i_account_subscription`,ASUB.`start_date`,ASUB.`finish_date`,ASUB.`obligatory`,
	ASUB.`billed_to`,ASUB.`activation_date`,SUBS.`invoice_description` FROM `Account_Subscriptions` AS ASUB,
	`Subscriptions` AS SUBS 
	WHERE ASUB.`i_subscription`=SUBS.`i_subscription` AND ASUB.`i_account`='$i_master_account' AND ASUB.`finish_date` IS NULL";*/
	
	$query="SELECT * FROM `did_member_api_result_object` dmapi,`did_member_details` dmd WHERE dmd.`mu_did_details_req_id`=dmapi.`mu_did_details_req_id` 

AND dmapi.`i_master_account`='$i_master_account' AND dmapi.`finish_date` IS NULL";
	$result = mysqli_query($dbLink, $query) or die("Query failed getSubscriptionCloseNameByUserIMasterAccountActive: " .mysqli_error()); 
	return $result;
	}
	function getRejectDocuments($UserID)
	{
	global $dbLink;
		$query="SELECT COUNT(*)  AS TotalReajectDocumentDIDPlans  FROM `did_member_details` amd, `did_member_subscription_attachment`
		dmsa,`did_subscription_document_controller` dsdc
		WHERE amd.`mu_did_doc_id`=dmsa.`mu_did_doc_id` AND amd.`did_mu_prefered_i_subscription` = dsdc.`i_subscription` 
		AND dmsa.`mu_ID`={$UserID}  AND dmsa.`is_valid`=2 AND dmsa.`flag`=0 ORDER BY dmsa.`submit_date`";
		$result = mysqli_query($dbLink, $query) or die("Query failed getRejectDocuments: " .mysqli_error());
		$row = mysqli_fetch_object($result);
		return $row->TotalReajectDocumentDIDPlans;
	}



	function getSubscriptionPlan($SubscriptionId)
	{
		global $pb_dbLink;
		
		$query =" SELECT * FROM  `Subscriptions` Subs WHERE Subs.`i_subscription`='$SubscriptionId' ";
		$result = mysqli_query($pb_dbLink, $query)or die("Query failed getSubscriptionPlan: " .mysqli_error());
		return $result;
	}
	
	function getApproveDIDdoumentList($userId,$i_subcriptionPlan,$Order_id,$mu_did_details_req_id)
	{
	global $dbLink;
	//$query = "SELECT * FROM did_member_subscription_attachment WHERE mu_ID = '$userId' "; 
	 $query="SELECT * FROM `did_member_details` amd, `did_member_subscription_attachment` dmsa
	WHERE amd.`mu_did_doc_id`=dmsa.`mu_did_doc_id` AND dmsa.`mu_ID`=$userId AND amd.`did_mu_prefered_i_subscription`='$i_subcriptionPlan'  AND amd.`order_id`='$Order_id'  AND  amd.`mu_did_details_req_id`='$mu_did_details_req_id'";
		$result = mysqli_query($dbLink, $query) or die("Query failed getApproveDIDdoumentList: " .mysqli_error()); 
		return $result;
	}
	function getorderproductname($order_id,$ProductID)
	{
		global $dbLink;
	    $query="SELECT did_mu_prefered_i_subscription FROM `did_member_details` where (order_id='$order_id' OR refrence_order_id='$order_id'  ) and (did_mu_prefered_i_subscription='$ProductID')";
		$result = mysqli_query($dbLink, $query) or die("Query failed getorderproductname: " .mysqli_error()); 
		//return $result;
		$subscription_id=mysqli_fetch_array($result);
		return $subscription_id['did_mu_prefered_i_subscription'];
	}
	
	function getsubscritiondocdeatils($GetScubcriptionPlan_id)
	{
		global $dbLink;
	    $query="SELECT * FROM `did_subscription_document_controller` WHERE i_subscription='{$GetScubcriptionPlan_id}'";
		$result = mysqli_query($dbLink, $query) or die("Query failed getsubscritiondocdeatils: " .mysqli_error()); 
		return $result;
	
	}
	
	
	function emailcoustomeraboutdid($from,$To,$from_subject,$userID,$email_messages,$did_number)
	{
		global $trace;
		global $config;
		// Retrieve referrer information
		$referrer = new user();
	    $referrer->populateByID($userID);
		// Prepare headers for sending html mails
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=windows-1256\r\n";
		$headers .= "From: " . $from . " \r\n";
		$headers .= "BCC: " . $config['EMAIL_DEVELOPMENT'] . " \r\n";
		$ReferrerName = ucwords($referrer->fName . " " . $referrer->lName);
		$ste_url_link="http://".$config['HOST'];
		if($from_subject=="SoftCallerIDChange")
		{
		$title ="Your SoftCaller ID Has Been Changed";
		$subject = "Your SoftCaller ID Has Been Changed";
		$email_messages ='<tr>
            	<td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;">
				<a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank">
				<font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Email_temp/logo.gif" alt="SoftCall" /></font></td> 
				</tr>
				<tr>
            	<!-- start logo row -->
            	<td align="left" valign="middle" height="36" style="padding-left:17px;">
				<font color="#6d6d6d" face="Verdana" style="font-size:12px;">Dear <b>'.$ReferrerName.' ('.$referrer->username.')</b>,</font>
				</td>
                <!-- end logo row -->
				<p>Your Caller ID [xxxxx] has been successfully changed to [NEWCallerID].</p>
            </tr>
            <tr>
            	<!-- start second row -->
            	<td  align="left" valign="middle" height="100" style="padding-left:17px; padding-right:17px;">
				<font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
				'.$email_messages.'<br/><br/>
			Thank you for using SoftCall.
			</font></td> 
                <!-- end second row -->
            </tr>';
		}
		if($from_subject=="DocumentsVerification")
		{
		$title="SoftCall DID Documents Verification";
		$subject .="SoftCall DID Documents Verification";
		$email_messages ='<tr>
            	<td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;">
				<a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank">
				<font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Email_temp/logo.gif" alt="SoftCall" /></font></td> 
				</tr>
				<tr>
            	<!-- start logo row -->
            	<td align="left" valign="middle" height="36" style="padding-left:17px;">
				<font color="#6d6d6d" face="Verdana" style="font-size:12px;">Dear SLS,</font></td>
                <!-- end logo row -->
				<p>Could you please verify the information of 
					'.$ReferrerName.' whose account is: '.$userID.' as he would like to buy a 
					SoftCall Number for [Country],[City] and Plan.
					<br>
					<a href="https://www.softcall.me/BackOffice/Admin/did_member_user_details.php?id=1106_69_3741"> Click Here</a>	to view the docs he has submitted
					</br>
					<br>
					Please verify that: <ul>
<li> The ID proof corresponds to the personal information that the user has provided: First name, Last Name are the same</li>
<li> The proof of address (the utility bills) corresponds to the personal information that the user has provided: First Name, Last Name, and Address is the same on the Personal Information provided by the user</li>
<li> The amount of proofs corresponds to the requested amount specific to that country / DID (sometimes it is one utility bill sometimes two)</li>
<li> The address location corresponds to the DID requirements: sometimes it needs to be the same town of the DID town request sometimes it needs to be somewhere in the world</li>
<li> That the utility provider exists (search the internet to find if the utility provider exists and really provides a utility service)</li>
<li> The address of the user really exists in the country / town (search for it)</li>
</ul>
					</br>
				</p>
            </tr>
            <tr>
            	<!-- start second row -->
            	<td  align="left" valign="middle" height="100" style="padding-left:17px; padding-right:17px;"><font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
				'.$email_messages.' <br/><br/><br/><br/>
			Thank you for using SoftCall.

</font></td> 
                <!-- end second row -->
            </tr>';
		}
		//Your SoftCall Phone Number is about to Expire
		if($from_subject=="PhoneNumberExpire")
		{
		$title="Your SoftCall Phone Number is about to Expire";
		$subject .="Your SoftCall Phone Number is about to Expire";
		$email_messages ='<tr>
            	<td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;"><a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank"><font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Email_temp/logo.gif" alt="SoftCall" /></font></td> 
                
            </tr>
            <tr>
            	<!-- start logo row -->
            	<td align="left" valign="middle" height="36" style="padding-left:17px;">
				<font color="#6d6d6d" face="Verdana" style="font-size:12px;">Dear <b>'.$ReferrerName.' ('.$referrer->username.')</b>,</font></td>
                <!-- end logo row -->
			<p align="justify"> Your SoftCall Phone Number [XXXXXX] and YYYYYY will expire on . Beyond this date [Expiration Date]  you will no longer be able to receive calls through this number. 
To keep it active, click <a href="https://www.softcall.me/Account/get_softcall_number.php">[here]</a> to extend your subscription.

To ease your life, you can set up an auto renew option among your functionalities. This will renew your subscription to this service, provided you have sufficient credit on your account.  Proceed with Auto-Renew ["Proceed with auto-renew" links to the location in the profile of the user where he can activate the Auto Renew]

For Support or other inquiries, do not hesitate to contact our <a here="http://www.softcall.me/phplive/web/icon_online_0.PNG?1404425555">Live Help</a> team available 24/7</p> 
			
            </tr>
            <tr>
            	<!-- start second row -->
            	<td  align="left" valign="middle" height="100" style="padding-left:17px; padding-right:17px;">
				<font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
				'.$email_messages.' <br/><br/><br/><br/>
			Thank you for using SoftCall.

</font></td> 
                <!-- end second row -->
            </tr>';
		}
		else
		{
		$subject .="";
		}
		if($from_subject=="Coustomer")
		{
		$title="SoftCall Phone Number Verification";
		$subject .="SoftCall Phone Number Verification";
		$email_messages1 ='<tr>
             <td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;"><a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank"><font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Email_temp/logo.gif" alt="SoftCall" /></font></td> 
                
            </tr>
            <tr>
             <!-- start logo row -->
             <td align="left" valign="middle" height="36" style="padding-left:17px;"><font color="#6d6d6d" face="Verdana" style="font-size:12px;">Dear <b>'.$ReferrerName.' ('.$referrer->username.')</b>,</font></td>
                <!-- end logo row -->
    <p>We have verified the information provided and are happy to announce that your SoftCall Phone number is "'.$did_number.'" and is activated</p>
            </tr>
            <tr>
             <!-- start second row -->
             <td  align="left" valign="middle" height="100" style="padding-left:17px; padding-right:17px;"><font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
    '.$email_messages.' <br/><br/>
   Thank you for using SoftCall.

</font></td> 
                <!-- end second row -->
            </tr>';
		}
		else
		{
		$subject .="";
		}
		$subject = "SoftCall-".$subject;
		$to = $To;
			$message ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>"$title"</title>
<style type="text/css">
table td {
	border-collapse: collapse;
	font-family: Verdana;
}
.active font{
	color: #FF0000;
}
</style>
</head>
<body style="margin:0; padding:0; background:#e8e8e8;">
<table width="100%" bgcolor="#e8e8e8" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!--| Main table -->
    <table width="546" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#e8e8e8">
        <tr>
        <!-- start top empty row -->
          <td height="30" align="center" valign="middle">&nbsp;</td>
        <!-- end top empry row -->
		</tr>
        <!-- Start main row -->
          <td align="center" valign="middle" style="border:1px solid #53c2eb;">
          	<table border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" width="100%">
			'.$email_messages.'             <tr>
            	<!-- start second row -->
            	<td  align="left" valign="middle" height="50" style="padding-left:17px; padding-bottom:10px;"><font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:24px;">Best Regards,<br />
<b>SoftCall Support <br></b></font><br/>
<img src="'.$ste_url_link.'/Email_temp/logo_signature.gif" alt="SoftCall" />
</td> 
                <!-- end second row -->
            </tr>
            <tr>

            	<!-- start third row -->
<!-- 2 Sept 2016 start -->

<td align="left" valign="top" style="padding-top:7px; padding-bottom:7px; border-top:1px solid #53c2eb;">
                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="94%">                    	
                        <tr>
                        	<!-- Start Promotions  -->
                      ';
						$groupplanid = explode(",",$config['PLAN_GROUP_ID']);
						foreach ($groupplanid as $vplanid){
							$PlanID =$vplanid;
							$plan = new plan($PlanID);
							$plan_result = $plan->getInfo();
							$PlanName 	= $plan->Name;
							$PlanDesc 	= $plan->Description;
							$PlanPrice	= $plan->Price;
							$PlanMinutes= number_format($plan->Minutes);
							$ShowPlanMinutes= $plan->ShowMinutes;
							$i_vd_plan  = $plan->i_vd_plan;
							$CountryISO = strtolower($plan->CountryISO);
							//$CountryName= $plan->CountryName;
							$CountryName= explode(",",$plan->CountryName);
							$ShowMinutes = $plan->ShowMinutes;
							
						     $message .= '<td align="left" valign="top" width="41%" style="padding-top:5px;">
                        	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                            	<tr>
                                <td align="left" valign="top" width="21%"><img src="'.$ste_url_link.'/Flags/32/'.$CountryISO.'.png" alt='.$PlanName.' /></td>
                                <td align="left" valign="top" width="79%">
                                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    	
										
										<tr><td align="left" valign="top"><font color="#004a90" face="Verdana" 
										style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">'.$PlanName .'</font></td></tr>
                                        <tr><td align="left" valign="top"><font color="#333333" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; 

line-height:11px;">'.$ShowMinutes.' Minutes for</font><font color="#25a4d3" face="Verdana" style="font-size:12px; mso-line-height-rule:exactly; line-height:17px;"><b> 

$'.$PlanPrice.'</b></td></tr>
                                        <tr><td align="left" valign="bottom" style="padding-top:3px;"><a href="'.$ste_url_link.'/Purchase/purchase.php" style="text-

decoration:none;" title="Buy Now" target="_blank"><font color="#018494" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:17px;"><img 

src="'.$ste_url_link.'/Email_temp/arrow.gif" alt="" /> Buy Now</font></a></td></tr>
                                    </table>
                                </td>
                                </tr>
                            </table>
                        </td>
                     	';
						}
                        
                        $message .='
						
						<!-- END Promotions  -->
						<td align="center" valign="top" width="17%">
                        <!--start Free Test Calls -->
                        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                        <tr><td align="center" valign="top"><a href="'.$ste_url_link.'/Home/membership.php?mode=AddUser" title="Free Test Calls"><font color="#004a90" 

face="Verdana" style="font-size:13px; mso-line-height-rule:exactly; line-height:11px;"><img src="'.$ste_url_link.'/Email_temp/free-call.gif" alt="Free Test Call" 

/></font></a></td></tr>
                        <tr><td align="left" valign="top"><font color="#605d5d" face="Arial" style="font-size:10px; mso-line-height-rule:exactly; line-

height:11px;">*Upon first sign-up</font></td></tr>
						</table>
                        <!-- End Free Test Calls -->
                        </td>
                        </tr>
                    </table>	
                </td>   
                
<!-- 2 Sept 2016 ends -->
                <!-- end third row -->

            </tr>
            
            
            
            <tr>
        <!-- Start Footer -->
              <td bgcolor="#45beeb" valign="middle" height="82"><table width="97%" cellspacing="0" cellpadding="0" border="0" align="right" >
                <tr>
                  <td height="26" align="left" valign="middle" width="83%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Support</b></font></td>
                  <td align="left" valign="middle" width="17%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Follow Us</b></font></td>
                  </tr>
                <tr>
                  <td width="444" align="left" valign="middle"><font face="Verdana" color="#12769b" style="font-size:11px; line-height:21px;"><a href="'.$ste_url_link.'/Home/forgotpassword.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Lost Password">Lost Password</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/index.php?emailLiveLink=true" style=" text-decoration:none; color:#12769b;" target="_blank" title="Live Help">Live Help</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/support_form.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Contact Us">Contact Us</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/membership.php?mode=UpdateUser" style=" text-decoration:none; color:#12769b;" target="_blank" title="Account Settings">Account Settings</a> <br/>
                    <a href="'.$ste_url_link.'/Home/material.php?id=112" style=" text-decoration:none; color:#12769b;" target="_blank" title="Privacy Policy">Privacy Policy</a> &nbsp;  | &nbsp; <a href="'.$ste_url_link.'/Home/material.php?id=111" style=" text-decoration:none; color:#12769b;" target="_blank" title="Terms Of Service">Terms Of Service </a></font></td>
                  <td align="left" width="100" valign="middle"><table width="96%" cellspacing="0" cellpadding="0" border="0" align="center" >
                    <tr>
                      <td width="43" height="42" align="left" valign="middle"><a href="http://www.facebook.com/SoftCall" style="text-decoration:none; color:#767373;" title="Facebook" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/fb.gif" alt="Facebook" border="0" width="26" height="26" /></font></a></td>
                      <td align="left" valign="middle"><a href="http://www.twitter.com/SoftCallME/" style="text-decoration:none;" title="Twitter" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/twitter.gif" alt="Twitter" border="0" width="26" height="26" /></font></a></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <!-- End Footer -->
            </tr>            
          	</table>
          </td>
        <!--- End Main Row -->  	
        </tr>
        <tr><td height="36" valign="middle" align="center"><font color="#6d6d6d" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:18px;">Copyright &copy; 2014 Cloud Communications & Computing Corp., All rights reserved.</font></td></tr>        
      </table>
      <!--end main table -->
      </td>
  </tr>
</table>
</body>
</html>';

		// Send Message
		$message = stripslashes(trim($message));
		if(preg_match("/win/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $headers);
		}
		elseif(preg_match("/linux/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $config['EMAIL_DEVELOPMENT']);
			//mail($to, $subject, $message, $headers);
		}

		//$this->trace->info("DID !");
	}

function getCountryNamebycountryId($countryID)
{
global $dbLink;
 $query=" SELECT * FROM `country`  WHERE c_ID='$countryID'";
 $result=mysqli_query($dbLink, $query);
 $country_name=mysqli_fetch_assoc($result);
 return $country_name['c_Name_En'];
}

function getdocusrequired($UserID,$i_subcriptionPlan)
{

global $dbLink;
$query="SELECT * FROM did_member_details dmd, did_subscription_document_controller dsdc,`did_member_subscription_attachment` dmsa
WHERE dsdc.`i_subscription`= dmd.`did_mu_prefered_i_subscription`
AND dmd.`mu_did_doc_id`=dmsa.`mu_did_doc_id`	
AND dmd.`mu_id`='$UserID' AND dsdc.`i_subscription`=$i_subcriptionPlan";
 $result=mysqli_query($dbLink, $query);
 return $result;
}
function getrequireddocdetails($i_subcriptionPlan)
{
global $dbLink;
$query="SELECT * FROM  did_subscription_document_controller dsdc
WHERE 
dsdc.`i_subscription`=$i_subcriptionPlan";
 $result=mysqli_query($dbLink, $query);
return $result;
}

function updaterejectedfiledocs($UserID,$mu_did_doc_id,$previousfilename,$filetoupdate,$filenametype)
{
global $dbLink;
for($j=0;$j< count($previousfilename);$j++)
{
$data_count=explode('--',$previousfilename[$j]);
$filevalue=explode('_',$data_count[1]);
}
$query= "UPDATE did_member_subscription_attachment SET documents_name_type='";
if($filevalue[1]=="Reject" && $filetoupdate!="")
{
for($i=0; $i< count($filetoupdate);$i++)
{
if(empty($filetoupdate[$i]))
{
$query.= $previousfilename[$i].';';
}
else
{
$query.=$UserID.'_'.$filetoupdate[$i].'-'.$filenametype[$i].'--1_Valid;'; 
}}
}
else
{
for($j=0;$j< count($previousfilename);$j++)
{
$query.=$UserID.'_'.$previousfilename[$j].'-'.$filenametype[$j].'--1_Valid;';
}
}
$query.="' , is_valid='1'"; 
$query.=" WHERE documents_name_type='";
for($j=0;$j< count($previousfilename);$j++)
{
$data_count=explode('--',$previousfilename[$j]);
$filevalue=explode('_',$data_count[1]);
if($filevalue[1]=="Valid")
{
$query.=$previousfilename[$j].';'; 
}
else
{
$query.=$previousfilename[$j].';'; 
}
}
$query.="' AND `mu_ID`=".$UserID." AND `mu_did_doc_id`=".$mu_did_doc_id;

  $result=mysqli_query($dbLink, $query);
 return $result;
}

function updatedid_member_subscription_attachmenttable($UserID,$mu_did_doc_id)
{
global $dbLink;
$query="UPDATE `did_member_subscription_attachment` SET `flag`=1 WHERE `mu_ID`='$UserID' AND `mu_did_doc_id`='$mu_did_doc_id'";
$result=mysqli_query($dbLink, $query);
return $result;
}
//find the details of did_member_api_result_object recored
function maxrecoredindid_member_api_result_object()
{
global $dbLink;
 $query="SELECT MAX(i_did_number) as i_did_number,mu_did_details_req_id FROM `did_member_api_result_object`";
$result=mysqli_query($dbLink, $query);
$row=mysqli_fetch_array($result) or die("Query failed maxrecoredindid_member_api_result_object: " .mysqli_error());

return $row;
}

function update_did_member_api_result_objectmu_did_details_req_id($mu_did_details_req_id,$i_account,$i_subscription_id,$i_did_number)
{
global $dbLink;
$query="update did_member_api_result_object set mu_did_details_req_id= '$mu_did_details_req_id' 
where i_account='$i_account' and i_subscription_id='$i_subscription_id' and i_did_number='$i_did_number'";
$result=mysqli_query($dbLink, $query) or die("Query failed update_did_member_api_result_objectmu_did_details_req_id: " .mysqli_error()); 
return $result;
}

function save_otrs_ticket_link($OTRS_link,$UserId,$mu_did_details_req_id)
{
global $dbLink;
 $query="UPDATE `did_member_details` SET `otrs_ticket_link`='{$OTRS_link}' WHERE `mu_did_details_req_id`={$mu_did_details_req_id} AND `mu_id`={$UserId}";
$result=mysqli_query($dbLink, $query) or die("Query failed save_otrs_ticket_link: " .mysqli_error()); 
return $result;
}

function save_OTRS_create_date($OTRS_create_date,$UserId,$mu_did_details_req_id)
{
global $dbLink;
 $query="UPDATE `did_member_details` SET `otrs_ticket_date`='{$OTRS_create_date}' WHERE `mu_did_details_req_id`={$mu_did_details_req_id} AND `mu_id`={$UserId}";
$result=mysqli_query($dbLink, $query) or die("Query failed save_OTRS_create_date: " .mysqli_error()); 
return $result;
}

function save_didww_response($didww_response,$UserId,$mu_did_details_req_id)
{
global $dbLink;
 $query="UPDATE `did_member_details` SET `didww_response`='{$didww_response}' WHERE `mu_did_details_req_id`={$mu_did_details_req_id} AND `mu_id`={$UserId}";
$result=mysqli_query($dbLink, $query) or die("Query failed save_OTRS_create_date: " .mysqli_error()); 
if($result)
{
echo $didww_response;
return true;
}
else
{
echo $didww_response;
return false;
}
}
function save_sls_comment($sls_comment,$UserId,$mu_did_details_req_id)
{
global $dbLink;
 $query="UPDATE `did_member_details` SET `sls_comment`='{$sls_comment}' WHERE `mu_did_details_req_id`={$mu_did_details_req_id} AND `mu_id`={$UserId}";
$result=mysqli_query($dbLink, $query) or die("Query failed save_OTRS_create_date: " .mysqli_error()); 
return $result;

}

function update_did_status($i_account,$i_subscription_id,$i_did_number)
{
global $dbLink;
$query="update did_member_api_result_object set mu_did_details_req_id= '$mu_did_details_req_id' 
where i_account='$i_account' and i_subscription_id='$i_subscription_id' and i_did_number='$i_did_number' and `did_status`=1";
$result=mysqli_query($dbLink, $query) or die("Query failed update_did_status: " .mysqli_error()); 
return $result;
}

function getDidNumberOfSubscription($i_account_subscription,$order_id,$mu_did_details_req_id)
{
global $dbLink;
$query="SELECT dmaro.i_account_subscription,dmaro.did_number ,dmaro.`activation_date`AS activation_date,dmaro.`customer_billed_to_date` 
AS Billingdate,dmaro.`did_expire_date_gmt` AS expiredate,dmaro.did_period,dmaro.order_id,dmd.order_id AS OrderNumber ,dmaro.did_status AS did_status
,dmd.mu_did_details_req_id FROM did_member_api_result_object dmaro, `did_member_details` dmd 
WHERE dmaro.i_subscription_id=dmd.did_mu_prefered_i_subscription AND dmaro.i_subscription_id=dmd.did_mu_prefered_i_subscription 
AND dmaro.`mu_did_details_req_id`= dmd.`mu_did_details_req_id` AND dmd.did_mu_prefered_i_subscription = '{$i_account_subscription}' AND dmd.`order_id`='{$order_id}'  AND dmd.`mu_did_details_req_id`='{$mu_did_details_req_id}';";
$result = mysqli_query($dbLink, $query) or die("Query failed getDidNumberOfSubscription: " .mysqli_error()); 
return $result;
}

function udpateslsvaliddocsdate($mu_did_doc_id)
{
global $dbLink;
$query="UPDATE did_member_subscription_attachment SET sls_approve_doc_date=NOW() where mu_did_doc_id='$mu_did_doc_id'";
$result = mysqli_query($dbLink, $query) or die("Query failed udpateslsvaliddocsdate: " .mysqli_error()); 
return $result;
}

function udpateslsrejectdocsdate($mu_did_doc_id)
{
global $dbLink;
$query="UPDATE did_member_subscription_attachment SET `sls_reject_doc_date`=NOW() where mu_did_doc_id='$mu_did_doc_id'";
$result = mysqli_query($dbLink, $query) or die("Query failed udpateslsrejectdocsdate: " .mysqli_error()); 
return $result;
}
function sendEmailToUserCustomer($from,$To,$CustomerName,$userID,$country_name,$i_account,$did_number,$city_name)
		{

			global $config;
			global $trace;
			global $DataArray;
			global $Notification_value;
			global $dbLink;
			global $customer_email;
			
			  $query = "SELECT Date(activation_date) as activation_date,Date(customer_billed_to_date) as customer_billed_to_date, Date(did_expire_date_gmt) as did_expire_date_gmt,country_name,city_name FROM did_member_api_result_object WHERE did_number='$did_number' AND flag = 1 ";
			
			$result = mysqli_query($dbLink, $query);
			if(mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_object($result);
				$did_start_date = $row->activation_date;
				$did_end_date = $row->did_expire_date_gmt;
				$did_billto_date = $row->customer_billed_to_date;
				$countryname = $row->country_name;
				$cityname = $row->city_name;
			}
			
			$DataArray=array('CustomerName'=>$CustomerName,'city_name'=>$cityname,'country_name'=>$countryname,'did_number'=>$did_number, 'did_start_date'=>$did_start_date,'did_end_date'=>$did_end_date,'did_billto_date'=>$did_billto_date);
	
			$customer_email=$To;
			$Notification_value='Your-SoftCall-Phone-number-is-available';
			
			include("../Admin/sftcnotification.php");
		}
/*function sendEmailToUserCustomer($from,$To,$CustomerName,$userID,$country_name,$i_account,$did_number,$city_name)
		{
		global $config;global $trace;
		$country_name=$this->getCountryNamebycountryId($country_name);
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=windows-1256\r\n";
		$headers .= "From: " . $from . " \r\n";
		$headers .= "BCC: " . $config['EMAIL_DEVELOPMENT'] . " \r\n";
		$ReferrerName=$CustomerName;
		$ste_url_link="https://".$config['HOST'];
		//$title ="SoftCall Phone Number Verification";
		$subject = "Your SoftCall Phone number is available";
		$to = $To;
		$CustomerName = $CustomerName;//ucwords($this->fName . " " . $this->lName);
		$message =
		'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SoftCall - Your SoftCall Phone number is available</title>
<style type="text/css">
table td {
	border-collapse: collapse;
	font-family: Verdana;
}
.active font{
	color: #FF0000;
}
</style>
</head>
<body style="margin:0; padding:0; background:#e8e8e8;">
<table width="100%" bgcolor="#e8e8e8" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!--| Main table -->
    <table width="546" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#e8e8e8">
        <tr>
        <!-- start top empty row -->
          <td height="30" align="center" valign="middle">&nbsp;</td>
        <!-- end top empry row -->
		</tr>
        <!-- Start main row -->
          <td align="center" valign="middle" style="border:1px solid #53c2eb;">
          	<table border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" width="100%">
            <tr>
            	<!-- start logo row -->
            	<td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;"><a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank"><font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Email_temp/logo.gif" alt="SoftCall" /></font></td> 
                <!-- end logo row -->
			 </tr>
			<tr>
			  	<!-- start logo row -->
            	<td align="left" valign="middle" height="36" style="padding-left:17px;"><font color="#6d6d6d" face="Verdana" style="font-size:12px;">
						Dear <span class="sTitle">'.$CustomerName.'</span>,</td>
						</tr>
						<tr>
                  	<td  align="left" valign="middle" height="100" style="padding-left:17px; padding-bottom:10px; padding-top:10px;">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr><td valign="middle" align="left" style="padding-right:10px;"> 
				<font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
				
        We have verified the information provided and are happy to announce that your SoftCall Phone number is "'.$did_number.'" for '.$country_name ." ".$city_name.'and is activated. </br>
				</font>
				<br></br>
				<br></br>
        <font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
		<br> Thank you for using SoftCall. </br>
If you face any issue or need help, please contact our customer support by sending an e-mail to <a href="mailto:support@softcall.me">support@softcall.me</a> or by using our <a href="'.$ste_url_link.'/Home/index.php?emailLiveLink=true">live-help chat</a>.
            </td>
                          
<td valign="middle" align="right"><a target="_blank" title="Live help" href="http://tr.im/4q545"><img width="48" alt="Live Help" src="'.$ste_url_link.'/Engines/mailTemplate/sftc_images/live-help.gif"></a></td>
</tr></tbody></table> </td></tr><!-- end second row -->
            </tr>
             <tr>
            	<!-- start second row -->
            	<td  align="left" valign="middle" height="50" style="padding-left:17px; padding-bottom:10px;"><font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:24px;">Best Regards,<br />
<b>SoftCall Support <br></b></font><br/>
<img src="'.$ste_url_link.'/Email_temp/logo_signature.gif" alt="SoftCall" />
</td> 
                <!-- end second row -->
				
            </tr>
			
            <tr>
            	<!-- start third row -->
            	<td align="left" valign="top" style="padding-top:7px; padding-bottom:7px; border-top:1px solid #53c2eb;">
                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="94%">                    	
                        <tr>
                        	<!-- Start World MPC -->
                        <td align="left" valign="top" width="41%" style="padding-top:5px;">
                        	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                            	<tr>
                                <td align="left" valign="top" width="21%"><img src="'.$ste_url_link.'/Email_temp/un-icon.gif" alt="World MCP" /></td>
                                <td align="left" valign="top" width="79%">
                                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    	<tr><td align="left" valign="top"><font color="#004a90" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">SoftCall Unlimited World</font></td></tr>
                                        <tr><td align="left" valign="top"><font color="#333333" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">Unlimited for</font><font color="#25a4d3" face="Verdana" style="font-size:12px; mso-line-height-rule:exactly; line-height:17px;"><b> $30</b></td></tr>
                                        <tr><td align="left" valign="bottom" style="padding-top:3px;"><a href="'.$ste_url_link.'/Purchase/purchase.php" style="text-decoration:none;" title="Buy Now" target="_blank"><font color="#018494" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:17px;"><img src="'.$ste_url_link.'/Email_temp/arrow.gif" alt="" /> Buy Now</font></a></td></tr>
                                    </table>
                                </td>
                                </tr>
                            </table>
                        </td>
                        <!-- End World MPC -->
                        <td align="center" valign="top" width="42%" style="padding-top:5px;">
                        <!-- Start Europe MCP -->
                        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                            	<tr>
                                <td align="left" valign="top" width="21%"><img src="'.$ste_url_link.'/Email_temp/europe-icon.gif" alt="Europe MCP" /></td>
                                <td align="left" valign="top" width="79%">
                                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    	<tr><td align="left" valign="top"><font color="#004a90" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">SoftCall Unlimited Europe</font></td></tr>
                                        <tr>
                                          <td align="left" valign="top"><font color="#333333" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">Unlimited for</font><font color="#25a4d3" face="Verdana" style="font-size:12px; mso-line-height-rule:exactly; line-height:17px;"><b> $50</b></td></tr>
                                        <tr><td align="left" valign="bottom" style="padding-top:3px;"><a href="'.$ste_url_link.'/Purchase/purchase.php" style="text-decoration:none;" title="Buy Now" target="_blank"><font color="#018494" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:17px;"><img src="'.$ste_url_link.'/Email_temp/arrow.gif" alt="" /> Buy Now</font></a></td></tr>
                                    </table>
                                </td>
                                </tr>
                            </table>
                        <!-- End Europe MCP -->                        
                        </td>
                        <td align="center" valign="top" width="17%">
                        <!--start Free Test Calls -->
                        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                        <tr><td align="center" valign="top"><a href="'.$ste_url_link.'/Home/membership.php?mode=AddUser" title="Free Test Calls"><font color="#004a90" face="Verdana" style="font-size:13px; mso-line-height-rule:exactly; line-height:11px;"><img src="'.$ste_url_link.'/Email_temp/free-call.gif" alt="Free Test Call" /></font></a></td></tr>
                        <tr><td align="left" valign="top"><font color="#605d5d" face="Arial" style="font-size:10px; mso-line-height-rule:exactly; line-height:11px;">*Upon first sign-up</font></td></tr>
						</table>
                        <!--End Free Test Calls -->
                        </td>
                        </tr>
                    </table>	
                </td> 
                <!-- end third row -->
            </tr>
            
            
            
            <tr>
        <!-- Start Footer -->
              <td bgcolor="#45beeb" valign="middle" height="82"><table width="97%" cellspacing="0" cellpadding="0" border="0" align="right" >
                <tr>
                  <td height="26" align="left" valign="middle" width="83%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Support</b></font></td>
                  <td align="left" valign="middle" width="17%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Follow Us</b></font></td>
                  </tr>
                <tr>
                  <td width="444" align="left" valign="middle"><font face="Verdana" color="#12769b" style="font-size:11px; line-height:21px;"><a href="'.$ste_url_link.'/Home/forgotpassword.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Lost Password">Lost Password</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/index.php?emailLiveLink=true" style=" text-decoration:none; color:#12769b;" target="_blank" title="Live Help">Live Help</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/support_form.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Contact Us">Contact Us</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/membership.php?mode=UpdateUser" style=" text-decoration:none; color:#12769b;" target="_blank" title="Account Settings">Account Settings</a> <br/>
                    <a href="'.$ste_url_link.'/Home/material.php?id=112" style=" text-decoration:none; color:#12769b;" target="_blank" title="Privacy Policy">Privacy Policy</a> &nbsp;  | &nbsp; <a href="'.$ste_url_link.'/Home/material.php?id=111" style=" text-decoration:none; color:#12769b;" target="_blank" title="Terms Of Service">Terms Of Service </a></font></td>
                  <td align="left" width="100" valign="middle"><table width="96%" cellspacing="0" cellpadding="0" border="0" align="center" >
                    <tr>
                      <td width="43" height="42" align="left" valign="middle"><a href="http://www.facebook.com/SoftCall" style="text-decoration:none; color:#767373;" title="Facebook" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/fb.gif" alt="Facebook" border="0" width="26" height="26" /></font></a></td>
                      <td align="left" valign="middle"><a href="http://www.twitter.com/SoftCallME/" style="text-decoration:none;" title="Twitter" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/twitter.gif" alt="Twitter" border="0" width="26" height="26" /></font></a></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <!-- End Footer -->
            </tr>            
          	</table>
          </td>
        <!--- End Main Row -->  	
        </tr>
        <tr><td height="36" valign="middle" align="center"><font color="#6d6d6d" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:18px;">Copyright &copy; 2014 Cloud Communications & Computing Corp., All rights reserved.</font></td></tr>        
      </table>
      <!--end main table -->
      </td>
  </tr>
</table>
</body>
</html>';

		// Send Message
		$message = stripslashes(trim($message));
		
		if(preg_match("/win/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $headers);
		}
		elseif(preg_match("/linux/",strtolower(PHP_OS)))
		{
			mail($to, $subject, $message, $headers);
		}
		$trace->info("Referral Earned BONUS Credits announcement has been successfully sent to the Referrer's email!");
}
*/
	//Send email to SLS if Documents is required on DID Buy
	function sendEmailToSLS($from,$To,$CustomerName,$userID,$country_name,$i_account,$OrderID,$selectedPlanId,$cityname)
	{
			global $config;
			global $trace;
			global $DataArray;
			global $Notification_value;
			global $dbLink;
			global $customer_email;
			
			$query = "SELECT docs_required_description,minimum_period,country_name,city_name FROM `did_subscription_document_controller` WHERE i_subscription='$selectedPlanId'";
			$result = mysqli_query($dbLink, $query);
			if(mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_object($result);
				$SUBSCRIPTION_DURATION = $row->minimum_period." Month";
				$Subscription_Document_Requirements = $row->docs_required_description;
				$countryname = $row->country_name;
				$cityname = $row->city_name;
			}
			$username = $this->getMemberUsername($userID);
			
			$DataArray=array('CUSTOMERNAME'=>$CustomerName,'ACCOUNT_USERNAME'=>$username,'SUBSCRIPTION_COUNTRY'=>$countryname,'SUBSCRIPTION_CITY'=>$cityname, 'SUBSCRIPTION_DURATION'=>$SUBSCRIPTION_DURATION,'Subscription_Document_Requirements'=>$Subscription_Document_Requirements);
		
			$Notification_value='SoftCall-DID-Documents-Verification';
		
			include("../Admin/sftcnotification.php");
		
	}	

/*	function sendEmailToSLS($from,$To,$CustomerName,$userID,$country_name,$i_account,$OrderID,$selectedPlanId,$cityname)
	{
		global $config;global $trace;
		//$referrer->i_account=$i_account;
		$country_name=$this->getCountryNamebycountryId($country_name);
		$this->populateByID($userID);
		// Prepare headers for sending html mails
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=windows-1256\r\n";
		$headers .= "From: " . $from . " \r\n";
		$headers .= "BCC: " . $config['EMAIL_DEVELOPMENT'] . " \r\n";
		//$ReferrerNames = ucwords($referrer->fName . " " . $referrer->lName);
		$ReferrerNames = ucwords($this->fName . " " . $this->lName);
		$ReferrerName = $CustomerName;
		$ste_url_link="http://".$config['HOST'];
		
		$title ="SoftCall DID Documents Verification";
		$subject = "DID Documents Verification";
		$subject = "SoftCall-".$subject;
	//	$user_id='<a href="../Admin/member_user_details.php?id=64314">64314</a>'
		$to = $To;
			$message ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Your SoftCall account has successfully been replenished</title>
<style type="text/css">
table td {
	border-collapse: collapse;
	font-family: Verdana;
}
.active font{
	color: #FF0000;
}
</style>
</head>
<body style="margin:0; padding:0; background:#e8e8e8;">
<table width="100%" bgcolor="#e8e8e8" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!--| Main table -->
    <table width="546" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#e8e8e8">
        <tr>
        <!-- start top empty row -->
          <td height="30" align="center" valign="middle">&nbsp;</td>
        <!-- end top empry row -->
		</tr>
        <!-- Start main row -->
          <td align="center" valign="middle" style="border:1px solid #53c2eb;">
          	<table border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" width="100%">
            <tr>
            	<!-- start logo row -->
            	<td bgcolor="#ecfbfc" align="left" valign="middle" height="86" style="padding-left:17px; border-bottom:1px solid #53c2eb;"><a href="'.$ste_url_link.'/Home/index.php" style="text-decoration:none;" title="SoftCall" target="_blank"><font color="#55b454" face="Verdana" size="5"><img src="'.$ste_url_link.'/Images/logo.gif" alt="SoftCall" /></font></td> 
                <!-- end logo row -->
            </tr>
            <tr>
            	<!-- start logo row -->
            	<td align="left" valign="middle" height="36" style="padding-left:17px;"><font color="#6d6d6d" face="Verdana" style="font-size:12px;"> Dear <b> Second Line Supportt </b>,</font></td>
                <!-- end logo row -->
            </tr>
            <!-------------------- start second row --------------------------->  
            <tr>
            
            	<td  align="left" valign="middle" height="200" style="padding-left:17px; padding-bottom:10px; padding-top:10px;">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr><td valign="middle" align="left" style="padding-right:10px;"> 
				<font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
				<p align="left">Could you pelase verify the provided information by <b><a href="'.$ste_url_link.'/BackOffice/Admin/member_user_index.php?id='.$userID.'">'.$ReferrerName.'</a></b> whose account is: '.
				$this->username.'
				  as he would like to buy a SoftCall Number for '. $country_name." ".$cityname.'.</p> 
				  <p> Please verify that the proof of ID corresponds to the personal information that the user has provided:
				 '.$ReferrerName.' is the same</p>
				 <p> Please verify that the proof of address (the utility bills) corresponds to the personal information that the user
					 has provided: First Name, Last Name,and Address is the same on the Personal Information provided by the user</p>
				<p> Please verify that the amount of  proofs corresponds to the requested amount specific to that country / DID 
					(sometimes it is one utility bill sometimes two) <br>Please verify that the address location corresponds to the DID 
requirements: sometimes it needs to be the same town of the DID town request sometimes it needs to be somewhere in the world</br>
<br> Please verify that the utility provider exists (search the internet to find if the utility provider exists and really provides 
a utility service)
</br> <br> Please verify that the address of the user really exists in the country / town (search for it)</br>
</p>
				</font>
				<br />
    <font face="Verdana" color="#6d6d6d" style="font-size:12px; line-height:18px;">
	
	Thank you for using SoftCall.<br />
<b><font  color="#44bdea">Soft</font><font  color="#004a90">Call</font> Sales team</b></font><br>
<img width="108" vspace="8" src="'.$ste_url_link.'/Images/logo.gif" alt="SoftCall">
            </td>
                          
<td valign="middle" align="right"><a target="_blank" title="Live help" href="http://tr.im/4q545"><img width="48" alt="Live Help" src="'.$ste_url_link.'/Engines/mailTemplate/sftc_images/live-help.gif"></a></td>
</tr></tbody></table> </td></tr>

<!--- end second row ----->


            <tr>
            	<!-- start third row -->
            	<tr>
            	<!-- start third row -->
            	<td align="left" valign="top" style="padding-top:7px; padding-bottom:7px; border-top:1px solid #53c2eb;">
                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="94%">                    	
                        <tr>
                        	<!-- Start World MPC -->
                        <td align="left" valign="top" width="41%" style="padding-top:5px;">
                        	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                            	<tr>
                                <td align="left" valign="top" width="21%"><img src="'.$ste_url_link.'/Email_temp/un-icon.gif" alt="World MCP" /></td>
                                <td align="left" valign="top" width="79%">
                                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    	<tr><td align="left" valign="top"><font color="#004a90" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">SoftCall Unlimited World</font></td></tr>
                                        <tr><td align="left" valign="top"><font color="#333333" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">Unlimited for</font><font color="#25a4d3" face="Verdana" style="font-size:12px; mso-line-height-rule:exactly; line-height:17px;"><b> $30</b></td></tr>
                                        <tr><td align="left" valign="bottom" style="padding-top:3px;"><a href="'.$ste_url_link.'/Purchase/purchase.php" style="text-decoration:none;" title="Buy Now" target="_blank"><font color="#018494" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:17px;"><img src="'.$ste_url_link.'/Email_temp/arrow.gif" alt="" /> Buy Now</font></a></td></tr>
                                    </table>
                                </td>
                                </tr>
                            </table>
                        </td>
                        <!-- End World MPC -->
                        <td align="center" valign="top" width="42%" style="padding-top:5px;">
                        <!-- Start Europe MCP -->
                        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                            	<tr>
                                <td align="left" valign="top" width="21%"><img src="'.$ste_url_link.'/Email_temp/europe-icon.gif" alt="Europe MCP" /></td>
                                <td align="left" valign="top" width="79%">
                                	<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    	<tr><td align="left" valign="top"><font color="#004a90" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">SoftCall Unlimited Europe</font></td></tr>
                                        <tr>
                                          <td align="left" valign="top"><font color="#333333" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:11px;">Unlimited for</font><font color="#25a4d3" face="Verdana" style="font-size:12px; mso-line-height-rule:exactly; line-height:17px;"><b> $50</b></td></tr>
                                        <tr><td align="left" valign="bottom" style="padding-top:3px;"><a href="'.$ste_url_link.'/Purchase/purchase.php" style="text-decoration:none;" title="Buy Now" target="_blank"><font color="#018494" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:17px;"><img src="'.$ste_url_link.'/Email_temp/arrow.gif" alt="" /> Buy Now</font></a></td></tr>
                                    </table>
                                </td>
                                </tr>
                            </table>
                        <!-- End Europe MCP -->                        
                        </td>
                        <td align="center" valign="top" width="17%">
                        <!--start Free Test Calls -->
                        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                        <tr><td align="center" valign="top"><a href="'.$ste_url_link.'/Home/membership.php?mode=AddUser" title="Free Test Calls"><font color="#004a90" face="Verdana" style="font-size:13px; mso-line-height-rule:exactly; line-height:11px;"><img src="'.$ste_url_link.'/Email_temp/free-call.gif" alt="Free Test Call" /></font></a></td></tr>
                        <tr><td align="left" valign="top"><font color="#605d5d" face="Arial" style="font-size:10px; mso-line-height-rule:exactly; line-height:11px;">*Upon first sign-up</font></td></tr>
						</table>
                        <!--End Free Test Calls -->
                        </td>
                        </tr>
                    </table>	
                </td> 
                <!-- end third row -->
            </tr>
            
            
            
            <tr>
        <!-- Start Footer -->
              <td bgcolor="#45beeb" valign="middle" height="82"><table width="97%" cellspacing="0" cellpadding="0" border="0" align="right" >
                <tr>
                  <td height="26" align="left" valign="middle" width="83%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Support</b></font></td>
                  <td align="left" valign="middle" width="17%"><font face="Verdana" color="#FFFFFF" style="font-size:13px;"><b>Follow Us</b></font></td>
                  </tr>
                <tr>
                  <td width="444" align="left" valign="middle"><font face="Verdana" color="#12769b" style="font-size:11px; line-height:21px;"><a href="'.$ste_url_link.'/Home/forgotpassword.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Lost Password">Lost Password</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/index.php?emailLiveLink=true" style=" text-decoration:none; color:#12769b;" target="_blank" title="Live Help">Live Help</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/support_form.php" style=" text-decoration:none; color:#12769b;" target="_blank" title="Contact Us">Contact Us</a> &nbsp; | &nbsp; <a href="'.$ste_url_link.'/Home/membership.php?mode=UpdateUser" style=" text-decoration:none; color:#12769b;" target="_blank" title="Account Settings">Account Settings</a> <br/>
                    <a href="'.$ste_url_link.'/Home/material.php?id=112" style=" text-decoration:none; color:#12769b;" target="_blank" title="Privacy Policy">Privacy Policy</a> &nbsp;  | &nbsp; <a href="'.$ste_url_link.'/Home/material.php?id=111" style=" text-decoration:none; color:#12769b;" target="_blank" title="Terms Of Service">Terms Of Service </a></font></td>
                  <td align="left" width="100" valign="middle"><table width="96%" cellspacing="0" cellpadding="0" border="0" align="center" >
                    <tr>
                      <td width="43" height="42" align="left" valign="middle"><a href="http://www.facebook.com/SoftCall" style="text-decoration:none; color:#767373;" title="Facebook" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/fb.gif" alt="Facebook" border="0" width="26" height="26" /></font></a></td>
                      <td align="left" valign="middle"><a href="http://www.twitter.com/SoftCallME/" style="text-decoration:none;" title="Twitter" target="_blank"><font face="Verdana" color="#12769b" size="2"><img src="'.$ste_url_link.'/Email_temp/twitter.gif" alt="Twitter" border="0" width="26" height="26" /></font></a></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <!-- End Footer -->
            </tr>            
          	</table>
          </td>
        <!--- End Main Row -->  	
        </tr>
        <tr><td height="36" valign="middle" align="center"><font color="#6d6d6d" face="Verdana" style="font-size:11px; mso-line-height-rule:exactly; line-height:18px;">Copyright &copy; 2014 Cloud Communications & Computing Corp., All rights reserved.</font></td></tr>        
      </table>
      <!--end main table -->
      </td>
  </tr>
</table>
</body>
</html>
'; 

		// Send Message
		$message = stripslashes(trim($message));
		if(preg_match("/win/",strtolower(PHP_OS)))
		{
			$msg = new email();
			$msg->send_mail(0, $to, $subject, $message, $headers);
		}
		elseif(preg_match("/linux/",strtolower(PHP_OS)))
		{
			mail($to, $subject, $message, $headers);
		}
		$trace->info("Email Notificatio to SLS for app ");
	
	}
*/
function update_did_member_api_result_object_auto_renewal_counter($mu_did_details_req_id,$counter)
  {
  global $dbLink;
  $query="Update did_member_api_result_object SET auto_renewal_counter = `auto_renewal_counter` +'$counter' where `mu_did_details_req_id`=$mu_did_details_req_id";
  	$result = mysqli_query($dbLink, $query);
  	return $result;
  }
  
  function getdidstatusvalues($mu_Id)
  {
  global $dbLink;
 $query="SELECT *,dmd.`order_id` AS SOFTCALL_ORDER_ID ,dmapi.`did_status` AS did_respose_value FROM `did_member_details` dmd ,`did_member_subscription_attachment` dmsa ,`did_subscription_document_controller` dsdc,`did_member_api_result_object`  dmapi  WHERE dmd.`mu_id`='$mu_Id'  AND dmd.`mu_did_doc_id`=dmsa.`mu_did_doc_id` AND dmd.`did_mu_prefered_i_subscription`=dsdc.`i_subscription` AND dmd.did_status='5' AND dmapi.`did_status`=-1
 AND dmd.`mu_did_details_req_id`=dmapi.`mu_did_details_req_id`";
  	$result = mysqli_query($dbLink, $query);
  	return $result;
  }
  
  function sendEmailToUserCustomeronCancelDID($from,$To,$CustomerName,$userID,$i_account,$did_number)
		{
			global $config;
			global $trace;
			global $DataArray;
			global $Notification_value;
			global $dbLink;
			global $customer_email;
			$ste_url_link="http://".$config['HOST'];
			$sftcDbObj  =   DbDidAccess::connect()->withSftcMaster();
			$query = "SELECT Date(activation_date) as activation_date,Date(customer_billed_to_date) as customer_billed_to_date, Date(did_expire_date_gmt) as did_expire_date_gmt,Date(finish_date) as finish_date,country_name,city_name FROM did_member_api_result_object WHERE did_number='$did_number'";
			
            $datanotification   =   $sftcDbObj->select($query);
			 if ($datanotification){
				
				$did_start_date = $datanotification[0]['activation_date'];
				$did_end_date = $datanotification[0]['did_expire_date_gmt'];
				$did_billto_date = $datanotification[0]['customer_billed_to_date'];
				$did_finish_date = $datanotification[0]['finish_date'];
				$countryname = $datanotification[0]['country_name'];
				$cityname =$datanotification[0]['city_name'];
				$DataArray=array('CustomerName'=>$CustomerName,'SUBSCRIPTION_CITY'=>$cityname,'SUBSCRIPTION_COUNTRY'=>$countryname,'DID_Number'=>$did_number,'finish_date'=>$did_finish_date,'link'=>$ste_url_link."/Account/subscription_plans.php");
				$customer_email=$To;
				$Notification_value='SoftCall-DID-Early-Customer-Cancellation';
				include_once($_SERVER['DOCUMENT_ROOT']."Home/sftcnotification.php");
				echo "Mail sent";
				}  
			else {
				echo "Mail Not sent";
			}	
			
		}

		// This function is used get all plans of user
	function getUserPlan($username)
	{
		global $dbLink;

		$this->populateByUsername($username);

		// last 120 days date
		$last_date = date('Y-m-d H:i:s', strtotime(" - 120 days"));
		$current_date = date('Y-m-d H:i:s');

		 $query  =
		"
				Select MP.UserPlanID, MP.PlanID, MP.StartDate, MP.EndDate, MP.i_product,
				PP.Name PlanName,  PP.Minutes TotalMinutes, MP.i_vd_plan, PP.Description, PP.PackageID, PP.Price
				From member_plan MP
				Inner Join package_plan PP On MP.PlanID = PP.PlanID
				Where MP.UserID = " . $this->id . "
				AND MP.StartDate >= '$last_date' 
				Order By PlanName, MP.StartDate DESC
				"; //AND MP.EndDate <= '$current_date' 
		$result = mysqli_query($dbLink, $query);
		return $result;
	}		
	
	function getDIDMembersCount($Criteria)
	{
	
		
		global $dbLink;

		$query  = "Select COUNT(*) From `member_user` mu inner join (did_member_details dmd INNER JOIN  did_member_api_result_object dmaro ON dmd.mu_did_details_req_id=dmaro.mu_did_details_req_id)  ON mu.mu_ID = dmd.mu_id ";
		// $query .= "Where 1=1 And (mu_ParentID Is NULL OR mu_ParentID = 0) ";
		$query .= "Where 1=1 ";

		// Member User ID ------------------------------------------------------------
		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(dmaro.activation_date, '%Y-%m-%d') >= '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(dmaro.activation_date, '%Y-%m-%d') <= '" . $Criteria['ToDate'] . "') ";
		}
				
		if($Criteria['MemberID'] != '')
		{
			$query .= "And (mu.mu_ID = " . $Criteria['MemberID'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['Username'] != '')
		{
			$query .= "And (mu.mu_Username Like '%" . $Criteria['Username'] . "%') ";
		}
		// Keyword ------------------------------
		
		if($Criteria['Did_type'] != '')
		{
				if ($Criteria['Did_type']=="8"){
					$query .= "And (dmaro.did_restore_flag =1) ";
				} else {
					$query .= "And (dmaro.flag Like '%" . $Criteria['Did_type'] . "%') ";
				}
		}
		
		if($Criteria['Didnumber'] != '')
		{
			$query .= "And (dmaro.did_number Like '%" . $Criteria['Didnumber'] . "%') ";
		}
		
		if($Criteria['CountryID'] != '')
		{
			$query .= "And (dmaro.country_iso = '" . $Criteria['CountryID'] . "') ";
		}
		
		
		// echo $query;
		// echo "<br>";
		//$result = mysqli_result(mysqli_query($dbLink, $query), 0, 0);

		//return $result;
		$result = mysqli_query($dbLink, $query);
		mysqli_data_seek($result, 0);
		$row = mysqli_fetch_array($result);
		return $row[0];
	}

function getDIDMembers($Criteria, $SortField, $SortType, $Start = -1, $Num = -1)
	{
		global $dbLink;
		global $lang;
		
		$query  = "Select mu.mu_ID MemberID,mu.mu_FName FName, mu.mu_MName MName, mu. mu_LName LName, mu.i_account,concat(mu_FName,' ',mu_MName,' ',mu_LName) CustomerName, mu.mu_Username UserName, ";
		$query .= " dmaro.did_number DidNumber, dmaro.activation_date ActivationDate, dmaro. i_subscription_id SUBSCRIPTIONID,dmaro.customer_billed_to_date BillToDate, dmaro.did_expire_date_gmt EndDate,";
		$query .= " dmaro.flag Status,dmaro.did_restore_flag Restored,dmd.order_id orderID,dmd.refrence_order_id reforderID,dmd.mu_did_details_req_id mu_did_details_req_id ";
		$query .= "From `member_user` mu inner join (did_member_details dmd INNER JOIN  did_member_api_result_object dmaro ON dmd.mu_did_details_req_id=dmaro.mu_did_details_req_id)  ON mu.mu_ID = dmd.mu_id ";
		// $query .= "Where 1=1 And (mu_ParentID Is NULL OR mu_ParentID = 0) ";
		//$query .= "Where 1=1 ";


		if($Criteria['FromDate'] != '') {
			$query .= "And (DATE_FORMAT(dmaro.activation_date, '%Y-%m-%d') >= '" . $Criteria['FromDate'] . "') ";
		}
		if($Criteria['ToDate'] != '') {
			$query .= "And (DATE_FORMAT(dmaro.activation_date, '%Y-%m-%d') <= '" . $Criteria['ToDate'] . "') ";
		}
				
		if($Criteria['MemberID'] != '')
		{
			$query .= "And (mu.mu_ID = " . $Criteria['MemberID'] . ") ";
		}
		// Username ------------------------------------------------------------
		if($Criteria['Username'] != '')
		{
			$query .= "And (mu.mu_Username Like '%" . $Criteria['Username'] . "%') ";
		}
		// Keyword ------------------------------
		
		if($Criteria['Did_type'] != '')
		{
				if ($Criteria['Did_type']=="8"){
					$query .= "And (dmaro.did_restore_flag =1) ";
				} else {
					$query .= "And (dmaro.flag Like '%" . $Criteria['Did_type'] . "%') ";
				}
		}
		
		if($Criteria['Didnumber'] != '')
		{
			$query .= "And (dmaro.did_number Like '%" . $Criteria['Didnumber'] . "%') ";
		}
		
		if($Criteria['CountryID'] != '')
		{
			$query .= "And (dmaro.country_iso = '" . $Criteria['CountryID'] . "') ";
		}
		
	
		$query .= "Order By $SortField $SortType ";
		// Limit Results
		if (($Start!=-1) && ($Num!=-1)) {
			$query .= "Limit $Start,$Num";
		}

		// echo($query);

		$result = mysqli_query($dbLink, $query);
		return $result;
	}

	
	function sendCouponEmailToCustomer($from,$To,$CustomerName,$order_id,$coup_code,$disc_val,$max_use, $valid_upto, $username, $total_discount_num)
		{

			global $config;
			global $trace;
			global $DataArray;
			global $Notification_value;
			global $dbLink;
			global $customer_email;
						
			$DataArray=array('CustomerName'=>$CustomerName,'order_id'=>$order_id,'coup_code'=>$coup_code,'disc_val'=>$disc_val, 'max_use'=>$max_use,'valid_upto'=>$valid_upto,'username'=>$username,'total_discount_num'=>$total_discount_num);
						
			$customer_email=$To;
			$Notification_value='Softcall-Discount-Coupon-Code';
			
			include("../Admin/sftcnotification.php");
		}

	// This function is used get all the unexpired plans of user
	// defined on 21st July, 2016 for extend mcp validity feature
	function getUserActivePlans($username)
	{
		global $dbLink;

		$this->populateByUsername($username);

		// last 120 days date
		$last_date = date('Y-m-d H:i:s', strtotime(" - 120 days"));
		$current_date = date('Y-m-d H:i:s');

		 $query  =
		"
				Select MP.UserPlanID, MP.PlanID, MP.StartDate, MP.EndDate, MP.i_product,
				PP.Name PlanName,  PP.Minutes TotalMinutes, MP.i_vd_plan, PP.Description, PP.PackageID, PP.Price
				From member_plan MP
				Inner Join package_plan PP On MP.PlanID = PP.PlanID
				Where MP.UserID = " . $this->id . "
				AND MP.Expired = '0' 
				Order By PlanName, MP.StartDate DESC
				"; //AND MP.EndDate <= '$current_date' 
		$result = mysqli_query($dbLink, $query);
		return $result;
	}
}

?>