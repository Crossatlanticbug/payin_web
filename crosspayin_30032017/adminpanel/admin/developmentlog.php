<?php
error_reporting(E_ALL);
ini_set("display_errors",1);
	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Development Log List";;

	global $logged_in;
	if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	$SessionPrefix = 'DevelopmentLog';

	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	
	$Criteria['FromDate']		     = LoadSessionValue('logFromDate', 'FromDate', $SessionPrefix, '');
	$Criteria['ToDate'] 		     = LoadSessionValue('logToDate', 'ToDate', $SessionPrefix, '');
	$Criteria['EmailID'] = (!empty($_GET['EmailID'])) ? $_GET['EmailID'] : LoadSessionValue('logEmailSearch', 'Emailid', $SessionPrefix, '');
	$Criteria['ID']  	     = LoadSessionValue('logID', 'ID', $SessionPrefix, '');
	
	$Criteria['UserIP']   = (!empty($_GET['UserIP'])) ? $_GET['UserIP'] : LoadSessionValue('logUserIP', 'UserIP', $SessionPrefix, '');
	// Search Criteria
	$Criteria['MobileNo'] = (!empty($_GET['MobileNo'])) ? $_GET['MobileNo'] : LoadSessionValue('logMobileNo', 'MobileNo', $SessionPrefix, '');
	
	$SortFieldDefault = "ID"; $SortTypeDefault = "DESC";

	// Order By
	$ItemsPerPage = LoadSessionValue('cmbItemsPerPage', 'ItemsPerPage', $SessionPrefix, "40");
	$SortField = (isset($_POST['cmbSortField']) ? $_POST['cmbSortField'] : $SortFieldDefault);
	$SortType	 = (isset($_POST['rdSortType']) ? $_POST['rdSortType'] : $SortTypeDefault);

	

	// Date timestamp used for the Excel File Exporting Name
	$Timestamp = date('YmdHis');
?>





<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="export" value="1">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Developmemnt Log List</td>
				</tr>
				<tr>
					<td valign="top" style="padding-top:10px;">
						<table border="0" cellpadding="3" cellspacing="0" width="380" class="table">
				  		<tr>
							  <td class="boxHeader">Filter Result</td>
							</tr>
							<tr>
								<td>
									<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="Silver">
										<tr>
											<td class="boxCaption" >Process date</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td>From</td>
														<td><input type="text" id="logFromDate" name="logFromDate" value="<?=$Criteria['FromDate'];?>" class="textbox" style="width:80px;text-align:center;" readonly></td>
														<td width="2"></td>
														<td valign="middle"><img id="imgFromDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
														<td width="5"></td>
														<td width="15">To</td>
														<td><input type="text" id="logToDate" name="logToDate" value="<?=$Criteria['ToDate'];?>" class="textbox" style="width:80px;text-align:center;" readonly></td>
														<td width="2"></td>
														<td valign="middle"><img id="imgToDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="CustomerRow">
											<td class="boxCaption">Email ID</td>
											<td class="boxContents">
												<input type="text" name="logEmailSearch" id="logEmailSearch" autocomplete="off" value="<?=$Criteria['EmailID']?>">
												
											</td>
										</tr>
										<tr id="IDRow">
											<td class="boxCaption" width="90">MobileNo</td>
											<td class="boxContents">
												<input type="text" name="logID" value="<?=$Criteria['ID'];?>" class="textbox" style="width:80px;" onkeyup="NumbersOnly(logID);">
											</td>
										</tr>
										<tr>
											<td class="boxCaption">IP</td>
											<td class="boxContents">
												<input type="text" name="logUserIP" value="<?=$Criteria['UserIP'];?>" class="textbox" style="width:100%;">
											</td>
										</tr>
										<tr>
											<td class="boxCaption">SortBy</td>
											<td class="boxContents">
												<table border="0" cellpadding="1" cellspacing="0" height="100%">
													<tr>
														<td width="149">
															<select name='cmbSortField' size='1' style="width:140px;">
																<option value="ID" <? if ($SortField=="id") echo "SELECTED";?> id="IDOption">ID</option>
																<option value="emailid" <? if($SortField=="emailid") echo "SELECTED";?> id="UsernameOption">EmailID</option>
															</select>
														</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="ASC" <? if ($SortType=='ASC') echo "checked"; ?>></td>
														<td width="35">ASC</td>
														<td width="28" valign="bottom"><input type="radio" name="rdSortType" value="DESC" <? if ($SortType=='DESC') echo "checked"; ?>></td>
														<td width="35">DESC</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr class="boxContents">
											<td colspan="2" style="text-align:<?=$lang['RIGHT']?>;" >
												<table border="0" cellpadding="3" cellspacing="0">
													<tr>
														
														<td>
															<input type="button" name="btnReset" value="Reset" class="button" onclick="ResetdevelopmentlogSearchForm();">
														</td>
														<td><input type="submit" name="btnSearch" value="Search" class="button"></td>
													</tr>
												</table>
											</td>
										</tr>							
										
										
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? if ( (isset($_POST['btnSearch'])) || (isset($_POST['cmbTimezone'])) || (!empty($Criteria['EmailID'])) || (!empty($Criteria['ID'])) || (!empty($Criteria['MobileNo']))|| (!empty($Criteria['UserIP'])))  {  ?>
				<tr>
				  <td height="10"></td>
				</tr>
				<?
					// Preparing parameters for getList() method
					$developmentlog->SortField 	= $SortField;
					$developmentlog->SortType  	= $SortType;
					$developmentlog->Criteria  	= $Criteria;
					
				?>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid Gray;">
							<?
							$result = $developmentlog->getList();
							$total_records = mysqli_num_rows($result);
							//$total_records -= 1; // Skip first row because it contains the fields headers

							if ($total_records > 0)
							{
						  	// Used for pagination
								$perpage = $ItemsPerPage;
								if (!isset($_GET['screen']))
								{
									$_SESSION[$SessionPrefix.'_Screen'] = (!isset($_SESSION[$SessionPrefix.'_Screen'])) ? 0 : $_SESSION[$SessionPrefix.'_Screen'];
								}
								else
								{
									$_SESSION[$SessionPrefix.'_Screen'] = $_GET['screen'];
								}
								$pages = ceil($total_records/$perpage);
								if ($_SESSION[$SessionPrefix.'_Screen'] >= $pages) $_SESSION[$SessionPrefix.'_Screen'] = 0;
								$screen  = $_SESSION[$SessionPrefix.'_Screen'];
								$start   = $screen*$perpage;
								$numItems = ($screen < $pages-1) ? ($start+$perpage) : $total_records;
								$developmentlog->Start = $start;
								$developmentlog->Num = $perpage;
								$result = $developmentlog->getList();

								
?>
								
						
							<tr>
								<td style="padding-bottom: 0px;">
									<table border="0" cellpadding="3" cellspacing="0" width="100%">
										<tr>
											<td width="1%" style="white-space: nowrap;">Items Per Page
												<select name='cmbItemsPerPage' size='1' style="width:40px;" onchange="submitForm();">
													<option value="5"  <?if ($ItemsPerPage=="5")  echo "SELECTED";?>>5</option>
													<option value="10" <?if ($ItemsPerPage=="10") echo "SELECTED";?>>10</option>
													<option value="20" <?if ($ItemsPerPage=="20") echo "SELECTED";?>>20</option>
													<option value="40" <?if ($ItemsPerPage=="40") echo "SELECTED";?>>40</option>
													<option value="100" <?if ($ItemsPerPage=="100") echo "SELECTED";?>>100</option>
												</select>
											</td>
											<? if ($total_records > $perpage) { ?>
									    <td>
									      <? Pagination($screen, 5, $pages, basename($_SERVER["SCRIPT_NAME"])); ?>
											</td>
											<? } ?>
											<td style="text-align:<?=$lang['RIGHT']?>;">
												<? echo "Displaying page ".($screen+1)." in ".$pages.", items ".($start+1)."-".$numItems." Of ".$total_records;?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
				  		<tr>
						
						
				  			<td>
									<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
										<tr>
											<td class="gridHeader" width="8%">ID</td>
											<!--<td class="gridHeader" width="8%">PaymentLog</td>
											<td class="gridHeader" width="7%">RechargeLog</td>-->
											<td class="gridHeader" width="13%">MobileNo</td>
											<td class="gridHeader" width="13%">EmailId</td>
											<td class="gridHeader" width="13%">ProcessDate</td>
											<td class="gridHeader" width="13%">UserIP</td>
											<td class="gridHeader" width="13%">PageURL</td>
										</tr>
										<?
									  	$i=1;

											while ($row=mysqli_fetch_object($result))
											{
												
													
											$class = ($i%2==0) ? "gridItem" : "gridAltItem" ;
												$color = ($i%2==0) ? "#EBEBEB" : "#F2F2F2" ;
													
										?>
										<tr class="<?=$class?>" onMouseover="this.style.backgroundColor='#EEE2C8'" onMouseout="this.style.backgroundColor='<?=$color?>'" >
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->id ?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->mobileno?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->emailid?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->processdate?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->userip?></td>
											<td style="text-align:center;color:<?=$StatusColor?>;"><?=$row->pageurl?></td>
										</tr>
									<?	
											$i++;
										}
									?>
									</table>
								</td>
							</tr>
							<? } else { ?>
							<tr>
				  			<td>
				  				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="grid">
				  					
										<tr>
											<td class="gridHeader" width="8%">ID</td>
											<td class="gridHeader" width="8%">PaymentLog</td>
											<td class="gridHeader" width="7%">RechargeLog</td>
											<td class="gridHeader" width="13%">MobileNo</td>
											<td class="gridHeader" width="13%">EmailId</td>
											<td class="gridHeader" width="13%">ProcessDate</td>
											<td class="gridHeader" width="13%">UserIP</td>
											<td class="gridHeader" width="13%">PageURL</td>
										</tr>
									
										<tr>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">-----</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
											<td class="gridEmpty">---------</td>
										</tr>
									</table>
								</td>
							</tr>			</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							
							
						</table>
					</td>
				</tr>
				<? }
			}?>
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">//<![CDATA[
	var cal = Calendar.setup({
  	onSelect: function(cal) { cal.hide() }
  });
  cal.manageFields("imgFromDate", "logFromDate", "%Y-%m-%d");
  cal.manageFields("imgToDate", "logToDate", "%Y-%m-%d");
//]]></script>

<? End_Response(); ?>