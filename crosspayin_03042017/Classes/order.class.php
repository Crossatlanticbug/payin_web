<?
error_reporting(E_ALL);
ini_set("display_errors",1);

// Class Definition
class order
{

	function __construct($OrderID = -1)
	{
		if ((!empty($OrderID)) && ($OrderID != -1))
		{
			$this->OrderID = $OrderID;
			$this->getRechargeOrderInfo();
		}
	}

	/*
	 * Retrieves order information
	*/
	function getRechargeOrderInfo($OrderID)
	{
		global $dbLink;

		$query  = "Select * From `rechargeorder` Where orderid = '" . $OrderID . "'";

		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		
		$this->mobileno = $row->mobileno;
		$this->operator = $row->operator;
		$this->mobiletype = $row->mobiletype;
		$this->amount = $row->amount;
		$this->transactionid = $row->transactionid;
		$this->rechargedate = $row->rechargedate;
		$this->payment = $row->payment;
		$this->paymentstatus = $row->paymentstatus;
		$this->userip = $row->userip;
		return $row;
	}

	/*
	 * Checks if a certain order exists or not
	*/
	function isExist()
	{
		global $dbLink;

		$query = "Select OrderID From `order` Where OrderID = '" . $this->OrderID . "'";
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0) {
			return true;
		}
		return false;
	}

	

	// This function is used to a insert a new order
	function AddRechargeOrder($rechargevalue)
	{
		global $dbLink;
			
		$query  = "Insert Into `rechargeorder` (`name`,`email`,`mobileno`,`operatorid`,`mobiletype`,`amount`,`rechargetype`,`payment`,`paymentstatus`,`userip`,rechargestatus,customerid,custacc,custno) ";
		$query .= "Values ('".$rechargevalue->name."','".$rechargevalue->email."','".$rechargevalue->mobileno."', '".$rechargevalue->operatorid."', '".$rechargevalue->payfor."', "; 
		$query .= "'".$rechargevalue->amount."','".$rechargevalue->rechargetype."',  '".$rechargevalue->payment."',  '".$rechargevalue->paymentstatus."', ";
		$query .= "'".$rechargevalue->userip."','".$rechargevalue->rechargestatus."','".$rechargevalue->customerid."','".$rechargevalue->custacc."','".$rechargevalue->custno."' )";
	//	exit($query);
		mysqli_query($dbLink, $query);
		return mysqli_insert_id($dbLink);
	}

	function UpdateRechargeOrder($rechargevalue,$orderid)
	{
		global $dbLink;
			
		$query  = "update `rechargeorder` set ".$rechargevalue." where orderid=  ".$orderid;
		//exit($query);
		mysqli_query($dbLink, $query);
		
	}

	function AddDevelopmentLog($logvalue)
	{
		global $dbLink;
		
		$query  = "Insert Into `development_log` (`paymentlog`,`rechargelog`,`mobileno`,`emailid`,`userip`,`pageurl`) ";
		$query .= "Values ('".$logvalue->paymentlog."','".$logvalue->rechargelog."','".$logvalue->mobileno."', '".$logvalue->emailid."', "; 
		$query .= "'".$logvalue->userip."',  '".$logvalue->pageurl."')";
	//	exit($query);
		mysqli_query($dbLink, $query);
		return mysqli_insert_id($dbLink);
	}
	
	function UpdateDevelopmentLog($logvalue,$logid)
	{
		global $dbLink;
		
		$query  = "update `development_log` set ".$logvalue." where id='". $logid."'";

		mysqli_query($dbLink, $query);
	
	}
	
	function AddMoney($addmoneyvalue)
	{
		global $dbLink;
			
		$query  = "Insert Into `addmoney` (`customerid`,`amount`,`payment`,`paymentstatus`,`transactionid`,`userip`) ";
		$query .= "Values ('".$addmoneyvalue->customerid."','".$addmoneyvalue->amount."','".$addmoneyvalue->payment."',";
		$query .= "'".$addmoneyvalue->paymentstatus."', '".$addmoneyvalue->transactionid."','".$addmoneyvalue->userip."' )";

		//exit($query);
		mysqli_query($dbLink, $query);
		return mysqli_insert_id($dbLink);
	}
	function getAddMoneyOrderInfo($OrderID)
	{
		global $dbLink;

		$query  = "Select am.amount AS amount,c.name AS name,c.email AS email,c.phone AS mobileno,c.balance as balance 
		FROM `addmoney` am INNER JOIN customer c ON am.customerid=c.id  Where am.id = '" . $OrderID . "'";
		
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		
		return $row;
		
	}
	
	function UpdateAddMoneyOrder($rechargevalue,$orderid)
	{
		global $dbLink;
			
		$query  = "update `addmoney` set ".$rechargevalue." where id=  ".$orderid;
		mysqli_query($dbLink, $query);
		
	}
	
	function UpdateBalance($amount,$userid)
	{
		global $dbLink;
			
		$query  = "update `customer` set balance = balance + ".$amount." where id=  '".$userid."'";
		mysqli_query($dbLink, $query);
		
	}
	
	function SendMoney($sendmoneyvalue)
	{
		global $dbLink;
			
		$query  = "Insert Into `sendmoney` (`frommobile`,`tomobile`,`amount`,`payment`,`transactionid`,`userip`,`paymentstatus`) ";
		$query .= "Values ('".$sendmoneyvalue->frommobile."','".$sendmoneyvalue->tomobile."','".$sendmoneyvalue->amount."',";
		$query .= "'".$sendmoneyvalue->payment."', '".$sendmoneyvalue->transactionid."','".$sendmoneyvalue->userip."','".$sendmoneyvalue->paymentstatus."' )";


		//exit($query);
		mysqli_query($dbLink, $query);
		return mysqli_insert_id($dbLink);
	}
	
	function updateSenderMoney($user,$amount)
	{
		global $dbLink;
			
		$query  = "update `customer` set balance = balance - ".$amount." where phone=  '".$user."'";
		mysqli_query($dbLink, $query);
		
	}
	function updateReceiverMoney($user,$amount)
	{
		global $dbLink;
			
		$query  = "update `customer` set balance = balance + ".$amount." where phone=  '".$user."'";
		mysqli_query($dbLink, $query);
		
	}
	
	function getsendmoney($phoneno)
	{
		global $dbLink;

		$query ="SELECT * FROM sendmoney WHERE frommobile='".$phoneno."' ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			return $result;
		}
		return false;
	}
	
	function getreceivemoney($phoneno)
	{
		global $dbLink;

		$query ="SELECT * FROM sendmoney WHERE tomobile='".$phoneno."' AND paymentstatus='success' ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			return $result;
		}
		return false;
	}
	
	function getrechargeinfo($id)
	{
		global $dbLink;

		$query ="SELECT * FROM rechargeorder WHERE customerid='".$id."' ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			return $result;
		}
		return false;
	}
	
	function Supportrechargeinfo($cond)
	{
		global $dbLink;

		$query ="SELECT * FROM rechargeorder WHERE ".$cond." ORDER BY rechargedate ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			return $result;
		}
		return false;
	}
	
	function getsenderorderinfo($senderid)
	{
		global $dbLink;

		$query ="SELECT * FROM sendmoney WHERE id='".$senderid."' AND paymentstatus='success' ";
	
		$result = mysqli_query($dbLink, $query);
		if (mysqli_num_rows($result)>0)
		{
			$row = mysqli_fetch_object($result);
				return $row;
		}
		return false;
	}
	
	function ExistOrder($OrderID)
	{
		global $dbLink;

		$query  = "SELECT * FROM rechargeorder WHERE orderid = '" . $OrderID . "'";
		
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		
		return $row;
		
	}
	
	function ExistAddOrder($OrderID)
	{
		global $dbLink;

		$query  = "SELECT * FROM addmoney WHERE id = '" . $OrderID . "'";
		
		$result = mysqli_query($dbLink, $query);
		$row = mysqli_fetch_object($result);
		
		return $row;
		
	}
	function UpdateVirtualBalance($objbal)
	{
		global $dbLink;
			
		// Get current date. Note that the date format for MySQL is Y-m-d (ex:2004-04-15)
		$creation_date = date("Y-m-d H:i:s");


		// Insert the information of the new created user into the database
		$query = "Insert Into `virtual_balance` (`amount`,`user`,`amounttype`,`comment`,`addedby`) ";
		$query .= "Values ('$objbal->amount','$objbal->user','$objbal->amounttype',";
		$query .= "'".$objbal->comment."','".$objbal->addedby."')";
		//echo $query;
		mysqli_query($dbLink, $query);
	}
	
	
}

?>