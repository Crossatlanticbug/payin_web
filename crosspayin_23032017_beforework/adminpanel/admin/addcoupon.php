<?php


 	include_once("../Includes/template.inc.php");

 	global $page_title;
	$page_title = "Coupon";
	
	global $logged_in;
  if ($logged_in == 0) {
  	$_SESSION['BO_PageTo'] = $_SERVER['SCRIPT_NAME'];
	  exit("<script>window.location.href='../main/index.php?err=ERR_NOT_LOGGEDIN';</script>");
	}

	// Include the member class
	require_once("../Classes/coupon.class.php");
	$coupon = new coupon();

	// Manage Page
	
	$id = (!empty($_GET['id'])) ? $_GET['id'] : ((!empty($_POST['id'])) ? $_POST['id'] : "");
	$name = ""; $username = ""; $password = ""; $enabled = "1";

	//------------------- Get user permissions on this page -------------------------------------------------------
	
	if ((isset($_POST['useraction'])) && ($_POST['useraction']=="AddEdit"))
	{
		$coupon_name = $_POST['txtCouponName'];
		$coupon_code = $_POST['txtCouponCode'];
		$amount = $_POST['txtAmount'];
		$start_date = $_POST['txtFromDate'];
		$end_date = $_POST['txtToDate'];
		$description = $_POST['txtDescription'];
		$coupon_location = $_POST['txtLocation'];
		$status = (isset($_POST['chkEnabled'])) ? "1" : "0";

		// Used in the old version
		 
		// $isAdmin = (isset($_POST['chkAdmin'])) ? "1" : "0";
		// $user = new member($name, $username, $email, $isAdmin, $enabled);

		$coupon = new coupon($coupon_name, $coupon_code, $amount, $start_date, $end_date, $description, $coupon_location, $status);

		// Edit Material Type
		if (!empty($id))
		{
			$member->updateUserInfo($id, $coupon);
		}
		// Add New Material Type
		else
		{
			$member->addNewCoupon($coupon);
		}
		exit("<script>window.location.href='../admin/coupon.php';</script>");
	}
	else
	{
		if (!empty($id))
		{
			// Check if Edit is allowed
			
			$result = $member->getCouponInfo($id);
			if (mysqli_num_rows($result)>0)
			{
				$row = mysqli_fetch_object($result);
				$coupon_name = stripslashes($row->Name);
				$coupon_code = stripslashes($row->Code);
				$amount = stripslashes($row->Amount);
				$start_date = stripslashes($row->StartDate);
				$end_date = stripslashes($row->EndDate);
				$description = stripslashes($row->Description);
				$coupon_location = stripslashes($row->Location);
				$status = $row->Enabled;

				// Used in the old version
				// $isAdmin = $row->IsAdmin;
			}
			$boxHeader = 	"Update Coupon";
		}
		else
		{
			// Check if Add is allowed
			$boxHeader = 	"Add New Coupon";
		}
	}

?>

<input type="hidden" name="id" value="<?=$id?>">

<table border="0" cellpadding="8" cellspacing="0" width="100%" height="100%">
  <tr>
    <td height="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				  <td valign="top" class="pageHeader">Manage Coupon</td>
				</tr>
				<tr>
				  <td height="30">
				  	<a href="user_index.php" class="link">Coupon List</a>
				  </td>
				</tr>
				<tr>
				  <td valign="top">
				  	<table border="0" cellpadding="5" cellspacing="0" width="100%" style="border:1px solid Gray;">
				  		<tr>
							<td class="boxHeader"><?=$boxHeader;?></td>
						</tr>
				  		<tr>
				  			<td>
									<table border="0" cellpadding="3" cellspacing="1" bgcolor="Silver">
										<tr class="boxCaption">
											<td>Name : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtCouponName" value="<?=$coupon_name?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Code : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtCouponCode" value="<?=$coupon_code?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Amount : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtAmount" value="<?=$amount?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>StartDate:  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" id="txtFromDate" name="txtFromDate" value="" style="height:18px;width:320px" class="textbox" ><img id="imgFromDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
										</tr>
										<tr class="boxCaption">
											<td>EndDate:  <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" id="txtToDate" name="txtToDate" value="" style="height:18px;width:320px" class="textbox" ><img id="imgToDate" border="0" src="../Calendar/calendar.gif" style="cursor:hand;"></td>
										</tr>
										<tr class="boxCaption">
											<td>Description : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtDescription" value="<?=$description?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxCaption">
											<td>Location : <span class="required">(*)</span></td>
										</tr>
										<tr class="boxContents">
											<td><input type="text" name="txtLocation" value="<?=$coupon_location?>" style="height:18px;width:320px" class="textbox"></td>
										</tr>
										<tr class="boxContents">
											<td>
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														
														<td width="28">Enabled:</td>
														<td width="30" align="center"><input type="Checkbox" name="chkEnabled" value="<?=$status?>" <?=($status)? "checked" : "";?>></td>
														<td height="20" align="RIGHT"><input type="button" name="btnCancel" value="Cancel" class="button" onclick="window.location.href='../Admin/coupon.php';"></td>
														<td width="4"></td>
														<td height="20" align="RIGHT" width="1"><input type="button" name="btnSave" value="Save" class="button" onclick="CheckCouponInfo();"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">//<![CDATA[
	var cal = Calendar.setup({
  	onSelect: function(cal) { cal.hide() }
  });
  cal.manageFields("imgFromDate", "txtFromDate", "%Y-%m-%d");
  cal.manageFields("imgToDate", "txtToDate", "%Y-%m-%d");
//]]></script>

<? End_Response(); ?>